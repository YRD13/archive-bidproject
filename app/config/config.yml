imports:
  - { resource: parameters.yml }
  - { resource: security.yml }

framework:
  translator:
    fallback: '%locale%'
  secret: '%secret%'
  router:
    resource: '%kernel.root_dir%/config/routing.yml'
    strict_requirements: ~
  form: ~
  csrf_protection: ~
  validation:
    enable_annotations: true
  templating:
    engines: ['twig']
  default_locale: '%locale%'
  trusted_proxies: ~
  session: ~
  fragments: ~
  http_method_override: true
  assets:
    version: '%deploy_timestamp%'

twig:
  debug: '%kernel.debug%'
  strict_variables: '%kernel.debug%'
  globals:
    deploy_timestamp: %deploy_timestamp%
    deploy_git: %deploy_git%
    oxioneo:
      google_map_script_uri: %google_map_script_uri%
    facebook_uri: %facebook_uri%
    twitter_uri: %twitter_uri%
    google_uri: %google_uri%
    google_map_key: %google_map_key%
    youtube_uri: %youtube_uri%
    youtube_player_uri: %youtube_player_uri%
    pinterest_uri: %pinterest_uri%
    table_class: 'table table-striped table-bordered table-hover table-condensed table-advance'
    email_contact: %email_contact%
    unity_money: %unity_money%
    unity_surface: %unity_surface%
    google:
      tagmanager: '%google-tagmanager%'
  form_themes:
    - 'EenovDefaultBundle:Form:fields.html.twig'

assetic:
  debug: '%kernel.debug%'
  use_controller: false
  bundles:
    - 'EenovAgencyBundle'
    - 'EenovAdminBundle'
    - 'EenovDefaultBundle'
    - 'EenovUserBundle'
  filters:
    cssrewrite: ~

doctrine:
  dbal:
    driver:   pdo_mysql
    server_version: 5.5
    host:     "%database_host%"
    port:     "%database_port%"
    dbname:   "%database_name%"
    user:     "%database_user%"
    password: "%database_password%"
    charset:  UTF8
    types:
      point:
        class: 'Eenov\DefaultBundle\ORM\PointType'
    mapping_types:
      point: point
  orm:
    auto_generate_proxy_classes: '%kernel.debug%'
    auto_mapping: true
    dql:
      datetime_functions:
        unix_timestamp: Eenov\DefaultBundle\ORM\UnixTimestamp

swiftmailer:
  transport: '%mailer_transport%'
  host: '%mailer_host%'
  username: '%mailer_user%'
  password: '%mailer_password%'
  spool:
    type: file
    path: '%kernel.root_dir%/spool/'

parameters:
  emails:
    contact:
      template: 'EenovDefaultBundle:Email:contact.html.twig'
      subject: 'Nouveau contact sur Oxioneo !'
    contact_agency:
      template: 'EenovDefaultBundle:Email:contactAgency.html.twig'
      subject: 'Nouveau contact agence sur Oxioneo !'
    contact_seller:
      template: 'EenovDefaultBundle:Email:contactSeller.html.twig'
      subject: 'Nouveau contact vendeur sur Oxioneo !'
    contact_buyer:
      template: 'EenovDefaultBundle:Email:contactBuyer.html.twig'
      subject: 'Nouveau contact acquéreur sur Oxioneo !'
    register:
      template: 'EenovDefaultBundle:Email:register.html.twig'
      subject: 'Bienvenue sur Oxioneo !'
    email_update:
      template: 'EenovDefaultBundle:Email:emailUpdate.html.twig'
      subject: 'Votre adresse email a changé sur Oxioneo !'
    email_validation:
      template: 'EenovDefaultBundle:Email:emailValidation.html.twig'
      subject: 'Vous devez valider votre adresse email sur Oxioneo !'
    email_validation_agency:
      template: 'EenovDefaultBundle:Email:emailValidationAgency.html.twig'
      subject: 'Bienvenue sur Oxioneo !'
    password_update:
      template: 'EenovDefaultBundle:Email:passwordUpdate.html.twig'
      subject: 'Votre mot de passe a changé sur Oxioneo !'
    password:
      template: 'EenovDefaultBundle:Email:password.html.twig'
      subject: 'Comment récupérer votre mot de passe sur Oxioneo ?'
    bid_contact:
      template: 'EenovDefaultBundle:Email:bidContact.html.twig'
      subject: 'Vous avez une demande de contact sur Oxioneo !'
    bid_contact_copy:
      template: 'EenovDefaultBundle:Email:bidContactCopy.html.twig'
      subject: '[CC] Vous avez une demande de contact sur Oxioneo !'
    mandate:
      template: 'EenovDefaultBundle:Email:mandate.html.twig'
      subject: 'Vous avez demandé à participer à une enchère sur Oxioneo !'
    auction_recall:
      template: 'EenovDefaultBundle:Email:auctionRecall.html.twig'
      subject: 'Rappel de votre inscription à l''enchère de {{ bid.title }} !'
    auction_end:
      template: 'EenovDefaultBundle:Email:auctionEnd.html.twig'
      subject: 'C''est bientôt la fin de l''enchère de {{ bid.title }} !'
    auction_ended:
      template: 'EenovDefaultBundle:Email:auctionEnded.html.twig'
      subject: 'L''enchère de {{ bid.title }} est terminée !'
    auction:
      template: 'EenovDefaultBundle:Email:auction.html.twig'
      subject: 'Nouvelle offre sur {{ bid.title }} !'
    access:
      template: 'EenovDefaultBundle:Email:access.html.twig'
      subject: 'Nouvelle demande d''accès à {{ bid.title }} !'
    access_validated:
      template: 'EenovDefaultBundle:Email:accessValidated.html.twig'
      subject: 'Votre accès à {{ bid.title }} a été validé !'
    access_invalidated:
      template: 'EenovDefaultBundle:Email:accessInvalidated.html.twig'
      subject: 'Votre accès à {{ bid.title }} a été refusé !'
    search:
      template: 'EenovDefaultBundle:Email:search.html.twig'
      subject: 'Votre recherche enregistré du jour !'
    slots:
      template: 'EenovDefaultBundle:Email:slots.html.twig'
      subject: 'Attention, vous avez des slots qui expirent dans 10 jours !'
    bid_admin_validate:
      template: 'EenovDefaultBundle:Email:bidAdminValidate.html.twig'
      subject: 'Une annonce attend votre validation sur Oxioneo !'
    bid_agency_validate:
      template: 'EenovDefaultBundle:Email:bidAgencyValidate.html.twig'
      subject: 'Une annonce attend votre validation sur Oxioneo !'
    bid_validated_to_agency:
      template: 'EenovDefaultBundle:Email:bidValidatedToAgency.html.twig'
      subject: 'Votre annonce a été validée sur Oxioneo !'
    bid_validated_to_seller:
      template: 'EenovDefaultBundle:Email:bidValidatedToSeller.html.twig'
      subject: 'Votre annonce a été validée sur Oxioneo !'
    user_created:
      template: 'EenovDefaultBundle:Email:userCreated.html.twig'
      subject: 'Vous avez demandé à participer à une enchère sur Oxioneo !'
      images:
        process: '%kernel.root_dir%/../src/Eenov/DefaultBundle/Resources/private/img/titre_process_acquereur.jpg'
        step1: '%kernel.root_dir%/../src/Eenov/DefaultBundle/Resources/private/img/etape_1.jpg'
        step2: '%kernel.root_dir%/../src/Eenov/DefaultBundle/Resources/private/img/etape_2.jpg'
        step3: '%kernel.root_dir%/../src/Eenov/DefaultBundle/Resources/private/img/etape_3.jpg'
        step4: '%kernel.root_dir%/../src/Eenov/DefaultBundle/Resources/private/img/etape_4.jpg'
        step5: '%kernel.root_dir%/../src/Eenov/DefaultBundle/Resources/private/img/etape_5.jpg'
        hr: '%kernel.root_dir%/../src/Eenov/DefaultBundle/Resources/private/img/hr.jpg'
    money_in_created:
      template: 'EenovDefaultBundle:Email:moneyInCreated.html.twig'
      subject: 'Création d''une transaction'
    money_in_updated:
      template: 'EenovDefaultBundle:Email:moneyInUpdated.html.twig'
      subject: 'Mise à jour d''une transaction'
    agency_created:
      template: 'EenovDefaultBundle:Email:agencyCreated.html.twig'
      subject: 'Création d''une agence'

eb_email:
  senders:
    -
      name: '%email_sender_name%'
      email: '%email_sender_email%'
  globals:
    p_style: 'margin: 10px 0; padding: 0; font-family: Arial, Sans-serif; font-size: 14px; color: #2d2628;'
    facebook_uri: '%facebook_uri%'
    google_uri: '%google_uri%'
    pinterest_uri: '%pinterest_uri%'
    twitter_uri: '%twitter_uri%'
  images:
    bonjour: '%kernel.root_dir%/../src/Eenov/DefaultBundle/Resources/private/img/bonjour.png'
    facebook: '%kernel.root_dir%/../src/Eenov/DefaultBundle/Resources/private/img/facebook.png'
    google: '%kernel.root_dir%/../src/Eenov/DefaultBundle/Resources/private/img/google.png'
    oxioneo: '%kernel.root_dir%/../src/Eenov/DefaultBundle/Resources/private/img/oxioneo.png'
    pinterest: '%kernel.root_dir%/../src/Eenov/DefaultBundle/Resources/private/img/pinterest.png'
    twitter: '%kernel.root_dir%/../src/Eenov/DefaultBundle/Resources/private/img/twitter.png'
  emails: '%emails%'

eb_translation:
  track_selected_links: 'active'

eb_doctrine:
  loggable:
    persisted: '%%entity%% créé avec succès !'
    updated: '%%entity%% modifié avec succès !'
    removed: '%%entity%% supprimé avec succès !'
  filesystem:
    depth: 6
    web_path: '%web_path%'
    secured_path: '%secured_path%'
  paginator:
    default_limit: 15

liip_imagine:
  resolvers:
    default:
      web_path:
        cache_prefix: 'images/cache'
  filter_sets:
    thumbnail_24_24:
      filters:
        upscale:
          min: [24, 24]
        thumbnail:
          size: [24, 24]
          allow_upscale: true
        strip: ~
    thumbnail_60_60:
      filters:
        upscale:
          min: [60, 60]
        thumbnail:
          size: [60, 60]
          allow_upscale: true
        strip: ~
    bid_default_view:
      filters:
        upscale:
          min: [300, 180]
        thumbnail:
          size: [300, 180]
          allow_upscale: true
        strip: ~
    bid_home:
      filters:
        upscale:
          min: [385, 220]
        thumbnail:
          size: [385, 220]
          allow_upscale: true
        strip: ~
    auction_agency_logo:
      filters:
        relative_resize:
          heighten: 51
        strip: ~
    auction_slide:
      filters:
        upscale:
          min: [1920, 1096]
        thumbnail:
          size: [1920, 1096]
          allow_upscale: true
        strip: ~
    agency_logo:
      filters:
        upscale:
          min: [370, 300]
        thumbnail:
          size: [370, 300]
          mode: inset
          allow_upscale: true
        strip: ~
    bid_agency_logo:
      filters:
        upscale:
          min: [160, 160]
        thumbnail:
          size: [160, 160]
          mode: inset
          allow_upscale: true
        strip: ~
    equipment:
      filters:
        upscale:
          min: [27, 24]
        thumbnail:
          size: [27, 24]
          allow_upscale: true
        strip: ~
    image_type:
      filters:
        upscale:
          min: [48, 48]
        thumbnail:
          size: [48, 48]
          allow_upscale: true
        strip: ~
    bid_preview:
      filters:
        upscale:
          min: [385, 220]
        thumbnail:
          size: [385, 220]
          allow_upscale: true
        strip: ~
    bid_slider:
      filters:
        upscale:
          min: [1170, 668]
        thumbnail:
          size: [1170, 668]
          allow_upscale: true
        strip: ~
    blog_index:
      filters:
        upscale:
          min: [500, 333]
        thumbnail:
          size: [500, 333]
          allow_upscale: true
        strip: ~
    blog_read:
      filters:
        relative_resize:
          widen: 500
        strip: ~

knp_snappy:
  pdf:
    enabled: true
    binary: '%kernel.root_dir%/../bin/wkhtmltopdf-amd64'
    options: []
  image:
    enabled: false

doctrine_migrations:
  dir_name: '%kernel.root_dir%/DoctrineMigrations'
  namespace: Application\Migrations
  table_name: migration_versions
  name: 'Oxioneo'

lemonway_sdk:
  directKitUrl: '%lemonway_direct_kit_url%'
  webKitUrl: '%lemonway_web_kit_url%'
  login: '%lemonway_login%'
  password: '%lemonway_password%'
  lang: '%lemonway_lang%'
  debug: '%lemonway_debug%'
