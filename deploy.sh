#!/bin/bash

set -e

if [ -z "$1" ]; then ENV="prod"; else ENV="$1"; fi

SCRIPT=`readlink -f "$0"`
SCRIPT_PATH=`dirname "${SCRIPT}"`
SCRIPT_CONSOLE="${SCRIPT_PATH}/app/console"
PHP=`which "php"`
GIT=`which "git"`

cat > "${SCRIPT_PATH}/env.php" <<EOL
<?php

define('ENV', '${ENV}');
define('DEBUG', 'dev' === '${ENV}');
define('DEPLOY_TIMESTAMP', `date +%s`);
define('DEPLOY_GIT', '`${GIT} rev-parse --verify HEAD`');

\$_SERVER['SYMFONY__DEPLOY_TIMESTAMP'] = DEPLOY_TIMESTAMP;
\$_SERVER['SYMFONY__DEPLOY_GIT'] = DEPLOY_GIT;

setlocale(LC_ALL, 'fr_FR.utf8');
mb_internal_encoding('utf-8');
date_default_timezone_set('Europe/Paris');
setlocale(LC_ALL, 'fr_FR.utf8', 'fr_FR');

EOL

echo "Reloading cache ..."
rm -rf "${SCRIPT_PATH}/app/cache"
rm -rf "${SCRIPT_PATH}/app/logs"

echo "Reloading database ..."
${PHP} "${SCRIPT_CONSOLE}" doctrine:migration:migrate -n -e "${ENV}"
${PHP} "${SCRIPT_CONSOLE}" doctrine:schema:validate -e "${ENV}"
${PHP} "${SCRIPT_CONSOLE}" doctrine:mapping:info -e "${ENV}"

echo "Reloading assets ..."
${PHP} "${SCRIPT_CONSOLE}" assets:install -e "${ENV}" --symlink
${PHP} "${SCRIPT_CONSOLE}" assetic:dump -e "${ENV}"

echo "Reloading cache ..."
${PHP} "${SCRIPT_CONSOLE}" cache:clear -e "${ENV}"
${PHP} "${SCRIPT_CONSOLE}" eb:doctrine:fix -e "${ENV}"
