#!/bin/bash

set -e

service apache2 stop
service cron stop

su www-data <<'EOF'

set -e

DATE=`date +"%Y%d%m%H%M%S"`

SOURCE_DIRECTORY=`pwd`
BACKUP_DIRECTORY="/var/www/Backups/${DATE}"
COMPOSER="/var/www/composer.phar"

if [ ! -f "${SOURCE_DIRECTORY}/web/app.php" ]; then
    echo "app.php not found !"
    exit 404
fi

mkdir -p "${BACKUP_DIRECTORY}"

${COMPOSER} self-update

rm -rf "${SOURCE_DIRECTORY}/app/cache"
cp -r "${SOURCE_DIRECTORY}" "${BACKUP_DIRECTORY}"
rm -rf "${BACKUP_DIRECTORY}/app/logs"
rm -rf "${BACKUP_DIRECTORY}/app/spool"
rm -rf "${BACKUP_DIRECTORY}/vendor"
rm -rf "${BACKUP_DIRECTORY}/web/images"
rm -rf "${BACKUP_DIRECTORY}/web/bundles"
zip -r "${BACKUP_DIRECTORY}.zip" "${BACKUP_DIRECTORY}"
rm -rf "${BACKUP_DIRECTORY}"

php "${SOURCE_DIRECTORY}/app/console" "eb:doctrine:database-backup" "${BACKUP_DIRECTORY}.sql" -e prod

cd "${SOURCE_DIRECTORY}"
git checkout env.php
git pull origin master

${COMPOSER} install

./deploy.sh

EOF

service apache2 start
service cron start
