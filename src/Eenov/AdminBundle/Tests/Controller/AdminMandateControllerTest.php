<?php

namespace Eenov\AdminBundle\Tests\Controller;

use Eenov\AdminBundle\Test\WebTestCase;
use Eenov\DefaultBundle\Entity\Mandate;

/**
 * Class AdminMandateControllerTest
 *
*
 */
class AdminMandateControllerTest extends WebTestCase
{
    /**
     * @var Mandate
     */
    private $mandate;

    public function testCreateAction()
    {
        $this->client->request('GET', '/administration/annonces/' . $this->mandate->getBid()->getId() . '/mandats/creer');
        $this->assertResponse($this->client);

        $form = $this->client->getCrawler()->filterXPath('//*[@id="create"]')->form();
        $this->client->submit($form, [
            'eenov_admin_mandate[buyer]' => 1,
            'eenov_admin_mandate[visited]' => '01/01/2015',
            'eenov_admin_mandate[file]' => $this->createFile('blog.jpg'),
        ]);
        $this->assertRedirect($this->client);
        $this->client->followRedirect();
        $this->assertResponse($this->client);
    }

    public function testDeleteAction()
    {
        $this->client->request('GET', '/administration/annonces/' . $this->mandate->getBid()->getId() . '/mandats/' . $this->mandate->getId() . '/supprimer');
        $this->assertResponse($this->client);

        $form = $this->client->getCrawler()->filterXPath('//*[@id="delete"]')->form();
        $this->client->submit($form);
        $this->assertRedirect($this->client);
        $this->client->followRedirect();
        $this->assertResponse($this->client);
    }

    public function testFileAction()
    {
        $this->client->request('GET', '/administration/annonces/' . $this->mandate->getBid()->getId() . '/mandats/' . $this->mandate->getId() . '/mandat.' . $this->mandate->getExtension());
        $this->assertResponse($this->client);
    }

    public function testIndexAction()
    {
        $this->client->request('GET', '/administration/annonces/' . $this->mandate->getBid()->getId() . '/mandats');
        $this->assertResponse($this->client);
    }

    public function testIndexPaginationAction()
    {
        $this->client->request('GET', '/administration/annonces/' . $this->mandate->getBid()->getId() . '/mandats/1');
        $this->assertResponse($this->client);
    }

    public function testUpdateAction()
    {
        $this->client->request('GET', '/administration/annonces/' . $this->mandate->getBid()->getId() . '/mandats/' . $this->mandate->getId() . '/modifier');
        $this->assertResponse($this->client);

        $form = $this->client->getCrawler()->filterXPath('//*[@id="update"]')->form();
        $this->client->submit($form, [
            'eenov_admin_mandate[buyer]' => 1,
            'eenov_admin_mandate[visited]' => '01/01/2015',
            'eenov_admin_mandate[file]' => $this->createFile('blog.jpg'),
        ]);
        $this->assertRedirect($this->client);
        $this->client->followRedirect();
        $this->assertResponse($this->client);
    }

    protected function setUp()
    {
        parent::setUp();
        $this->mandate = $this->client->getContainer()->get('doctrine.orm.default_entity_manager')->getRepository(Mandate::class)->find(1);
    }
}
