<?php

namespace Eenov\AdminBundle\Tests\Controller;

use Eenov\AdminBundle\Test\WebTestCase;
use Eenov\DefaultBundle\Entity\Bid;
use Eenov\DefaultBundle\Entity\Image;

/**
 * Class AdminImageControllerTest
 *
*
 */
class AdminImageControllerTest extends WebTestCase
{
    /**
     * @var Image
     */
    private $image;

    /**
     * @var Bid
     */
    private $bid;

    protected function setUp()
    {
        parent::setUp();
        $bid = $this->client->getContainer()->get('doctrine.orm.default_entity_manager')->getRepository(Bid::class)->find(1);
        $this->bid = $bid;
        $this->image = $this->bid->getDraft()->getImages()->first();
    }

    public function testIndexAction()
    {
        $this->client->request('GET', '/administration/annonces/' . $this->bid->getId() . '/images');
        $this->assertResponse($this->client);
    }

    public function testDeleteAction()
    {
        $this->client->request('GET', '/administration/annonces/' . $this->bid->getId() . '/images/' . $this->image->getId() . '/modifier-previsualisation');
        $this->assertRedirect($this->client);
        $this->client->followRedirect();
        $this->assertResponse($this->client);
    }

    public function testDefaultAction()
    {
        $this->client->request('GET', '/administration/annonces/' . $this->bid->getId() . '/images/' . $this->image->getId() . '/supprimer');
        $this->assertRedirect($this->client);
        $this->client->followRedirect();
        $this->assertResponse($this->client);
    }

    public function testUploadAction()
    {
        $this->client->request('POST', '/administration/annonces/' . $this->bid->getId() . '/images/televerser', [
            'type' => 1,
        ], [
            'f1' => $this->createFile('blog.jpg'),
            'f2' => $this->createFile('blog.jpg'),
        ]);
        $this->assertResponse($this->client, true, 200, 'application/json');
    }
}
