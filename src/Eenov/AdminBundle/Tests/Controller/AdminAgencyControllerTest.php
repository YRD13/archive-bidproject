<?php

namespace Eenov\AdminBundle\Tests\Controller;

use Eenov\AdminBundle\Test\WebTestCase;
use Eenov\DefaultBundle\Entity\Agency;
use Eenov\DefaultBundle\Entity\User;

/**
 * Class AdminAgencyControllerTest
 *
*
 */
class AdminAgencyControllerTest extends WebTestCase
{
    /**
     * @var User
     */
    private $commercial;

    /**
     * @var User
     */
    private $owner;

    protected function setUp()
    {
        parent::setUp();
        $commercials = $this->client->getContainer()->get('doctrine.orm.default_entity_manager')->getRepository(User::class)->findBy(['role' => User::ROLE_COMMERCIAL]);
        $this->commercial = array_shift($commercials);
        $owners = $this->client->getContainer()->get('doctrine.orm.default_entity_manager')->getRepository(User::class)->findBy(['role' => User::ROLE_AGENCY_OWNER]);
        $this->owner = array_shift($owners);
    }

    public function testIndexAction()
    {
        $this->client->request('GET', '/administration/agences');
        $this->assertResponse($this->client);
    }

    public function testIndexPaginationAction()
    {
        $this->client->request('GET', '/administration/agences/1');
        $this->assertResponse($this->client);
    }

    public function testCreateAction()
    {
        $this->client->request('GET', '/administration/agences/creer');
        $this->assertResponse($this->client);

        $form = $this->client->getCrawler()->filterXPath('//*[@id="create"]')->form();
        $this->client->submit($form, [
            'eenov_admin_agency[mother]' => null,
            'eenov_admin_agency[name]' => 'Lorem ipsum',
            'eenov_admin_agency[phone]' => '0632277478',
            'eenov_admin_agency[email]' => 'john@doe.com',
            'eenov_admin_agency[address]' => '48, fud fgdgd',
            'eenov_admin_agency[city]' => 'Bordeaux',
            'eenov_admin_agency[zip]' => 33000,
            'eenov_admin_agency[country]' => 'France',
            'eenov_admin_agency[file]' => $this->createFile('blog.jpg'),
            'eenov_admin_agency[commercial]' => $this->commercial->getId(),
            'eenov_admin_agency[owner]' => $this->owner->getId(),
            'eenov_admin_agency[type]' => Agency::TYPE_EURL,
            'eenov_admin_agency[siret]' => null,
            'eenov_admin_agency[rcs]' => null,
            'eenov_admin_agency[cardNumber]' => null,
            'eenov_admin_agency[website]' => null,
        ]);
        $this->assertRedirect($this->client);
        $this->client->followRedirect();
        $this->assertResponse($this->client);
    }

    public function testUpdateAction()
    {
        $this->client->request('GET', '/administration/agences/1/modifier');
        $this->assertResponse($this->client);

        $form = $this->client->getCrawler()->filterXPath('//*[@id="update"]')->form();
        $this->client->submit($form, [
            'eenov_admin_agency[mother]' => null,
            'eenov_admin_agency[name]' => 'Lorem ipsum',
            'eenov_admin_agency[phone]' => '0632277478',
            'eenov_admin_agency[email]' => 'john@doe.com',
            'eenov_admin_agency[address]' => '48, fud fgdgd',
            'eenov_admin_agency[city]' => 'Bordeaux',
            'eenov_admin_agency[zip]' => 33000,
            'eenov_admin_agency[country]' => 'France',
            'eenov_admin_agency[file]' => $this->createFile('blog.jpg'),
            'eenov_admin_agency[commercial]' => $this->commercial->getId(),
            'eenov_admin_agency[owner]' => $this->owner->getId(),
            'eenov_admin_agency[type]' => Agency::TYPE_EURL,
            'eenov_admin_agency[siret]' => null,
            'eenov_admin_agency[rcs]' => null,
            'eenov_admin_agency[cardNumber]' => null,
            'eenov_admin_agency[website]' => null,
        ]);
        $this->assertRedirect($this->client);
        $this->client->followRedirect();
        $this->assertResponse($this->client);
    }

    public function testDeleteAction()
    {
        $this->client->request('GET', '/administration/agences/1/supprimer');
        $this->assertResponse($this->client);

        $form = $this->client->getCrawler()->filterXPath('//*[@id="delete"]')->form();
        $this->client->submit($form);
        $this->assertRedirect($this->client);
        $this->client->followRedirect();
        $this->assertResponse($this->client);
    }
}
