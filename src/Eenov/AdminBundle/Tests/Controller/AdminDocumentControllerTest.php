<?php

namespace Eenov\AdminBundle\Tests\Controller;

use Eenov\AdminBundle\Test\WebTestCase;
use Eenov\DefaultBundle\Entity\Bid;
use Eenov\DefaultBundle\Entity\Document;

/**
 * Class AdminDocumentControllerTest
 *
*
 */
class AdminDocumentControllerTest extends WebTestCase
{
    /**
     * @var Document
     */
    private $document;

    /**
     * @var Bid
     */
    private $bid;

    protected function setUp()
    {
        parent::setUp();
        $bid = $this->client->getContainer()->get('doctrine.orm.default_entity_manager')->getRepository(Bid::class)->find(1);
        $this->bid = $bid;
        $this->document = $this->bid->getDraft()->getDocuments()->first();
    }

    public function testIndexAction()
    {
        $this->client->request('GET', '/administration/annonces/' . $this->bid->getId() . '/documents');
        $this->assertResponse($this->client);
    }

    public function testDeleteAction()
    {
        $this->client->request('GET', '/administration/annonces/' . $this->bid->getId() . '/documents/' . $this->document->getId() . '/supprimer');
        $this->assertRedirect($this->client);
        $this->client->followRedirect();
        $this->assertResponse($this->client);
    }

    public function testUploadAction()
    {
        $this->client->request('POST', '/administration/annonces/' . $this->bid->getId() . '/documents/televerser', [
            'type' => 1,
        ], [
            'f1' => $this->createFile('blog.jpg'),
            'f2' => $this->createFile('blog.jpg'),
        ]);
        $this->assertResponse($this->client, true, 200, 'application/json');
    }
}
