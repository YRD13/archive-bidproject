<?php

namespace Eenov\AdminBundle\Tests\Controller;

use Eenov\AdminBundle\Test\WebTestCase;

/**
 * Class AdminAuctionControllerTest
 *
*
 */
class AdminAuctionControllerTest extends WebTestCase
{
    public function testIndexAction()
    {
        $this->client->request('GET', '/administration/annonces/1/encheres');
        $this->assertResponse($this->client);
    }
}
