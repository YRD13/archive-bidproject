<?php

namespace Eenov\AdminBundle\Tests\Controller;

use Eenov\AdminBundle\Test\WebTestCase;

/**
 * Class AdminAdvertTypeControllerTest
 *
*
 */
class AdminAdvertTypeControllerTest extends WebTestCase
{
    public function testCreateAction()
    {
        $this->client->request('GET', '/administration/annonces-types/creer');
        $this->assertResponse($this->client);

        $form = $this->client->getCrawler()->filterXPath('//*[@id="create"]')->form();
        $this->client->submit($form, [
            'eenov_admin_advert_type[name]' => 'Lorem ispum dolor sit amet',
            'eenov_admin_advert_type[isGround]' => false,
            'eenov_admin_advert_type[isBuilding]' => false,
        ]);
        $this->assertRedirect($this->client);
        $this->client->followRedirect();
        $this->assertResponse($this->client);
    }

    public function testDeleteAction()
    {
        $this->client->request('GET', '/administration/annonces-types/1/supprimer');
        $this->assertResponse($this->client);

        $form = $this->client->getCrawler()->filterXPath('//*[@id="delete"]')->form();
        $this->client->submit($form);
        $this->assertRedirect($this->client);
        $this->client->followRedirect();
        $this->assertResponse($this->client);
    }

    public function testIndexAction()
    {
        $this->client->request('GET', '/administration/annonces-types');
        $this->assertResponse($this->client);
    }

    public function testIndexPaginationAction()
    {
        $this->client->request('GET', '/administration/annonces-types/1');
        $this->assertResponse($this->client);
    }

    public function testUpdateAction()
    {
        $this->client->request('GET', '/administration/annonces-types/1/modifier');
        $this->assertResponse($this->client);

        $form = $this->client->getCrawler()->filterXPath('//*[@id="update"]')->form();
        $this->client->submit($form, [
            'eenov_admin_advert_type[name]' => 'Lorem ispum dolor sit amet',
            'eenov_admin_advert_type[isGround]' => false,
            'eenov_admin_advert_type[isBuilding]' => false,
        ]);
        $this->assertRedirect($this->client);
        $this->client->followRedirect();
        $this->assertResponse($this->client);
    }
}
