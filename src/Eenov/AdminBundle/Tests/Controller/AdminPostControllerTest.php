<?php

namespace Eenov\AdminBundle\Tests\Controller;

use Eenov\AdminBundle\Test\WebTestCase;

/**
 * Class AdminPostControllerTest
 *
*
 */
class AdminPostControllerTest extends WebTestCase
{
    public function testCreateAction()
    {
        $this->client->request('GET', '/administration/articles/creer');
        $this->assertResponse($this->client);

        $form = $this->client->getCrawler()->filterXPath('//*[@id="create"]')->form();
        $this->client->submit($form, [
            'eenov_admin_post[title]' => 'Lorem ispum dolor sit amet',
            'eenov_admin_post[text]' => 'Lorem ispum dolor sit amet. Lorem ispum dolor sit amet. Lorem ispum dolor sit amet. Lorem ispum dolor sit amet. Lorem ispum dolor sit amet. Lorem ispum dolor sit amet. Lorem ispum dolor sit amet. Lorem ispum dolor sit amet. Lorem ispum dolor sit amet. Lorem ispum dolor sit amet. Lorem ispum dolor sit amet. ',
            'eenov_admin_post[isPublished]' => true,
            'eenov_admin_post[file]' => $this->createFile('blog.jpg'),
        ]);
        $this->assertRedirect($this->client);
        $this->client->followRedirect();
        $this->assertResponse($this->client);
    }

    public function testDeleteAction()
    {
        $this->client->request('GET', '/administration/articles/1/supprimer');
        $this->assertResponse($this->client);

        $form = $this->client->getCrawler()->filterXPath('//*[@id="delete"]')->form();
        $this->client->submit($form);
        $this->assertRedirect($this->client);
        $this->client->followRedirect();
        $this->assertResponse($this->client);
    }

    public function testIndexAction()
    {
        $this->client->request('GET', '/administration/articles');
        $this->assertResponse($this->client);
    }

    public function testIndexPaginationAction()
    {
        $this->client->request('GET', '/administration/articles/1');
        $this->assertResponse($this->client);
    }

    public function testUpdateAction()
    {
        $this->client->request('GET', '/administration/articles/1/modifier');
        $this->assertResponse($this->client);

        $form = $this->client->getCrawler()->filterXPath('//*[@id="update"]')->form();
        $this->client->submit($form, [
            'eenov_admin_post[title]' => 'Lorem ispum dolor sit amet',
            'eenov_admin_post[text]' => 'Lorem ispum dolor sit amet. Lorem ispum dolor sit amet. Lorem ispum dolor sit amet. Lorem ispum dolor sit amet. Lorem ispum dolor sit amet. Lorem ispum dolor sit amet. Lorem ispum dolor sit amet. Lorem ispum dolor sit amet. Lorem ispum dolor sit amet. Lorem ispum dolor sit amet. Lorem ispum dolor sit amet. ',
            'eenov_admin_post[isPublished]' => true,
            'eenov_admin_post[file]' => $this->createFile('blog.jpg'),
        ]);
        $this->assertRedirect($this->client);
        $this->client->followRedirect();
        $this->assertResponse($this->client);
    }
}
