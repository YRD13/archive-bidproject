<?php

namespace Eenov\AdminBundle\Tests\Controller;

use Eenov\AdminBundle\Test\WebTestCase;

/**
 * Class AdminControllerTest
 *
*
 */
class AdminControllerTest extends WebTestCase
{
    public function testIndexAction()
    {
        $this->client->request('GET', '/administration');
        $this->assertRedirect($this->client);
    }
}
