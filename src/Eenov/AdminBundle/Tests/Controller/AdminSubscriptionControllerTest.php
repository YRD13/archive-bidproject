<?php

namespace Eenov\AdminBundle\Tests\Controller;

use Eenov\AdminBundle\Test\WebTestCase;
use Eenov\DefaultBundle\Entity\Subscription;

/**
 * Class AdminSubscriptionControllerTest
 *
*
 */
class AdminSubscriptionControllerTest extends WebTestCase
{
    /**
     * @var Subscription
     */
    private $subscription;

    public function testCreateAction()
    {
        $this->client->request('GET', '/administration/agences/' . $this->subscription->getAgency()->getId() . '/abonnements/creer');
        $this->assertResponse($this->client);

        $form = $this->client->getCrawler()->filterXPath('//*[@id="create"]')->form();
        $this->client->submit($form, [
            'eenov_admin_subscription[price]' => 1,
            'eenov_admin_subscription[started]' => '01/01/2020',
        ]);
        $this->assertRedirect($this->client);
        $this->client->followRedirect();
        $this->assertResponse($this->client);
    }

    public function testCreateEntireAction()
    {
        $this->client->request('GET', '/administration/agences/' . $this->subscription->getAgency()->getId() . '/abonnements/creer-sur-mesure');
        $this->assertResponse($this->client);

        $form = $this->client->getCrawler()->filterXPath('//*[@id="create"]')->form();
        $this->client->submit($form, [
            'eenov_admin_subscription_entire[name]' => 'Lorem ispum',
            'eenov_admin_subscription_entire[duration]' => 12,
            'eenov_admin_subscription_entire[amount]' => 500,
            'eenov_admin_subscription_entire[perSlot]' => false,
            'eenov_admin_subscription_entire[count]' => 1,
            'eenov_admin_subscription_entire[started]' => '01/01/2020',
        ]);
        $this->assertRedirect($this->client);
        $this->client->followRedirect();
        $this->assertResponse($this->client);
    }

    public function testDeleteAction()
    {
        $this->client->request('GET', '/administration/agences/' . $this->subscription->getAgency()->getId() . '/abonnements/' . $this->subscription->getId() . '/supprimer');
        $this->assertResponse($this->client);

        $form = $this->client->getCrawler()->filterXPath('//*[@id="delete"]')->form();
        $this->client->submit($form);
        $this->assertRedirect($this->client);
        $this->client->followRedirect();
        $this->assertResponse($this->client);
    }

    public function testIndexAction()
    {
        $this->client->request('GET', '/administration/agences/' . $this->subscription->getAgency()->getId() . '/abonnements');
        $this->assertResponse($this->client);
    }

    public function testIndexPaginationAction()
    {
        $this->client->request('GET', '/administration/agences/' . $this->subscription->getAgency()->getId() . '/abonnements/1');
        $this->assertResponse($this->client);
    }

    protected function setUp()
    {
        parent::setUp();
        $this->subscription = $this->client->getContainer()->get('doctrine.orm.default_entity_manager')->getRepository(Subscription::class)->find(1);
    }
}
