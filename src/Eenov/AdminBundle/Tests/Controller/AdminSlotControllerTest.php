<?php

namespace Eenov\AdminBundle\Tests\Controller;

use Eenov\AdminBundle\Test\WebTestCase;
use Eenov\DefaultBundle\Entity\Slot;

/**
 * Class AdminSlotControllerTest
 *
*
 */
class AdminSlotControllerTest extends WebTestCase
{
    /**
     * @var Slot
     */
    private $slot;

    public function testCreateAction()
    {
        $this->client->request('GET', '/administration/agences/' . $this->slot->getAgency()->getId() . '/slots/creer');
        $this->assertResponse($this->client);

        $form = $this->client->getCrawler()->filterXPath('//*[@id="create"]')->form();
        $this->client->submit($form, [
            'eenov_admin_slot[started]' => '01/01/2015',
            'eenov_admin_slot[ended]' => '01/02/2015',
        ]);
        $this->assertRedirect($this->client);
        $this->client->followRedirect();
        $this->assertResponse($this->client);
    }

    public function testDeleteAction()
    {
        $this->client->request('GET', '/administration/agences/' . $this->slot->getAgency()->getId() . '/slots/' . $this->slot->getId() . '/supprimer');
        $this->assertResponse($this->client);

        $form = $this->client->getCrawler()->filterXPath('//*[@id="delete"]')->form();
        $this->client->submit($form);
        $this->assertRedirect($this->client);
        $this->client->followRedirect();
        $this->assertResponse($this->client);
    }

    public function testIndexAction()
    {
        $this->client->request('GET', '/administration/agences/' . $this->slot->getAgency()->getId() . '/slots');
        $this->assertResponse($this->client);
    }

    public function testIndexPaginationAction()
    {
        $this->client->request('GET', '/administration/agences/' . $this->slot->getAgency()->getId() . '/slots/1');
        $this->assertResponse($this->client);
    }

    public function testUpdateAction()
    {
        $this->client->request('GET', '/administration/agences/' . $this->slot->getAgency()->getId() . '/slots/' . $this->slot->getId() . '/modifier');
        $this->assertResponse($this->client);

        $form = $this->client->getCrawler()->filterXPath('//*[@id="update"]')->form();
        $this->client->submit($form, [
            'eenov_admin_slot[started]' => '01/01/2015',
            'eenov_admin_slot[ended]' => '01/02/2015',
        ]);
        $this->assertRedirect($this->client);
        $this->client->followRedirect();
        $this->assertResponse($this->client);
    }

    protected function setUp()
    {
        parent::setUp();
        $this->slot = $this->client->getContainer()->get('doctrine.orm.default_entity_manager')->getRepository(Slot::class)->find(1);
    }
}
