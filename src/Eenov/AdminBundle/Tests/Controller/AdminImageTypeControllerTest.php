<?php

namespace Eenov\AdminBundle\Tests\Controller;

use Eenov\AdminBundle\Test\WebTestCase;

/**
 * Class AdminImageTypeControllerTest
 *
*
 */
class AdminImageTypeControllerTest extends WebTestCase
{
    public function testCreateAction()
    {
        $this->client->request('GET', '/administration/types-images/creer');
        $this->assertResponse($this->client);

        $form = $this->client->getCrawler()->filterXPath('//*[@id="create"]')->form();
        $this->client->submit($form, [
            'eenov_admin_image_type[name]' => 'Lorem ispum dolor sit amet',
            'eenov_admin_image_type[file]' => $this->createFile('equipment.png'),
        ]);
        $this->assertRedirect($this->client);
        $this->client->followRedirect();
        $this->assertResponse($this->client);
    }

    public function testDeleteAction()
    {
        $this->client->request('GET', '/administration/types-images/1/supprimer');
        $this->assertResponse($this->client);

        $form = $this->client->getCrawler()->filterXPath('//*[@id="delete"]')->form();
        $this->client->submit($form);
        $this->assertRedirect($this->client);
        $this->client->followRedirect();
        $this->assertResponse($this->client);
    }

    public function testIndexAction()
    {
        $this->client->request('GET', '/administration/types-images');
        $this->assertResponse($this->client);
    }

    public function testIndexPaginationAction()
    {
        $this->client->request('GET', '/administration/types-images/1');
        $this->assertResponse($this->client);
    }

    public function testUpdateAction()
    {
        $this->client->request('GET', '/administration/types-images/1/modifier');
        $this->assertResponse($this->client);

        $form = $this->client->getCrawler()->filterXPath('//*[@id="update"]')->form();
        $this->client->submit($form, [
            'eenov_admin_image_type[name]' => 'Lorem ispum dolor sit amet',
            'eenov_admin_image_type[file]' => $this->createFile('equipment.png'),
        ]);
        $this->assertRedirect($this->client);
        $this->client->followRedirect();
        $this->assertResponse($this->client);
    }
}
