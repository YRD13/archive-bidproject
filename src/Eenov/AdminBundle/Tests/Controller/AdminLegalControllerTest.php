<?php

namespace Eenov\AdminBundle\Tests\Controller;

use Eenov\AdminBundle\Test\WebTestCase;
use Eenov\DefaultBundle\Entity\Legal;

/**
 * Class AdminLegalControllerTest
 *
*
 */
class AdminLegalControllerTest extends WebTestCase
{
    /**
     * @var Legal
     */
    private $legal;

    public function testCreateAction()
    {
        $this->client->request('GET', '/administration/agences/' . $this->legal->getAgency()->getId() . '/documents/creer');
        $this->assertResponse($this->client);

        $form = $this->client->getCrawler()->filterXPath('//*[@id="create"]')->form();
        $this->client->submit($form, [
            'eenov_admin_legal[type]' => Legal::TYPE_CONTRACT,
            'eenov_admin_legal[file]' => $this->createFile('blog.jpg'),
        ]);
        $this->assertRedirect($this->client);
        $this->client->followRedirect();
        $this->assertResponse($this->client);
    }

    public function testDeleteAction()
    {
        $this->client->request('GET', '/administration/agences/' . $this->legal->getAgency()->getId() . '/documents/' . $this->legal->getId() . '/supprimer');
        $this->assertResponse($this->client);

        $form = $this->client->getCrawler()->filterXPath('//*[@id="delete"]')->form();
        $this->client->submit($form);
        $this->assertRedirect($this->client);
        $this->client->followRedirect();
        $this->assertResponse($this->client);
    }

    public function testFileAction()
    {
        $this->client->request('GET', '/administration/agences/' . $this->legal->getAgency()->getId() . '/documents/consulter/' . $this->legal->getId() . '.' . $this->legal->getExtension());
        $this->assertResponse($this->client);
    }

    public function testIndexAction()
    {
        $this->client->request('GET', '/administration/agences/' . $this->legal->getAgency()->getId() . '/documents');
        $this->assertResponse($this->client);
    }

    public function testIndexPaginationAction()
    {
        $this->client->request('GET', '/administration/agences/' . $this->legal->getAgency()->getId() . '/documents/1');
        $this->assertResponse($this->client);
    }

    protected function setUp()
    {
        parent::setUp();
        $this->legal = $this->client->getContainer()->get('doctrine.orm.default_entity_manager')->getRepository(Legal::class)->find(1);
    }
}
