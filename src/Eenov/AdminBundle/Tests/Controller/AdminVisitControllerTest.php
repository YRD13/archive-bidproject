<?php

namespace Eenov\AdminBundle\Tests\Controller;

use Eenov\AdminBundle\Test\WebTestCase;
use Eenov\DefaultBundle\Entity\Visit;

/**
 * Class AdminVisitControllerTest
 *
*
 */
class AdminVisitControllerTest extends WebTestCase
{
    /**
     * @var Visit
     */
    private $visit;

    public function testCreateAction()
    {
        $this->client->request('GET', '/administration/annonces/' . $this->visit->getBid()->getId() . '/visites/creer');
        $this->assertResponse($this->client);

        $form = $this->client->getCrawler()->filterXPath('//*[@id="create"]')->form();
        $this->client->submit($form, [
            'eenov_admin_visit[date]' => '01/01/2015',
            'eenov_admin_visit[started]' => '08:00',
            'eenov_admin_visit[ended]' => '18:00',
        ]);
        $this->assertRedirect($this->client);
        $this->client->followRedirect();
        $this->assertResponse($this->client);
    }

    public function testDeleteAction()
    {
        $this->client->request('GET', '/administration/annonces/' . $this->visit->getBid()->getId() . '/visites/' . $this->visit->getId() . '/supprimer');
        $this->assertResponse($this->client);

        $form = $this->client->getCrawler()->filterXPath('//*[@id="delete"]')->form();
        $this->client->submit($form);
        $this->assertRedirect($this->client);
        $this->client->followRedirect();
        $this->assertResponse($this->client);
    }

    public function testIndexAction()
    {
        $this->client->request('GET', '/administration/annonces/' . $this->visit->getBid()->getId() . '/visites');
        $this->assertResponse($this->client);
    }

    public function testIndexPaginationAction()
    {
        $this->client->request('GET', '/administration/annonces/' . $this->visit->getBid()->getId() . '/visites/1');
        $this->assertResponse($this->client);
    }

    public function testUpdateAction()
    {
        $this->client->request('GET', '/administration/annonces/' . $this->visit->getBid()->getId() . '/visites/' . $this->visit->getId() . '/modifier');
        $this->assertResponse($this->client);

        $form = $this->client->getCrawler()->filterXPath('//*[@id="update"]')->form();
        $this->client->submit($form, [
            'eenov_admin_visit[date]' => '01/01/2015',
            'eenov_admin_visit[started]' => '08:00',
            'eenov_admin_visit[ended]' => '18:00',
        ]);
        $this->assertRedirect($this->client);
        $this->client->followRedirect();
        $this->assertResponse($this->client);
    }

    protected function setUp()
    {
        parent::setUp();
        $this->visit = $this->client->getContainer()->get('doctrine.orm.default_entity_manager')->getRepository(Visit::class)->find(1);
    }
}
