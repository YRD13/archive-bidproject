<?php

namespace Eenov\AdminBundle\Tests\Controller;

use Eenov\AdminBundle\Test\WebTestCase;

/**
 * Class AdminDocumentTypeControllerTest
 *
*
 */
class AdminDocumentTypeControllerTest extends WebTestCase
{
    public function testCreateAction()
    {
        $this->client->request('GET', '/administration/types-documents/creer');
        $this->assertResponse($this->client);

        $form = $this->client->getCrawler()->filterXPath('//*[@id="create"]')->form();
        $this->client->submit($form, [
            'eenov_admin_document_type[order]' => 1,
            'eenov_admin_document_type[title]' => 'Lorem ispum dolor sit amet',
            'eenov_admin_document_type[subtitle]' => 'Lorem ispum dolor sit amet',
            'eenov_admin_document_type[name]' => 'Lorem ispum dolor sit amet',
            'eenov_admin_document_type[isPublic]' => true,
        ]);
        $this->assertRedirect($this->client);
        $this->client->followRedirect();
        $this->assertResponse($this->client);
    }

    public function testDeleteAction()
    {
        $this->client->request('GET', '/administration/types-documents/1/supprimer');
        $this->assertResponse($this->client);

        $form = $this->client->getCrawler()->filterXPath('//*[@id="delete"]')->form();
        $this->client->submit($form);
        $this->assertRedirect($this->client);
        $this->client->followRedirect();
        $this->assertResponse($this->client);
    }

    public function testIndexAction()
    {
        $this->client->request('GET', '/administration/types-documents');
        $this->assertResponse($this->client);
    }

    public function testIndexPaginationAction()
    {
        $this->client->request('GET', '/administration/types-documents/1');
        $this->assertResponse($this->client);
    }

    public function testUpdateAction()
    {
        $this->client->request('GET', '/administration/types-documents/1/modifier');
        $this->assertResponse($this->client);

        $form = $this->client->getCrawler()->filterXPath('//*[@id="update"]')->form();
        $this->client->submit($form, [
            'eenov_admin_document_type[order]' => 1,
            'eenov_admin_document_type[title]' => 'Lorem ispum dolor sit amet',
            'eenov_admin_document_type[subtitle]' => 'Lorem ispum dolor sit amet',
            'eenov_admin_document_type[name]' => 'Lorem ispum dolor sit amet',
            'eenov_admin_document_type[isPublic]' => true,
        ]);
        $this->assertRedirect($this->client);
        $this->client->followRedirect();
        $this->assertResponse($this->client);
    }
}
