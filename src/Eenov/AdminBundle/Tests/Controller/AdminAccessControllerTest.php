<?php

namespace Eenov\AdminBundle\Tests\Controller;

use Eenov\AdminBundle\Test\WebTestCase;
use Eenov\DefaultBundle\Entity\Access;
use Eenov\DefaultBundle\Entity\User;

/**
 * Class AdminAccessControllerTest
 *
*
 */
class AdminAccessControllerTest extends WebTestCase
{
    /**
     * @var Access
     */
    private $access;

    /**
     * @var User
     */
    private $user;

    protected function setUp()
    {
        parent::setUp();
        $this->access = $this->client->getContainer()->get('doctrine.orm.default_entity_manager')->getRepository(Access::class)->find(1);
        $this->user = $this->client->getContainer()->get('doctrine.orm.default_entity_manager')->getRepository(User::class)->findOneBy(['role' => User::ROLE_USER]);
    }

    public function testIndexAction()
    {
        $this->client->request('GET', '/administration/annonces/' . $this->access->getBid()->getId() . '/acces');
        $this->assertResponse($this->client);
    }

    public function testIndexPaginationAction()
    {
        $this->client->request('GET', '/administration/annonces/' . $this->access->getBid()->getId() . '/acces/1');
        $this->assertResponse($this->client);
    }

    public function testCreateAction()
    {
        $this->client->request('GET', '/administration/annonces/' . $this->access->getBid()->getId() . '/acces/creer');
        $this->assertResponse($this->client);

        $form = $this->client->getCrawler()->filterXPath('//*[@id="create"]')->form();
        $this->client->submit($form, [
            'eenov_admin_access[user]' => $this->user->getId(),
            'eenov_admin_access[readonly]' => true,
            'eenov_admin_access[userComment]' => null,
            'eenov_admin_access[agencyComment]' => null,
            'eenov_admin_access[mandate]' => null,
            'eenov_admin_access[validate]' => 0,
        ]);
        $this->assertRedirect($this->client);
        $this->client->followRedirect();
        $this->assertResponse($this->client);
    }

    public function testUpdateAction()
    {
        $this->client->request('GET', '/administration/annonces/' . $this->access->getBid()->getId() . '/acces/' . $this->access->getId() . '/modifier');
        $this->assertResponse($this->client);

        $form = $this->client->getCrawler()->filterXPath('//*[@id="update"]')->form();
        $this->client->submit($form, [
            'eenov_admin_access[user]' => $this->user->getId(),
            'eenov_admin_access[readonly]' => true,
            'eenov_admin_access[userComment]' => null,
            'eenov_admin_access[agencyComment]' => null,
            'eenov_admin_access[mandate]' => null,
            'eenov_admin_access[validate]' => 0,
        ]);
        $this->assertRedirect($this->client);
        $this->client->followRedirect();
        $this->assertResponse($this->client);
    }

    public function testDeleteAction()
    {
        $this->client->request('GET', '/administration/annonces/' . $this->access->getBid()->getId() . '/acces/' . $this->access->getId() . '/supprimer');
        $this->assertResponse($this->client);

        $form = $this->client->getCrawler()->filterXPath('//*[@id="delete"]')->form();
        $this->client->submit($form);
        $this->assertRedirect($this->client);
        $this->client->followRedirect();
        $this->assertResponse($this->client);
    }
}
