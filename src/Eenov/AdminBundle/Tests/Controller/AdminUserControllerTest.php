<?php

namespace Eenov\AdminBundle\Tests\Controller;

use Eenov\AdminBundle\Test\WebTestCase;
use Eenov\DefaultBundle\Entity\Doctrine\AcquisitionInterface;
use Eenov\DefaultBundle\Entity\Doctrine\BudgetInterface;
use Eenov\DefaultBundle\Entity\Doctrine\BuyerInterface;
use Eenov\DefaultBundle\Entity\Doctrine\MaritalStatusInterface;
use Eenov\DefaultBundle\Entity\Doctrine\ProjectNatureInterface;
use Eenov\DefaultBundle\Entity\Doctrine\SituationInterface;
use Eenov\DefaultBundle\Entity\Doctrine\TitleInterface;

/**
 * Class AdminUserControllerTest
 *
*
 */
class AdminUserControllerTest extends WebTestCase
{
    public function testIndexAction()
    {
        $this->client->request('GET', '/administration/utilisateurs');
        $this->assertResponse($this->client);
    }

    public function testIndexPaginationAction()
    {
        $this->client->request('GET', '/administration/utilisateurs/1');
        $this->assertResponse($this->client);
    }

    public function testCreateAction()
    {
        $this->client->request('GET', '/administration/utilisateurs/creer');
        $this->assertResponse($this->client);

        $form = $this->client->getCrawler()->filterXPath('//*[@id="create"]')->form();
        $this->client->submit($form, [
            'eenov_admin_user_full[username]' => 'john@doe.com',
            'eenov_admin_user_full[firstname]' => 'John',
            'eenov_admin_user_full[lastname]' => 'DOE',
            'eenov_admin_user_full[rawPassword][first]' => 'thisisatest',
            'eenov_admin_user_full[rawPassword][second]' => 'thisisatest',
        ]);
        $this->assertRedirect($this->client);
        $this->client->followRedirect();
        $this->assertResponse($this->client);
    }

    public function testUpdateAction()
    {
        $this->client->request('GET', '/administration/utilisateurs/1/modifier');
        $this->assertResponse($this->client);

        $form = $this->client->getCrawler()->filterXPath('//*[@id="update"]')->form();
        $this->client->submit($form, [
            'eenov_admin_user_full[username]' => 'john@doe.com',
            'eenov_admin_user_full[firstname]' => 'John',
            'eenov_admin_user_full[lastname]' => 'DOE',
        ]);
        $this->assertRedirect($this->client);
        $this->client->followRedirect();
        $this->assertResponse($this->client);
    }

    public function testProfileAction()
    {
        $this->client->request('GET', '/administration/utilisateurs/1/modifier-profil');
        $this->assertResponse($this->client);

        $form = $this->client->getCrawler()->filterXPath('//*[@id="update"]')->form();
        $this->client->submit($form, [
            'eenov_user_profile[title]' => TitleInterface::TITLE_MR,
            'eenov_user_profile[placeOfBirth]' => 'BigotBourg',
            'eenov_user_profile[birthday]' => '24/11/1995',
            'eenov_user_profile[maritalStatus]' => MaritalStatusInterface::MARITAL_STATUS_COMCUBINAGE,
            'eenov_user_profile[childCount]' => 3,
            'eenov_user_profile[address]' => '51, avenue de Adam 60967 Guillot',
            'eenov_user_profile[zip]' => '64 007',
            'eenov_user_profile[city]' => 'Ramos-sur-Bourgeois',
            'eenov_user_profile[country]' => 'Nepal',
            'eenov_user_profile[phone]' => '+33 (0)9 93 23 52 16',
            'eenov_user_profile[cellphone]' => '0620849842',
            'eenov_user_profile[situation]' => SituationInterface::SITUATION_CDD,
            'eenov_user_profile[situationDate]' => [
                'month' => 5,
                'year' => 2014,
            ],
            'eenov_user_profile[buyerStatus]' => BuyerInterface::BUYER_STATUS_OWNER,
            'eenov_user_profile[buyerStatusDate]' => [
                'month' => 5,
                'year' => 2014,
            ],
            'eenov_user_profile[advertType]' => 2,
            'eenov_user_profile[advertTypes][0]' => 2,
            'eenov_user_profile[budget]' => BudgetInterface::BUDGET_100000_150000,
            'eenov_user_profile[projectNature]' => ProjectNatureInterface::PROJECT_NATURE_PRINCIPAL_RESIDENCE,
            'eenov_user_profile[acquisition]' => AcquisitionInterface::ACQUISITION_FIRST,
            'eenov_user_profile[sellBeforeBuying]' => true,
            'eenov_user_profile[sellSinceDate]' => [
                'month' => 5,
                'year' => 2014,
            ],
            'eenov_user_profile[sellers][0]' => 1,
        ]);
        $this->assertRedirect($this->client);
        $this->client->followRedirect();
        $this->assertResponse($this->client);
    }

    public function testPasswordAction()
    {
        $this->client->request('GET', '/administration/utilisateurs/1/modifier-mot-de-passe');
        $this->assertResponse($this->client);

        $form = $this->client->getCrawler()->filterXPath('//*[@id="update"]')->form();
        $this->client->submit($form, [
            'eenov_admin_user_password[rawPassword][first]' => 'thisisanewpassword',
            'eenov_admin_user_password[rawPassword][second]' => 'thisisanewpassword',
        ]);
        $this->assertRedirect($this->client);
        $this->client->followRedirect();
        $this->assertResponse($this->client);
    }

    public function testDeleteAction()
    {
        $this->client->request('GET', '/administration/utilisateurs/1/supprimer');
        $this->assertResponse($this->client);

        $form = $this->client->getCrawler()->filterXPath('//*[@id="delete"]')->form();
        $this->client->submit($form);
        $this->assertRedirect($this->client);
        $this->client->followRedirect();
        $this->assertResponse($this->client);
    }
}
