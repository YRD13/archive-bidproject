<?php

namespace Eenov\AdminBundle\Tests\Controller;

use Eenov\AdminBundle\Test\WebTestCase;
use Eenov\DefaultBundle\Entity\Asset;

/**
 * Class AdminAssetControllerTest
 *
*
 */
class AdminAssetControllerTest extends WebTestCase
{
    /**
     * @var Asset
     */
    private $asset;

    public function testCreateAction()
    {
        $this->client->request('GET', '/administration/agences/' . $this->asset->getAgency()->getId() . '/encours-financier/creer');
        $this->assertResponse($this->client);

        $now = new \DateTime();
        $now->add(new \DateInterval('P1M'));

        $form = $this->client->getCrawler()->filterXPath('//*[@id="create"]')->form();
        $this->client->submit($form, [
            'eenov_admin_asset[date]' => $now->format('d/m/Y'),
            'eenov_admin_asset[amount]' => -500,
            'eenov_admin_asset[comment]' => 'Lorem ipsum dolor sit amet',
        ]);
        $this->assertRedirect($this->client);
        $this->client->followRedirect();
        $this->assertResponse($this->client);
    }

    public function testDeleteAction()
    {
        $this->client->request('GET', '/administration/agences/' . $this->asset->getAgency()->getId() . '/encours-financier/' . $this->asset->getId() . '/supprimer');
        $this->assertResponse($this->client);

        $form = $this->client->getCrawler()->filterXPath('//*[@id="delete"]')->form();
        $this->client->submit($form);
        $this->assertRedirect($this->client);
        $this->client->followRedirect();
        $this->assertResponse($this->client);
    }

    public function testIndexAction()
    {
        $this->client->request('GET', '/administration/agences/' . $this->asset->getAgency()->getId() . '/encours-financier');
        $this->assertResponse($this->client);
    }

    public function testIndexPaginationAction()
    {
        $this->client->request('GET', '/administration/agences/' . $this->asset->getAgency()->getId() . '/encours-financier/1');
        $this->assertResponse($this->client);
    }

    public function testUpdateAction()
    {
        $this->client->request('GET', '/administration/agences/' . $this->asset->getAgency()->getId() . '/encours-financier/' . $this->asset->getId() . '/modifier');
        $this->assertResponse($this->client);

        $now = new \DateTime();
        $now->add(new \DateInterval('P1M'));

        $form = $this->client->getCrawler()->filterXPath('//*[@id="update"]')->form();
        $this->client->submit($form, [
            'eenov_admin_asset[date]' => $now->format('d/m/Y'),
            'eenov_admin_asset[amount]' => -500,
            'eenov_admin_asset[comment]' => 'Lorem ipsum dolor sit amet',
        ]);
        $this->assertRedirect($this->client);
        $this->client->followRedirect();
        $this->assertResponse($this->client);
    }

    protected function setUp()
    {
        parent::setUp();
        $this->asset = $this->client->getContainer()->get('doctrine.orm.default_entity_manager')->getRepository(Asset::class)->find(1);
    }
}
