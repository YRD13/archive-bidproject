<?php

namespace Eenov\AdminBundle\Tests\Controller;

use Eenov\AdminBundle\Test\WebTestCase;
use Symfony\Component\Yaml\Yaml;

/**
 * Class AdminEmailControllerTest
 *
*
 */
class AdminEmailControllerTest extends WebTestCase
{
    /**
     * getTemplates
     *
     * @return array[]
     */
    public function getTemplates()
    {
        $parameters = Yaml::parse(__DIR__ . '/../../../../../app/config/config.yml');

        return array_map(function ($parameter) {
            return [$parameter];
        }, array_keys($parameters['parameters']['emails']));
    }

    /**
     * @param string $parameter
     *
     * @dataProvider getTemplates
     */
    public function testIframeAction($parameter)
    {
        $this->client->request('GET', '/administration/emails/iframe/' . $parameter);
        $this->assertResponse($this->client);
    }

    public function testIndexAction()
    {
        $this->client->request('GET', '/administration/emails');
        $this->assertResponse($this->client);
    }

    /**
     * @param string $parameter
     *
     * @dataProvider getTemplates
     */
    public function testReadAction($parameter)
    {
        $this->client->request('GET', '/administration/emails/' . $parameter);
        $this->assertResponse($this->client);
    }
}
