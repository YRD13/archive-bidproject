<?php

namespace Eenov\AdminBundle\Test;

use Eenov\DefaultBundle\Test\WebTestCase as BaseWebTestCase;
use Symfony\Bundle\FrameworkBundle\Client;

/**
 * Class WebTestCase
 *
*
 */
class WebTestCase extends BaseWebTestCase
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        parent::setUp();
        $this->client = static::createClient();
        $this->login($this->client, 'admin@oxioneo.com');
    }
}
