<?php

namespace Eenov\AdminBundle\DependencyInjection;

use Eenov\AdminBundle\Form as AdminForm;
use Eenov\DefaultBundle\DependencyInjection\AbstractExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Parameter;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class EenovAdminExtension
 *
*
 */
class EenovAdminExtension extends AbstractExtension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $forms = [
            AdminForm\AccessType::class => [],
            AdminForm\AdvertType::class => [],
            AdminForm\AdvertTypeType::class => [],
            AdminForm\AgencyFilterType::class => [],
            AdminForm\AgencyType::class => [new Reference('doctrine.orm.default_entity_manager')],
            AdminForm\AroundType::class => [],
            AdminForm\AssetType::class => [],
            AdminForm\BankType::class => [],
            AdminForm\BidDatesType::class => [],
            AdminForm\BidType::class => [new Parameter('unity_money')],
            AdminForm\BillType::class => [],
            AdminForm\BidFilterType::class => [],
            AdminForm\BidSlotType::class => [],
            AdminForm\DocumentTypeType::class => [],
            AdminForm\EquipmentType::class => [],
            AdminForm\ImageTypeType::class => [],
            AdminForm\LegalType::class => [],
            AdminForm\PostType::class => [],
            AdminForm\PriceType::class => [],
            AdminForm\MandateType::class => [],
            AdminForm\PartnerType::class => [],
            AdminForm\PdfBillFilterType::class => [],
            AdminForm\SellerType::class => [],
            AdminForm\SlotType::class => [],
            AdminForm\SubscriptionEntireType::class => [],
            AdminForm\SubscriptionType::class => [],
            AdminForm\UserFilterType::class => [],
            AdminForm\UserFullType::class => [],
            AdminForm\UserPasswordType::class => [new Reference('security.encoder_factory')],
            AdminForm\UserType::class => [],
            AdminForm\VisitType::class => [],
        ];

        $this->addForms($container, $forms);
    }
}
