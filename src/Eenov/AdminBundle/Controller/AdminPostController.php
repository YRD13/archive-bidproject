<?php

namespace Eenov\AdminBundle\Controller;

use Eenov\DefaultBundle\Entity\Post;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminPostController
 *
*
 * @Route("/articles")
 */
class AdminPostController extends Controller
{
    /**
     * Index
     *
     * @return array
     * @Route("/{page}", requirements={"page":"\d+"}, defaults={"page":1})
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $paginatorHelper = $this->get('eb_paginator_helper');
        $paginator = $this->get('doctrine.orm.default_entity_manager')->getRepository(Post::class)->getPaginator($paginatorHelper);

        return [
            'paginator' => $paginator,
        ];
    }

    /**
     * Create
     *
     * @param Request $request
     *
     * @return array
     * @Route("/creer")
     * @Method("GET|POST")
     * @Template()
     */
    public function createAction(Request $request)
    {
        $form = $this->createForm('eenov_admin_post', $post = new Post());
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->persist($post);
            $em->flush();

            return $this->redirectToRoute('eenov_admin_adminpost_index');
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * Update
     *
     * @param Request $request Request
     * @param Post    $post    Post
     *
     * @return array
     * @Route("/{post}/modifier", requirements={"post":"\d+"})
     * @ParamConverter("post", class="EenovDefaultBundle:Post")
     * @Method("GET|POST")
     * @Template()
     */
    public function updateAction(Request $request, Post $post)
    {
        $form = $this->createForm('eenov_admin_post', $post);
        if ($form->handleRequest($request)->isValid()) {
            $this->get('doctrine.orm.default_entity_manager')->flush();

            return $this->redirectToRoute('eenov_admin_adminpost_index');
        }

        return [
            'form' => $form->createView(),
            'post' => $post,
        ];
    }

    /**
     * Delete
     *
     * @param Request $request Request
     * @param Post    $post    Post
     *
     * @return array
     * @Route("/{post}/supprimer", requirements={"post":"\d+"})
     * @ParamConverter("post", class="EenovDefaultBundle:Post")
     * @Method("GET|POST")
     * @Template()
     */
    public function deleteAction(Request $request, Post $post)
    {
        $form = $this->createForm('form');
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->remove($post);
            $em->flush();

            return $this->redirectToRoute('eenov_admin_adminpost_index');
        }

        return [
            'form' => $form->createView(),
            'post' => $post,
        ];
    }
}
