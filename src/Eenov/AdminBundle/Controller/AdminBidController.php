<?php

namespace Eenov\AdminBundle\Controller;

use Eenov\DefaultBundle\Entity\AdvertBuilding;
use Eenov\DefaultBundle\Entity\AdvertGround;
use Eenov\DefaultBundle\Entity\Bid;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminBidController
 *
*
 * @Route("/appels-offre")
 */
class AdminBidController extends Controller
{
    /**
     * Index
     *
     * @param Request $request
     *
     * @return array
     * @Route("/{page}", requirements={"page":"\d+"}, defaults={"page":1})
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $filter = $this->get('form.factory')->createNamed(null, 'eenov_admin_bid_filter', [], ['method' => 'GET', 'csrf_protection' => false])->handleRequest($request);
        $filterData = ($filter->isSubmitted() && $filter->isValid()) ? $filter->getData() : [];

        $paginatorHelper = $this->get('eb_paginator_helper');
        $paginator = $this->get('doctrine.orm.default_entity_manager')->getRepository(Bid::class)->getPaginator($paginatorHelper, $filterData);

        return [
            'paginator' => $paginator,
            'filter' => $filter->createView(),
        ];
    }

    /**
     * Create
     *
     * @param Request $request
     *
     * @return array
     * @Route("/creer")
     * @Method("GET|POST")
     * @Template()
     */
    public function createAction(Request $request)
    {
        $form = $this->createForm('eenov_admin_bid', $bid = new Bid());
        if ($form->handleRequest($request)->isSubmitted()) {
            if ($form->isValid()) {
                $bid->setStepPrice($bid->calculateStepPrice($bid->getFirstPrice(), $bid->getEstimatedPrice()));
                $em = $this->get('doctrine.orm.default_entity_manager');
                $em->persist($bid);
                $em->flush();

                return $this->redirectToRoute('eenov_admin_adminbid_update', [
                    'bid' => $bid->getId(),
                ]);
            } else {
                $form->addError(new FormError('Une erreur est survenue. L\'enchère n\'a pas été sauvegardée. Vous trouverez plus de détails sur les erreurs au niveau de chaque champ du formulaire'));
            }
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * Admin
     *
     * @param Request $request Request
     * @param Bid     $bid     Bid
     *
     * @return array
     * @Route("/{bid}/administration", requirements={"bid":"\d+"})
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @Method("GET|POST")
     * @Template()
     */
    public function adminAction(Request $request, Bid $bid)
    {
        $formDates = $this->createForm('eenov_admin_bid_dates', $bid, ['validation_groups' => ['Dates']]);
        if ($formDates->handleRequest($request)->isValid()) {
            $this->get('doctrine.orm.default_entity_manager')->flush();

            $this->get('eenov.default_bundle.session.session')->success('Annonce mise à jour avec succès');

            return $this->redirectToRoute('eenov_admin_adminbid_admin', [
                'bid' => $bid->getId(),
            ]);
        }

        $formSlot = $this->createForm('eenov_admin_bid_slot', $bid, ['validation_groups' => ['Slot']]);
        if ($formSlot->handleRequest($request)->isValid()) {
            $this->get('doctrine.orm.default_entity_manager')->flush();

            $this->get('eenov.default_bundle.session.session')->success('Annonce mise à jour avec succès');

            return $this->redirectToRoute('eenov_admin_adminbid_admin', [
                'bid' => $bid->getId(),
            ]);
        }

        $formLock = $this->get('form.factory')->createNamed('lock', 'form', $bid, ['validation_groups' => ['Slot']]);
        if ($formLock->handleRequest($request)->isValid()) {
            $bid->setLock(!$bid->getLock());
            $this->get('doctrine.orm.default_entity_manager')->flush();

            $this->get('eenov.default_bundle.session.session')->success('Annonce mise à jour avec succès');

            return $this->redirectToRoute('eenov_admin_adminbid_admin', [
                'bid' => $bid->getId(),
            ]);
        }

        return [
            'formDates' => $formDates->createView(),
            'formSlot' => $formSlot->createView(),
            'formLock' => $formLock->createView(),
            'bid' => $bid,
        ];
    }

    /**
     * Text
     *
     * @param Bid $bid
     *
     * @return array
     * @Route("/{bid}/texte-annonce", requirements={"bid":"\d+"})
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @Method("GET")
     * @Template()
     */
    public function textAction(Bid $bid)
    {
        return [
            'bid' => $bid,
        ];
    }

    /**
     * Delete
     *
     * @param Request $request Request
     * @param Bid     $bid     Bid
     *
     * @return array
     * @Route("/{bid}/supprimer", requirements={"bid":"\d+"})
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @Method("GET|POST")
     * @Template()
     */
    public function deleteAction(Request $request, Bid $bid)
    {
        $form = $this->createForm('form');
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->remove($bid);
            $em->flush();

            $this->get('eenov.default_bundle.session.session')->success('Annonce supprimée avec succès');

            return $this->redirectToRoute('eenov_admin_adminbid_index');
        }

        return [
            'form' => $form->createView(),
            'bid' => $bid,
        ];
    }

    /**
     * File
     *
     * @param Bid $bid Bid
     *
     * @return BinaryFileResponse
     * @Route("/{bid}/mandat.{_format}", requirements={"bid":"\d+"})
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @Method("GET")
     */
    public function fileAction(Bid $bid)
    {
        if (null === $bid->getPath()) {
            throw $this->createNotFoundException();
        }

        return new BinaryFileResponse($bid->getPath(), 200, [
            'Content-Type' => 'application/force-download',
            'Cache-Control' => 'no-cache, private',
            'Content-Disposition' => sprintf('attachment; filename="%s"', $bid->getFilename()),
            'Content-Transfer-Encoding' => 'binary',
            'Content-Length' => $bid->getSize(),
        ]);
    }

    /**
     * Update
     *
     * @param Request $request Request
     * @param Bid     $bid     Bid
     *
     * @return array
     * @Route("/{bid}/modifier", requirements={"bid":"\d+"})
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @Method("GET|POST")
     * @Template()
     */
    public function updateAction(Request $request, Bid $bid)
    {
        $form = $this->createForm('eenov_admin_bid', $bid);
        if ($form->handleRequest($request)->isSubmitted()) {

            if ($form->isValid()) {
                $heureDebut=$form->get('hourStart')->getData();
                if($heureDebut == null ){
                    $heureDebut = 12;
                }
                $bid->setHourStart($heureDebut);
                $bid->setSellStarted( $form->get('sellStarted')->getData());
                $bid->setSellEnded( $form->get('sellEnded')->getData());


                /*$DepartOffre =$form->get('priceStartOld')->getData();
                $DepartOffreEstimated =$form->get('priceEstimatedOld')->getData();*/

                $DepartOffre =$form->get('firstPrice')->getData();
                $DepartOffreEstimated =$form->get('estimatedPrice')->getData();

                $honoraireCalculation =floatval(str_replace(",", ".", $form->get('honoraireCalculation')->getData()));
                $honoraireCalculation2 =floatval(str_replace(",", ".", $form->get('honoraireCalculation2')->getData()));

                if ($honoraireCalculation !="" && $honoraireCalculation2 == "") {
                    $bid->setHonorairePrice($honoraireCalculation);

                    $bid->setFirstPrice($DepartOffre);
                    $bid->setPriceStartOld($DepartOffre - $honoraireCalculation);
                    $bid->setEstimatedPrice($DepartOffreEstimated);
                    $bid->setPriceEstimatedOld($DepartOffreEstimated-$honoraireCalculation);
                }elseif ($honoraireCalculation =="" && $honoraireCalculation2 != ""){
                    $honoraireMontant_first = round($DepartOffre*$honoraireCalculation2/100);
                    $honoraireMontant_estimated = round($DepartOffreEstimated*$honoraireCalculation2/100);
                    $bid->setHonorairePrice($honoraireMontant_estimated);
                    $bid->setFirstPrice($DepartOffre);
                    $bid->setPriceStartOld($DepartOffre - $honoraireMontant_first);
                    $bid->setEstimatedPrice($DepartOffreEstimated);
                    $bid->setPriceEstimatedOld($DepartOffreEstimated - $honoraireMontant_estimated);

                }

                $bid->setStepPrice($bid->calculateStepPrice($bid->getFirstPrice(), $bid->getEstimatedPrice()));

                $this->get('doctrine.orm.default_entity_manager')->persist($bid);
                $this->get('doctrine.orm.default_entity_manager')->flush();
                $this->get('eenov.default_bundle.session.session')->success('Annonce mise à jour avec succès');
                return $this->redirectToRoute('eenov_admin_adminbid_update', [
                    'bid' => $bid->getId(),
                ]);
            } else {
                $form->addError(new FormError('Une erreur est survenue. L\'enchère n\'a pas été sauvegardée. Vous trouverez plus de détails sur les erreurs au niveau de chaque champ du formulaire.'));
            }
        }

        return [
            'form' => $form->createView(),
            'bid' => $bid,
        ];
    }

    /**
     * Building
     *
     * @param Request $request Request
     * @param Bid     $bid     Bid
     *
     * @return array|RedirectResponse
     * @Route("/{bid}/immeuble", requirements={"bid":"\d+"})
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @Method("GET|POST")
     * @Template()
     */
    public function buildingAction(Request $request, Bid $bid)
    {
        $building = $bid->getDraft()->getBuilding() ?: new AdvertBuilding();
        $bid->getDraft()->setBuilding($building);
        $form = $this->createForm('eenov_agency_advert_building', $building);
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->persist($building);
            $em->flush();

            $this->get('eenov.default_bundle.session.session')->success('Annonce mise à jour avec succès');

            return $this->redirectToRoute('eenov_admin_adminbid_building', [
                'bid' => $bid->getId(),
            ]);
        }

        return [
            'bid' => $bid,
            'form' => $form->createView(),
        ];
    }

    /**
     * Ground
     *
     * @param Request $request Request
     * @param Bid     $bid     Bid
     *
     * @return array|RedirectResponse
     * @Route("/{bid}/terrain", requirements={"bid":"\d+"})
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @Method("GET|POST")
     * @Template()
     */
    public function groundAction(Request $request, Bid $bid)
    {
        $ground = $bid->getDraft()->getGround() ?: new AdvertGround();
        $bid->getDraft()->setGround($ground);
        $form = $this->createForm('eenov_agency_advert_ground', $ground);
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->persist($ground);
            $em->flush();

            $this->get('eenov.default_bundle.session.session')->success('Annonce mise à jour avec succès');

            return $this->redirectToRoute('eenov_admin_adminbid_ground', [
                'bid' => $bid->getId(),
            ]);
        }

        return [
            'bid' => $bid,
            'form' => $form->createView(),
        ];
    }

    /**
     * Validate
     *
     * @param Request $request Request
     * @param Bid     $bid     Bid
     *
     * @return array
     * @Route("/{bid}/valider", requirements={"bid":"\d+"})
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @Method("GET|POST")
     * @Template()
     */
    public function validateAction(Request $request, Bid $bid)
    {
        $form = $this->createForm('form');
        if ($form->handleRequest($request)->isValid()) {
            $bid->setAdminValidated(new \DateTime());
            $this->get('eenov.default_bundle.advert.advert_cloner')->progress($bid);
            $this->get('doctrine.orm.default_entity_manager')->flush();

            $this->get('eenov.default_bundle.session.session')->success('Annonce validée avec succès');

            return $this->redirectToRoute('eenov_admin_adminbid_validate', [
                'bid' => $bid->getId(),
            ]);
        }

        $forceRevalidateForm = null;
        if (null !== $bid->getPublish()) {
            $forceRevalidateForm = $this->get('form.factory')->createNamed('revalidate', 'form');
            if ($forceRevalidateForm->handleRequest($request)->isValid()) {
                $bid->setAdminValidated(new \DateTime());
                $this->get('eenov.default_bundle.advert.advert_cloner')->progress($bid);
                $this->get('doctrine.orm.default_entity_manager')->flush();

                $this->get('eenov.default_bundle.session.session')->success('Annonce revalidée avec succès');

                return $this->redirectToRoute('eenov_admin_adminbid_validate', [
                    'bid' => $bid->getId(),
                ]);
            }
        }

        return [
            'adminValidationForm' => $form->createView(),
            'adminForceRevalidateForm' => $forceRevalidateForm ? $forceRevalidateForm->createView() : null,
            'bid' => $bid,
        ];
    }
}
