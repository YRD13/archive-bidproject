<?php

namespace Eenov\AdminBundle\Controller;

use Eenov\DefaultBundle\Entity\Bid;
use Eenov\DefaultBundle\Entity\Image;
use Eenov\DefaultBundle\Entity\ImageType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminImageController
 *
*
 * @Route("/annonces/{bid}/images", requirements={"bid":"\d+"})
 */
class AdminImageController extends Controller
{
    /**
     * Index
     *
     * @param Bid $bid Bid
     *
     * @return array
     * @Route()
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Bid $bid)
    {
        $images = array_map(function (Image $image) use ($bid) {
            return $this->imageToArray($bid, $image);
        }, $bid->getDraft()->getImages()->toArray());

        return [
            'bid' => $bid,
            'images' => $images,
            'imagesTypes' => $this->get('doctrine.orm.default_entity_manager')->getRepository(ImageType::class)->findAll(),
            'image_upload' => $this->generateUrl('eenov_admin_adminimage_upload', ['bid' => $bid->getId()]),
        ];
    }

    /**
     * Delete
     *
     * @param Bid   $bid   Bid
     * @param Image $image Image
     *
     * @return JsonResponse
     * @Route("/{image}/supprimer", requirements={"image":"\d+"})
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @ParamConverter("image", class="EenovDefaultBundle:Image")
     */
    public function deleteAction(Bid $bid, Image $image)
    {
        $em = $this->get('doctrine.orm.default_entity_manager');
        $em->remove($image);
        $em->flush();

        return $this->redirectToRoute('eenov_admin_adminimage_index', [
            'bid' => $bid->getId(),
        ]);
    }

    /**
     * Default
     *
     * @param Bid   $bid   Bid
     * @param Image $image Image
     *
     * @return JsonResponse
     * @Route("/{image}/modifier-previsualisation", requirements={"image":"\d+"})
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @ParamConverter("image", class="EenovDefaultBundle:Image")
     */
    public function defaultAction(Bid $bid, Image $image)
    {
        $bid->getDraft()->setFile($this->get('eenov.default_bundle.advert.advert_cloner')->duplicateFile($image));
        $this->get('doctrine.orm.default_entity_manager')->flush();

        return $this->redirectToRoute('eenov_admin_adminimage_index', [
            'bid' => $bid->getId(),
        ]);
    }

    /**
     * Upload
     *
     * @param Request $request Request
     * @param Bid     $bid     Bid
     *
     * @return JsonResponse
     * @Route("/televerser")
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     */
    public function uploadAction(Request $request, Bid $bid)
    {
        $data = [];
        $em = $this->get('doctrine.orm.default_entity_manager');
        if (null !== $type = $em->getRepository(ImageType::class)->find($request->request->getInt('type'))) {
            foreach ($request->files as $file) {
                $image = new Image();
                $image
                    ->setAdvert($bid->getDraft())
                    ->setFile($file)
                    ->setType($type);

                if (0 === $this->get('validator')->validate($image)->count()) {
                    $em->persist($image);
                    $em->flush();

                    $data[] = $this->imageToArray($bid, $image);
                }
            }
        }

        return new JsonResponse(['files' => $data, 'errors' => []]);
    }

    /**
     * imageToArray
     *
     * @param Bid   $bid   Bid
     * @param Image $image Image
     *
     * @return array
     */
    private function imageToArray(Bid $bid, Image $image)
    {
        return [
            'id' => $image->getId(),
            'type' => (string)$image->getType(),
            'path' => $image->getUri(),
            'delete_uri' => $this->generateUrl('eenov_admin_adminimage_delete', [
                'bid' => $bid->getId(),
                'image' => $image->getId(),
            ]),
            'default_uri' => $this->generateUrl('eenov_admin_adminimage_default', [
                'bid' => $bid->getId(),
                'image' => $image->getId(),
            ]),
            'crop_uri' => (string)$this->get('liip_imagine.cache.manager')->generateUrl($image->getUri(), 'bid_default_view'),
            'isThumbnail' => false,
        ];
    }
}
