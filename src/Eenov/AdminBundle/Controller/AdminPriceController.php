<?php

namespace Eenov\AdminBundle\Controller;

use Eenov\DefaultBundle\Entity\Price;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminPriceController
 *
*
 * @Route("/tarifs")
 */
class AdminPriceController extends Controller
{
    /**
     * Index
     *
     * @return array
     * @Route("/{page}", requirements={"page":"\d+"}, defaults={"page":1})
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $paginatorHelper = $this->get('eb_paginator_helper');
        $paginator = $this->get('doctrine.orm.default_entity_manager')->getRepository(Price::class)->getPaginator($paginatorHelper);

        return [
            'paginator' => $paginator,
        ];
    }

    /**
     * Create
     *
     * @param Request $request
     *
     * @return array
     * @Route("/creer")
     * @Method("GET|POST")
     * @Template()
     */
    public function createAction(Request $request)
    {
        $form = $this->createForm('eenov_admin_price', $price = new Price());
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->persist($price);
            $em->flush();

            return $this->redirectToRoute('eenov_admin_adminprice_index');
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * Update
     *
     * @param Request $request Request
     * @param Price   $price   Price
     *
     * @return array
     * @Route("/{price}/modifier", requirements={"price":"\d+"})
     * @ParamConverter("price", class="EenovDefaultBundle:Price")
     * @Method("GET|POST")
     * @Template()
     */
    public function updateAction(Request $request, Price $price)
    {
        $form = $this->createForm('eenov_admin_price', $price);
        if ($form->handleRequest($request)->isValid()) {
            $this->get('doctrine.orm.default_entity_manager')->flush();

            return $this->redirectToRoute('eenov_admin_adminprice_index');
        }

        return [
            'form' => $form->createView(),
            'price' => $price,
        ];
    }

    /**
     * Delete
     *
     * @param Request $request Request
     * @param Price   $price   Price
     *
     * @return array
     * @Route("/{price}/supprimer", requirements={"price":"\d+"})
     * @ParamConverter("price", class="EenovDefaultBundle:Price")
     * @Method("GET|POST")
     * @Template()
     */
    public function deleteAction(Request $request, Price $price)
    {
        $form = $this->createForm('form');
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->remove($price);
            $em->flush();

            return $this->redirectToRoute('eenov_admin_adminprice_index');
        }

        return [
            'form' => $form->createView(),
            'price' => $price,
        ];
    }
}
