<?php

namespace Eenov\AdminBundle\Controller;

use Eenov\DefaultBundle\Entity\Agency;
use Eenov\DefaultBundle\Entity\Legal;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class AdminLegalController
 *
*
 * @Route("/agences/{agency}/documents", requirements={"agency":"\d+"})
 */
class AdminLegalController extends Controller
{
    /**
     * Create
     *
     * @param Request $request Request
     * @param Agency  $agency  Agency
     *
     * @return array
     * @Route("/creer")
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @Method("GET|POST")
     * @Template()
     */
    public function createAction(Request $request, Agency $agency)
    {
        $legal = new Legal();
        $legal->setAgency($agency);
        $form = $this->createForm('eenov_admin_legal', $legal);
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->persist($legal);
            $em->flush();

            return $this->redirectToRoute('eenov_admin_adminlegal_index', [
                'agency' => $agency->getId(),
            ]);
        }

        return [
            'agency' => $agency,
            'form' => $form->createView(),
        ];
    }

    /**
     * Delete
     *
     * @param Request $request Request
     * @param Agency  $agency  Agency
     * @param Legal   $legal   Legal
     *
     * @return array
     * @Route("/{legal}/supprimer", requirements={"legal":"\d+"})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @ParamConverter("legal", class="EenovDefaultBundle:Legal")
     * @Method("GET|POST")
     * @Template()
     */
    public function deleteAction(Request $request, Agency $agency, Legal $legal)
    {
        $form = $this->createForm('form');
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->remove($legal);
            $em->flush();

            return $this->redirectToRoute('eenov_admin_adminlegal_index', [
                'agency' => $agency->getId(),
            ]);
        }

        return [
            'agency' => $agency,
            'form' => $form->createView(),
            'legal' => $legal,
        ];
    }

    /**
     * Index
     *
     * @param Agency $agency
     *
     * @return array
     * @Route("/{page}", requirements={"page":"\d+"}, defaults={"page":1})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Agency $agency)
    {
        $paginatorHelper = $this->get('eb_paginator_helper');
        $paginator = $this->get('doctrine.orm.default_entity_manager')->getRepository(Legal::class)->getPaginator($paginatorHelper, ['agency' => $agency]);

        return [
            'agency' => $agency,
            'paginator' => $paginator,
        ];
    }

    /**
     * File
     *
     * @param Legal $legal
     *
     * @return BinaryFileResponse
     * @Route("/consulter/{legal}.{_format}", requirements={"legal":"\d+"})
     * @ParamConverter("legal", class="EenovDefaultBundle:Legal")
     * @Method("GET")
     */
    public function fileAction(Legal $legal)
    {
        if (false === is_file($legal->getPath())) {
            throw new NotFoundHttpException();
        }

        return new BinaryFileResponse($legal->getPath(), 200, [
            'Content-Type' => 'application/force-download',
            'Cache-Control' => 'no-cache, private',
            'Content-Disposition' => sprintf('attachment; filename="%s"', $legal->getFilename()),
            'Content-Transfer-Encoding' => 'binary',
            'Content-Length' => $legal->getSize(),
        ]);
    }
}
