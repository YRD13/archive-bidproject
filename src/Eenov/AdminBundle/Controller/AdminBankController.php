<?php

namespace Eenov\AdminBundle\Controller;

use Eenov\DefaultBundle\Entity\Bank;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminBankController
 *
*
 * @Route("/banques")
 */
class AdminBankController extends Controller
{
    /**
     * Index
     *
     * @return array
     * @Route("/{page}", requirements={"page":"\d+"}, defaults={"page":1})
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $paginatorHelper = $this->get('eb_paginator_helper');
        $paginator = $this->get('doctrine.orm.default_entity_manager')->getRepository(Bank::class)->getPaginator($paginatorHelper);

        return [
            'paginator' => $paginator,
        ];
    }

    /**
     * Create
     *
     * @param Request $request
     *
     * @return array
     * @Route("/creer")
     * @Method("GET|POST")
     * @Template()
     */
    public function createAction(Request $request)
    {
        $form = $this->createForm('eenov_admin_bank', $bank = new Bank());
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->persist($bank);
            $em->flush();

            return $this->redirectToRoute('eenov_admin_adminbank_index');
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * Update
     *
     * @param Request $request Request
     * @param Bank    $bank    Bank
     *
     * @return array
     * @Route("/{bank}/modifier", requirements={"bank":"\d+"})
     * @ParamConverter("bank", class="EenovDefaultBundle:Bank")
     * @Method("GET|POST")
     * @Template()
     */
    public function updateAction(Request $request, Bank $bank)
    {
        $form = $this->createForm('eenov_admin_bank', $bank);
        if ($form->handleRequest($request)->isValid()) {
            $this->get('doctrine.orm.default_entity_manager')->flush();

            return $this->redirectToRoute('eenov_admin_adminbank_index');
        }

        return [
            'form' => $form->createView(),
            'bank' => $bank,
        ];
    }

    /**
     * Delete
     *
     * @param Request $request Request
     * @param Bank    $bank    Bank
     *
     * @return array
     * @Route("/{bank}/supprimer", requirements={"bank":"\d+"})
     * @ParamConverter("bank", class="EenovDefaultBundle:Bank")
     * @Method("GET|POST")
     * @Template()
     */
    public function deleteAction(Request $request, Bank $bank)
    {
        $form = $this->createForm('form');
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->remove($bank);
            $em->flush();

            return $this->redirectToRoute('eenov_admin_adminbank_index');
        }

        return [
            'form' => $form->createView(),
            'bank' => $bank,
        ];
    }
}
