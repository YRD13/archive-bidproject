<?php

namespace Eenov\AdminBundle\Controller;

use Eenov\DefaultBundle\Entity\Bid;
use Eenov\DefaultBundle\Entity\Mandate;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminMandateController
 *
*
 * @Route("/annonces/{bid}/mandats", requirements={"bid":"\d+"})
 */
class AdminMandateController extends Controller
{
    /**
     * Create
     *
     * @param Request $request Request
     * @param Bid     $bid     Bid
     *
     * @return array
     * @Route("/creer")
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @Method("GET|POST")
     * @Template()
     */
    public function createAction(Request $request, Bid $bid)
    {
        $mandate = new Mandate();
        $mandate->setBid($bid);
        $form = $this->createForm('eenov_admin_mandate', $mandate);
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->persist($mandate);
            $em->flush();

            return $this->redirectToRoute('eenov_admin_adminmandate_index', [
                'bid' => $bid->getId(),
            ]);
        }

        return [
            'bid' => $bid,
            'form' => $form->createView(),
        ];
    }

    /**
     * Delete
     *
     * @param Request $request Request
     * @param Bid     $bid     Bid
     * @param Mandate $mandate Mandate
     *
     * @return array
     * @Route("/{mandate}/supprimer", requirements={"mandate":"\d+"})
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @ParamConverter("mandate", class="EenovDefaultBundle:Mandate")
     * @Method("GET|POST")
     * @Template()
     */
    public function deleteAction(Request $request, Bid $bid, Mandate $mandate)
    {
        $form = $this->createForm('form');
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->remove($mandate);
            $em->flush();

            return $this->redirectToRoute('eenov_admin_adminmandate_index', [
                'bid' => $bid->getId(),
            ]);
        }

        return [
            'bid' => $bid,
            'form' => $form->createView(),
            'mandate' => $mandate,
        ];
    }

    /**
     * File
     *
     * @param Bid     $bid     Bid
     * @param Mandate $mandate Mandate
     *
     * @return BinaryFileResponse
     * @Route("/{mandate}/mandat.{_format}", requirements={"mandate":"\d+"})
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @ParamConverter("mandate", class="EenovDefaultBundle:Mandate")
     * @Method("GET")
     */
    public function fileAction(Bid $bid, Mandate $mandate)
    {
        if ($mandate->getBid()->getId() !== $bid->getId()) {
            throw $this->createNotFoundException();
        }
        if (null === $mandate->getPath()) {
            throw $this->createNotFoundException();
        }

        return new BinaryFileResponse($mandate->getPath(), 200, [
            'Content-Type' => 'application/force-download',
            'Cache-Control' => 'no-cache, private',
            'Content-Disposition' => sprintf('attachment; filename="%s"', $mandate->getFilename()),
            'Content-Transfer-Encoding' => 'binary',
            'Content-Length' => $mandate->getSize(),
        ]);
    }

    /**
     * Index
     *
     * @param Bid $bid Bid
     *
     * @return array
     * @Route("/{page}", requirements={"page":"\d+"}, defaults={"page":1})
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Bid $bid)
    {
        $paginatorHelper = $this->get('eb_paginator_helper');
        $paginator = $this->get('doctrine.orm.default_entity_manager')->getRepository(Mandate::class)->getPaginator($paginatorHelper, ['bid' => $bid]);

        return [
            'bid' => $bid,
            'paginator' => $paginator,
        ];
    }

    /**
     * Update
     *
     * @param Request $request Request
     * @param Bid     $bid     Bid
     * @param Mandate $mandate Mandate
     *
     * @return array
     * @Route("/{mandate}/modifier", requirements={"mandate":"\d+"})
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @ParamConverter("mandate", class="EenovDefaultBundle:Mandate")
     * @Method("GET|POST")
     * @Template()
     */
    public function updateAction(Request $request, Bid $bid, Mandate $mandate)
    {
        $form = $this->createForm('eenov_admin_mandate', $mandate);
        if ($form->handleRequest($request)->isValid()) {
            $this->get('doctrine.orm.default_entity_manager')->flush();

            return $this->redirectToRoute('eenov_admin_adminmandate_index', [
                'bid' => $bid->getId(),
            ]);
        }

        return [
            'bid' => $bid,
            'form' => $form->createView(),
            'mandate' => $mandate,
        ];
    }
}
