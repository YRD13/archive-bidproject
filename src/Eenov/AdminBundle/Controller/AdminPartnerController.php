<?php

namespace Eenov\AdminBundle\Controller;

use Eenov\DefaultBundle\Entity\Partner;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminPartnerController
 *
*
 * @Route("/partenaires")
 */
class AdminPartnerController extends Controller
{
    /**
     * Index
     *
     * @return array
     * @Route("/{page}", requirements={"page":"\d+"}, defaults={"page":1})
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $paginatorHelper = $this->get('eb_paginator_helper');
        $paginator = $this->get('doctrine.orm.default_entity_manager')->getRepository(Partner::class)->getPaginator($paginatorHelper);

        return [
            'paginator' => $paginator,
        ];
    }

    /**
     * Create
     *
     * @param Request $request
     *
     * @return array
     * @Route("/creer")
     * @Method("GET|POST")
     * @Template()
     */
    public function createAction(Request $request)
    {
        $form = $this->createForm('eenov_admin_partner', $partner = new Partner());
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->persist($partner);
            $em->flush();

            return $this->redirectToRoute('eenov_admin_adminpartner_index');
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * Update
     *
     * @param Request $request Request
     * @param Partner $partner Partner
     *
     * @return array
     * @Route("/{partner}/modifier", requirements={"partner":"\d+"})
     * @ParamConverter("partner", class="EenovDefaultBundle:Partner")
     * @Method("GET|POST")
     * @Template()
     */
    public function updateAction(Request $request, Partner $partner)
    {
        $form = $this->createForm('eenov_admin_partner', $partner);
        if ($form->handleRequest($request)->isValid()) {
            $this->get('doctrine.orm.default_entity_manager')->flush();

            return $this->redirectToRoute('eenov_admin_adminpartner_index');
        }

        return [
            'form' => $form->createView(),
            'partner' => $partner,
        ];
    }

    /**
     * Delete
     *
     * @param Request $request Request
     * @param Partner $partner Partner
     *
     * @return array
     * @Route("/{partner}/supprimer", requirements={"partner":"\d+"})
     * @ParamConverter("partner", class="EenovDefaultBundle:Partner")
     * @Method("GET|POST")
     * @Template()
     */
    public function deleteAction(Request $request, Partner $partner)
    {
        $form = $this->createForm('form');
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->remove($partner);
            $em->flush();

            return $this->redirectToRoute('eenov_admin_adminpartner_index');
        }

        return [
            'form' => $form->createView(),
            'partner' => $partner,
        ];
    }
}
