<?php

namespace Eenov\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class AdminController
 *
*
 */
class AdminController extends Controller
{
    /**
     * Index
     *
     * @return array
     * @Route()
     * @Method("GET")
     */
    public function indexAction()
    {
        return $this->redirectToRoute('eenov_admin_adminuser_index');
    }
}
