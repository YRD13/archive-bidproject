<?php

namespace Eenov\AdminBundle\Controller;

use Eenov\DefaultBundle\Entity\DocumentType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminDocumentTypeController
 *
*
 * @Route("/types-documents")
 */
class AdminDocumentTypeController extends Controller
{
    /**
     * Index
     *
     * @return array
     * @Route("/{page}", requirements={"page":"\d+"}, defaults={"page":1})
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $paginatorHelper = $this->get('eb_paginator_helper');
        $paginator = $this->get('doctrine.orm.default_entity_manager')->getRepository(DocumentType::class)->getPaginator($paginatorHelper);

        return [
            'paginator' => $paginator,
        ];
    }

    /**
     * Create
     *
     * @param Request $request
     *
     * @return array
     * @Route("/creer")
     * @Method("GET|POST")
     * @Template()
     */
    public function createAction(Request $request)
    {
        $form = $this->createForm('eenov_admin_document_type', $type = new DocumentType());
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->persist($type);
            $em->flush();

            return $this->redirectToRoute('eenov_admin_admindocumenttype_index');
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * Update
     *
     * @param Request      $request Request
     * @param DocumentType $type    Type
     *
     * @return array
     * @Route("/{type}/modifier", requirements={"type":"\d+"})
     * @ParamConverter("type", class="EenovDefaultBundle:DocumentType", options={"id":"type"})
     * @Method("GET|POST")
     * @Template()
     */
    public function updateAction(Request $request, DocumentType $type)
    {
        $form = $this->createForm('eenov_admin_document_type', $type);
        if ($form->handleRequest($request)->isValid()) {
            $this->get('doctrine.orm.default_entity_manager')->flush();

            return $this->redirectToRoute('eenov_admin_admindocumenttype_index');
        }

        return [
            'form' => $form->createView(),
            'type' => $type,
        ];
    }

    /**
     * Delete
     *
     * @param Request      $request Request
     * @param DocumentType $type    Type
     *
     * @return array
     * @Route("/{type}/supprimer", requirements={"type":"\d+"})
     * @ParamConverter("type", class="EenovDefaultBundle:DocumentType", options={"id":"type"})
     * @Method("GET|POST")
     * @Template()
     */
    public function deleteAction(Request $request, DocumentType $type)
    {
        $form = $this->createForm('form');
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->remove($type);
            $em->flush();

            return $this->redirectToRoute('eenov_admin_admindocumenttype_index');
        }

        return [
            'form' => $form->createView(),
            'type' => $type,
        ];
    }
}
