<?php

namespace Eenov\AdminBundle\Controller;

use Eenov\DefaultBundle\Entity\Seller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminSellerController
 *
*
 * @Route("/organismes")
 */
class AdminSellerController extends Controller
{
    /**
     * Index
     *
     * @return array
     * @Route("/{page}", requirements={"page":"\d+"}, defaults={"page":1})
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $paginatorHelper = $this->get('eb_paginator_helper');
        $paginator = $this->get('doctrine.orm.default_entity_manager')->getRepository(Seller::class)->getPaginator($paginatorHelper);

        return [
            'paginator' => $paginator,
        ];
    }

    /**
     * Create
     *
     * @param Request $request
     *
     * @return array
     * @Route("/creer")
     * @Method("GET|POST")
     * @Template()
     */
    public function createAction(Request $request)
    {
        $form = $this->createForm('eenov_admin_seller', $seller = new Seller());
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->persist($seller);
            $em->flush();

            return $this->redirectToRoute('eenov_admin_adminseller_index');
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * Update
     *
     * @param Request $request Request
     * @param Seller  $seller  Seller
     *
     * @return array
     * @Route("/{seller}/modifier", requirements={"seller":"\d+"})
     * @ParamConverter("seller", class="EenovDefaultBundle:Seller")
     * @Method("GET|POST")
     * @Template()
     */
    public function updateAction(Request $request, Seller $seller)
    {
        $form = $this->createForm('eenov_admin_seller', $seller);
        if ($form->handleRequest($request)->isValid()) {
            $this->get('doctrine.orm.default_entity_manager')->flush();

            return $this->redirectToRoute('eenov_admin_adminseller_index');
        }

        return [
            'form' => $form->createView(),
            'seller' => $seller,
        ];
    }

    /**
     * Delete
     *
     * @param Request $request Request
     * @param Seller  $seller  Seller
     *
     * @return array
     * @Route("/{seller}/supprimer", requirements={"seller":"\d+"})
     * @ParamConverter("seller", class="EenovDefaultBundle:Seller")
     * @Method("GET|POST")
     * @Template()
     */
    public function deleteAction(Request $request, Seller $seller)
    {
        $form = $this->createForm('form');
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->remove($seller);
            $em->flush();

            return $this->redirectToRoute('eenov_admin_adminseller_index');
        }

        return [
            'form' => $form->createView(),
            'seller' => $seller,
        ];
    }
}
