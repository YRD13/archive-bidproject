<?php

namespace Eenov\AdminBundle\Controller;

use Eenov\DefaultBundle\Entity\Bid;
use Eenov\DefaultBundle\Entity\Visit;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminVisitController
 *
*
 * @Route("/annonces/{bid}/visites", requirements={"bid":"\d+"})
 */
class AdminVisitController extends Controller
{
    /**
     * Index
     *
     * @param Bid $bid Bid
     *
     * @return array
     * @Route("/{page}", requirements={"page":"\d+"}, defaults={"page":1})
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Bid $bid)
    {
        $paginatorHelper = $this->get('eb_paginator_helper');
        $paginator = $this->get('doctrine.orm.default_entity_manager')->getRepository(Visit::class)->getPaginator($paginatorHelper, ['bid' => $bid]);

        return [
            'bid' => $bid,
            'paginator' => $paginator,
        ];
    }

    /**
     * Create
     *
     * @param Request $request Request
     * @param Bid     $bid     Bid
     *
     * @return array
     * @Route("/creer")
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @Method("GET|POST")
     * @Template()
     */
    public function createAction(Request $request, Bid $bid)
    {
        $visit = new Visit();
        $visit->setBid($bid);
        $form = $this->createForm('eenov_admin_visit', $visit);
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->persist($visit);
            $em->flush();

            return $this->redirectToRoute('eenov_admin_adminvisit_index', [
                'bid' => $bid->getId(),
            ]);
        }

        return [
            'bid' => $bid,
            'form' => $form->createView(),
        ];
    }

    /**
     * Update
     *
     * @param Request $request Request
     * @param Bid     $bid     Bid
     * @param Visit   $visit   Visit
     *
     * @return array
     * @Route("/{visit}/modifier", requirements={"visit":"\d+"})
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @ParamConverter("visit", class="EenovDefaultBundle:Visit")
     * @Method("GET|POST")
     * @Template()
     */
    public function updateAction(Request $request, Bid $bid, Visit $visit)
    {
        $form = $this->createForm('eenov_admin_visit', $visit);
        if ($form->handleRequest($request)->isValid()) {
            $this->get('doctrine.orm.default_entity_manager')->flush();

            return $this->redirectToRoute('eenov_admin_adminvisit_index', [
                'bid' => $bid->getId(),
            ]);
        }

        return [
            'bid' => $bid,
            'form' => $form->createView(),
            'visit' => $visit,
        ];
    }

    /**
     * Delete
     *
     * @param Request $request Request
     * @param Bid     $bid     Bid
     * @param Visit   $visit   Visit
     *
     * @return array
     * @Route("/{visit}/supprimer", requirements={"visit":"\d+"})
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @ParamConverter("visit", class="EenovDefaultBundle:Visit")
     * @Method("GET|POST")
     * @Template()
     */
    public function deleteAction(Request $request, Bid $bid, Visit $visit)
    {
        $form = $this->createForm('form');
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->remove($visit);
            $em->flush();

            return $this->redirectToRoute('eenov_admin_adminvisit_index', [
                'bid' => $bid->getId(),
            ]);
        }

        return [
            'bid' => $bid,
            'form' => $form->createView(),
            'visit' => $visit,
        ];
    }
}
