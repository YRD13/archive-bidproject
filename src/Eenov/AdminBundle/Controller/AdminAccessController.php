<?php

namespace Eenov\AdminBundle\Controller;

use Eenov\DefaultBundle\Entity\Access;
use Eenov\DefaultBundle\Entity\Bid;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminAccessController
 *
*
 * @Route("/annonces/{bid}/acces", requirements={"bid":"\d+"})
 */
class AdminAccessController extends Controller
{
    /**
     * Create
     *
     * @param Request $request Request
     * @param Bid     $bid     Bid
     *
     * @return array
     * @Route("/creer")
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @Method("GET|POST")
     * @Template()
     */
    public function createAction(Request $request, Bid $bid)
    {
        $access = new Access();
        $access->setBid($bid);
        $form = $this->createForm('eenov_admin_access', $access);
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->persist($access);
            $em->flush();

            return $this->redirectToRoute('eenov_admin_adminaccess_index', [
                'bid' => $bid->getId(),
            ]);
        }

        return [
            'bid' => $bid,
            'form' => $form->createView(),
        ];
    }

    /**
     * Delete
     *
     * @param Request $request Request
     * @param Bid     $bid     Bid
     * @param Access  $access  Access
     *
     * @return array
     * @Route("/{access}/supprimer", requirements={"access":"\d+"})
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @ParamConverter("access", class="EenovDefaultBundle:Access")
     * @Method("GET|POST")
     * @Template()
     */
    public function deleteAction(Request $request, Bid $bid, Access $access)
    {
        $form = $this->createForm('form');
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->remove($access);
            $em->flush();

            return $this->redirectToRoute('eenov_admin_adminaccess_index', [
                'bid' => $bid->getId(),
            ]);
        }

        return [
            'bid' => $bid,
            'form' => $form->createView(),
            'access' => $access,
        ];
    }

    /**
     * Index
     *
     * @param Bid $bid Bid
     *
     * @return array
     * @Route("/{page}", requirements={"page":"\d+"}, defaults={"page":1})
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Bid $bid)
    {
        $paginatorHelper = $this->get('eb_paginator_helper');
        $paginator = $this->get('doctrine.orm.default_entity_manager')->getRepository(Access::class)->getPaginator($paginatorHelper, ['bid' => $bid]);

        return [
            'bid' => $bid,
            'paginator' => $paginator,
        ];
    }

    /**
     * Update
     *
     * @param Request $request Request
     * @param Bid     $bid     Bid
     * @param Access  $access  Access
     *
     * @return array
     * @Route("/{access}/modifier", requirements={"access":"\d+"})
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @ParamConverter("access", class="EenovDefaultBundle:Access")
     * @Method("GET|POST")
     * @Template()
     */
    public function updateAction(Request $request, Bid $bid, Access $access)
    {
        $form = $this->createForm('eenov_admin_access', $access);
        if ($form->handleRequest($request)->isValid()) {
            $this->get('doctrine.orm.default_entity_manager')->flush();

            return $this->redirectToRoute('eenov_admin_adminaccess_index', [
                'bid' => $bid->getId(),
            ]);
        }

        return [
            'bid' => $bid,
            'form' => $form->createView(),
            'access' => $access,
        ];
    }
}
