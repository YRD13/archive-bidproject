<?php

namespace Eenov\AdminBundle\Controller;

use Eenov\DefaultBundle\Entity\ImageType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminImageTypeController
 *
*
 * @Route("/types-images")
 */
class AdminImageTypeController extends Controller
{
    /**
     * Index
     *
     * @return array
     * @Route("/{page}", requirements={"page":"\d+"}, defaults={"page":1})
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $paginatorHelper = $this->get('eb_paginator_helper');
        $paginator = $this->get('doctrine.orm.default_entity_manager')->getRepository(ImageType::class)->getPaginator($paginatorHelper);

        return [
            'paginator' => $paginator,
        ];
    }

    /**
     * Create
     *
     * @param Request $request
     *
     * @return array
     * @Route("/creer")
     * @Method("GET|POST")
     * @Template()
     */
    public function createAction(Request $request)
    {
        $form = $this->createForm('eenov_admin_image_type', $type = new ImageType());
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->persist($type);
            $em->flush();

            return $this->redirectToRoute('eenov_admin_adminimagetype_index');
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * Update
     *
     * @param Request   $request Request
     * @param ImageType $type    Type
     *
     * @return array
     * @Route("/{type}/modifier", requirements={"type":"\d+"})
     * @ParamConverter("type", class="EenovDefaultBundle:ImageType", options={"id":"type"})
     * @Method("GET|POST")
     * @Template()
     */
    public function updateAction(Request $request, ImageType $type)
    {
        $form = $this->createForm('eenov_admin_image_type', $type);
        if ($form->handleRequest($request)->isValid()) {
            $this->get('doctrine.orm.default_entity_manager')->flush();

            return $this->redirectToRoute('eenov_admin_adminimagetype_index');
        }

        return [
            'form' => $form->createView(),
            'type' => $type,
        ];
    }

    /**
     * Delete
     *
     * @param Request   $request Request
     * @param ImageType $type    Type
     *
     * @return array
     * @Route("/{type}/supprimer", requirements={"type":"\d+"})
     * @ParamConverter("type", class="EenovDefaultBundle:ImageType", options={"id":"type"})
     * @Method("GET|POST")
     * @Template()
     */
    public function deleteAction(Request $request, ImageType $type)
    {
        $form = $this->createForm('form');
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->remove($type);
            $em->flush();

            return $this->redirectToRoute('eenov_admin_adminimagetype_index');
        }

        return [
            'form' => $form->createView(),
            'type' => $type,
        ];
    }
}
