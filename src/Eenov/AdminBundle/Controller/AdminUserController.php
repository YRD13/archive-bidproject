<?php

namespace Eenov\AdminBundle\Controller;

use Eenov\DefaultBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminUserController
 *
*
 * @Route("/utilisateurs")
 */
class AdminUserController extends Controller
{
    /**
     * Index
     *
     * @param Request $request
     *
     * @return array
     * @Route("/{page}", requirements={"page":"\d+"}, defaults={"page":1})
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $filter = $this->get('form.factory')->createNamed(null, 'eenov_admin_user_filter', [], ['method' => 'GET', 'csrf_protection' => false])->handleRequest($request);
        $filterData = ($filter->isSubmitted() && $filter->isValid()) ? $filter->getData() : [];

        $paginatorHelper = $this->get('eb_paginator_helper');
        $paginator = $this->get('doctrine.orm.default_entity_manager')->getRepository(User::class)->getPaginator($paginatorHelper, $filterData);

        return [
            'filter' => $filter->createView(),
            'paginator' => $paginator,
        ];
    }

    /**
     * Create
     *
     * @param Request $request
     *
     * @return array
     * @Route("/creer")
     * @Method("GET|POST")
     * @Template()
     */
    public function createAction(Request $request)
    {
        $form = $this->createForm('eenov_admin_user_full', $user = new User());
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('eenov_admin_adminuser_index');
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * Update
     *
     * @param Request $request Request
     * @param User    $user    User
     *
     * @return array
     * @Route("/{user}/modifier", requirements={"user":"\d+"})
     * @ParamConverter("user", class="EenovDefaultBundle:User")
     * @Method("GET|POST")
     * @Template()
     */
    public function updateAction(Request $request, User $user)
    {
        $form = $this->createForm('eenov_admin_user_full', $user);
        if ($form->handleRequest($request)->isValid()) {
            $this->get('doctrine.orm.default_entity_manager')->flush();

            return $this->redirectToRoute('eenov_admin_adminuser_index');
        }

        return [
            'form' => $form->createView(),
            'user' => $user,
        ];
    }

    /**
     * Delete
     *
     * @param Request $request Request
     * @param User    $user    User
     *
     * @return array
     * @Route("/{user}/supprimer", requirements={"user":"\d+"})
     * @ParamConverter("user", class="EenovDefaultBundle:User")
     * @Method("GET|POST")
     * @Template()
     */
    public function deleteAction(Request $request, User $user)
    {
        $form = $this->createForm('form');
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->remove($user);
            $em->flush();

            return $this->redirectToRoute('eenov_admin_adminuser_index');
        }

        return [
            'form' => $form->createView(),
            'user' => $user,
        ];
    }

    /**
     * Password
     *
     * @param Request $request Request
     * @param User    $user    User
     *
     * @return array
     * @Route("/{user}/modifier-mot-de-passe")
     * @ParamConverter("user", class="EenovDefaultBundle:User")
     * @Template()
     */
    public function passwordAction(Request $request, User $user)
    {
        $form = $this->createForm('eenov_admin_user_password', $user);
        if ($form->handleRequest($request)->isValid()) {
            $this->get('doctrine.orm.default_entity_manager')->flush();

            $this->get('eenov.default_bundle.session.session')->success('Mot de passe modifié');

            return $this->redirectToRoute('eenov_admin_adminuser_password', [
                'user' => $user->getId(),
            ]);
        }

        return [
            'user' => $user,
            'form' => $form->createView()
        ];
    }

    /**
     * Profile
     *
     * @param Request $request Request
     * @param User    $user    User
     *
     * @return array
     * @Route("/{id}/modifier-profil")
     * @ParamConverter("user", class="EenovDefaultBundle:User")
     * @Template()
     */
    public function profileAction(Request $request, User $user)
    {
        $form = $this->createForm('eenov_user_profile', $user->getProfile());
        if ($form->handleRequest($request)->isValid()) {
            $this->get('doctrine.orm.default_entity_manager')->flush();

            $this->get('eenov.default_bundle.session.session')->success('Profil modifié');

            return $this->redirectToRoute('eenov_admin_adminuser_profile', [
                'id' => $user->getId(),
            ]);
        }

        return [
            'user' => $user,
            'form' => $form->createView()
        ];
    }
}
