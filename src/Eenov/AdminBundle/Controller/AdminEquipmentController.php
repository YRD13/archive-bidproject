<?php

namespace Eenov\AdminBundle\Controller;

use Eenov\DefaultBundle\Entity\Equipment;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminEquipmentController
 *
*
 * @Route("/equipements")
 */
class AdminEquipmentController extends Controller
{
    /**
     * Index
     *
     * @return array
     * @Route("/{page}", requirements={"page":"\d+"}, defaults={"page":1})
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $paginatorHelper = $this->get('eb_paginator_helper');
        $paginator = $this->get('doctrine.orm.default_entity_manager')->getRepository(Equipment::class)->getPaginator($paginatorHelper);

        return [
            'paginator' => $paginator,
        ];
    }

    /**
     * Create
     *
     * @param Request $request
     *
     * @return array
     * @Route("/creer")
     * @Method("GET|POST")
     * @Template()
     */
    public function createAction(Request $request)
    {
        $form = $this->createForm('eenov_admin_equipment', $equipment = new Equipment());
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->persist($equipment);
            $em->flush();

            return $this->redirectToRoute('eenov_admin_adminequipment_index');
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * Update
     *
     * @param Request   $request   Request
     * @param Equipment $equipment Equipment
     *
     * @return array
     * @Route("/{equipment}/modifier", requirements={"equipment":"\d+"})
     * @ParamConverter("equipment", class="EenovDefaultBundle:Equipment")
     * @Method("GET|POST")
     * @Template()
     */
    public function updateAction(Request $request, Equipment $equipment)
    {
        $form = $this->createForm('eenov_admin_equipment', $equipment);
        if ($form->handleRequest($request)->isValid()) {
            $this->get('doctrine.orm.default_entity_manager')->flush();

            return $this->redirectToRoute('eenov_admin_adminequipment_index');
        }

        return [
            'form' => $form->createView(),
            'equipment' => $equipment,
        ];
    }

    /**
     * Delete
     *
     * @param Request   $request   Request
     * @param Equipment $equipment Equipment
     *
     * @return array
     * @Route("/{equipment}/supprimer", requirements={"equipment":"\d+"})
     * @ParamConverter("equipment", class="EenovDefaultBundle:Equipment")
     * @Method("GET|POST")
     * @Template()
     */
    public function deleteAction(Request $request, Equipment $equipment)
    {
        $form = $this->createForm('form');
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->remove($equipment);
            $em->flush();

            return $this->redirectToRoute('eenov_admin_adminequipment_index');
        }

        return [
            'form' => $form->createView(),
            'equipment' => $equipment,
        ];
    }
}
