<?php

namespace Eenov\AdminBundle\Controller;

use Eenov\DefaultBundle\Entity\Agency;
use Eenov\DefaultBundle\Entity\Subscription;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminSubscriptionController
 *
*
 * @Route("/agences/{agency}/abonnements", requirements={"agency":"\d+"})
 */
class AdminSubscriptionController extends Controller
{
    /**
     * Index
     *
     * @param Agency $agency
     *
     * @return array
     * @Route("/{page}", requirements={"page":"\d+"}, defaults={"page":1})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Agency $agency)
    {
        $paginatorHelper = $this->get('eb_paginator_helper');
        $paginator = $this->get('doctrine.orm.default_entity_manager')->getRepository(Subscription::class)->getPaginator($paginatorHelper, ['agency' => $agency]);

        return [
            'agency' => $agency,
            'paginator' => $paginator,
        ];
    }

    /**
     * Create
     *
     * @param Request $request Request
     * @param Agency  $agency  Agency
     *
     * @return array
     * @Route("/creer")
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @Method("GET|POST")
     * @Template()
     */
    public function createAction(Request $request, Agency $agency)
    {
        $subscription = new Subscription();
        $subscription->setAgency($agency);
        $form = $this->createForm('eenov_admin_subscription', $subscription);
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->persist($subscription);
            $em->flush();

            return $this->redirectToRoute('eenov_admin_adminsubscription_index', [
                'agency' => $agency->getId(),
            ]);
        }

        return [
            'agency' => $agency,
            'form' => $form->createView(),
        ];
    }

    /**
     * Create entire
     *
     * @param Request $request Request
     * @param Agency  $agency  Agency
     *
     * @return array
     * @Route("/creer-sur-mesure")
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @Method("GET|POST")
     * @Template()
     */
    public function createEntireAction(Request $request, Agency $agency)
    {
        $subscription = new Subscription();
        $subscription->setAgency($agency);
        $form = $this->createForm('eenov_admin_subscription_entire', $subscription);
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->persist($subscription);
            $em->flush();

            return $this->redirectToRoute('eenov_admin_adminsubscription_index', [
                'agency' => $agency->getId(),
            ]);
        }

        return [
            'agency' => $agency,
            'form' => $form->createView(),
        ];
    }

    /**
     * Delete
     *
     * @param Request      $request      Request
     * @param Agency       $agency       Agency
     * @param Subscription $subscription Subscription
     *
     * @return array
     * @Route("/{subscription}/supprimer", requirements={"subscription":"\d+"})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @ParamConverter("subscription", class="EenovDefaultBundle:Subscription")
     * @Method("GET|POST")
     * @Template()
     */
    public function deleteAction(Request $request, Agency $agency, Subscription $subscription)
    {
        $form = $this->createForm('form');
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->remove($subscription);
            $em->flush();

            return $this->redirectToRoute('eenov_admin_adminsubscription_index', [
                'agency' => $agency->getId(),
            ]);
        }

        return [
            'agency' => $agency,
            'form' => $form->createView(),
            'subscription' => $subscription,
        ];
    }
}
