<?php

namespace Eenov\AdminBundle\Controller;

use Eenov\DefaultBundle\Entity\Agency;
use Eenov\DefaultBundle\Entity\Bid;
use Eenov\DefaultBundle\Entity\Contact;
use Eenov\DefaultBundle\Entity\MoneyIn;
use Eenov\DefaultBundle\Entity\User;
use Faker\Factory;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AdminEmailController
 *
*
 * @Route("/emails")
 */
class AdminEmailController extends Controller
{
    /**
     * Index
     *
     * @return array
     * @Route()
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $emails = $this->container->getParameter('emails');
        ksort($emails);

        return [
            'emails' => $emails,
        ];
    }

    /**
     * Read
     *
     * @param string $email Email
     *
     * @return array
     * @Route("/{email}")
     * @Method("GET")
     * @Template()
     */
    public function readAction($email)
    {
        return [
            'id' => $email,
            'email' => $this->container->getParameter('emails')[$email],
        ];
    }

    /**
     * Iframe
     *
     * @param string $email Email
     *
     * @return array
     * @Route("/iframe/{email}")
     * @Method("GET")
     */
    public function iframeAction($email)
    {
        return new Response($this->get('eb_email.mailer.mailer')->render($email, $this->getTemplateData($email)));
    }

    /**
     * getTemplateData
     *
     * @param string $email
     *
     * @return array
     */
    private function getTemplateData($email)
    {
        $data = [];
        $faker = Factory::create('fr_FR');

        $subject = 'Sujet pour un email très important';

        $em = $this->getDoctrine()->getManager();

        $userCountQb = $em->getRepository(User::class)->createQueryBuilder('user');
        $userCount = (int)$userCountQb->select($userCountQb->expr()->countDistinct('user.id'))->getQuery()->getSingleScalarResult();
        $userOffset = mt_rand(0, $userCount - 1);

        $userQb = $em->getRepository(User::class)->createQueryBuilder('user');
        $user = $userQb->orderBy('user.id', 'ASC')->setFirstResult($userOffset)->setMaxResults(1)->getQuery()->getOneOrNullResult();

        $agencyCountQb = $em->getRepository(Agency::class)->createQueryBuilder('agency');
        $agencyCount = (int)$agencyCountQb->select($agencyCountQb->expr()->countDistinct('agency.id'))->getQuery()->getSingleScalarResult();
        $agencyOffset = mt_rand(0, $agencyCount - 1);

        $agencyQb = $em->getRepository(Agency::class)->createQueryBuilder('agency');
        $agency = $agencyQb->orderBy('agency.id', 'ASC')->setFirstResult($agencyOffset)->setMaxResults(1)->getQuery()->getOneOrNullResult();

        $bidCountQb = $em->getRepository(Bid::class)->createQueryBuilder('bid');
        $bidCount = (int)$bidCountQb->select($bidCountQb->expr()->countDistinct('bid.id'))->getQuery()->getSingleScalarResult();
        $bidOffset = mt_rand(0, $bidCount - 1);

        $bidQb = $em->getRepository(Bid::class)->createQueryBuilder('bid');
        $bid = $bidQb->orderBy('bid.id', 'ASC')->setFirstResult($bidOffset)->setMaxResults(1)->getQuery()->getOneOrNullResult();

        $moneyInCountQb = $em->getRepository(MoneyIn::class)->createQueryBuilder('moneyIn');
        $moneyInCount = (int)$moneyInCountQb->select($moneyInCountQb->expr()->countDistinct('moneyIn.id'))->getQuery()->getSingleScalarResult();
        $moneyInOffset = mt_rand(0, $moneyInCount - 1);

        $moneyInQb = $em->getRepository(MoneyIn::class)->createQueryBuilder('moneyIn');
        $moneyIn = $moneyInQb->orderBy('moneyIn.id', 'ASC')->setFirstResult($moneyInOffset)->setMaxResults(1)->getQuery()->getOneOrNullResult();

        switch ($email) {
            case 'agency_created':
                $data['agency'] = $agency;
                break;
            case 'money_in_created':
            case 'money_in_updated':
                $data['moneyIn'] = $moneyIn;
                break;
            case 'contact':
            case 'bid_contact':
            case 'bid_contact_copy':
            case 'contact_agency':
            case 'contact_seller':
            case 'contact_buyer':
                $data['agency'] = $agency;
                $data['bid'] = $bid;
                $data['contact'] = [
                    'email' => $faker->email,
                    'name' => $faker->realText(50),
                    'firstname' => $faker->firstName,
                    'lastname' => $faker->lastName,
                    'phone' => $faker->phoneNumber,
                    'subject' => $subject,
                    'type' => Contact::getTypeList()[array_rand(Contact::getTypeList())],
                    'typeName' => Contact::getTypeNameList()[array_rand(Contact::getTypeNameList())],
                    'message' => $faker->realText(500),
                ];
                break;
            case 'register':
            case 'email_update':
            case 'email_validation':
            case 'password':
                $data['user'] = $user;
                break;
            case 'access':
            case 'access_validated':
            case 'access_invalidated':
            case 'auction':
                $data['bid'] = $bid;
                break;
            case 'auction_recall':
                $data['text'] = 'dans 3 jours';
                $data['bid'] = $bid;
                break;
            case 'auction_end':
                $data['text'] = '2 heures';
                $data['bid'] = $bid;
                break;
            case 'auction_ended':
                $data['text'] = '20 minutes';
                $data['bid'] = $bid;
                break;
            case 'bid_agency_validate':
            case 'bid_admin_validate':
                $data['bid'] = $bid;
                break;
            case 'user_created':
                $data['user'] = $user;
                break;
            case 'mandate':
                $data['mandate'] = $mandate;
                break;
            default:
                break;
        }

        return $data;
    }
}
