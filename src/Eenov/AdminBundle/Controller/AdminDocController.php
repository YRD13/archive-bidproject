<?php

namespace Eenov\AdminBundle\Controller;

use Eenov\DefaultBundle\Model\Doc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminDocController
 *
*
 * @Route("/doc")
 */
class AdminDocController extends Controller
{
    /**
     * Index
     *
     * @return array
     * @Route()
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $docs = array_map(function ($type, $name) {
            $path = $this->get('kernel')->getRootDir() . DIRECTORY_SEPARATOR . Doc::APP_DIRECTORY . DIRECTORY_SEPARATOR . $type . '.pdf';
            $realpath = realpath($path);

            $updated = null;
            if ($realpath) {
                if (false !== $updatedTimestamp = filectime($realpath)) {
                    if (false !== $updatedDate = \DateTime::createFromFormat('U', $updatedTimestamp)) {
                        $updated = $updatedDate;
                    }
                }
            }

            return [
                'name' => $name,
                'type' => $type,
                'path' => sprintf(Doc::PATH_PATTERN, $type),
                'updated' => $updated,
            ];
        }, Doc::getTypeList(), Doc::getTypeNameList());

        return [
            'docs' => $docs,
        ];
    }

    /**
     * Update
     *
     * @param Request $request
     *
     * @return array
     * @Route("/{type}")
     * @Method("GET|POST")
     * @Template()
     */
    public function updateAction(Request $request, $type)
    {
        $name = Doc::getTypeNameList()[$type];

        $doc = new Doc();
        $doc->setType($type);

        $form = $this->createForm('eenov_default_model_doc', $doc);
        if ($form->handleRequest($request)->isValid()) {
            $path = $this->get('kernel')->getRootDir() . DIRECTORY_SEPARATOR . Doc::APP_DIRECTORY;
            $realpath = realpath($path);
            $doc->getFile()->move($realpath, sprintf('%s.pdf', $type));

            $this->get('eenov.default_bundle.session.session')->success('Le fichier "%s" a bien été modifié !', $name);

            return $this->redirectToRoute('eenov_admin_admindoc_index');
        }

        return [
            'type' => $type,
            'name' => $name,
            'path' => sprintf(Doc::PATH_PATTERN, $type),
            'form' => $form->createView(),
        ];
    }
}
