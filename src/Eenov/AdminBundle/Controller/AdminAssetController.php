<?php

namespace Eenov\AdminBundle\Controller;

use Eenov\DefaultBundle\Entity\Agency;
use Eenov\DefaultBundle\Entity\Asset;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminAssetController
 *
*
 * @Route("/agences/{agency}/encours-financier", requirements={"agency":"\d+"})
 */
class AdminAssetController extends Controller
{
    /**
     * Index
     *
     * @param Agency $agency
     *
     * @return array
     * @Route("/{page}", requirements={"page":"\d+"}, defaults={"page":1})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Agency $agency)
    {
        $paginatorHelper = $this->get('eb_paginator_helper');
        $paginator = $this->get('doctrine.orm.default_entity_manager')->getRepository(Asset::class)->getPaginator($paginatorHelper, ['agency' => $agency]);

        return [
            'agency' => $agency,
            'paginator' => $paginator,
        ];
    }

    /**
     * Create
     *
     * @param Request $request Request
     * @param Agency  $agency  Agency
     *
     * @return array
     * @Route("/creer")
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @Method("GET|POST")
     * @Template()
     */
    public function createAction(Request $request, Agency $agency)
    {
        $asset = new Asset();
        $asset->setAgency($agency);
        $form = $this->createForm('eenov_admin_asset', $asset);
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->persist($asset);
            $em->flush();

            return $this->redirectToRoute('eenov_admin_adminasset_index', [
                'agency' => $agency->getId(),
            ]);
        }

        return [
            'agency' => $agency,
            'form' => $form->createView(),
        ];
    }

    /**
     * Update
     *
     * @param Request $request Request
     * @param Agency  $agency  Agency
     * @param Asset   $asset   Asset
     *
     * @return array
     * @Route("/{asset}/modifier", requirements={"asset":"\d+"})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @ParamConverter("asset", class="EenovDefaultBundle:Asset")
     * @Method("GET|POST")
     * @Template()
     */
    public function updateAction(Request $request, Agency $agency, Asset $asset)
    {
        $form = $this->createForm('eenov_admin_asset', $asset);
        if ($form->handleRequest($request)->isValid()) {
            $this->get('doctrine.orm.default_entity_manager')->flush();

            return $this->redirectToRoute('eenov_admin_adminasset_index', [
                'agency' => $agency->getId(),
            ]);
        }

        return [
            'agency' => $agency,
            'form' => $form->createView(),
            'asset' => $asset,
        ];
    }

    /**
     * Delete
     *
     * @param Request $request Request
     * @param Agency  $agency  Agency
     * @param Asset   $asset   Asset
     *
     * @return array
     * @Route("/{asset}/supprimer", requirements={"asset":"\d+"})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @ParamConverter("asset", class="EenovDefaultBundle:Asset")
     * @Method("GET|POST")
     * @Template()
     */
    public function deleteAction(Request $request, Agency $agency, Asset $asset)
    {
        $form = $this->createForm('form');
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->remove($asset);
            $em->flush();

            return $this->redirectToRoute('eenov_admin_adminasset_index', [
                'agency' => $agency->getId(),
            ]);
        }

        return [
            'agency' => $agency,
            'form' => $form->createView(),
            'asset' => $asset,
        ];
    }
}
