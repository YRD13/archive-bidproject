<?php

namespace Eenov\AdminBundle\Controller;

use Eenov\DefaultBundle\Entity\Agency;
use Eenov\DefaultBundle\Entity\Slot;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminSlotController
 *
*
 * @Route("/agences/{agency}/slots", requirements={"agency":"\d+"})
 */
class AdminSlotController extends Controller
{
    /**
     * Index
     *
     * @param Agency $agency
     *
     * @return array
     * @Route("/{page}", requirements={"page":"\d+"}, defaults={"page":1})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Agency $agency)
    {
        $paginatorHelper = $this->get('eb_paginator_helper');
        $paginator = $this->get('doctrine.orm.default_entity_manager')->getRepository(Slot::class)->getPaginator($paginatorHelper, ['agency' => $agency]);

        return [
            'agency' => $agency,
            'paginator' => $paginator,
        ];
    }

    /**
     * Create
     *
     * @param Request $request
     * @param Agency  $agency Agency
     *
     * @return array
     * @Route("/creer")
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @Method("GET|POST")
     * @Template()
     */
    public function createAction(Request $request, Agency $agency)
    {
        $slot = new Slot();
        $slot->setAgency($agency);
        $form = $this->createForm('eenov_admin_slot', $slot);
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->persist($slot);
            $em->flush();

            return $this->redirectToRoute('eenov_admin_adminslot_index', [
                'agency' => $agency->getId(),
            ]);
        }

        return [
            'agency' => $agency,
            'form' => $form->createView(),
        ];
    }

    /**
     * Update
     *
     * @param Request $request Request
     * @param Agency  $agency  Agency
     * @param Slot    $slot    Slot
     *
     * @return array
     * @Route("/{slot}/modifier", requirements={"slot":"\d+"})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @ParamConverter("slot", class="EenovDefaultBundle:Slot")
     * @Method("GET|POST")
     * @Template()
     */
    public function updateAction(Request $request, Agency $agency, Slot $slot)
    {
        $form = $this->createForm('eenov_admin_slot', $slot);
        if ($form->handleRequest($request)->isValid()) {
            $this->get('doctrine.orm.default_entity_manager')->flush();

            return $this->redirectToRoute('eenov_admin_adminslot_index', [
                'agency' => $agency->getId(),
            ]);
        }

        return [
            'agency' => $agency,
            'form' => $form->createView(),
            'slot' => $slot,
        ];
    }

    /**
     * Delete
     *
     * @param Request $request Request
     * @param Agency  $agency  Agency
     * @param Slot    $slot    Slot
     *
     * @return array
     * @Route("/{slot}/supprimer", requirements={"slot":"\d+"})
     * @ParamConverter("slot", class="EenovDefaultBundle:Slot")
     * @Method("GET|POST")
     * @Template()
     */
    public function deleteAction(Request $request, Agency $agency, Slot $slot)
    {
        $form = $this->createForm('form');
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->remove($slot);
            $em->flush();

            return $this->redirectToRoute('eenov_admin_adminslot_index', [
                'agency' => $agency->getId(),
            ]);
        }

        return [
            'agency' => $agency,
            'form' => $form->createView(),
            'slot' => $slot,
        ];
    }
}
