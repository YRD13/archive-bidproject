<?php

namespace Eenov\AdminBundle\Controller;

use Doctrine\ORM\EntityRepository;
use Eenov\DefaultBundle\Entity\Advert;
use Eenov\DefaultBundle\Entity\AdvertType;
use Eenov\DefaultBundle\Entity\Profile;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\NotNull;

/**
 * Class AdminAdvertTypeController
 *
*
 * @Route("/annonces-types")
 */
class AdminAdvertTypeController extends Controller
{
    /**
     * Index
     *
     * @return array
     * @Route("/{page}", requirements={"page":"\d+"}, defaults={"page":1})
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $paginatorHelper = $this->get('eb_paginator_helper');
        $paginator = $this->get('doctrine.orm.default_entity_manager')->getRepository(AdvertType::class)->getPaginator($paginatorHelper);

        return [
            'paginator' => $paginator,
        ];
    }

    /**
     * Create
     *
     * @param Request $request
     *
     * @return array
     * @Route("/creer")
     * @Method("GET|POST")
     * @Template()
     */
    public function createAction(Request $request)
    {
        $form = $this->createForm('eenov_admin_advert_type', $type = new AdvertType());
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->persist($type);
            $em->flush();

            return $this->redirectToRoute('eenov_admin_adminadverttype_index');
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * Update
     *
     * @param Request    $request Request
     * @param AdvertType $type    Advert type
     *
     * @return array
     * @Route("/{type}/modifier", requirements={"type":"\d+"})
     * @ParamConverter("type", class="EenovDefaultBundle:AdvertType", options={"id":"type"})
     * @Method("GET|POST")
     * @Template()
     */
    public function updateAction(Request $request, AdvertType $type)
    {
        $form = $this->createForm('eenov_admin_advert_type', $type);
        if ($form->handleRequest($request)->isValid()) {
            $this->get('doctrine.orm.default_entity_manager')->flush();

            return $this->redirectToRoute('eenov_admin_adminadverttype_index');
        }

        return [
            'form' => $form->createView(),
            'type' => $type,
        ];
    }

    /**
     * Delete
     *
     * @param Request    $request Request
     * @param AdvertType $type    Advert type
     *
     * @return array
     * @Route("/{type}/supprimer", requirements={"type":"\d+"})
     * @ParamConverter("type", class="EenovDefaultBundle:AdvertType", options={"id":"type"})
     * @Method("GET|POST")
     * @Template()
     */
    public function deleteAction(Request $request, AdvertType $type)
    {
        // Create a form to replace the type by another one
        $fb = $this->get('form.factory')->createNamedBuilder('admin_advert_type_delete');
        $fb->add('replacement', 'entity', [
            'label' => 'Remplacement (pour les annonces et les profils qui utilisent ce type)',
            'class' => AdvertType::class,
            'query_builder' => function (EntityRepository $er) use ($type) {
                $qb = $er->createQueryBuilder('a');
                $qb
                    ->andWhere($qb->expr()->neq('a', ':type'))
                    ->setParameter('type', $type);

                return $qb;
            },
            'constraints' => [
                new NotNull(),
            ],
        ]);
        $form = $fb->getForm();
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            if (null !== $replacement = $form->get('replacement')) {
                array_map(function (Advert $advert) use ($replacement) {
                    $advert->setType($replacement->getData());
                }, $em->getRepository(Advert::class)->findBy(['type' => $type]));
                array_map(function (Profile $profile) use ($replacement) {
                    $profile->setAdvertType($replacement->getData());
                }, $em->getRepository(Profile::class)->findBy(['advertType' => $type]));
            }
            $em->flush();
            $em->remove($type);
            $em->flush();

            return $this->redirectToRoute('eenov_admin_adminadverttype_index');
        }

        return [
            'form' => $form->createView(),
            'type' => $type,
        ];
    }
}
