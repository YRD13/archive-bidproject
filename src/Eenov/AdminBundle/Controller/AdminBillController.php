<?php

namespace Eenov\AdminBundle\Controller;

use Eenov\DefaultBundle\Entity\Agency;
use Eenov\DefaultBundle\Entity\Bill;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class AdminBillController
 *
*
 * @Route("/agences/{agency}/factures", requirements={"agency":"\d+"})
 */
class AdminBillController extends Controller
{
    /**
     * Create
     *
     * @param Request $request Request
     * @param Agency  $agency  Agency
     *
     * @return array
     * @Route("/creer")
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @Method("GET|POST")
     * @Template()
     */
    public function createAction(Request $request, Agency $agency)
    {
        $bill = new Bill();
        $bill->setAgency($agency);
        $form = $this->createForm('eenov_admin_bill', $bill);
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->persist($bill);
            $em->flush();

            return $this->redirectToRoute('eenov_admin_adminbill_index', [
                'agency' => $agency->getId(),
            ]);
        }

        return [
            'agency' => $agency,
            'form' => $form->createView(),
        ];
    }

    /**
     * Delete
     *
     * @param Request $request Request
     * @param Agency  $agency  Agency
     * @param Bill    $bill    Bill
     *
     * @return array
     * @Route("/{bill}/supprimer", requirements={"bill":"\d+"})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @ParamConverter("bill", class="EenovDefaultBundle:Bill")
     * @Method("GET|POST")
     * @Template()
     */
    public function deleteAction(Request $request, Agency $agency, Bill $bill)
    {
        $form = $this->createForm('form');
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->remove($bill);
            $em->flush();

            return $this->redirectToRoute('eenov_admin_adminbill_index', [
                'agency' => $agency->getId(),
            ]);
        }

        return [
            'agency' => $agency,
            'form' => $form->createView(),
            'bill' => $bill,
        ];
    }

    /**
     * Index
     *
     * @param Agency $agency
     *
     * @return array
     * @Route("/{page}", requirements={"page":"\d+"}, defaults={"page":1})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Agency $agency)
    {
        $paginatorHelper = $this->get('eb_paginator_helper');
        $paginator = $this->get('doctrine.orm.default_entity_manager')->getRepository(Bill::class)->getPaginator($paginatorHelper, ['agency' => $agency]);

        return [
            'agency' => $agency,
            'paginator' => $paginator,
        ];
    }

    /**
     * File
     *
     * @param Bill $bill
     *
     * @return BinaryFileResponse
     * @Route("/consulter/{bill}.{_format}", requirements={"bill":"\d+"})
     * @ParamConverter("bill", class="EenovDefaultBundle:Bill")
     * @Method("GET")
     */
    public function fileAction(Bill $bill)
    {
        if (false === is_file($bill->getPath())) {
            throw new NotFoundHttpException();
        }

        return new BinaryFileResponse($bill->getPath(), 200, [
            'Content-Type' => 'application/force-download',
            'Cache-Control' => 'no-cache, private',
            'Content-Disposition' => sprintf('attachment; filename="%s"', $bill->getFilename()),
            'Content-Transfer-Encoding' => 'binary',
            'Content-Length' => $bill->getSize(),
        ]);
    }
}
