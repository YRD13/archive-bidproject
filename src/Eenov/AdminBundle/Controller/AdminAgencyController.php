<?php

namespace Eenov\AdminBundle\Controller;

use Eenov\DefaultBundle\Entity\Agency;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminAgencyController
 *
*
 * @Route("/agences")
 */
class AdminAgencyController extends Controller
{
    /**
     * Index
     *
     * @param Request $request
     *
     * @return array
     * @Route("/{page}", requirements={"page":"\d+"}, defaults={"page":1})
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $filter = $this->get('form.factory')->createNamed(null, 'eenov_admin_agency_filter', [], ['method' => 'GET', 'csrf_protection' => false])->handleRequest($request);
        $filterData = ($filter->isSubmitted() && $filter->isValid()) ? $filter->getData() : [];

        $paginatorHelper = $this->get('eb_paginator_helper');
        $paginator = $this->get('doctrine.orm.default_entity_manager')->getRepository(Agency::class)->getPaginator($paginatorHelper, $filterData);

        return [
            'filter' => $filter->createView(),
            'paginator' => $paginator,
        ];
    }

    /**
     * Create
     *
     * @param Request $request
     *
     * @return array
     * @Route("/creer")
     * @Method("GET|POST")
     * @Template()
     */
    public function createAction(Request $request)
    {
        $form = $this->createForm('eenov_admin_agency', $agency = new Agency());
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->persist($agency);
            $em->flush();

            return $this->redirectToRoute('eenov_admin_adminagency_index');
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * Update
     *
     * @param Request $request Request
     * @param Agency  $agency  Agency
     *
     * @return array
     * @Route("/{agency}/modifier", requirements={"agency":"\d+"})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @Method("GET|POST")
     * @Template()
     */
    public function updateAction(Request $request, Agency $agency)
    {
        $form = $this->createForm('eenov_admin_agency', $agency);
        if ($form->handleRequest($request)->isValid()) {
            $this->get('doctrine.orm.default_entity_manager')->flush();

            return $this->redirectToRoute('eenov_admin_adminagency_update', [
                'agency' => $agency->getId(),
            ]);
        }

        return [
            'form' => $form->createView(),
            'agency' => $agency,
        ];
    }

    /**
     * Delete
     *
     * @param Request $request Request
     * @param Agency  $agency  Agency
     *
     * @return array
     * @Route("/{agency}/supprimer", requirements={"agency":"\d+"})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @Method("GET|POST")
     * @Template()
     */
    public function deleteAction(Request $request, Agency $agency)
    {
        $form = $this->createForm('form');
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->remove($agency);
            $em->flush();

            return $this->redirectToRoute('eenov_admin_adminagency_index');
        }

        return [
            'form' => $form->createView(),
            'agency' => $agency,
        ];
    }
}
