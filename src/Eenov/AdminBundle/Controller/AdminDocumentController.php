<?php

namespace Eenov\AdminBundle\Controller;

use Eenov\DefaultBundle\Entity\Bid;
use Eenov\DefaultBundle\Entity\Document;
use Eenov\DefaultBundle\Entity\DocumentType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminDocumentController
 *
*
 * @Route("/annonces/{bid}/documents", requirements={"bid":"\d+"})
 */
class AdminDocumentController extends Controller
{
    /**
     * Index
     *
     * @param Bid $bid Bid
     *
     * @return array
     * @Route()
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Bid $bid)
    {
        return [
            'bid' => $bid,
            'documents' => array_map(function (Document $document) use ($bid) {
                return $this->documentToArray($bid, $document);
            }, $bid->getDraft()->getDocuments()->toArray()),
            'types' => $this->get('doctrine.orm.default_entity_manager')->getRepository(DocumentType::class)->getAllTypesOrdered(),
            'document_upload' => $this->generateUrl('eenov_admin_admindocument_upload', ['bid' => $bid->getId()]),
        ];
    }

    /**
     * Delete
     *
     * @param Bid      $bid      Bid
     * @param Document $document Document
     *
     * @return JsonResponse
     * @Route("/{document}/supprimer", requirements={"document":"\d+"})
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @ParamConverter("document", class="EenovDefaultBundle:Document", options={"id":"document"})
     */
    public function deleteAction(Bid $bid, Document $document)
    {
        $em = $this->get('doctrine.orm.default_entity_manager');
        $em->remove($document);
        $em->flush();

        return $this->redirectToRoute('eenov_admin_admindocument_index', [
            'bid' => $bid->getId(),
        ]);
    }

    /**
     * Upload
     *
     * @param Request $request Request
     * @param Bid     $bid     Bid
     *
     * @return JsonResponse
     * @Route("/televerser")
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     */
    public function uploadAction(Request $request, Bid $bid)
    {
        $data = [];
        $em = $this->get('doctrine.orm.default_entity_manager');
        if (null !== $type = $em->getRepository(DocumentType::class)->find($request->request->getInt('type'))) {
            foreach ($request->files as $file) {
                $document = new Document();
                $document
                    ->setAdvert($bid->getDraft())
                    ->setFile($file)
                    ->setType($type);

                if (0 === $this->get('validator')->validate($document)->count()) {
                    $em->persist($document);
                    $em->flush();

                    $data[] = $this->documentToArray($bid, $document);
                }
            }
        }

        return new JsonResponse(['files' => $data, 'errors' => []]);
    }

    /**
     * documentToArray
     *
     * @param Bid      $bid      Bid
     * @param Document $document Document
     *
     * @return array
     */
    private function documentToArray(Bid $bid, Document $document)
    {
        return [
            'id' => $document->getId(),
            'type' => (string)$document->getType(),
            'path' => $this->generateUrl('eenov_default_document_download', [
                'document' => $document->getId(),
                '_format' => $document->getExtension(),
            ]),
            'delete_uri' => $this->generateUrl('eenov_admin_admindocument_delete', [
                'bid' => $bid->getId(),
                'document' => $document->getId(),
            ]),
            'isThumbnail' => false,
        ];
    }
}
