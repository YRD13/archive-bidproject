<?php

namespace Eenov\AdminBundle\Controller;

use Eenov\DefaultBundle\Entity\Bid;
use Eenov\DefaultBundle\Entity\Auction;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class AdminAuctionController
 *
*
 * @Route("/annonces/{bid}/encheres", requirements={"bid":"\d+"})
 */
class AdminAuctionController extends Controller
{
    /**
     * Index
     *
     * @param Bid $bid Bid
     *
     * @return array
     * @Route("/{page}", requirements={"page":"\d+"}, defaults={"page":1})
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Bid $bid)
    {
        $paginatorHelper = $this->get('eb_paginator_helper');
        $paginator = $this->get('doctrine.orm.default_entity_manager')->getRepository(Auction::class)->getPaginator($paginatorHelper, ['bid' => $bid]);

        return [
            'bid' => $bid,
            'paginator' => $paginator,
        ];
    }
}
