<?php

namespace Eenov\AdminBundle\Controller;

use Eenov\DefaultBundle\Entity\Around;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminAroundController
 *
*
 * @Route("/alentours")
 */
class AdminAroundController extends Controller
{
    /**
     * Index
     *
     * @return array
     * @Route("/{page}", requirements={"page":"\d+"}, defaults={"page":1})
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $paginatorHelper = $this->get('eb_paginator_helper');
        $paginator = $this->get('doctrine.orm.default_entity_manager')->getRepository(Around::class)->getPaginator($paginatorHelper);

        return [
            'paginator' => $paginator,
        ];
    }

    /**
     * Create
     *
     * @param Request $request
     *
     * @return array
     * @Route("/creer")
     * @Method("GET|POST")
     * @Template()
     */
    public function createAction(Request $request)
    {
        $form = $this->createForm('eenov_admin_around', $around = new Around());
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->persist($around);
            $em->flush();

            return $this->redirectToRoute('eenov_admin_adminaround_index');
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * Update
     *
     * @param Request $request Request
     * @param Around  $around  Around
     *
     * @return array
     * @Route("/{around}/modifier", requirements={"around":"\d+"})
     * @ParamConverter("around", class="EenovDefaultBundle:Around")
     * @Method("GET|POST")
     * @Template()
     */
    public function updateAction(Request $request, Around $around)
    {
        $form = $this->createForm('eenov_admin_around', $around);
        if ($form->handleRequest($request)->isValid()) {
            $this->get('doctrine.orm.default_entity_manager')->flush();

            return $this->redirectToRoute('eenov_admin_adminaround_index');
        }

        return [
            'form' => $form->createView(),
            'around' => $around,
        ];
    }

    /**
     * Delete
     *
     * @param Request $request Request
     * @param Around  $around  Around
     *
     * @return array
     * @Route("/{around}/supprimer", requirements={"around":"\d+"})
     * @ParamConverter("around", class="EenovDefaultBundle:Around")
     * @Method("GET|POST")
     * @Template()
     */
    public function deleteAction(Request $request, Around $around)
    {
        $form = $this->createForm('form');
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->remove($around);
            $em->flush();

            return $this->redirectToRoute('eenov_admin_adminaround_index');
        }

        return [
            'form' => $form->createView(),
            'around' => $around,
        ];
    }
}
