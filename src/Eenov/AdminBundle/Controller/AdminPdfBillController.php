<?php

namespace Eenov\AdminBundle\Controller;

use Eenov\DefaultBundle\EenovDefaultBundle;
use Eenov\DefaultBundle\Entity\Bill;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class AdminPdfBillController
 *
*
 * @Route("/factures")
 */
class AdminPdfBillController extends Controller
{
    /**
     * Index
     *
     * @param Request $request
     *
     * @return array
     * @Route("/{page}", requirements={"page":"\d+"}, defaults={"page":1})
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $now = new \DateTime();

        $filter = $this->get('form.factory')->createNamed(null, 'eenov_admin_pdf_bill_filter', [
            'started' => EenovDefaultBundle::getFirstMonthDay($now),
            'ended' => EenovDefaultBundle::getLastMonthDay($now),
        ], [
            'csrf_protection' => false,
            'method' => 'GET',
        ]);
        $filter->handleRequest($request);
        $filterData = [];
        if ($filter->isSubmitted() && $filter->isValid()) {
            $filterData = $filter->getData();
        }

        $paginatorHelper = $this->get('eb_paginator_helper');
        $paginator = $this->get('doctrine.orm.default_entity_manager')->getRepository(Bill::class)->getPaginator($paginatorHelper, $filterData);

        if ($filter instanceof Form) {
            if (null !== $button = $filter->getClickedButton()) {
                if ('export' === $button->getName()) {
                    $this->get('session')->getFlashBag()->add('warning', 'En cours de dev\' ...');

                    return $this->redirectToRoute('eenov_admin_adminpdfbill_index');
                }
            }
        }

        return [
            'filter' => $filter->createView(),
            'paginator' => $paginator,
        ];
    }

    /**
     * File
     *
     * @param Bill $bill
     *
     * @return BinaryFileResponse
     * @Route("/consulter/{bill}.{_format}", requirements={"bill":"\d+"})
     * @ParamConverter("bill", class="EenovDefaultBundle:Bill")
     * @Method("GET")
     */
    public function fileAction(Bill $bill)
    {
        if (false === is_file($bill->getPath())) {
            throw new NotFoundHttpException();
        }

        return new BinaryFileResponse($bill->getPath(), 200, [
            'Content-Type' => 'application/force-download',
            'Cache-Control' => 'no-cache, private',
            'Content-Disposition' => sprintf('attachment; filename="%s"', $bill->getFilename()),
            'Content-Transfer-Encoding' => 'binary',
            'Content-Length' => $bill->getSize(),
        ]);
    }
}
