<?php

namespace Eenov\AdminBundle\Form;

use Eenov\DefaultBundle\Entity\User;
use Eenov\DefaultBundle\Form\FormNameTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class UserType
 *
*
 */
class UserType extends AbstractType
{
    use FormNameTrait;

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'eenov_default_user';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('profile', 'eenov_user_profile');
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
