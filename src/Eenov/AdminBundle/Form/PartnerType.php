<?php

namespace Eenov\AdminBundle\Form;

use Eenov\DefaultBundle\Entity\Partner;
use Eenov\DefaultBundle\Form\FormNameTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PartnerType
 *
*
 */
class PartnerType extends AbstractType
{
    use FormNameTrait;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', [
                'label' => 'Nom',
            ])
            ->add('order', 'integer', [
                'label' => 'Priorité (de 0 min. à 100 max)',
            ])
            ->add('file', 'file', [
                'label' => 'Logo (image carré sur fond blanc ou transparent)',
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Partner::class,
        ]);
    }
}
