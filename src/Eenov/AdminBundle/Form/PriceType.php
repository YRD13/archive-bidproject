<?php

namespace Eenov\AdminBundle\Form;

use Eenov\DefaultBundle\Entity\Price;
use Eenov\DefaultBundle\Form\FormNameTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PriceType
 *
*
 */
class PriceType extends AbstractType
{
    use FormNameTrait;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', [
                'label' => 'Nom',
            ])
            ->add('duration', 'integer', [
                'label' => 'Durée (en mois)',
            ])
            ->add('amount', 'number', [
                'label' => 'Prix',
            ])
            ->add('perSlot', 'checkbox', [
                'label' => 'Prix par slot ? (sinon prix par mois)',
                'required' => false,
            ])
            ->add('count', 'integer', [
                'label' => 'Nombre de slot (par mois ou vide pour illimité)',
                'required' => false,
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Price::class,
        ]);
    }
}
