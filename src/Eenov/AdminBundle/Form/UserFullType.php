<?php

namespace Eenov\AdminBundle\Form;

use Eenov\DefaultBundle\Entity\User;
use Eenov\DefaultBundle\Form\FormNameTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;

/**
 * Class UserFullType
 *
*
 */
class UserFullType extends AbstractType
{
    use FormNameTrait;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if (null === $options['roles']) {
            $choices = User::getRoleNameList();
        } else {
            $choices = array_combine($options['roles'], array_map(function ($role) {
                return User::getRoleNameList()[$role];
            }, $options['roles']));
        }

        if (1 === count($choices)) {
            $roleKeys = array_keys($choices);

            $builder
                ->add('role', 'hidden', [
                    'data' => array_shift($roleKeys),
                    'read_only' => true,
                ]);
        } else {
            $builder
                ->add('role', 'choice', [
                    'label' => 'Rôle',
                    'choices' => User::getRoleNameList(),
                ]);
        }

        $builder
            ->add('username', 'email', [
                'label' => 'Email',
                'placeholder' => 'Email',
            ])
            ->add('firstname', 'text', [
                'label' => 'Prénom',
                'placeholder' => 'Prénom',
            ])
            ->add('lastname', 'text', [
                'label' => 'Nom',
                'placeholder' => 'Nom',
            ])
            ->add('phone', 'text', [
                'label' => 'Téléphone fixe',
                'placeholder' => 'Téléphone fixe',
                'mapped' => false,
            ])
            ->add('cellphone', 'text', [
                'label' => 'Téléphone mobile',
                'placeholder' => 'Téléphone mobile',
                'mapped' => false,
            ])
            ->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
                $user = $event->getData();
                if (null === $user || ($user instanceof User && null === $user->getId())) {
                    $event
                        ->getForm()
                        ->add('rawPassword', 'repeated', [
                            'type' => 'password',
                            'first_options' => [
                                'label' => 'Mot de passe',
                                'placeholder' => 'Mot de passe',
                            ],
                            'second_options' => [
                                'label' => 'Mot de passe (validation)',
                                'placeholder' => 'Mot de passe (validation)',
                            ],
                        ])
                        ->add('cgu', 'checkbox', [
                            'label' => 'J\'accepte les CGU',
                            'mapped' => false,
                            'data' => true,
                            'constraints' => [
                                new IsTrue([
                                    'message' => 'Vous devez accepter les conditions générales de vente ',
                                ]),
                            ],
                        ]);
                }

                if (null !== $user) {
                    if (null !== $profile = $user->getProfile()) {
                        $event->getForm()->get('phone')->setData($profile->getPhone());
                        $event->getForm()->get('cellphone')->setData($profile->getCellphone());
                    }
                }
            })
            ->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
                $user = $event->getData();
                if ($user instanceof User) {
                    if (null !== $phone = $event->getForm()->get('phone')->getData()) {
                        $user->getProfile()->setPhone($phone);
                    }
                    if (null !== $cellphone = $event->getForm()->get('cellphone')->getData()) {
                        $user->getProfile()->setCellphone($cellphone);
                    }
                }
            });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'roles' => null,
        ]);
    }
}
