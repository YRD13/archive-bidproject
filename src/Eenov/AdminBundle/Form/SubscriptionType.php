<?php

namespace Eenov\AdminBundle\Form;

use Eenov\DefaultBundle\Entity\Price;
use Eenov\DefaultBundle\Entity\Subscription;
use Eenov\DefaultBundle\Form\FormNameTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SubscriptionType
 *
*
 */
class SubscriptionType extends AbstractType
{
    use FormNameTrait;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('price', 'entity', [
                'label' => 'Tarif',
                'class' => Price::class,
                'mapped' => false,
            ])
            ->add('started', 'datepicker', [
                'label' => 'Date de début',
            ])
            ->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
                $subscription = $event->getData();
                if ($subscription instanceof Subscription) {
                    if (null !== $price = $event->getForm()->get('price')) {
                        if (null !== $price->getData()) {
                            $subscription->populate($price->getData());
                        }
                    }
                }
            });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Subscription::class,
        ]);
    }
}
