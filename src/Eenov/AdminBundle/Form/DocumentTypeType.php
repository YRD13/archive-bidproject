<?php

namespace Eenov\AdminBundle\Form;

use Eenov\DefaultBundle\Entity\DocumentType;
use Eenov\DefaultBundle\Form\FormNameTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DocumentTypeType
 *
*
 */
class DocumentTypeType extends AbstractType
{
    use FormNameTrait;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('order', 'integer', [
                'label' => 'Ordre',
            ])
            ->add('title', 'text', [
                'label' => 'Titre',
            ])
            ->add('subtitle', 'text', [
                'label' => 'Sous-titre',
                'required' => false,
            ])
            ->add('name', 'text', [
                'label' => 'Nom',
            ])
            ->add('isPublic', 'checkbox', [
                'label' => 'Public',
                'required' => false,
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DocumentType::class,
        ]);
    }
}
