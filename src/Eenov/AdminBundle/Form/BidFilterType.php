<?php

namespace Eenov\AdminBundle\Form;

use Eenov\DefaultBundle\Form\FormNameTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class BidFilterType
 *
*
 */
class BidFilterType extends AbstractType
{
    use FormNameTrait;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('seller', 'text', [
                'label' => 'Vendeur',
                'required' => false,
            ])
            ->add('location', 'text', [
                'label' => 'Lieu',
                'required' => false,
            ])
            ->add('title', 'text', [
                'label' => 'Annonce',
                'required' => false,
            ])
            ->add('agency', 'text', [
                'label' => 'Agence',
                'required' => false,
            ]);
    }
}
