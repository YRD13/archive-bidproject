<?php

namespace Eenov\AdminBundle\Form;

use Eenov\DefaultBundle\Entity\Legal;
use Eenov\DefaultBundle\Form\FormNameTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class LegalType
 *
*
 */
class LegalType extends AbstractType
{
    use FormNameTrait;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', 'choice', [
                'label' => 'Type',
                'choices' => Legal::getTypeNameList(),
            ])
            ->add('file', 'file', [
                'label' => 'Fichier',
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Legal::class,
        ]);
    }
}
