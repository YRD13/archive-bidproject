<?php

namespace Eenov\AdminBundle\Form;

use Eenov\DefaultBundle\Entity\User;
use Eenov\DefaultBundle\Form\FormNameTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class UserFilterType
 *
*
 */
class UserFilterType extends AbstractType
{
    use FormNameTrait;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', 'text', [
                'label' => 'Email',
                'required' => false,
            ])
            ->add('firstname', 'text', [
                'label' => 'Prénom',
                'required' => false,
            ])
            ->add('lastname', 'text', [
                'label' => 'Nom',
                'required' => false,
            ])
            ->add('role', 'choice', [
                'label' => 'Type',
                'required' => false,
                'choices' => User::getRoleNameList(),
            ]);
    }
}
