<?php

namespace Eenov\AdminBundle\Form;

use Eenov\DefaultBundle\Entity\ImageType;
use Eenov\DefaultBundle\Form\FormNameTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ImageTypeType
 *
*
 */
class ImageTypeType extends AbstractType
{
    use FormNameTrait;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', [
                'label' => 'Nom',
            ])
            ->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
                $type = $event->getData();
                if ($type instanceof ImageType) {
                    $event->getForm()->add('file', 'file', [
                        'label' => 'Image',
                        'required' => null === $type->getId(),
                    ]);
                }
            });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ImageType::class,
        ]);
    }
}
