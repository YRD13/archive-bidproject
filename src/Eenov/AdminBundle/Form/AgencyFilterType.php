<?php

namespace Eenov\AdminBundle\Form;

use Eenov\DefaultBundle\Form\FormNameTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class AgencyFilterType
 *
*
 */
class AgencyFilterType extends AbstractType
{
    use FormNameTrait;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', [
                'label' => 'Nom',
                'required' => false,
            ])
            ->add('displayName', 'text', [
                'label' => 'Nom commercial',
                'required' => false,
            ])
            ->add('phone', 'text', [
                'label' => 'Téléphone de l\'agence',
                'required' => false,
            ])
            ->add('owner', 'text', [
                'label' => 'Propriétaire',
                'required' => false,
            ])
            ->add('responsible', 'text', [
                'label' => 'Responsable',
                'required' => false,
            ])
            ->add('contact', 'text', [
                'label' => 'Contact',
                'required' => false,
            ])
            ->add('email', 'email', [
                'label' => 'Email',
                'required' => false,
            ]);
    }
}
