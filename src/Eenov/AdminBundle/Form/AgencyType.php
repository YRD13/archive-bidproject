<?php

namespace Eenov\AdminBundle\Form;

use Doctrine\ORM\EntityManager;
use Eenov\DefaultBundle\Entity\Agency;
use Eenov\DefaultBundle\Entity\User;
use Eenov\DefaultBundle\Form\FormNameTrait;
use Eenov\DefaultBundle\Repository\UserRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AgencyType
 *
*
 */
class AgencyType extends AbstractType
{
    use FormNameTrait;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('mother', 'entity', [
                'label' => 'Agence mère',
                'class' => Agency::class,
                'required' => false,
            ])
            ->add('name', 'text', [
                'label' => 'Nom',
            ])
            ->add('displayName', 'text', [
                'label' => 'Nom commercial (affiché sur le site)',
                'required' => false,
            ])
            ->add('phone', 'text', [
                'label' => 'Téléphone de l\'agence',
            ])
            ->add('email', 'email', [
                'label' => 'Email',
            ])
            ->add('address', 'text', [
                'label' => 'Adresse',
            ])
            ->add('zip', 'text', [
                'label' => 'Code postal',
            ])
            ->add('city', 'text', [
                'label' => 'Ville',
            ])
            ->add('country', 'text', [
                'label' => 'Pays',
            ])
            ->add('type', 'choice', [
                'label' => 'Raison sociale',
                'choices' => Agency::getTypeNameList(),
                'required' => false,
            ])
            ->add('siret', 'integer', [
                'label' => 'SIRET',
                'required' => false,
            ])
            ->add('rcs', 'text', [
                'label' => 'Ville du RCS',
                'required' => false,
            ])
            ->add('cardNumber', 'text', [
                'label' => 'Numéro carte professionnelle',
                'required' => false,
            ])
            ->add('website', 'text', [
                'label' => 'Site web',
                'required' => false,
            ])
            ->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
                $agency = $event->getData();
                if ($agency instanceof Agency) {
                    $event
                        ->getForm()
                        ->add('owner', 'entity', [
                            'label' => 'Propriétaire',
                            'class' => User::class,
                            'query_builder' => function (UserRepository $er) use ($agency) {
                                $qb = $er->createQueryBuilder('a');

                                return $qb
                                    ->andWhere($qb->expr()->eq('a.role', ':role'))
                                    ->setParameter('role', User::ROLE_AGENCY_OWNER)
                                    ->orderBy('a.lastname', 'ASC')
                                    ->orderBy('a.firstname', 'ASC');
                            },
                            'property' => 'entirename',
                        ])
                        ->add('responsible', 'entity', [
                            'label' => 'Responsable',
                            'class' => User::class,
                            'query_builder' => function (UserRepository $er) use ($agency) {
                                $qb = $er->createQueryBuilder('a');

                                return $qb
                                    ->leftJoin('a.agencyManaged', 'b')
                                    ->andWhere($qb->expr()->orX(
                                        $qb->expr()->isNull('b.id'),
                                        $qb->expr()->eq('b.id', ':agency_id')
                                    ))
                                    ->setParameter('agency_id', $agency->getId())
                                    ->andWhere($qb->expr()->eq('a.role', ':role'))
                                    ->setParameter('role', User::ROLE_AGENCY_RESPONSIBLE)
                                    ->orderBy('a.lastname', 'ASC')
                                    ->orderBy('a.firstname', 'ASC');
                            },
                            'required' => false,
                            'property' => 'entirename',
                        ])
                        ->add('contact', 'entity', [
                            'label' => 'Contact',
                            'class' => User::class,
                            'query_builder' => function (UserRepository $er) use ($agency) {
                                $qb = $er->createQueryBuilder('a');

                                return $qb
                                    ->leftJoin('a.agencyRepresented', 'b')
                                    ->andWhere($qb->expr()->orX(
                                        $qb->expr()->isNull('b.id'),
                                        $qb->expr()->eq('b.id', ':agency_id')
                                    ))
                                    ->setParameter('agency_id', $agency->getId())
                                    ->andWhere($qb->expr()->eq('a.role', ':role'))
                                    ->setParameter('role', User::ROLE_AGENCY_CONTACT)
                                    ->orderBy('a.lastname', 'ASC')
                                    ->orderBy('a.firstname', 'ASC');
                            },
                            'required' => false,
                            'property' => 'entirename',
                        ])
                        ->add('commercial', 'entity', [
                            'label' => 'Commercial attaché',
                            'class' => User::class,
                            'required' => false,
                            'query_builder' => function (UserRepository $er) use ($agency) {
                                $qb = $er->createQueryBuilder('a');

                                return $qb
                                    ->andWhere($qb->expr()->eq('a.role', ':role'))
                                    ->setParameter('role', User::ROLE_COMMERCIAL)
                                    ->orderBy('a.lastname', 'ASC')
                                    ->orderBy('a.firstname', 'ASC');
                            },
                            'property' => 'entirename',
                        ])
                        ->add('file', 'file', [
                            'label' => 'Logo',
                            'required' => null === $agency->getId(),
                        ]);
                }
            });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Agency::class,
            'cascade_validation' => true,
        ]);
    }
}
