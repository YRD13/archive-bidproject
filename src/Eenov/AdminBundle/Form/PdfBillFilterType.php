<?php

namespace Eenov\AdminBundle\Form;

use Eenov\DefaultBundle\Entity\Agency;
use Eenov\DefaultBundle\Form\FormNameTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class PdfBillFilterType
 *
*
 */
class PdfBillFilterType extends AbstractType
{
    use FormNameTrait;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('started', 'datepicker', [
                'label' => 'Date de début',
                'required' => false,
            ])
            ->add('ended', 'datepicker', [
                'label' => 'Date de fin',
                'required' => false,
            ])
            ->add('agency', 'entity', [
                'label' => 'Agence',
                'class' => Agency::class,
                'required' => false,
            ])
            ->add('export', 'submit', [
                'label' => 'Exporter',
            ]);
    }
}
