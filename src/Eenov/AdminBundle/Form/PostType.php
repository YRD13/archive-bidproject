<?php

namespace Eenov\AdminBundle\Form;

use Eenov\DefaultBundle\Entity\Post;
use Eenov\DefaultBundle\Form\FormNameTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PostType
 *
*
 */
class PostType extends AbstractType
{
    use FormNameTrait;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'text', [
                'label' => 'Titre',
            ])
            ->add('text', 'wysiwyg', [
                'label' => 'Contenu',
            ])
            ->add('isPublished', 'checkbox', [
                'label' => 'Publié ?',
                'required' => false,
            ])
            ->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
                $post = $event->getData();
                if ($post instanceof Post) {
                    $event->getForm()->add('file', 'file', [
                        'label' => 'Image',
                        'required' => null === $post->getId(),
                    ]);
                }
            });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Post::class,
        ]);
    }
}
