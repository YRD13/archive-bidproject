<?php

namespace Eenov\AdminBundle\Form;

use Doctrine\ORM\EntityRepository;
use Eenov\DefaultBundle\Entity\Access;
use Eenov\DefaultBundle\Entity\Mandate;
use Eenov\DefaultBundle\Entity\User;
use Eenov\DefaultBundle\Form\FormNameTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AccessType
 *
*
 */
class AccessType extends AbstractType
{
    use FormNameTrait;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user', 'entity', [
                'label' => 'Utilisateur',
                'class' => User::class,
            ])
            ->add('readonly', 'checkbox', [
                'label' => 'Spectateur (pas le droit d\'enchérir)',
                'required' => false,
            ])
            ->add('userComment', 'textarea', [
                'label' => 'Commentaire de l\'utilisateur',
                'required' => false,
            ])
            ->add('agencyComment', 'textarea', [
                'label' => 'Commentaire de l\'agence',
                'required' => false,
            ])
            ->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
                $access = $event->getData();
                if ($access instanceof Access) {
                    $event
                        ->getForm()
                        ->add('mandate', 'entity', [
                            'label' => 'Mandat de participation acquéreur',
                            'class' => Mandate::class,
                            'query_builder' => function (EntityRepository $er) use ($access) {
                                $qb = $er->createQueryBuilder('a');

                                return $qb
                                    ->leftJoin('a.access', 'b')
                                    ->andWhere($qb->expr()->isNull('b.id'))
                                    ->andWhere($qb->expr()->eq('a.bid', ':bid'))
                                    ->setParameter('bid', $access->getBid());
                            },
                            'required' => false,
                        ])
                        ->add('validate', 'choice', [
                            'label' => 'Valider',
                            'choices' => [
                                0 => '-',
                                1 => 'Validé',
                                2 => 'Invalidé',
                            ],
                            'mapped' => false,
                            'data' => $access->hasBeenValidated() ? ($access->isValid() ? 1 : 2) : 0,
                        ]);
                }
            })
            ->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
                $access = $event->getData();
                if ($access instanceof Access) {
                    if ($event->getForm()->has('validate')) {
                        $validate = $event->getForm()->get('validate')->getData();
                        if (1 === $validate && false == $access->hasBeenValidated() && false === $access->isValid()) {
                            $access->validate();
                        } elseif (2 === $validate && false == $access->hasBeenValidated() && true === $access->isValid()) {
                            $access->invalidate();
                        } elseif (0 === $validate) {
                            $access
                                ->setValidated(null)
                                ->setInvalidated(null);
                        }
                    }
                }
            });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Access::class,
        ]);
    }
}
