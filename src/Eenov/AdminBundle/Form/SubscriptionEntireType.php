<?php

namespace Eenov\AdminBundle\Form;

use Eenov\DefaultBundle\Entity\Price;
use Eenov\DefaultBundle\Entity\Subscription;
use Eenov\DefaultBundle\Form\FormNameTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SubscriptionEntireType
 *
*
 */
class SubscriptionEntireType extends AbstractType
{
    use FormNameTrait;

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'eenov_admin_price';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('started', 'datepicker', [
                'label' => 'Date de début',
            ])
            ->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
                $subscription = $event->getData();
                if ($subscription instanceof Subscription) {
                    $price = new Price();
                    $price
                        ->setName($subscription->getName())
                        ->setDuration($subscription->getDuration())
                        ->setAmount($subscription->getAmount())
                        ->setCount($subscription->getCount());

                    $subscription->populate($price);
                }
            });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Subscription::class,
        ]);
    }
}
