<?php

namespace Eenov\AdminBundle\Form;

use Eenov\DefaultBundle\Entity\Visit;
use Eenov\DefaultBundle\Form\FormNameTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class VisitType
 *
*
 */
class VisitType extends AbstractType
{
    use FormNameTrait;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date', 'datepicker', [
                'label' => 'Date',
            ])
            ->add('started', 'hour', [
                'label' => 'Début',
            ])
            ->add('ended', 'hour', [
                'label' => 'Fin',
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Visit::class,
        ]);
    }
}
