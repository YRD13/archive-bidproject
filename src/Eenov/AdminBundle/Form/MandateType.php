<?php

namespace Eenov\AdminBundle\Form;

use Eenov\DefaultBundle\Entity\Mandate;
use Eenov\DefaultBundle\Form\FormNameTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class MandateType
 *
*
 */
class MandateType extends AbstractType
{
    use FormNameTrait;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('buyer', 'text', [
                'label' => 'Acheteur',
            ])
            ->add('buyerEmail', 'email', [
                'label' => 'Email de l\'acheteur',
                'required' => false,
            ])
            ->add('file', 'file', [
                'label' => 'Fichier',
            ])
            ->add('visited', 'datepicker', [
                'label' => 'Date de la visite',
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Mandate::class,
        ]);
    }
}
