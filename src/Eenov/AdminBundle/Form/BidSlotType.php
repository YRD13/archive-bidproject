<?php

namespace Eenov\AdminBundle\Form;

use Doctrine\ORM\EntityRepository;
use Eenov\DefaultBundle\Entity\Bid;
use Eenov\DefaultBundle\Entity\Slot;
use Eenov\DefaultBundle\Form\FormNameTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class BidSlotType
 *
*
 */
class BidSlotType extends AbstractType
{
    use FormNameTrait;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
                $bid = $event->getData();
                if ($bid instanceof Bid) {
                    $event
                        ->getForm()
                        ->add('slot', 'entity', [
                            'label' => 'Slot',
                            'class' => Slot::class,
                            'required' => false,
                            'query_builder' => function (EntityRepository $er) use ($bid) {
                                $qb = $er->createQueryBuilder('a');

                                $qb
                                    ->leftJoin('a.agency', 'agency')
                                    ->andWhere($qb->expr()->eq('agency', ':agency'))
                                    ->setParameter('agency', $bid->getAgency())
                                    ->leftJoin('a.bid', 'b')
                                    ->andWhere($qb->expr()->isNull('b.id'))
                                    ->orWhere($qb->expr()->eq('b.id', ':id'))
                                    ->setParameter('id', $bid->getId());

                                return $qb;
                            },
                        ]);
                }
            });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Bid::class,
            'cascade_validation' => true,
        ]);
    }
}
