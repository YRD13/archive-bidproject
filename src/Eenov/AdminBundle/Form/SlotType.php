<?php

namespace Eenov\AdminBundle\Form;

use Eenov\DefaultBundle\Entity\Slot;
use Eenov\DefaultBundle\Form\FormNameTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SlotType
 *
*
 */
class SlotType extends AbstractType
{
    use FormNameTrait;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('started', 'datepicker', [
                'label' => 'Début'
            ])
            ->add('ended', 'datepicker', [
                'label' => 'Fin'
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Slot::class,
        ]);
    }
}
