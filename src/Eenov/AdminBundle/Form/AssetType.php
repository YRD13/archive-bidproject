<?php

namespace Eenov\AdminBundle\Form;

use Eenov\DefaultBundle\Entity\Asset;
use Eenov\DefaultBundle\Form\FormNameTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AssetType
 *
*
 */
class AssetType extends AbstractType
{
    use FormNameTrait;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date', 'datepicker', [
                'label' => 'Date (le mois est le plus important)',
            ])
            ->add('amount', 'number', [
                'label' => 'Montant (peut être négatif ou positif)',
            ])
            ->add('comment', 'textarea', [
                'label' => 'Commentaire',
                'required' => false
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Asset::class,
        ]);
    }
}
