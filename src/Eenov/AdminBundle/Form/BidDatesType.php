<?php

namespace Eenov\AdminBundle\Form;

use Eenov\DefaultBundle\Entity\Bid;
use Eenov\DefaultBundle\Form\FormNameTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class BidDatesType
 *
*
 */
class BidDatesType extends AbstractType
{
    use FormNameTrait;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('sellStarted', 'datepicker', [
                'label' => 'Début des offres à 12h le',
                'required' => false,
            ])
            ->add('sellEnded', 'datepicker', [
                'label' => 'Fin de l\'enchère à 12h le',
                'required' => false,
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Bid::class,
            'cascade_validation' => true,
        ]);
    }
}
