<?php

namespace Eenov\AdminBundle\Form;

use Eenov\DefaultBundle\Entity\AdvertType;
use Eenov\DefaultBundle\Form\FormNameTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AdvertTypeType
 *
*
 */
class AdvertTypeType extends AbstractType
{
    use FormNameTrait;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', [
                'label' => 'Nom',
            ])
            ->add('isGround', 'checkbox', [
                'label' => 'Configuration d\'un terrain',
                'required' => false,
            ])
            ->add('isBuilding', 'checkbox', [
                'label' => 'Configuration d\'un immeuble',
                'required' => false,
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AdvertType::class,
        ]);
    }
}
