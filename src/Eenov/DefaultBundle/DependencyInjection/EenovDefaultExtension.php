<?php

namespace Eenov\DefaultBundle\DependencyInjection;

use Eenov\DefaultBundle\Form as DefaultForm;
use Eenov\DefaultBundle\PDF\Unite;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Parameter;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class EenovDefaultExtension
 *
*
 */
class EenovDefaultExtension extends AbstractExtension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $forms = [
            DefaultForm\Model\DocType::class => [],
            DefaultForm\AgencyType::class => [new Reference('filesystem')],
            DefaultForm\AdvancedSearchType::class => [new Parameter('unity_surface'), new Parameter('unity_money')],
            DefaultForm\BidContactType::class => [],
            DefaultForm\ContactAgencyType::class => [],
            DefaultForm\ContactBuyerType::class => [],
            DefaultForm\ContactSellerType::class => [],
            DefaultForm\ContactType::class => [],
            DefaultForm\PointType::class => [],
            DefaultForm\SearchSmallType::class => [new Parameter('unity_money')],
            DefaultForm\SearchType::class => [new Parameter('unity_surface'), new Parameter('unity_money')],
            DefaultForm\UserPasswordType::class => [],
            DefaultForm\UserResetType::class => [],
            DefaultForm\UserType::class => [],
        ];
        $this->addForms($container, $forms);

        $container->setParameter('unite_command', Unite::getCommand());

        $loader = new Loader\XmlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('advert.xml');
        $loader->load('calendar.xml');
        $loader->load('constraint.xml');
        $loader->load('event_listener.xml');
        $loader->load('form_extension.xml');
        $loader->load('form_field.xml');
        $loader->load('geo.xml');
        $loader->load('lemon_way.xml');
        $loader->load('mailer.xml');
        $loader->load('pdf.xml');
        $loader->load('request.xml');
        $loader->load('session.xml');
        $loader->load('sms.xml');
        $loader->load('twig.xml');
    }
}
