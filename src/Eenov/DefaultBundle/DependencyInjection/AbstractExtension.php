<?php

namespace Eenov\DefaultBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * Class AbstractExtension
 *
*
 */
abstract class AbstractExtension extends Extension
{
    /**
     * Add forms
     *
     * @param ContainerBuilder $container Container builder
     * @param array            $forms     Form definitions
     */
    protected function addForms(ContainerBuilder $container, array $forms)
    {
        foreach ($forms as $class => $arguments) {
            $id = mb_strcut(str_replace('_bundle_form', '', str_replace('\\', '_', Container::underscore($class))), 0, -5);

            $definition = new Definition($class, $arguments);
            $definition
                ->addTag('form.type', ['alias' => $id])
                ->addMethodCall('setName', [$id]);

            $container->setDefinition($id, $definition);
        }
    }
}
