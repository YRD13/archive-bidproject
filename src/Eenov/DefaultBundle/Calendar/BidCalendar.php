<?php

namespace Eenov\DefaultBundle\Calendar;

use Eenov\DefaultBundle\Entity\Bid;
use Eenov\DefaultBundle\Entity\Visit;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class BidCalendar
 *
*
 */
class BidCalendar
{
    const COLOR_BID = '#3a87ad';
    const COLOR_VISIT = '#C61B67';

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * bidToEvent
     *
     * @param Bid $bid
     *
     * @return array
     */
    public function bidToEvent(Bid $bid)
    {
        return [
            'title' => sprintf(
                '%s - %s',
                mb_strtoupper($bid->getSellerLastname()),
                $bid->getDraft()->getType()
            ),
            'start' => $bid->getSellStarted()->format(\DateTime::ISO8601),
            'end' => $bid->getSellRealEnded()->format(\DateTime::ISO8601),
            'allDay' => false,
            'color' => self::COLOR_BID,
            'url' => $this->router->generate('eenov_agency_agencybid_update', [
                'agency' => $bid->getAgency()->getId(),
                'bid' => $bid->getId(),
            ]),
        ];
    }

    /**
     * visitToEvent
     *
     * @param Visit $visit
     *
     * @return array
     */
    public function visitToEvent(Visit $visit)
    {
        $start = clone $visit->getDate();
        $start->setTime((int)$visit->getStarted()->format('H'), (int)$visit->getStarted()->format('i'), (int)$visit->getStarted()->format('s'));

        $end = clone $visit->getDate();
        $end->setTime((int)$visit->getEnded()->format('H'), (int)$visit->getEnded()->format('i'), (int)$visit->getEnded()->format('s'));

        return [
            'title' => sprintf(
                '%s %s - %s',
                mb_strtoupper($visit->getBid()->getSellerLastname()),
                $visit->getStarted()->format('H:i'),
                $visit->getEnded()->format('H:i')
            ),
            'start' => $start->format(\DateTime::ISO8601),
            'end' => $end->format(\DateTime::ISO8601),
            'allDay' => false,
            'color' => self::COLOR_VISIT,
            'url' => $this->router->generate('eenov_agency_agencybid_update', [
                'agency' => $visit->getBid()->getAgency()->getId(),
                'bid' => $visit->getBid()->getId(),
            ]),
        ];
    }
}
