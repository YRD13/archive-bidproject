<?php

namespace Eenov\DefaultBundle\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Class Overlap
 *
*
 * @Annotation()
 */
class Overlap extends Constraint
{
    /**
     * {@inheritdoc}
     */
    public function validatedBy()
    {
        return 'overlap_validator';
    }

    /**
     * {@inheritdoc}
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
