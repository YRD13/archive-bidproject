<?php

namespace Eenov\DefaultBundle\Constraint;

use Doctrine\ORM\EntityManager;
use Eenov\DefaultBundle\Entity\Subscription;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Class OverlapValidator
 *
*
 */
class OverlapValidator extends ConstraintValidator
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function validate($value, Constraint $constraint)
    {
        if ($value && $constraint instanceof Overlap) {
            if ($value instanceof Subscription) {
                if (null === $value->getCount()) {
                    $subscription = $this->em->getRepository(Subscription::class)->findOverlap($value->getAgency(), $value->getStarted(), $value->getEnded());
                } else {
                    $subscription = $this->em->getRepository(Subscription::class)->findUnlimitedOverlap($value->getAgency(), $value->getStarted(), $value->getEnded());
                }

                if (null !== $subscription) {
                    if ($this->context instanceof ExecutionContextInterface) {
                        $this->context->buildViolation(sprintf('Cet abonnement est en conflit avec %s', $subscription))->addViolation();
                    }
                }
            }
        }
    }
}
