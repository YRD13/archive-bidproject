<?php

namespace Eenov\DefaultBundle\Form;

use Eenov\DefaultBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;

/**
 * Class UserType
 *
*
 */
class UserType extends AbstractType
{
    use FormNameTrait;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', 'email', [
                'label' => 'Email',
                'placeholder' => 'Email',
            ])
            ->add('firstname', 'text', [
                'label' => 'Prénom',
                'placeholder' => 'Prénom',
            ])
            ->add('lastname', 'text', [
                'label' => 'Nom',
                'placeholder' => 'Nom',
            ])
            ->add('birthday', 'birthday', [
                'label' => 'Date de naissance (jj/mm/aaaa)',
                'placeholder' => array(
                    'year' => 'Année', 'month' => 'Mois', 'day' => 'Jour',
                )
            ])
            ->add('country', 'text', [
                'label' => 'Votre pays',
                'placeholder' => 'Votre pays',
            ])
            ->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
                $user = $event->getData();
                if ($user instanceof User) {
                    if (null === $user->getId()) {
                        $event
                            ->getForm()
                            ->add('rawPassword', 'repeated', [
                                'type' => 'password',
                                'first_options' => [
                                    'label' => 'Mot de passe',
                                    'placeholder' => 'Mot de passe',
                                ],
                                'second_options' => [
                                    'label' => 'Mot de passe (validation)',
                                    'placeholder' => 'Mot de passe (validation)',
                                ],
                            ])
                            ->add('cgu', 'checkbox', [
                                'label' => 'J\'accepte les CGU',
                                'mapped' => false,
                                'data' => true,
                                'constraints' => [
                                    new IsTrue([
                                        'message' => 'Vous devez accepter les conditions générales de vente ',
                                    ]),
                                ],
                            ]);
                    }
                }
            });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
