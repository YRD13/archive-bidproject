<?php

namespace Eenov\DefaultBundle\Form;

use Eenov\DefaultBundle\Entity\AdvertType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class AdvancedSearchType
 *
*
 */
class AdvancedSearchType extends AbstractType
{
    use FormNameTrait;

    /**
     * @var string
     */
    private $unitySurface;

    /**
     * @var string
     */
    private $unityMoney;

    /**
     * @param string $unitySurface
     * @param string $unityMoney
     */
    public function __construct($unitySurface, $unityMoney)
    {
        $this->unitySurface = $unitySurface;
        $this->unityMoney = $unityMoney;
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'eenov_default_search';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('types', 'entity', [
                'label' => 'Types de bien',
                'class' => AdvertType::class,
                'required' => false,
                'multiple' => true,
                'expanded' => true,
            ])
            ->add('fieldMin', 'integer', [
                'label' => sprintf('Minimum (%s)', $this->unitySurface),
                'placeholder' => sprintf('Minimum (%s)', $this->unitySurface),
                'required' => false,
            ])
            ->add('fieldMax', 'integer', [
                'label' => sprintf('Maximum (%s)', $this->unitySurface),
                'placeholder' => sprintf('Maximum (%s)', $this->unitySurface),
                'required' => false,
            ])
            ->add('priceMin', 'integer', [
                'label' => sprintf('Minimum (%s)', $this->unityMoney),
                'placeholder' => sprintf('Minimum (%s)', $this->unityMoney),
                'required' => false,
            ])
            ->add('priceMax', 'integer', [
                'label' => sprintf('Maximum (%s)', $this->unityMoney),
                'placeholder' => sprintf('Maximum (%s)', $this->unityMoney),
                'required' => false,
            ])
            ->add('bedrooms', 'checkbox', [
                'label' => '1',
                'required' => false,
            ])
//            ->add('isFuture', 'checkbox', [
//                'label' => 'Enchères à venir',
//                'required' => false,
//                'empty_data' => '1',
//            ])
//            ->add('isCurrent', 'checkbox', [
//                'label' => 'Enchères en cours',
//                'required' => false,
//                'empty_data' => '1',
//            ])
//            ->add('isPast', 'checkbox', [
//                'label' => 'Enchères terminés',
//                'required' => false,
//            ])
            ->add('equipments', 'entity', [
                'class' => 'Eenov\DefaultBundle\Entity\Equipment',
                'required' => false,
                'multiple' => true,
                'expanded' => true,
            ]);
    }
}
