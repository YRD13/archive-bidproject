<?php

namespace Eenov\DefaultBundle\Form\Field;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

/**
 * Class FakeIntegerField
 *
*
 */
class FakeIntegerField extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'fake_integer';
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'integer';
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['type'] = 'text';
    }
}
