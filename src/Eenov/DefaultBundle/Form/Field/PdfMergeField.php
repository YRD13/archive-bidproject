<?php

namespace Eenov\DefaultBundle\Form\Field;

use Eenov\DefaultBundle\PDF\Unite;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PdfMergeField
 *
*
 */
class PdfMergeField extends AbstractType
{
    /**
     * @var Unite
     */
    private $unite;

    /**
     * @param Unite $unite
     */
    public function __construct(Unite $unite)
    {
        $this->unite = $unite;
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'file';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
            $files = $event->getData();
            $filesCount = count($files);
            if (0 === $filesCount) {
                $event->setData(null);
            } elseif (1 === $filesCount) {
                $event->setData(array_shift($files));
            } elseif (0 !== $filesCount) {
                $done = false;

                if ($this->unite->canMerge($files)) {
                    if (null !== $pdf = $this->unite->merge($files)) {
                        $event->setData($pdf);
                        $done = true;
                    }
                }

                if (!$done) {
                    $zipFilename = uniqid() . '.zip';
                    $zipPath = sys_get_temp_dir() . DIRECTORY_SEPARATOR . $zipFilename;

                    $zip = new \ZipArchive();
                    $zip->open($zipPath, \ZipArchive::CREATE);
                    foreach ($files as $file) {
                        if ($file instanceof UploadedFile) {
                            $zip->addFile(
                                $file->getRealPath(),
                                $file->getClientOriginalName()
                            );
                        }
                    }
                    $zip->close();

                    if (file_exists($zipPath)) {
                        $event->setData(new UploadedFile(
                            $zipPath,
                            $zipFilename,
                            'application/zip',
                            filesize($zipPath),
                            null,
                            true
                        ));
                    }
                }
            }
        });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'multiple' => true,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'pdfmerge';
    }
}
