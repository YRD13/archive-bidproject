<?php

namespace Eenov\DefaultBundle\Form\Transformer;

use Symfony\Component\Form\DataTransformerInterface;

/**
 * Class HourTransformer
 *
*
 */
class HourTransformer implements DataTransformerInterface
{
    /**
     * {@inheritdoc}
     */
    public function transform($value)
    {
        if ('' === $value) {
            return null;
        }
        if ($value instanceof \DateTime) {
            return $value->format('H:i');
        }

        return $value;
    }

    /**
     * {@inheritdoc}
     */
    public function reverseTransform($value)
    {
        return \DateTime::createFromFormat('H:i', $value);
    }
}
