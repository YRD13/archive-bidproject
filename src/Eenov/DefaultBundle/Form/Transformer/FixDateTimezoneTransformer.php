<?php

namespace Eenov\DefaultBundle\Form\Transformer;

use Symfony\Component\Form\DataTransformerInterface;

/**
 * Class FixDateTimezoneTransformer
 *
*
 */
class FixDateTimezoneTransformer implements DataTransformerInterface
{
    /**
     * {@inheritdoc}
     */
    public function transform($value)
    {
        if ($value instanceof \DateTime) {
            return \DateTime::createFromFormat('dmY', $value->format('dmY'), new \DateTimeZone('UTC'));
        }

        return $value;
    }

    /**
     * {@inheritdoc}
     */
    public function reverseTransform($value)
    {
        return $value;
    }
}
