<?php

namespace Eenov\DefaultBundle\Form;

use Eenov\DefaultBundle\ORM\Point;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PointType
 *
*
 */
class PointType extends AbstractType
{
    use FormNameTrait;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('latitude', 'hidden')
            ->add('longitude', 'hidden')
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $data = $event->getData();
                if (isset($data['latitude'])) {
                    $data['latitude'] = str_replace(',', '.', $data['latitude']);
                }
                if (isset($data['longitude'])) {
                    $data['longitude'] = str_replace(',', '.', $data['longitude']);
                }
                $event->setData($data);
            })
            ->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
                $point = $event->getData();
                if ($point instanceof Point) {
                    if (null === $point->getLatitude() || null === $point->getLongitude()) {
                        $event->getForm()->addError(new FormError('Vous devez confirmer l\'adresse fournie par le bouton valider en dessous de cette dernière.'));
                    } else {
                        $event->setData(new Point($point->getLatitude(), $point->getLongitude()));
                    }
                }
            });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Point::class,
        ]);
    }
}
