<?php

namespace Eenov\DefaultBundle\Form;

use Doctrine\ORM\EntityRepository;
use Eenov\DefaultBundle\Entity\AdvertType;
use Eenov\DefaultBundle\Entity\Equipment;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SearchType
 *
*
 */
class SearchType extends AbstractType
{
    use FormNameTrait;

    /**
     * @var string
     */
    private $unitySurface;

    /**
     * @var string
     */
    private $unityMoney;

    /**
     * @param string $unitySurface Surface unity
     * @param string $unityMoney   Money unity
     */
    public function __construct($unitySurface, $unityMoney)
    {
        $this->unitySurface = $unitySurface;
        $this->unityMoney = $unityMoney;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('where', 'text', [
                'label' => 'Où souhaitez-vous acheter ?',
                'placeholder' => 'Où souhaitez-vous acheter ?',
                'required' => false,
            ])
            ->add('firstPriceMin', 'fake_integer', [
                'label' => sprintf('Prix minimum (%s)', $this->unityMoney),
                'placeholder' => sprintf('Minimum (%s)', $this->unityMoney),
                'required' => false,
            ])
            ->add('firstPriceMax', 'fake_integer', [
                'label' => sprintf('Prix Maximum (%s)', $this->unityMoney),
                'placeholder' => sprintf('Maximum (%s)', $this->unityMoney),
                'required' => false,
            ])
            ->add('roomsMin', 'fake_integer', [
                'label' => 'Nombre de pièces minimum',
                'placeholder' => 'Minimum',
                'required' => false,
            ])
            ->add('roomsMax', 'fake_integer', [
                'label' => 'Nombre de pièces maximum',
                'placeholder' => 'Maximum',
                'required' => false,
            ])
            ->add('shabMin', 'fake_integer', [
                'label' => 'Surface minimum',
                'placeholder' => 'Minimum',
                'required' => false,
            ])
            ->add('shabMax', 'fake_integer', [
                'label' => 'Surface maximum',
                'placeholder' => 'Maximum',
                'required' => false,
            ])
            ->add('types', 'entity', [
                'label' => 'Types',
                'class' => AdvertType::class,
                'multiple' => true,
                'expanded' => true,
                'query_builder' => function (EntityRepository $er) {
                    $qb = $er->createQueryBuilder('a');

                    return $qb->addOrderBy('a.name', 'ASC');
                },
            ])
            ->add('equipments', 'entity', [
                'label' => 'Équipements',
                'class' => Equipment::class,
                'multiple' => true,
                'expanded' => true,
                'query_builder' => function (EntityRepository $er) {
                    $qb = $er->createQueryBuilder('a');

                    return $qb->addOrderBy('a.name', 'ASC');
                },
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'allow_extra_fields' => true,
        ]);
    }
}
