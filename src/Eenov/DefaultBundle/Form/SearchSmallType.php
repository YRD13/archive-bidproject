<?php

namespace Eenov\DefaultBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

/**
 * Class SearchSmallType
 *
*
 */
class SearchSmallType extends AbstractType
{
    use FormNameTrait;

    /**
     * @var string
     */
    private $unityMoney;

    /**
     * @param string $unityMoney
     */
    public function __construct($unityMoney)
    {
        $this->unityMoney = $unityMoney;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('where', 'text', [
                'label' => 'Où souhaitez-vous acheter ?',
                'placeholder' => 'ville',
                'required' => false,
                'attr' => [
                    'autocomplete' => 'off',
                ],
            ])
            ->add('firstPriceMax', 'fake_integer', [
                'label' => sprintf('Maximum (%s)', $this->unityMoney),
                'placeholder' => 'mon budget',
                'required' => false,
                'attr' => [
                    'autocomplete' => 'off',
                ],
            ]);
    }
}
