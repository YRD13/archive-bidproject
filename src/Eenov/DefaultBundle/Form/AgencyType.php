<?php

namespace Eenov\DefaultBundle\Form;

use Eenov\DefaultBundle\Entity\Agency;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class AgencyType
 *
*
 */
class AgencyType extends AbstractType
{
    use FormNameTrait;

    /**
     * @var Filesystem
     */
    private $fs;

    /**
     * @param Filesystem $fs
     */
    public function __construct(Filesystem $fs)
    {
        $this->fs = $fs;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('owner', 'eenov_default_user', [
                'constraints' => [
                    new Assert\Valid(),
                ],
            ])
            ->add('name', 'text', [
                'label' => 'Nom de l\'agence',
                'placeholder' => 'Nom de l\'agence',
            ])
            ->add('phone', 'text', [
                'label' => 'Téléphone de l\'agence',
                'placeholder' => 'Téléphone de l\'agence',
            ])
            ->add('email', 'email', [
                'label' => 'Email de l\'agence',
                'placeholder' => 'Email de l\'agence',
            ])
            ->add('address', 'text', [
                'label' => 'Adresse de l\'agence',
                'placeholder' => 'Adresse de l\'agence',
            ])
            ->add('zip', 'text', [
                'label' => 'Code postal',
                'placeholder' => 'Code postal',
            ])
            ->add('city', 'text', [
                'label' => 'Ville',
                'placeholder' => 'Ville',
            ])
            ->add('country', 'text', [
                'label' => 'Pays',
                'placeholder' => 'Pays',
            ])
            ->add('type', 'choice', [
                'label' => 'Raison sociale',
                'placeholder' => 'Raison sociale',
                'choices' => Agency::getTypeNameList(),
                'required' => false,
            ])
            ->add('siret', 'integer', [
                'label' => 'SIRET',
                'placeholder' => 'SIRET',
                'required' => false,
            ])
            ->add('rcs', 'text', [
                'label' => 'Ville du RCS',
                'placeholder' => 'Ville du RCS',
                'required' => false,
            ])
            ->add('website', 'text', [
                'label' => 'Site web',
                'placeholder' => 'Site web',
                'required' => false,
            ])
            ->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
                $agency = $event->getData();
                if ($agency instanceof Agency) {
                    if (null === $agency->getFile()) {
                        $originFile = __DIR__ . '/../Resources/public/img/agency-default-logo.png';
                        $targetFile = sys_get_temp_dir() . DIRECTORY_SEPARATOR . uniqid() . '.png';

                        $this->fs->copy($originFile, $targetFile, true);

                        $agency->setFile(new File($targetFile));
                    }
                }
            });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Agency::class,
        ]);
    }
}
