<?php

namespace Eenov\DefaultBundle\Form;

use Eenov\DefaultBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotNull;

/**
 * Class UserResetType
 *
*
 */
class UserResetType extends AbstractType
{
    use FormNameTrait;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('rawPassword', 'repeated', [
                'type' => 'password',
                'first_options' => [
                    'label' => 'Mot de passe',
                    'placeholder' => 'Mot de passe',
                ],
                'second_options' => [
                    'label' => 'Mot de passe (validation)',
                    'placeholder' => 'Mot de passe (validation)',
                ],
                'constraints' => [
                    new NotNull(),
                    new Length(['min' => 6, 'max' => 50]),
                ],
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
