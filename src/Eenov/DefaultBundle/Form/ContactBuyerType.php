<?php

namespace Eenov\DefaultBundle\Form;

use Eenov\DefaultBundle\Entity\ContactBuyer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Collection;

/**
 * Class ContactBuyerType
 *
*
 */
class ContactBuyerType extends AbstractType
{
    use FormNameTrait;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', 'text', [
                'label' => 'Prénom',
                'placeholder' => 'Prénom',
            ])
            ->add('lastname', 'text', [
                'label' => 'Nom',
                'placeholder' => 'Nom',
            ])
            ->add('email', 'email', [
                'label' => 'Adresse email',
                'placeholder' => 'Adresse email',
            ])
            ->add('phone', 'text', [
                'label' => 'Téléphone',
                'placeholder' => 'Téléphone',
                'required' => false,
            ])
            ->add('subject', 'text', [
                'label' => 'Sujet',
                'placeholder' => 'Sujet',
            ])
            ->add('message', 'textarea', [
                'label' => 'Message',
                'placeholder' => 'Message',
                'attr' => [
                    'cols' => 90,
                    'rows' => 10,
                ],
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ContactBuyer::class,
        ]);
    }
}
