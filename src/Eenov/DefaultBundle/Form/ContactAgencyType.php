<?php

namespace Eenov\DefaultBundle\Form;

use Eenov\DefaultBundle\Entity\ContactAgency;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ContactOfferType
 *
*
 */
class ContactAgencyType extends AbstractType
{
    use FormNameTrait;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', [
                'label' => 'Dénomination sociale',
                'placeholder' => 'Agence',
            ])
            ->add('chief', 'text', [
                'label' => 'Nom du représentant légal',
                'placeholder' => 'Nom',
            ])
            ->add('id', 'text', [
                'label' => 'N° SIRET/SIREN',
                'placeholder' => 'SIRET/SIREN',
            ])
            ->add('firstname', 'text', [
                'label' => 'Prénom',
                'placeholder' => 'Prénom',
            ])
            ->add('lastname', 'text', [
                'label' => 'Nom',
                'placeholder' => 'Nom',
            ])
            ->add('email', 'email', [
                'label' => 'Adresse email',
                'placeholder' => 'Adresse email',
            ])
            ->add('phone', 'text', [
                'label' => 'Téléphone',
                'placeholder' => 'Téléphone',
                'required' => false,
            ])
            ->add('subject', 'text', [
                'label' => 'Sujet',
                'placeholder' => 'Sujet',
            ])
            ->add('message', 'textarea', [
                'label' => 'Message',
                'placeholder' => 'Message',
                'attr' => [
                    'cols' => 90,
                    'rows' => 10,
                ],
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ContactAgency::class,
        ]);
    }
}
