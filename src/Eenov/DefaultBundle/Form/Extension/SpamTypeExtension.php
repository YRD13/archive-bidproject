<?php

namespace Eenov\DefaultBundle\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

/**
 * Class SpamTypeExtension
 *
*
 */
class SpamTypeExtension extends AbstractTypeExtension
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            if ('GET' !== $event->getForm()->getConfig()->getMethod()) {
                if ($event->getForm()->isRoot()) {
                    $data = $event->getData();
                    if (!isset($data['stoken']) || '' !== $data['stoken']) {
                        $event->getForm()->addError(new FormError('"stoken" invalide !'));
                    }
                    if (isset($data['stoken'])) {
                        unset($data['stoken']);
                        $event->setData($data);
                    }
                }
            }
        });
    }

    /**
     * {@inheritdoc}
     */
    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        if (!$view->parent && $options['compound']) {
            if ('GET' !== $form->getConfig()->getMethod()) {
                $stoken = $form->getConfig()->getFormFactory()->createNamed('stoken', 'hidden', '', [
                    'mapped' => false,
                    'required' => false,
                ]);

                $view->children['stoken'] = $stoken->createView($view);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getExtendedType()
    {
        return 'form';
    }
}
