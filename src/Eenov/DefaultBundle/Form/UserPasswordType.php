<?php

namespace Eenov\DefaultBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class UserPasswordType
 *
*
 */
class UserPasswordType extends AbstractType
{
    use FormNameTrait;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', 'email', [
                'label' => 'Adresse email',
                'placeholder' => 'Adresse email',
                'constraints' => [
                    new NotBlank(),
                    new Email(),
                ],
            ]);
    }
}
