<?php

namespace Eenov\DefaultBundle\Form;

use Symfony\Component\Form\AbstractType;

/**
 * Class BidContactType
 *
*
 */
class BidContactType extends AbstractType
{
    use FormNameTrait;

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'eenov_default_contact';
    }
}
