<?php

namespace Eenov\DefaultBundle\Form;

/**
 * Trait FormNameTrait
 *
*
 */
trait FormNameTrait
{
    /**
     * @var null|string
     */
    private $name;

    /**
     * Get Name
     *
     * @return null|string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set Name
     *
     * @param null|string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
}
