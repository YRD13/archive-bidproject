<?php

namespace Eenov\DefaultBundle\Twig\Extension;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Class RouterExtension
 *
*
 */
class RouterExtension extends \Twig_Extension
{
    /**
     * @var null|Request
     */
    private $request;

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'router_extension';
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('is_active', [$this, 'isActive']),
            new \Twig_SimpleFunction('if_active', [$this, 'ifActive'], ['is_safe' => ['html']]),
        ];
    }

    /**
     * If active ...
     *
     * @param string|string[] $routes  Routes
     * @param string          $class   Class
     * @param string          $default Default
     *
     * @return string
     */
    public function ifActive($routes, $class = 'active', $default = '')
    {
        if ($this->isActive($routes)) {
            return sprintf(' class="%s %s"', $class, $default);
        }
        if ($default) {
            return sprintf(' class="%s"', $default);
        }

        return '';
    }

    /**
     * Is active ...
     *
     * @param string|string[] $routes
     *
     * @return string
     */
    public function isActive($routes)
    {
        // Request is required to get the current _route
        if (null === $this->request) {
            return false;
        }
        $currentRoute = $this->request->attributes->get('_route');

        $routes = (array)$routes;
        foreach ($routes as $route) {
            if (0 === strpos($currentRoute, $route)) {
                return true;
            }
        }

        return false;
    }

    /**
     * On kernel request
     *
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if (HttpKernelInterface::MASTER_REQUEST === $event->getRequestType()) {
            $this->request = $event->getRequest();
        }
    }
}
