<?php

namespace Eenov\DefaultBundle\Twig\Extension;

use Eenov\DefaultBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class UserExtension
 *
*
 */
class UserExtension extends \Twig_Extension
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var int[]
     */
    private $userCache = [];

    /**
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'user_extension';
    }

    /**
     * {@inheritdoc}
     */
    public function getGlobals()
    {
        return array_combine(User::getRoleList(), User::getRoleList());
    }

    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('user_anonymize', [$this, 'anonymize']),
        ];
    }

    /**
     * Anonymize
     *
     * @param User $user
     *
     * @return string
     */
    public function anonymize(User $user)
    {
        if (null !== $token = $this->tokenStorage->getToken()) {
            if (null !== $me = $token->getUser()) {
                if ($me instanceof User) {
                    if ($me->getId() === $user->getId()) {
                        return $user->getLastname();
                    }
                }
            }
        }

        if (!isset($this->userCache[$user->getId()])) {
            $this->userCache[$user->getId()] = 1 + count($this->userCache);
        }

        return sprintf('Internaute #%u', $this->userCache[$user->getId()]);
    }
}
