<?php

namespace Eenov\DefaultBundle\Twig\Extension;

use Doctrine\ORM\EntityManager;
use Eenov\DefaultBundle\Calendar\BidCalendar;
use Eenov\DefaultBundle\Entity\Auction;
use Eenov\DefaultBundle\Entity\Bid;
use Eenov\DefaultBundle\Entity\User;
use Eenov\DefaultBundle\Entity\Wish;
use Eenov\DefaultBundle\Letter\IntToWord;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

/**
 * Class AdvertExtension
 *
*
 */
class AdvertExtension extends \Twig_Extension
{
    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var CsrfTokenManagerInterface
     */
    private $csrfTokenManager;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var string
     */
    private $unitySurface;

    /**
     * @var string
     */
    private $unityMoney;

    /**
     * @var int[]
     */
    private $wichIdsCache;

    /**
     * @param RouterInterface               $router               Router
     * @param AuthorizationCheckerInterface $authorizationChecker Authorization checker
     * @param TokenStorageInterface         $tokenStorage         Token storage
     * @param CsrfTokenManagerInterface     $csrfTokenManager     Csrf token manager
     * @param EntityManager                 $em                   Manager
     * @param string                        $unitySurface         Unity surface
     * @param string                        $unityMoney           Unity money
     */
    public function __construct(RouterInterface $router, AuthorizationCheckerInterface $authorizationChecker, TokenStorageInterface $tokenStorage, CsrfTokenManagerInterface $csrfTokenManager, EntityManager $em, $unitySurface, $unityMoney)
    {
        $this->router = $router;
        $this->authorizationChecker = $authorizationChecker;
        $this->tokenStorage = $tokenStorage;
        $this->csrfTokenManager = $csrfTokenManager;
        $this->em = $em;
        $this->unitySurface = $unitySurface;
        $this->unityMoney = $unityMoney;
    }

    /**
     * Get area
     *
     * @param float  $area          Area
     * @param int    $decimals      Decimals
     * @param string $dec_point     Decimal point
     * @param string $thousands_sep Thousands separator
     *
     * @return string
     */
    public function getArea($area, $decimals = 2, $dec_point = ',', $thousands_sep = ' ')
    {
        return number_format($area, $decimals, $dec_point, $thousands_sep) . ' ' . $this->unitySurface;
    }

    /**
     * Get auction
     *
     * @param Bid $bid
     *
     * @return Auction|null
     */
    public function getAuctionBest(Bid $bid)
    {
        return $this->em->getRepository(Auction::class)->findLatestAuctionFor($bid);
    }

    /**
     * Get user auction
     *
     * @param Bid $bid
     *
     * @return Auction|null
     */
    public function getAuctionMine(Bid $bid)
    {
        return $this->em->getRepository(Auction::class)->findLatestAuctionFor($bid, $this->tokenStorage->getToken()->getUser());
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'advert_extension';
    }

    /**
     * {@inheritdoc}
     */
    public function getGlobals()
    {
        return [
            'CALENDAR_COLOR_VISIT' => BidCalendar::COLOR_VISIT,
            'CALENDAR_COLOR_BID' => BidCalendar::COLOR_BID,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('price', [$this, 'getPrice']),
            new \Twig_SimpleFilter('price_text', [$this, 'getPriceText']),
            new \Twig_SimpleFilter('area', [$this, 'getArea']),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('wishlit_button', [$this, 'getWishlistButton'], ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('auction_mine', [$this, 'getAuctionMine']),
            new \Twig_SimpleFunction('auction_best', [$this, 'getAuctionBest']),
        ];
    }

    /**
     * Get price
     *
     * @param float  $price        Price
     * @param string $suffix       Suffix
     * @param int    $decimals     Decimals
     * @param string $decPoint     Decimal point
     * @param string $thousandsSep Thousands separator
     *
     * @return string
     */
    public function getPrice($price, $suffix = '', $decimals = 0, $decPoint = ',', $thousandsSep = ' ')
    {
        if (!is_numeric($price)) {
            return '?';
        }

        return trim(number_format($price, $decimals, $decPoint, $thousandsSep) . ' ' . $this->unityMoney . ' ' . $suffix);
    }

    /**
     * Get price text
     *
     * @param float $price
     *
     * @return string
     */
    public function getPriceText($price)
    {
        return IntToWord::toWord($price);
    }

    /**
     * Wishlist button
     *
     * @param Bid         $bid         Bid
     * @param null|string $deleteClass Delete class after
     *
     * @return string
     */
    public function getWishlistButton(Bid $bid, $deleteClass = null)
    {
        // Connected user ?
        if (false === $this->authorizationChecker->isGranted(User::ROLE_USER)) {
            return sprintf(
                '<a class="wishlist" href="%1$s" data-popup="%1$s" title="Connectez-vous pour accéder à votre liste d\'envies"></a>',
                $this->router->generate('eenov_default_default_login')
            );
        }

        // This advert is already in this user wish list ?
        $active = false;
        if (null !== $token = $this->tokenStorage->getToken()) {
            if (null !== $user = $token->getUser()) {
                if ($user instanceof User) {
                    if (null === $this->wichIdsCache) {
                        $this->wichIdsCache = $this->em->getRepository(Wish::class)->findWishIdsFor($user);
                    }

                    $active = in_array($bid->getId(), $this->wichIdsCache);
                }
            }
        }

        return sprintf(
            '<button class="%1$s wishlist" title="%2$s" data-token="%3$s" data-uri="%4$s" data-delete="%5$s">%2$s</button>',
            $active ? 'active pulse' : '',
            $active ? 'Ajouter à ma liste d\'envies' : 'Supprimer de ma liste d\'envies',
            $this->csrfTokenManager->getToken(Wish::class)->getValue(),
            $this->router->generate('eenov_user_userwishlistapi_toggle', ['bid' => $bid->getId(), '_format' => 'json']),
            $deleteClass
        );
    }
}
