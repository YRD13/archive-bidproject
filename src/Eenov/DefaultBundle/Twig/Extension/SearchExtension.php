<?php

namespace Eenov\DefaultBundle\Twig\Extension;

use Eenov\DefaultBundle\Entity\Search;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class SearchExtension
 *
*
 */
class SearchExtension extends \Twig_Extension
{
    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var string
     */
    private $unitySurface;

    /**
     * @var string
     */
    private $unityMoney;

    /**
     * @var null|Request
     */
    private $request;

    /**
     * @param FormFactoryInterface $formFactory  Form factory
     * @param RouterInterface      $router       Router
     * @param string               $unitySurface Unity surface
     * @param string               $unityMoney   Unity money
     */
    public function __construct(FormFactoryInterface $formFactory, RouterInterface $router, $unitySurface, $unityMoney)
    {
        $this->formFactory = $formFactory;
        $this->router = $router;
        $this->unityMoney = $unityMoney;
        $this->unitySurface = $unitySurface;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'search_extension';
    }

    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('yes_no_icon', [$this, 'renderYesNoIcon'], ['is_safe' => ['html']]),
            new \Twig_SimpleFilter('yes_no_thumb', [$this, 'renderYesNoThumb'], ['is_safe' => ['html']]),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('get_search_delete_form', [$this, 'getSearchDeleteForm']),
            new \Twig_SimpleFunction('get_search_update_form', [$this, 'getSearchUpdateForm']),
            new \Twig_SimpleFunction('search_texts', [$this, 'getSearchTexts'], ['is_safe' => ['html']]),
        ];
    }

    /**
     * Get search delete form
     *
     * @param Search $search
     *
     * @return FormView
     */
    public function getSearchDeleteForm(Search $search)
    {
        $form = $this->formFactory->create('form', [], [
            'method' => 'DELETE',
            'action' => $this->router->generate('eenov_user_userprofile_deletesearch', ['search' => $search->getId()])
        ]);

        return $form->createView();
    }

    /**
     * Get search update form
     *
     * @param Search $search
     *
     * @return FormView
     */
    public function getSearchUpdateForm(Search $search)
    {
        $form = $this->formFactory->create('eenov_user_search', $search, [
            'method' => 'PUT',
            'action' => $this->router->generate('eenov_user_userprofile_updatesearch', ['search' => $search->getId()])
        ]);

        return $form->createView();
    }

    /**
     * Get search texts
     *
     * @param Search $search
     *
     * @return string[]
     */
    public function getSearchTexts(Search $search)
    {
        $filters = $search->getFilters();
        $text = [];

        // Type
        if (array_key_exists('type', $filters) && null !== $type = $filters['type']) {
            $text[] = sprintf('Type : %s', $type);
        }

        // Where
        if (array_key_exists('where', $filters) && null !== $where = $filters['where']) {
            $text[] = sprintf('Où : %s', $where);
        }

        // Floor
        $shabMin = array_key_exists('shabMin', $filters) ? $filters['shabMin'] : null;
        $shabMax = array_key_exists('shabMax', $filters) ? $filters['shabMax'] : null;
        if (null == $shabMin && null === $shabMax) {
            $text[] = 'Surface indifférente';
        } elseif (null === $shabMax) {
            $text[] = sprintf('Surface supérieure à %s %s', $shabMin, $this->unitySurface);
        } elseif (null === $shabMin) {
            $text[] = sprintf('Surface inférieure à %s %s', $shabMax, $this->unitySurface);
        } else {
            $text[] = sprintf('Surface entre %s %s et %s %s', $shabMin, $this->unitySurface, $shabMax, $this->unitySurface);
        }

        // Price
        $priceMin = array_key_exists('priceMin', $filters) ? $filters['priceMin'] : null;
        $priceMax = array_key_exists('priceMax', $filters) ? $filters['priceMax'] : null;
        if (null == $priceMin && null === $priceMax) {
            $text[] = 'Prix indifférent';
        } elseif (null === $priceMax) {
            $text[] = sprintf('Prix supérieur à %s%s', $priceMin, $this->unityMoney);
        } elseif (null === $priceMin) {
            $text[] = sprintf('Prix inférieur à %s%s', $priceMax, $this->unityMoney);
        } else {
            $text[] = sprintf('Prix entre %s%s et %s%s', $priceMin, $this->unityMoney, $priceMax, $this->unityMoney);
        }

        return $text;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if (HttpKernelInterface::MASTER_REQUEST === $event->getRequestType()) {
            $this->request = $event->getRequest();
        }
    }

    /**
     * Render yes no icon
     *
     * @param mixed $yesNo
     *
     * @return string
     */
    public function renderYesNoIcon($yesNo)
    {
        return sprintf('<span class="badge badge-%s">%s</span>', $yesNo ? 'success' : 'danger', $yesNo ? 'Oui' : 'Non');
    }

    /**
     * Render yes no thumb
     *
     * @param mixed $yesNo
     *
     * @return string
     */
    public function renderYesNoThumb($yesNo)
    {
        return sprintf('<span class="yesnothumb badge badge-%s"><span class="fa fa-%s"></span></span>', $yesNo ? 'success' : 'danger', $yesNo ? 'thumbs-up' : 'thumbs-down');
    }
}
