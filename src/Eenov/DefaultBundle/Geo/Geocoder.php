<?php

namespace Eenov\DefaultBundle\Geo;

use Doctrine\ORM\EntityManager;
use Eenov\DefaultBundle\Entity\Geocoding;
use Eenov\DefaultBundle\ORM\Point;
use Geocoder\HttpAdapter\CurlHttpAdapter;
use Geocoder\Provider\GoogleMapsProvider;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * Class Geocoder
 *
*
 */
class Geocoder
{
    /**
     * @var bool
     */
    private $googleMapEnabled;

    /**
     * @var string
     */
    private $googleMapKey;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param bool                 $googleMapEnabled Google Map Enabled
     * @param string               $googleMapKey     Google Map Key
     * @param EntityManager        $em               Entity Manager
     * @param null|LoggerInterface $logger           Logger
     */
    public function __construct($googleMapEnabled, $googleMapKey, EntityManager $em, LoggerInterface $logger = null)
    {
        $this->googleMapEnabled = $googleMapEnabled;
        $this->googleMapKey = $googleMapKey;
        $this->em = $em;
        $this->logger = $logger ?: new NullLogger();
    }

    /**
     * Get Geocoding
     *
     * @param string $search
     *
     * @return null|Geocoding
     */
    public function getGeocoding($search)
    {
        if (empty($search)) {
            return null;
        }

        $context = ['search' => $search];
        $this->logger->debug('geocoding', $context);

        if (null !== $geocoding = $this->em->getRepository(Geocoding::class)->findOneBy(['search' => $search])) {
            $context['geocoding_id'] = $geocoding->getId();
            $this->logger->debug('geocoding found in database', $context);

            return $geocoding;
        }

        $searchLen = mb_strlen($search);
        if ($searchLen < 3 || $searchLen > 255) {
            $this->logger->debug('geocoding impossible (invalid length)', $context);

            return null;
        }

        if ($this->googleMapEnabled) {
            $this->logger->debug('geocoding with google', $context);

            try {
                $this->logger->debug('geocoding with google (calling)', $context);

                $googleMaps = new GoogleMapsProvider(new CurlHttpAdapter(), 'fr_FR', 'France', true, $this->googleMapKey);
                $geos = $googleMaps->getGeocodedData($search);
                foreach ($geos as $geo) {
                    if (is_array($geo) && isset($geo['latitude']) && isset($geo['longitude'])) {
                        $context['latitude'] = $geo['latitude'];
                        $context['longitude'] = $geo['longitude'];
                        $this->logger->debug('geocoding with google done', $context);

                        $geocoding = new Geocoding();
                        $geocoding
                            ->setSearch($search)
                            ->setPoint(new Point((float)$geo['latitude'], (float)$geo['longitude']));

                        $this->em->persist($geocoding);
                        $this->em->flush();

                        return $geocoding;
                    }
                }
            } catch (\Exception $e) {
                $context['error'] = $e->getMessage();
                $this->logger->error('geocoding with google error', $context);
            }
        }

        return null;
    }
}
