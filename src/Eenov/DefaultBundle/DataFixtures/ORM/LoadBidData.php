<?php

namespace Eenov\DefaultBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Eenov\DefaultBundle\Entity\Access;
use Eenov\DefaultBundle\Entity\Advert;
use Eenov\DefaultBundle\Entity\AdvertType;
use Eenov\DefaultBundle\Entity\Agency;
use Eenov\DefaultBundle\Entity\Around;
use Eenov\DefaultBundle\Entity\Bid;
use Eenov\DefaultBundle\Entity\Document;
use Eenov\DefaultBundle\Entity\DocumentType;
use Eenov\DefaultBundle\Entity\Equipment;
use Eenov\DefaultBundle\Entity\Image;
use Eenov\DefaultBundle\Entity\ImageType;
use Eenov\DefaultBundle\Entity\Mandate;
use Eenov\DefaultBundle\Entity\Visit;
use Eenov\DefaultBundle\ORM\Point;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class LoadBidData
 *
*
 */
class LoadBidData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    const COUNT = 10;

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 50;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        $randomName = [
            'Maison plein centre',
            'Appartement plein centre',
            'Belle maison',
            'Bel appartement',
            'Maison bien située',
            'Maison de village',
            'Appartement de village',
            'Pavillon de village',
            'Cadre agréable',
            'Maison agréable',
            'Pavillon agréable',
            'Appartement agréable',
            'Appartement ancien',
            'Cadre ancien',
            'Maison ancienne',
            'Pavillon ancien',
        ];

        $cities = [
            ['Bordeaux', 44.837912, -0.579541],
            ['Bègles', 44.8086, -0.5478],
            ['Pessac', 44.806667, -0.631111],
            ['Lormont', 44.879167, -0.521667],
            ['Tarbes', 43.2307, 0.0726],
            ['Toulouse', 43.604482, 1.443962],
            ['Mérignac', 44.843056, -0.644722],
            ['Pau', 43.3017, -0.3686],
        ];

        $arounds = $em->getRepository(Around::class)->findAll();
        $equipments = $em->getRepository(Equipment::class)->findAll();
        $types = $em->getRepository(AdvertType::class)->findAll();
        $agencies = $em->getRepository(Agency::class)->findAll();
        $imageTypes = $em->getRepository(ImageType::class)->findAll();
        $documentTypes = $em->getRepository(DocumentType::class)->findAll();

        for ($i = 1; $i < self::COUNT; $i++) {
            $date = self::getFaker()->dateTimeBetween(sprintf('-%u years', mt_rand(5, 30)));
            $date->setTime(0, 0, 0);

            $city = $cities[array_rand($cities)];

            $draft = new Advert();
            $draft
                ->setType($types[array_rand($types)])
                ->setFile($this->createImageFile())
                ->setNew(rand(0, 1))
                ->setDisplayAddress(self::getFaker()->boolean())
                ->setAddress(self::getFaker()->streetAddress)
                ->setZip(self::getFaker()->postcode)
                ->setCity($city[0])
                ->setCountry(self::getFaker()->country)
                ->setMandateAgency(self::getFaker()->word)
                ->setDescription(self::getFaker()->sentence(100))
                ->setYear(mt_rand(1960, 2014))
                ->setEstate(Advert::getEstateList()[array_rand(Advert::getEstateList())])
                ->setRooms(rand(1, 5))
                ->setBedrooms(rand(1, 5))
                ->setBathrooms(rand(1, 5))
                ->setToilets(rand(1, 3))
                ->setShowerrooms(rand(0, 5))
                ->setFloor(rand(0, 5))
                ->setHeatingSystem(Advert::getHeatingSystemList()[array_rand(Advert::getHeatingSystemList())])
                ->setHeatingType(Advert::getHeatingTypeList()[array_rand(Advert::getHeatingTypeList())])
                ->setExposure(Advert::getExposureList()[array_rand(Advert::getExposureList())])
                ->setInternet(Advert::getInternetList()[array_rand(Advert::getInternetList())])
                ->setDpe(self::getFaker()->boolean() ? rand(0, 550) : null)
                ->setGes(self::getFaker()->boolean() ? rand(0, 95) : null)
                ->setPoint(new Point($city[1], $city[2]))
                ->setShab(mt_rand(10000, 20000) / 100)
                ->setLivingRoom(mt_rand(3000, 8000) / 100)
                ->setField(mt_rand(100000, 500000) / 100);

            foreach ($arounds as $around) {
                if (self::getFaker()->boolean(25)) {
                    $draft->addAround($around);
                }
            }

            foreach ($equipments as $equipment) {
                if (self::getFaker()->boolean(25)) {
                    $draft->addEquipment($equipment);
                }
            }

            do {
                foreach ($imageTypes as $imageType) {
                    if (self::getFaker()->boolean(50)) {
                        if (null !== $file = $this->createImageFile($imageType)) {
                            $image = new Image();
                            $image
                                ->setFile($file)
                                ->setType($imageType);

                            $draft->addImage($image);
                        }
                    }
                }
            } while (6 > $draft->getImages()->count());

            foreach ($documentTypes as $documentType) {
                if (self::getFaker()->boolean(50)) {
                    $document = new Document();
                    $document
                        ->setFile($this->createDocumentFile())
                        ->setType($documentType);

                    $draft->addDocument($document);
                }
            }

            $em->persist($draft);

            $bid = new Bid();
            $bid
                ->setTitle(0 === $i ? 'Jolie maison' : $randomName[array_rand($randomName)])
                ->setAgency($agencies[array_rand($agencies)])
                ->setSellerFirstname(self::getFaker()->firstName)
                ->setSellerLastname(self::getFaker()->lastName)
                ->setSellerEmail(self::getFaker()->unique()->email)
                ->setSellStarted(self::getFaker()->dateTimeBetween('+15 days', '+30 days'))
                ->setSellEnded(self::getFaker()->dateTimeBetween('+30 days', '+45 days'))
                ->setFirstPrice($firstPrice = (100000 * mt_rand(1, 50)))
                ->setEstimatedPrice($firstPrice + 10000 * mt_rand(1, 5))
                ->setDraft($draft);
            $em->persist($bid);

            $jMax = mt_rand(5, 10);
            for ($j = 0; $j < $jMax; $j++) {
                $date = clone $bid->getSellStarted();
                $date->sub(new \DateInterval(sprintf('P%uD', $j + 5)));

                $visit = new Visit();
                $visit
                    ->setDate($date)
                    ->setStarted(\DateTime::createFromFormat('H:i', sprintf('%02u:%02u', mt_rand(8, 11), 25 * mt_rand(0, 3))))
                    ->setEnded(\DateTime::createFromFormat('H:i', sprintf('%02u:%02u', mt_rand(14, 21), 25 * mt_rand(0, 3))))
                    ->setBid($bid);

                $em->persist($visit);
            }

            for ($j = 0; $j < 5; $j++) {
                $mandate = new Mandate();
                $mandate
                    ->setBid($bid)
                    ->setBuyer(self::getFaker()->lastName)
                    ->setFile($this->createDocumentFile())
                    ->setVisited(self::getFaker()->dateTimeBetween('-15 days', '+5 days'));

                $em->persist($mandate);

                $access = new Access();
                $access
                    ->setBid($bid)
                    ->setMandate($mandate)
                    ->setUser($this->createUser($em))
                    ->validate();

                $em->persist($access);
            }

            $em->flush();

            if (null !== $slot = $this->container->get('eenov.default_bundle.advert.slot_dealer')->findAvailableSlotFor($bid->getAgency())) {
                $publish = $this->container->get('eenov.default_bundle.advert.advert_cloner')->duplicateAdvert($draft);
                $bid
                    ->setPublish($publish)
                    ->setSlot($slot)
                    ->setAdminValidated(new \DateTime())
                    ->setAgencyValidated(new \DateTime());

                $em->persist($publish);
            }

            $em->flush();
        }
    }
}
