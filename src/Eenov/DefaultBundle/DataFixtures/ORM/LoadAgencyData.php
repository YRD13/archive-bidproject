<?php
namespace Eenov\DefaultBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Eenov\DefaultBundle\Entity\Agency;
use Eenov\DefaultBundle\Entity\Asset;
use Eenov\DefaultBundle\Entity\Legal;
use Eenov\DefaultBundle\Entity\Price;
use Eenov\DefaultBundle\Entity\Slot;
use Eenov\DefaultBundle\Entity\Subscription;
use Eenov\DefaultBundle\Entity\User;

/**
 * Class LoadUserData
 *
*
 */
class LoadAgencyData extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{
    const COUNT = 5;

    /**
     * {@inheritdoc}
     */
    function getOrder()
    {
        return 5;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        $commercialUsers = $em->getRepository(User::class)->findBy(['role' => User::ROLE_COMMERCIAL]);
        $prices = $em->getRepository(Price::class)->findAll();

        for ($i = 0; $i < self::COUNT; $i++) {
            $agency = new Agency();
            $agency
                ->setCommercial(self::getFaker()->boolean() ? $commercialUsers[array_rand($commercialUsers)] : null)
                ->setOwner($this->createUser($em, User::ROLE_AGENCY_OWNER))
                ->setResponsible(self::getFaker()->boolean() ? $this->createUser($em, User::ROLE_AGENCY_RESPONSIBLE) : null)
                ->setContact(self::getFaker()->boolean() ? $this->createUser($em, User::ROLE_AGENCY_CONTACT) : null)
                ->setEmail(self::getFaker()->email)
                ->setAddress(self::getFaker()->address)
                ->setZip(self::getFaker()->postcode)
                ->setCity(self::getFaker()->city)
                ->setCountry(self::getFaker()->country)
                ->setName(self::getFaker()->company)
                ->setPhone(self::getFaker()->phoneNumber)
                ->setFile($this->createImageFile());
            $em->persist($agency);

            foreach ($prices as $price) {
                if (self::getFaker()->boolean()) {
                    $subscription = new Subscription();
                    $subscription
                        ->setAgency($agency)
                        ->setStarted(new \DateTime())
                        ->populate($price);

                    $em->persist($subscription);
                }

                $ended = new \DateTime();
                $ended->add(new \DateInterval('P1M'));

                $slot = new Slot();
                $slot
                    ->setAgency($agency)
                    ->setStarted(new \DateTime())
                    ->setEnded($ended);

                $em->persist($slot);
            }

            for ($j = 0; $j < mt_rand(2, 5); $j++) {
                $asset = new Asset();
                $asset
                    ->setAgency($agency)
                    ->setDate(new \DateTime())
                    ->setAmount(500 - mt_rand(0, 1000))
                    ->setComment(self::getFaker()->text(255));

                $em->persist($asset);
            }

            for ($j = 0; $j < mt_rand(2, 5); $j++) {
                $legal = new Legal();
                $legal
                    ->setAgency($agency)
                    ->setType(Legal::TYPE_RIB)
                    ->setFile($this->createDocumentFile());

                $em->persist($legal);
            }
        }

        $em->flush();
    }
}
