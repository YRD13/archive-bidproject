<?php

namespace Eenov\DefaultBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Eenov\DefaultBundle\Entity\Bank;

/**
 * Class LoadBankData
 *
*
 */
class LoadBankData implements FixtureInterface, OrderedFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 1;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        $banks = [
            'Banque Courtois',
            'Banque Populaire',
            'Barclays',
            'BNP',
            'Boursorama',
            'Caisse d\'Epargne',
            'CIC',
            'Credit Agricole',
            'Credit Mutuel',
            'Fortuneo',
            'Hello Bank',
            'HSBC',
            'ING direct',
            'La poste',
            'LCL',
            'Société Générale',
        ];
        foreach ($banks as $name) {
            $bank = new Bank();
            $bank->setName($name);

            $em->persist($bank);
        }

        $em->flush();
    }
}
