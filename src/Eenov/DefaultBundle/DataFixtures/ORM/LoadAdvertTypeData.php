<?php

namespace Eenov\DefaultBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Eenov\DefaultBundle\Entity\AdvertType;

/**
 * Class LoadAdvertTypeData
 *
*
 */
class LoadAdvertTypeData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 1;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        $types = [
            ['Maison', false, false],
            ['Appartement', false, false],
            ['Chalet', false, false],
            ['Terrain', true, false],
            ['Immeuble', false, true],
            ['Bureau', false, false],
            ['Commerce', false, false],
            ['Ferme', false, false],
            ['Échoppe', false, false],
            ['Chateau', false, false],
            ['Longère', false, false],
        ];

        foreach ($types as $details) {
            $type = new AdvertType();
            $type
                ->setName($details[0])
                ->setIsGround($details[1])
                ->setIsBuilding($details[2]);

            $em->persist($type);
        }

        $em->flush();
    }
}
