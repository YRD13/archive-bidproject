<?php

namespace Eenov\DefaultBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Eenov\DefaultBundle\Entity\Seller;

/**
 * Class LoadSellerData
 *
*
 */
class LoadSellerData implements FixtureInterface, OrderedFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 1;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        $sellers = [
            '4 %',
            'AJP',
            'Avis',
            'Cabinet Bedin',
            'Century 21',
            'Citya',
            'ERA',
            'Foncia',
            'Guy Hoquet',
            'L\'adresse',
            'La bourse de l\'immobilier',
            'Laforêt',
            'ORPI',
        ];
        foreach ($sellers as $name) {
            $seller = new Seller();
            $seller
                ->setName($name);

            $em->persist($seller);
        }

        $em->flush();
    }
}
