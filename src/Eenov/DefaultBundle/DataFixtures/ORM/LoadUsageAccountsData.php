<?php

namespace Eenov\DefaultBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Eenov\DefaultBundle\Entity\Agency;
use Eenov\DefaultBundle\Entity\User;

/**
 * Class LoadUsageAccountsData
 *
*
 */
class LoadUsageAccountsData extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    function getOrder()
    {
        return 100;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        $commercial = $this->createUser($em, User::ROLE_SUPER_COMMERCIAL, 'super-commercial@oxioneo.com', 'aa');
        $subcommercial = $this->createUser($em, User::ROLE_COMMERCIAL, 'commercial@oxioneo.com', 'aa');
        $user1 = $this->createUser($em, User::ROLE_USER, 'user1@oxioneo.com', 'aa');
        $user2 = $this->createUser($em, User::ROLE_USER, 'user2@oxioneo.com', 'aa');
        $admin = $this->createUser($em, User::ROLE_ADMIN, 'admin@oxioneo.com', 'aa');
        $moderator = $this->createUser($em, User::ROLE_MODERATOR, 'moderator@oxioneo.com', 'aa');
        $owner = $this->createUser($em, User::ROLE_AGENCY_OWNER, 'mother@oxioneo.com', 'aa');
        $contact = $this->createUser($em, User::ROLE_AGENCY_CONTACT, 'agency@oxioneo.com', 'aa');
        $em->flush();

        $commercial->setKey(null);
        $subcommercial->setKey(null);
        $user1->setKey(null);
        $user2->setKey(null);
        $admin->setKey(null);
        $moderator->setKey(null);
        $owner->setKey(null);
        $contact->setKey(null);
        $em->flush();

        $agencies = $em->getRepository(Agency::class)->findAll();

        /** @var Agency $mother */
        $mother = array_shift($agencies);
        $mother->setOwner($owner);

        /** @var Agency $daughter */
        $daughter = array_shift($agencies);
        $daughter
            ->setOwner($owner)
            ->setContact($contact)
            ->setMother($mother);

        $em->flush();
    }
}
