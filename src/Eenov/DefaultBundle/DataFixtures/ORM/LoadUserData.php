<?php

namespace Eenov\DefaultBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Eenov\DefaultBundle\Entity\User;

/**
 * Class LoadUserData
 *
*
 */
class LoadUserData extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    function getOrder()
    {
        return 2;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        $roles = [
            User::ROLE_ADMIN,
            User::ROLE_COMMERCIAL,
            User::ROLE_MODERATOR,
            User::ROLE_SUPER_COMMERCIAL,
            User::ROLE_USER,
        ];

        foreach ($roles as $role) {
            for ($i = 0; $i < 5; $i++) {
                $this->createUser($em, $role);
            }
        }

        $em->flush();
    }
}
