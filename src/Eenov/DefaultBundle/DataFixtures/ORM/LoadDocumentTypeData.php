<?php

namespace Eenov\DefaultBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Eenov\DefaultBundle\Entity\DocumentType;

/**
 * Class LoadDocumentTypeData
 *
*
 */
class LoadDocumentTypeData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 1;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        $types = [
            'Tous les biens' => [
                'Mandat de recherche exclusif d\'acquéreurs',
                'Demande d\'agrément',
                'Titre de propriété',
                'Taxe foncières',
                'Facture GAZ',
                'Facture Electricité',
                'Plan du bien',
                'Diagnostics techniques' => [
                    'Diagnostic Amiante',
                    'Diagnostic Plomb',
                    'Diagnostic DPE + GES',
                    'Diagnostic Termites',
                    'Diagnostic Sécurité gaz',
                    'Diagnostic Sécurité électrique',
                    'Diagnostic Etat des risques naturels et technologiques',
                    'Diagnostic Assainissement',
                    'Diagnostic Sécurité piscine',
                ],
            ],
            'Biens en copropriété' => [
                'Règlement de copropriété et ses actes modificateurs',
                'Trois derniers procès-verbaux d\'assemblée générale',
                'Budget prévisionnel',
                'Carnet d\'entretien de l\'immeuble',
                'Etat descriptif de division de l\'immeuble et ses actes modificateurs',
                'Diagnostic Carrez',
            ],
            'Biens en lotissement' => [
                'Cahier des charges du lotissement',
            ],
            'Terrains à bâtir (issu ou non d\'une division parcellaire)' => [
                'Plan de bornage',
                'Extrait du plan cadastral',
                'Relevé de surface',
                'Déclaration préalable',
            ],
            'Biens construit depuis moins de 5 ans' => [
                'Copie du permis de construire',
                'Garantie décennale',
                'Garantie dommage ouvrage',
                'Garantie de parfait achèvement',
                'Déclaration d\'achèvement des travaux',
                'Certificat de conformité',
            ],
            'Biens vendus/loués' => [
                'Bail',
                'Etat des lieux',
                'Congé pour vente au locataire',
            ],
        ];

        $order = 0;
        foreach ($types as $title => $titles) {
            foreach ($titles as $subtitle => $names) {
                if (is_string($names)) {
                    $type = new DocumentType();
                    $type
                        ->setOrder($order++)
                        ->setTitle($title)
                        ->setName($names)
                        ->setIsPublic(self::getFaker()->boolean(80));

                    $em->persist($type);
                } elseif (is_array($names)) {
                    foreach ($names as $name) {
                        $type = new DocumentType();
                        $type
                            ->setOrder($order++)
                            ->setTitle($title)
                            ->setSubtitle($subtitle)
                            ->setName($name)
                            ->setIsPublic(self::getFaker()->boolean(80));

                        $em->persist($type);
                    }
                }
            }
        }

        $em->flush();
    }
}
