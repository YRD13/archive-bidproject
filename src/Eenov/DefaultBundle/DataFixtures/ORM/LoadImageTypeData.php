<?php

namespace Eenov\DefaultBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Eenov\DefaultBundle\Entity\ImageType;

/**
 * Class LoadImageTypeData
 *
*
 */
class LoadImageTypeData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 1;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        $types = [
            'garage' => 'Garage',
            'jardin' => 'Jardin',
            'terrasse' => 'Terrasse',
            'piscine' => 'Piscine',
            'cave' => 'Cave',
            'parking' => 'Parking',
            'cheminee' => 'Cheminée',
            'jaccuzzi' => 'Jaccuzzi',
            'ascenseur' => 'Ascenseur',
            'accessibilite_handicape' => 'Accessibilite handicapé',
            'terrain_tennis' => 'Terrain de tennis',
            'buanderie' => 'Buanderie',
            'salle_jeux' => 'Salle de jeux',
            'home_cinema' => 'Home cinema',
            'salle_sport' => 'Salle de sport',
            'escaliers' => 'Escaliers',
            'barbecue' => 'Barbecue',
            'potager' => 'Potager',
            'vue_exterieur' => 'Vue exterieur',
            'entree' => 'Entrée',
            'salon' => 'Salon',
            'salle_manger' => 'Salle à manger',
            'cuisine' => 'Cuisine',
            'cuisine_equipee' => 'Cuisine équipée',
            'salle_eau' => 'Salle d\'eau',
            'salle_bain' => 'Salle de bain',
            'bureau' => 'Bureau',
            'chambre_enfant' => 'Chambre d\'enfant',
            'chambre' => 'Chambre',
        ];

        foreach ($types as $key => $name) {
            $target = sys_get_temp_dir() . '/' . uniqid() . '.png';
            copy(__DIR__ . '/../../Resources/private/fixtures/types/' . $key . '.png', $target);

            $type = new ImageType();
            $type
                ->setName($name)
                ->setFile(new \SplFileInfo($target));

            $em->persist($type);
        }

        $em->flush();
    }
}
