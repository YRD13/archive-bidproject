<?php

namespace Eenov\DefaultBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture as CoreAbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Eenov\DefaultBundle\Entity\AdvertType;
use Eenov\DefaultBundle\Entity\Bank;
use Eenov\DefaultBundle\Entity\ImageType;
use Eenov\DefaultBundle\Entity\Profile;
use Eenov\DefaultBundle\Entity\Seller;
use Eenov\DefaultBundle\Entity\User;
use Faker\Factory;
use Faker\Generator;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class AbstractFixture
 *
*
 */
abstract class AbstractFixture extends CoreAbstractFixture
{
    /**
     * @var Generator
     */
    private static $faker;

    /**
     * @var string[]
     */
    public static $jobs = ['Acheteur', 'Acheteur d\'espaces pub', 'Acheteur Informatique Et Télécom', 'Actuaire', 'Adjoint administratif', 'Adjoint administratif d\'administration centrale', 'Adjoint administratif territorial', 'Adjoint technique de recherche et de formation', 'Adjoint territorial d\'animation', 'Administrateur base de données', 'Administrateur De Bases De Données', 'Administrateur de biens', 'Administrateur De Réseau', 'Administrateur judiciaire', 'Affréteur', 'Agent administratif et agent des services techniques', 'Agent d\'entretien d\'ascenseurs', 'Agent d\'exploitation des équipements audiovisuels', 'Agent de maintenance en mécanique', 'Agent de maîtrise', 'Agent de police municipale', 'Agent de réservation', 'Agent des services techniques d\'administration centrale', 'Agent des services techniques de préfecture', 'Agent des systèmes d\'information et de communication', 'Agent immobilier', 'Agent spécialisé de police technique et scientifique', 'Agent technique de recherche et de formation', 'Aide comptable', 'Aide de laboratoire', 'Aide médico-psychologique', 'Aide soignant', 'Aide soignant', 'Aide technique de laboratoire', 'Ambulancier', 'Analyste D\'Exploitation', 'Analyste financier', 'Analyste programmeur', 'Animateur', 'Animateur', 'Animateur de club de vacances', 'Animateur de formation', 'animateur environnement', 'Animateur socioculturel', 'Antiquaire', 'Archéologue', 'Architecte', 'Architecte d\'intérieur', 'Architecte De Bases De Données', 'Architecte De Réseau', 'Architecte De Système D\'Information', 'Architecte Matériel', 'Archiviste', 'Artiste-peintre', 'Assistant de conservation', 'Assistant de justice', 'Assistant de ressources humaines', 'Assistant de service social', 'Assistant de service social', 'Assistant des bibliothèques', 'Assistant ingénieur', 'Assistant médico-technique', 'Assistant socio-éducatif', 'Assistant son', 'Assistant vétérinaire', 'Assistante de gestion PMI/PME', 'Assistante maternelle', 'Assistante sociale', 'Astronome', 'Attaché d\'administration et d\'intendance', 'Attaché d\'administration hospitalière', 'Attaché d\'administration centrale', 'Attaché d\'administration scolaire et universitaire', 'Attaché de conservateur de patrimoine', 'Attaché de police', 'Attaché de préfecture', 'Attaché de presse', 'Auditeur Informatique', 'Auteur-scénariste multimédia', 'Auxiliaire de puériculture', 'Auxiliaire de vie sociale', 'Auxilliaire de vie', 'Avocat', 'Barman', 'Bibliothécaire', 'Bibliothécaire adjoint spécialisé', 'Bijoutier joaillier', 'Billettiste', 'Bio-informaticien', 'Biologiste, Vétérinaire, Pharmacien', 'Bobinier de la construction électrique', 'Boucher', 'Boulanger', 'Brasseur malteur', 'Bronzier', 'Bûcheron', 'Cadreur', 'Capitaine de Sapeur-Pompier', 'Carreleur', 'Carrossier réparateur', 'Caviste', 'Charcutier-traiteur', 'Chargé de clientèle', 'Chargé De Référencement', 'Chargé de relations publiques', 'Charpentier', 'Chaudronnier', 'Chef d\'atelier des industries graphiques', 'Chef de chantier', 'Chef de comptoir', 'Chef de fabrication', 'Chef de produits voyages', 'Chef De Projet - Project Manager', 'Chef de projet informatique', 'Chef de projet multimedia', 'Chef de publicité', 'Chef de rayon', 'Chef de service de Police municipale', 'Chef opérateur', 'Chercheur', 'Chercheur En Informatique', 'Chirurgien-dentiste', 'Chocolatier confiseur', 'Clerc de notaire', 'Coiffeur', 'Comédien', 'Commis de cuisine', 'Commissaire de police', 'Commissaire priseur', 'comportementaliste', 'Comptable', 'Concepteur De Jeux Électroniques', 'Concepteur rédacteur', 'Concierge d\'hôtel', 'Conducteur', 'Conducteur d\'appareils', 'Conducteur d\'autobus', 'Conducteur d\'automobile', 'Conducteur d\'engins en BTP', 'Conducteur de machine à imprimer d\'exploitation complexe', 'Conducteur de machine à imprimer simple', 'Conducteur de machines', 'Conducteur de machines agro', 'Conducteur de station d\'épuration', 'Conducteur de taxi', 'conducteur de train', 'Conducteur de travaux', 'Conducteur routier', 'Conseil En Assistance À Maitrise D\'Ouvrage', 'Conseiller d\'insertion et de probation', 'Conseiller d\'orientation', 'Conseiller d\'orientation-psychologue', 'Conseiller en développement touristique', 'Conseiller en économie sociale et familiale', 'Conseiller socio-éducatif', 'Conseiller territorial des activités physiques et sportives', 'Conseillers principaux d\'éducation', 'Conservateur de bibliothèque', 'Conservateur du patrimoine', 'Consultant Communication & Réseaux', 'Consultant E-Business', 'Consultant En Conduite Du Changement', 'Consultant En E-Learning', 'Consultant En Gestion De La Relation Client', 'Consultant En Organisation Des Systèmes D\'Information', 'Consultant En Technologies', 'Consultant Erp', 'Consultant Fonctionnel', 'Consultant Informatique', 'Contrôleur aérien', 'Contrôleur de gestion', 'Contrôleur de travaux', 'Contrôleur des services techniques du matériel', 'Contrôleur des systèmes d\'information et de communication', 'Contrôleur du travail', 'Contrôleur en électricité et électronique', 'Convoyeur de fonds', 'Coordinatrice de crèches', 'Correcteur', 'Costumier-habilleur', 'Courtier d\'assurances', 'Couvreur', 'Créateur de parfum', 'Cuisinier', 'Cyberdocumentaliste', 'Danseur', 'Décorateur-scénographe', 'Délégué médical', 'Déménageur', 'Démographe', 'Dépanneur tv électroménager', 'Designer automobile', 'Dessinateur de presse', 'Dessinateur industriel', 'Détective privé', 'Développeur', 'Diététicien', 'Directeur artistique', 'Directeur Commercial', 'Directeur d\'établissement social et médico-social', 'Directeur d\'hôpital', 'Directeur d\'établissement d\'enseignement artistique', 'Directeur d\'établissement sanitaire et social', 'Directeur d\'office de tourisme', 'Directeur de collection', 'Directeur de parc naturel', 'Directeur De Projet', 'Directeur de ressources humaines', 'Directeur des soins', 'Directeur Des Systèmes D\'Information', 'Directeur Technique', 'Docker', 'Documentaliste', 'Douanier', 'Ebéniste', 'Eboueur', 'Eco-conseiller', 'Ecotoxicologue', 'Educateur de jeunes enfants', 'Educateur spécialisé', 'Educateur sportif', 'Educateur technique spécialisé', 'Educateur territorial des activités physiques et sportives', 'Electricien de maintenance', 'Electricien du bâtiment', 'Electricien électronicien auto', 'Employé de groupage', 'Employé de restauration rapide', 'Employés du hall des hôtels', 'Encadreur', 'Enseignant Chercheur', 'Entraîneur sportif', 'Ergonome', 'Ergonome', 'Ergothérapeute', 'Esthéticienne-cosméticienne', 'Etalagiste décorateur', 'Ethnologue', 'Expert automobile', 'Expert comptable', 'Expert En Sécurité Informatique', 'Facteur instrumental', 'Femme de chambre ou valet de chambre', 'Fleuriste', 'Forfaitiste', 'Formateur En Informatique', 'Game designer', 'Garçon de café', 'Garde du corps', 'Garde-champêtre', 'Gardien d\'immeuble', 'Gardien d\'immeuble', 'Gardien de la paix', 'Gendarme', 'Géographe', 'Géologue', 'Géomètre topographe', 'Gérant d\'hôtel', 'Gestionnaire De Parc Micro-Informatique', 'Graphiste multimédia', 'Greffier', 'Guichetetier', 'Guide accompagnateur', 'Guide de haute montagne', 'Guide Interprète', 'Horiculteur', 'Hot-Liner Technicien Help-Desk', 'Hôtesse d\'accueil', 'Hôtesse de l\'air', 'Hotliner', 'Huissier de justice', 'Iconographe', 'Infirmier', 'Infirmier anesthésiste diplômé d\'Etat', 'Infirmier chef', 'Infirmier de bloc opératoire diplômé d\'Etat', 'Infirmier diplômé d\'Etat', 'Infirmiere', 'Ingénieur agroalimentaire', 'Ingénieur Commercial', 'Ingénieur d\'études sanitaires', 'Ingénieur d\'étude et de développement', 'Ingénieur d\'études', 'Ingénieur De Construction De Réseaux', 'Ingénieur de laboratoire', 'Ingénieur de production', 'Ingénieur de recherche', 'Ingénieur de recherche produit', 'Ingénieur Déploiement De Réseau', 'Ingénieur des services techniques du matériel', 'Ingénieur des travaux', 'Ingénieur Développement De Composants', 'Ingénieur Développement Logiciels', 'Ingénieur Développement Matériel Électronique', 'ingénieur du génie rural des eaux et forêts', 'Ingénieur du génie sanitaire', 'Ingénieur du son', 'Ingénieur en chef', 'Ingénieur Intégration', 'Ingénieur logistique', 'Ingénieur Qualités Méthodes', 'Ingénieur Sécurité', 'Ingénieur Support Technique', 'Ingénieur système-réseau', 'Ingénieur Systèmes Et Réseaux', 'Ingénieur Technico-Commercial', 'Ingénieur Validation', 'Inspecteur de l\'action sanitaire et sociale', 'Inspecteur des systèmes d\'information et de communication', 'Inspecteurs du travail', 'Installateur en télécommunications', 'Intégrateur Web', 'Interprète', 'Jeunes sapeurs-pompiers', 'Journaliste', 'Journaliste d\'entreprise', 'Journaliste radio', 'Journaliste reporter', 'Juge d\'Instance', 'Juge d\'instruction', 'Juge de Grande Instance', 'Juge de l\'application des peines', 'Juge de l\'exécution', 'Juge des affaires familiales', 'Juge des enfants', 'Juriste Informatique', 'Les adjoints administratifs de la police nationale', 'Les adjoints de sécurité', 'Les Directeurs de services pénitentiaires', 'Les greffiers', 'Les personnels de la protection judiciaire de la jeunesse', 'Libraire', 'Lieutenant de police', 'Lieutenant de sapeurs-pompiers', 'Livreur', 'Maçon', 'Magasinier en chef des bibliothèques', 'Magistrat', 'Maître chien', 'Maître d\'hôtel', 'Maître de conférence', 'Maître ouvrier des établissements d\'enseignement', 'Maître-nageur sauveteur', 'Maîtres des établissements d\'enseignement privés sous contrat', 'Major de sapeur-pompier', 'Manipulateur d\'électroradiologie médicale', 'Manutentionnaire cariste', 'Maquettiste', 'Masseur kinésithérapeute', 'Mécanicien 2 roues', 'Mécanicien auto', 'Médecin', 'Médecin de l\'éducation nationale', 'Médecin inspecteur de santé publique', 'Médecin pharmacien de Sapeur-pompier', 'Médecin Territorial', 'Média-planner', 'Médiateur social', 'Menuisier', 'Météorologue', 'Métiers de la production', 'Moniteur d\'auto-école', 'Moniteur de ski', 'Moniteur éducateur', 'Monteur', 'Monteur électricien réseau edf', 'Monteur en installations thermiques chauffagiste', 'Musicien', 'Netsurfer', 'Notaire', 'Océanographe', 'Opérateur sur machine de production électrique', 'Opérateur territorial des activités physiques et sportives', 'Opératrice de saisie', 'Opticien lunetier', 'Orthophoniste', 'Orthoptiste', 'Ouvrier agricole', 'Ouvrier d\'entretien et d\'accueil (OEA)', 'Ouvrier d\'État', 'Ouvrier professionnel (OP)', 'Ouvrier professionnel, maître-ouvrier', 'Paramétreur De Progiciels', 'Pâtissier', 'Paysagiste', 'Pédicure podologue', 'Peintre en bâtiment', 'Pépinieriste', 'Personnel de surveillance', 'Personnel pénitentaire d\'insertion et de probation', 'Personnel technique de l\'administration pénitentiaire', 'Pharmacien', 'Pharmacien inspecteur de santé publique', 'Photographe', 'Pigiste', 'Pilote d\'avion', 'Planneur stratégique', 'Plombier', 'Poissonnier', 'Préparateur en pharmacie', 'Préparateur en pharmacie avec Pub', 'Professeur agrégé', 'Professeur certifié', 'Professeur d\'arts plastiques', 'Professeur d\'éducation physique et sportive', 'Professeur d\'Université', 'Professeur de lycée et collège', 'Professeur de lycée professionnel', 'Professeur de musique', 'Professeur des écoles', 'Professeur FLE', 'Projectionniste', 'Prothésiste', 'Prothésiste dentaire', 'Psychanalyste', 'Psychologue', 'Psychomotricien', 'Puéricultrice', 'Réalisateur', 'Réceptionniste', 'Rédacteur chef', 'Rédacteur en assurances', 'Rédacteur en chef', 'Rédacteur Technique', 'Rééducateur', 'Régisseur', 'Relieur-doreur', 'Responsable d\'agence bancaire', 'Responsable D\'Exploitation', 'Responsable D\'Un Système D\'Information Métier', 'Responsable de communication', 'Responsable De Compte', 'Responsable De Marketing Opérationnel', 'Responsable De Service Informatique', 'Responsable Des Études', 'Responsable logistique', 'Responsable marketing', 'Responsable Sécurité Informatique', 'Sage-Femme', 'Sapeur pompier', 'Sapeur-pompier', 'Sapeur-Pompier Volontaire', 'Scripte', 'Secrétaire administratif', 'Secrétaire administratif des affaires sanitaires et sociales', 'Secrétaire assistante', 'Secrétaire de Mairie', 'Secrétaire de rédaction', 'Secrétaire juridique', 'Secrétaire médico-sociale', 'Secrétariat administratif d\'administration centrale', 'Sécurité civile', 'Serveur de restaurant', 'Skippeur', 'Sociologue', 'Solier moquettiste', 'Sommelier', 'Soudeur', 'Standardiste', 'Stenotypiste', 'Story-boarder', 'Styliste', 'Superviseur De Hot-Line', 'Taxidermiste', 'Technicien', 'Technicien biologiste', 'Technicien Chimiste', 'Technicien de fabrication', 'Technicien de l\'éducation nationale', 'Technicien de l\'intervention sociale et familiale', 'Technicien de labo photo', 'Technicien de laboratoire', 'Technicien en analyses biomédicales', 'Technicien en mécanique', 'Technicien forestier', 'Technicien ligne haute tension', 'Technicien maintenance auto', 'Technicien Micro', 'Technicien Réseau', 'Technicien traitement déchets', 'Techniciens de recherche et de formation', 'Télévendeur', 'Toiletteur', 'Tôlier', 'Tourneur-fraiseur', 'Trader', 'Traducteur', 'Traffic Manager', 'Urbaniste', 'Vendeur en magasin', 'Vendeur En Micro-Informatique', 'Verrier', 'Vétérinaire', 'Viticulteur', 'VRP', 'Webdesigner', 'Webmarketeur', 'Webmaster', 'Webmestre', 'Webplanner'];

    /**
     * Get Faker
     *
     * @return Generator
     */
    public static function getFaker()
    {
        return self::$faker ?: self::$faker = Factory::create('fr_FR');
    }

    /**
     * Correspondance type avec image
     *
     * @var array
     */
    private $types = [
        'Jardin' => [1, 5, 6, 8, 34, 35, 36, 37],
        'Terrasse' => [1, 5, 7, 46, 47, 48, 49],
        'Piscine' => [1, 4],
        'Vue exterieur' => [1, 2, 4, 6, 7],
        'Garage' => [2, 30, 31, 32, 33],
        'Parking' => [2],
        'Entrée' => [2, 26, 27, 28, 29],
        'Home cinema' => [3],
        'Salon' => [3, 9],
        'Salle à manger' => [3, 9, 42, 43, 44, 45],
        'Barbecue' => [5],
        'Potager' => [8],
        'Cuisine' => [9, 22, 23, 24, 25],
        'Cuisine équipée' => [9],
        'Bureau' => [10, 11, 12, 13],
        'Chambre' => [14, 15, 16, 17],
        'Cheminée' => [18, 19, 20, 21],
        'Salle de bain' => [38, 39, 40, 41],
    ];

    /**
     * Create document file
     *
     * @return UploadedFile
     * @throws \Exception
     */
    protected function createDocumentFile()
    {
        $target = sys_get_temp_dir() . '/' . uniqid('document_', true) . '.pdf';
        $source = __DIR__ . '/../../Resources/private/fixtures/exemple-1.pdf';
        if (false === copy($source, $target)) {
            throw new \Exception(sprintf('Cannot copy %s', $source));
        }
        if (false === file_exists($target)) {
            throw new \Exception(sprintf('Cannot find %s', $source));
        }

        return new UploadedFile($target, 'document.pdf', 'application/pdf', filesize($source));
    }

    /**
     * Create image file
     *
     * @param null|ImageType $type
     *
     * @return UploadedFile
     * @throws \Exception
     */
    protected function createImageFile(ImageType $type = null)
    {
        if (null !== $type) {
            if (false === array_key_exists($type->getName(), $this->types)) {
                return null;
            }

            $rand = $this->types[$type->getName()][array_rand($this->types[$type->getName()])];
        } else {
            $rand = mt_rand(1, 49);
        }

        $target = sys_get_temp_dir() . '/' . uniqid('image_', true) . '.jpg';
        $source = __DIR__ . sprintf('/../../Resources/private/fixtures/exemple-%u.jpg', $rand);
        if (false === file_exists($source)) {
            throw new \Exception(sprintf('La source %s n\'existe pas', $source));
        }
        if (false === copy($source, $target)) {
            throw new \Exception(sprintf('Cannot copy %s', $source));
        }
        if (false === file_exists($target) || false === is_readable($target)) {
            throw new \Exception(sprintf('Cannot find %s', $source));
        }

        return new UploadedFile($target, 'image.jpg', 'image/jpeg', filesize($source));
    }

    /**
     * Create user
     *
     * @param ObjectManager $em
     * @param null          $role
     * @param null          $username
     * @param null          $rawPassword
     *
     * @return User
     */
    protected function createUser(ObjectManager $em, $role = null, $username = null, $rawPassword = null)
    {
        $user = new User($role ?: User::ROLE_USER);
        $user
            ->setUsername($username ?: self::getFaker()->unique()->email)
            ->setFirstname(self::getFaker()->firstName)
            ->setLastname(self::getFaker()->lastName)
            ->setRawPassword($rawPassword ?: 'demo')
            ->setProfile($this->createProfile($em));

        $em->persist($user);

        return $user;
    }

    /**
     * Create user profile
     *
     * @param ObjectManager $em Manager
     *
     * @return Profile
     */
    protected function createProfile(ObjectManager $em)
    {
        $types = $em->getRepository(AdvertType::class)->findAll();
        $sellers = $em->getRepository(Seller::class)->findAll();
        $banks = $em->getRepository(Bank::class)->findAll();

        $userProfile = new Profile();
        $userProfile
            ->setPhone(self::getFaker()->phoneNumber)
            ->setCellphone(self::getFaker()->phoneNumber)
            ->setTitle(Profile::getTitleList()[array_rand(Profile::getTitleList())])
            ->setBirthday(self::getFaker()->dateTimeBetween('-40 years', '-18years'))
            ->setPlaceOfBirth(self::getFaker()->city)
            ->setAddress(self::getFaker()->address)
            ->setZip(self::getFaker()->postcode)
            ->setCity(self::getFaker()->city)
            ->setCountry(self::getFaker()->country)
            ->setChildCount(mt_rand(0, 5))
            ->setMaritalStatus(Profile::getMaritalStatusList()[array_rand(Profile::getMaritalStatusList())])
            ->setBuyerStatus(Profile::getBuyerStatusList()[array_rand(Profile::getBuyerStatusList())])
            ->setBuyerStatusDate(self::getFaker()->boolean() ? self::getFaker()->dateTimeBetween('-40 years', '-1years') : null)
            ->setAdvertType($types[array_rand($types)])
            ->addAdvertType($types[array_rand($types)])
            ->setBudget(mt_rand(100000, 500000))
            ->setJob(self::$jobs[array_rand(self::$jobs)])
            ->setSituation(Profile::getSituationList()[array_rand(Profile::getSituationList())])
            ->setSituationDate(self::getFaker()->boolean() ? self::getFaker()->dateTimeBetween('-40 years', '-1years') : null)
            ->setFunding(Profile::getFundingList()[array_rand(Profile::getFundingList())])
            ->setBank($banks[array_rand($banks)])
            ->setLoan(mt_rand(1, 50) / 10)
            ->setLoanDuration(mt_rand(6, 120))
            ->setContribution(mt_rand(10000, 200000))
            ->setCollateral(mt_rand(0, 50000))
            ->setBridgingLoan(mt_rand(0, 20000))
            ->setIncomes(mt_rand(50000, 120000))
            ->setPreviousIncomes(mt_rand(45000, 115000))
            ->setProjectNature(Profile::getProjectNatureList()[array_rand(Profile::getProjectNatureList())])
            ->setAcquisition(Profile::getAcquisitionList()[array_rand(Profile::getAcquisitionList())])
            ->setSellBeforeBuying(self::getFaker()->boolean())
            ->setSellSinceDate(self::getFaker()->boolean() ? self::getFaker()->dateTimeBetween('-40 years', '-1years') : null)
            ->addSeller($sellers[array_rand($sellers)]);

        return $userProfile;
    }
}
