<?php

namespace Eenov\DefaultBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Eenov\DefaultBundle\Entity\Around;

/**
 * Class LoadAroundData
 *
*
 */
class LoadAroundData implements FixtureInterface, OrderedFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 1;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        $arounds = ['Supermarché', 'École', 'Parc', 'Bibliothèque'];
        foreach ($arounds as $name) {
            $around = new Around();
            $around->setName($name);

            $em->persist($around);
        }

        $em->flush();
    }
}
