<?php

namespace Eenov\DefaultBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Eenov\DefaultBundle\Entity\Equipment;

/**
 * Class LoadEquipmentData
 *
*
 */
class LoadEquipmentData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 4;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        $equipments = [
            'garage' => 'Garage',
            'jardin' => 'Jardin',
            'terrasse' => 'Terrasse',
            'piscine' => 'Piscine',
            'cuisine-equipee' => 'Cuisine équipée',
            'cave' => 'Cave',
            'clim' => 'Climatisation',
            'chauffage' => 'Chauffage',
            'connexion-internet' => 'Connexion internet',
            'interphone' => 'Interphone',
            'parking' => 'Parking',
            'cheminee' => 'Cheminée',
            'jaccuzzi' => 'Jaccuzzi',
            'ascenseur' => 'Ascenseur',
            'accessibilite-handicape' => 'Accessibilité handicapé',
            'terrain-tennis' => 'Terrain tennis',
            'buanderie' => 'Buanderie',
            'salle-de-jeu' => 'Salle de jeu',
            'home-cinema' => 'Home cinema',
            'salle-de-sport' => 'Salle de sport',
            'escaliers' => 'Escaliers',
            'barbecue' => 'Barbecue',
            'potager' => 'Potager',
            'systeme-de-securite' => 'Système de sécurité',
        ];

        foreach ($equipments as $key => $name) {
            $target = sys_get_temp_dir() . '/' . uniqid() . '.png';
            copy(__DIR__ . '/../../Resources/private/fixtures/equipements/' . $key . '.png', $target);

            $equipment = new Equipment();
            $equipment
                ->setName($name)
                ->setFile(new \SplFileInfo($target));

            $em->persist($equipment);
        }

        $em->flush();
    }
}
