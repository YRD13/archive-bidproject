<?php

namespace Eenov\DefaultBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Eenov\DefaultBundle\Entity\Post;

/**
 * Class LoadPostData
 *
*
 */
class LoadPostData extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 1;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        $text = <<<EOL
<p>
    Les 29 et 30 octobre à Paris a eu lieu la deuxième édition du salon RENT2014,
    Real Estate & New Technologies. Ce salon regroupe aujourd'hui l'ensemble des
    acteurs majeurs de l'innovation dans le secteur de l'immobilier. Vecteur de
    rencontres et d'échanges entre professionnels, le salon a été l'occasion de
    découvrir les nouvelles tendances du marché.
</p>
<p>
    C'est dans cette optique que OXIONEO a présenté sa solution inédite dédiée aux
    professionnels de l'immobilier.
</p>
<p>
    Ce salon nous a permis de rencontrer de nombreux professionnels de l'immobilier
    et futurs partenaires pour leur proposer des solutions toujours plus complètes.
    Nous avons participé aux nombreuses conférences proposées, pour comprendre
    les nouveaux enjeux pour des transactions plus qualitatives.
</p>
<p>
    Un réel succès pour OXIONEO, qui est perçu par l'ensembles des
    professionnelles comme la solution innovante pour gagner des parts de marché.
</p>
EOL;

        $target = sys_get_temp_dir() . '/' . uniqid('image_', true) . '.jpg';
        copy(__DIR__ . '/../../Resources/private/blog.jpg', $target);

        $post = new Post();
        $post
            ->setTitle('Oxioneo est au salon RENT 2014')
            ->setText($text)
            ->setFile(new \SplFileInfo($target))
            ->setIsPublished(true);

        $em->persist($post);

        $em->flush();
    }
}
