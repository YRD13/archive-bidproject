<?php

namespace Eenov\DefaultBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Eenov\DefaultBundle\Entity\Price;

/**
 * Class LoadPriceData
 *
*
 */
class LoadPriceData implements FixtureInterface, OrderedFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    function getOrder()
    {
        return 1;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        $abos = [
            ['INFINITE', 24, null, 590, false],
            ['MAX', 12, null, 790, false],
            ['ONE', 12, null, 990, true],
            ['START', 12, null, 390, false],
        ];
        foreach ($abos as $abo) {
            $package = new Price();
            $package
                ->setName($abo[0])
                ->setDuration($abo[1])
                ->setCount($abo[2])
                ->setAmount($abo[3])
                ->setPerSlot($abo[4]);

            $em->persist($package);
        }

        $em->flush();
    }
}
