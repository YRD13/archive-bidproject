<?php

namespace Eenov\DefaultBundle\Advert;

use Doctrine\ORM\EntityManager;
use EB\DoctrineBundle\Entity\FileInterface;
use Eenov\DefaultBundle\Entity\Advert;
use Eenov\DefaultBundle\Entity\AdvertBuilding;
use Eenov\DefaultBundle\Entity\AdvertGround;
use Eenov\DefaultBundle\Entity\Bid;
use Eenov\DefaultBundle\Entity\Document;
use Eenov\DefaultBundle\Entity\Image;
use Eenov\DefaultBundle\ORM\Point;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class AdvertCloner
 *
*
 */
class AdvertCloner
{
    /**
     * @var Filesystem
     */
    private $fs;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @param Filesystem    $fs Filesystem
     * @param EntityManager $em Manager
     */
    public function __construct(Filesystem $fs, EntityManager $em)
    {
        $this->fs = $fs;
        $this->em = $em;
    }

    /**
     * Duplicate advert
     *
     * @param Advert      $advert Advert
     * @param null|Advert $target Target
     *
     * @return Advert
     */
    public function duplicateAdvert(Advert $advert, Advert $target = null)
    {
        $target = $target ?: new Advert();
        $target
            ->setType($advert->getType())
            ->setNew($advert->getNew())
            ->setDisplayAddress($advert->getDisplayAddress())
            ->setAddress($advert->getAddress())
            ->setZip($advert->getZip())
            ->setCity($advert->getCity())
            ->setCountry($advert->getCountry())
            ->setMandateAgency($advert->getMandateAgency())
            ->setDescription($advert->getDescription())
            ->setYear($advert->getYear())
            ->setEstate($advert->getEstate())
            ->setRooms($advert->getRooms())
            ->setBedrooms($advert->getBedrooms())
            ->setBathrooms($advert->getBathrooms())
            ->setToilets($advert->getToilets())
            ->setShowerrooms($advert->getShowerrooms())
            ->setFloor($advert->getFloor())
            ->setHeatingSystem($advert->getHeatingSystem())
            ->setHeatingType($advert->getHeatingType())
            ->setExposure($advert->getExposure())
            ->setInternet($advert->getInternet())
            ->setDpe($advert->getDpe())
            ->setGes($advert->getGes())
            ->setPoint(new Point($advert->getPoint()->getLatitude(), $advert->getPoint()->getLongitude()))
            ->setShab($advert->getShab())
            ->setLivingRoom($advert->getLivingRoom())
            ->setField($advert->getField())
            ->setIsCondo($advert->getIsCondo())
            ->setCondoLots($advert->getCondoLots())
            ->setCondoCharges($advert->getCondoCharges())
            ->setCondoChargesRecurrence($advert->getCondoChargesRecurrence())
            ->setCondoHasOngoingProceedings($advert->getCondoHasOngoingProceedings());

        // File
        if (null !== $advert->getUri()) {
            $target
                ->setFile($this->duplicateFile($advert));
        }

        if (null === $ground = $advert->getGround()) {
            $target->setGround(null);
        } else {
            $target->setGround($this->duplicateGround($ground, $target->getGround()));
        }

        if (null === $building = $advert->getBuilding()) {
            $target->setBuilding(null);
        } else {
            $target->setBuilding($this->duplicateBuilding($building, $target->getBuilding()));
        }

        $target->getArounds()->clear();
        foreach ($advert->getArounds() as $around) {
            $target->addAround($around);
        }

        $target->getEquipments()->clear();
        foreach ($advert->getEquipments() as $equipment) {
            $target->addEquipment($equipment);
        }

        // Reset images
        foreach ($target->getImages() as $image) {
            $this->em->remove($image);
        }
        $target->getImages()->clear();
        foreach ($advert->getImages() as $image) {
            $target->addImage($this->duplicateImage($image));
        }

        // Reset documents
        foreach ($target->getDocuments() as $document) {
            $this->em->remove($document);
        }
        $target->getDocuments()->clear();
        foreach ($advert->getDocuments() as $document) {
            $target->addDocument($this->duplicateDocument($document));
        }

        return $target;
    }

    /**
     * Duplicate building
     *
     * @param AdvertBuilding      $building
     * @param null|AdvertBuilding $new
     *
     * @return AdvertBuilding
     */
    public function duplicateBuilding(AdvertBuilding $building, AdvertBuilding $new = null)
    {
        $new = $new ?: new AdvertBuilding();

        $new
            ->setAdvert($building->getAdvert());

        return $new;
    }

    /**
     * Duplicate document
     *
     * @param Document $document
     *
     * @return Document
     */
    public function duplicateDocument(Document $document)
    {
        $new = new Document();
        $new
            ->setFile($this->duplicateFile($document))
            ->setType($document->getType());

        return $new;
    }

    /**
     * Duplicate image
     *
     * @param FileInterface $file
     *
     * @return UploadedFile
     */
    public function duplicateFile(FileInterface $file)
    {
        $target = sys_get_temp_dir() . '/' . uniqid() . '.' . $file->getExtension();
        $this->fs->copy($file->getPath(), $target);

        return new UploadedFile($target, $file->getFilename(), $file->getMime(), $file->getSize());
    }

    /**
     * Duplicate ground
     *
     * @param AdvertGround      $ground
     * @param null|AdvertGround $new
     *
     * @return AdvertGround
     */
    public function duplicateGround(AdvertGround $ground, AdvertGround $new = null)
    {
        $new = $new ?: new AdvertGround();
        $new
            ->setPluZoning($ground->getPluZoning())
            ->setFrontageLength($ground->getFrontageLength())
            ->setFieldDepth($ground->getFieldDepth())
            ->setSoilNature($ground->getSoilNature())
            ->setServiced($ground->getServiced())
            ->setServicingTypes($ground->getServicingTypes());

        return $new;
    }

    /**
     * Duplicate image
     *
     * @param Image $image
     *
     * @return Image
     */
    public function duplicateImage(Image $image)
    {
        $new = new Image();
        $new
            ->setFile($this->duplicateFile($image))
            ->setType($image->getType());

        return $new;
    }

    /**
     * Progress
     *
     * @param Bid $bid
     */
    public function progress(Bid $bid)
    {
        if (null !== $slot = $bid->getSlot()) {
            // Deal with automatic agency validation
            if (null === $bid->getAgencyValidated()) {
                if ($bid->getAgency()->getId() === $slot->getAgency()->getId()) {
                    $bid->setAgencyValidated(new \DateTime());
                }
            }

            // Validate when the two of them are ok
            if (null !== $bid->getAgencyValidated() && null !== $bid->getAdminValidated()) {
                $publish = $this->duplicateAdvert($bid->getDraft(), $bid->getPublish());
                $this->em->persist($publish);
                $bid->setPublish($publish);
            }
        }
    }
}
