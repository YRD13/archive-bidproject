<?php

namespace Eenov\DefaultBundle\Advert;

use Doctrine\ORM\EntityManager;
use Eenov\DefaultBundle\Entity\Agency;
use Eenov\DefaultBundle\Entity\Slot;
use Eenov\DefaultBundle\Entity\Subscription;

/**
 * Class SlotDealer
 *
*
 */
class SlotDealer
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Is Allowed
     *
     * @param Agency $agency
     *
     * @return bool
     */
    public function isAllowed(Agency $agency)
    {
        if (0 < $this->em->getRepository(Subscription::class)->countForAgency($agency)) {
            return true;
        }

        if (0 < $this->em->getRepository(Slot::class)->countForAgency($agency)) {
            return true;
        }

        if (null !== $mother = $agency->getMother()) {
            return $this->isAllowed($mother);
        }

        return false;
    }

    /**
     * Find available slot for
     *
     * @param Agency $agency
     *
     * @return null|Slot
     */
    public function findAvailableSlotFor(Agency $agency)
    {
        if (null !== $slot = $this->findAvailableSlotForAgency($agency)) {
            return $slot;
        }

        while (null !== $agency = $agency->getMother()) {
            if (null !== $slot = $this->findAvailableSlotForAgency($agency)) {
                return $slot;
            }
        }

        return null;
    }

    /**
     * Find available slot for
     *
     * @param Agency $agency
     *
     * @return null|Slot
     */
    private function findAvailableSlotForAgency(Agency $agency)
    {
        try {
            // Find subscription for this agency
            $subscriptions = $this->em->getRepository(Subscription::class)->findLastSubscriptionsForAgency($agency);
            foreach ($subscriptions as $subscription) {
                // Unlimited subscription
                if (null === $subscription->getCount()) {
                    $started = new \DateTime();
                    $started->setTime(0, 0, 0);

                    $ended = new \DateTime();
                    $ended->setTime(23, 59, 59);

                    $slot = new Slot();
                    $slot
                        ->setStarted($started)
                        ->setEnded($ended)
                        ->setAgency($agency);

                    $subscription
                        ->addSlot($slot);

                    return $slot;
                }

                // Limited subscription
                $slots = $this->em->getRepository(Slot::class)->findLastSlotsForSubscription($subscription);
                foreach ($slots as $slot) {
                    return $slot;
                }
            }

            // Find standalone slots
            $slots = $this->em->getRepository(Slot::class)->findLastStandaloneSlots($agency);
            foreach ($slots as $slot) {
                return $slot;
            }
        } catch (\Exception $e) {
        }

        return null;
    }
}
