<?php

namespace Eenov\DefaultBundle\Controller;

use Doctrine\ORM\EntityRepository;
use Eenov\DefaultBundle\DataFixtures\ORM\AbstractFixture;
use Eenov\DefaultBundle\Entity\Access;
use Eenov\DefaultBundle\Entity\AdvertType;
use Eenov\DefaultBundle\Entity\Bank;
use Eenov\DefaultBundle\Entity\Bid;
use Eenov\DefaultBundle\Entity\Profile;
use Eenov\DefaultBundle\Entity\Seller;
use Eenov\DefaultBundle\Entity\Slot;
use Eenov\DefaultBundle\Entity\User;
use Faker\Factory;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DemoController
 *
*
 * @Route("/demonstration")
 */
class DemoController extends Controller
{
    /**
     * Index
     *
     * @param Request $request
     *
     * @return array
     * @Route()
     * @Method("GET|POST")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        if (in_array($request->getHost(), ['www.oxioneo-immo.com', 'oxioneo-immo.com'])) {
            throw $this->createNotFoundException();
        }

        $em = $this->get('doctrine.orm.default_entity_manager');

        $qb1 = function (EntityRepository $er) {
            $qb = $er->createQueryBuilder('a');

            return $qb
                ->andWhere($qb->expr()->isNotNull('a.key'))
                ->addOrderBy('a.lastname', 'ASC')
                ->addOrderBy('a.firstname', 'ASC');
        };

        $validateEmailBuilder = $this->get('form.factory')->createNamedBuilder('validate_email', 'form');
        $validateEmailBuilder->add('user', 'entity', ['label' => 'Utilisateur', 'class' => User::class, 'query_builder' => $qb1]);
        $validateEmailForm = $validateEmailBuilder->getForm();
        if ($validateEmailForm->handleRequest($request)->isSubmitted() && $validateEmailForm->isValid()) {
            $user = $validateEmailForm->get('user')->getData();
            if ($user instanceof User) {
                $user->setKey(null);
                $em->flush();
                $this->get('eenov.default_bundle.session.session')->success(sprintf('Email de %s validé !', $user));
            }

            return $this->redirectToRoute('eenov_default_demo_index');
        }

        $qb2 = function (EntityRepository $er) {
            return $er
                ->createQueryBuilder('a')
                ->addOrderBy('a.lastname', 'ASC')
                ->addOrderBy('a.firstname', 'ASC');
        };

        $completeProfileBuilder = $this->get('form.factory')->createNamedBuilder('complete_profile', 'form');
        $completeProfileBuilder->add('user', 'entity', ['label' => 'Utilisateur', 'class' => User::class, 'query_builder' => $qb2]);
        $completeProfileForm = $completeProfileBuilder->getForm();
        if ($completeProfileForm->handleRequest($request)->isSubmitted() && $completeProfileForm->isValid()) {
            $user = $completeProfileForm->get('user')->getData();
            if ($user instanceof User) {
                $types = $em->getRepository(AdvertType::class)->findAll();
                $sellers = $em->getRepository(Seller::class)->findAll();
                $banks = $em->getRepository(Bank::class)->findAll();
                $faker = Factory::create('fr_FR');

                $userProfile = $user->getProfile();
                $userProfile
                    ->setPhone($faker->phoneNumber)
                    ->setCellphone($faker->phoneNumber)
                    ->setTitle(Profile::getTitleList()[array_rand(Profile::getTitleList())])
                    ->setBirthday($faker->dateTimeBetween('-40 years', '-18years'))
                    ->setPlaceOfBirth($faker->city)
                    ->setAddress($faker->address)
                    ->setZip($faker->postcode)
                    ->setCity($faker->city)
                    ->setCountry($faker->country)
                    ->setChildCount(mt_rand(0, 5))
                    ->setMaritalStatus(Profile::getMaritalStatusList()[array_rand(Profile::getMaritalStatusList())])
                    ->setBuyerStatus(Profile::getBuyerStatusList()[array_rand(Profile::getBuyerStatusList())])
                    ->setBuyerStatusDate($faker->dateTimeBetween('-20 years', '-1years'))
                    ->setAdvertType($types[array_rand($types)])
                    ->addAdvertType($types[array_rand($types)])
                    ->setBudget(mt_rand(100000, 500000))
                    ->setJob(AbstractFixture::$jobs[array_rand(AbstractFixture::$jobs)])
                    ->setSituation(Profile::getSituationList()[array_rand(Profile::getSituationList())])
                    ->setSituationDate($faker->dateTimeBetween('-20 years', '-1years'))
                    ->setFunding(Profile::getFundingList()[array_rand(Profile::getFundingList())])
                    ->setBank($banks[array_rand($banks)])
                    ->setLoan(mt_rand(1, 50) / 10)
                    ->setLoanDuration(mt_rand(6, 120))
                    ->setContribution(mt_rand(10000, 200000))
                    ->setCollateral(mt_rand(0, 50000))
                    ->setBridgingLoan(mt_rand(0, 20000))
                    ->setIncomes(mt_rand(50000, 120000))
                    ->setPreviousIncomes(mt_rand(45000, 115000))
                    ->setProjectNature(Profile::getProjectNatureList()[array_rand(Profile::getProjectNatureList())])
                    ->setAcquisition(Profile::getAcquisitionList()[array_rand(Profile::getAcquisitionList())])
                    ->setSellBeforeBuying($faker->boolean())
                    ->setSellSinceDate($faker->dateTimeBetween('-20 years', '-1years'))
                    ->addSeller($sellers[array_rand($sellers)]);

                $this->get('doctrine.orm.default_entity_manager')->flush();
                $this->get('eenov.default_bundle.session.session')->success(sprintf('Profil de %s complété !', $user));
            }

            return $this->redirectToRoute('eenov_default_demo_index');
        }

        $qb3 = function (EntityRepository $er) {
            $qb = $er->createQueryBuilder('a');

            return $qb
                ->andWhere($qb->expr()->isNull('a.validated'))
                ->addOrderBy('a.created', 'DESC');
        };

        $validateAccessBuilder = $this->get('form.factory')->createNamedBuilder('validate_access', 'form');
        $validateAccessBuilder->add('access', 'entity', ['label' => 'Accès', 'class' => Access::class, 'query_builder' => $qb3]);
        $validateAccessForm = $validateAccessBuilder->getForm();
        if ($validateAccessForm->handleRequest($request)->isSubmitted() && $validateAccessForm->isValid()) {
            $access = $validateAccessForm->get('access')->getData();
            if ($access instanceof Access) {
                $access->validate();
                $em->flush();
                $this->get('eenov.default_bundle.session.session')->success(sprintf('Accès %s validé !', $access));
            }

            return $this->redirectToRoute('eenov_default_demo_index');
        }

        $qb4 = function (EntityRepository $er) {
            $qb = $er->createQueryBuilder('a');

            return $qb
                ->andWhere($qb->expr()->orX(
                    $qb->expr()->isNull('a.slot'),
                    $qb->expr()->isNull('a.publish')
                ))
                ->addOrderBy('a.title', 'DESC');
        };

        $validateBidBuilder = $this->get('form.factory')->createNamedBuilder('validate_bid', 'form');
        $validateBidBuilder->add('bid', 'entity', ['label' => 'Enchère', 'class' => Bid::class, 'query_builder' => $qb4]);
        $validateBidForm = $validateBidBuilder->getForm();
        if ($validateBidForm->handleRequest($request)->isSubmitted() && $validateBidForm->isValid()) {
            $bid = $validateBidForm->get('bid')->getData();
            if ($bid instanceof Bid) {
                if (null === $slot = $bid->getSlot()) {
                    $slot = new Slot();
                    $slot
                        ->setAgency($bid->getAgency())
                        ->setBid($bid)
                        ->setEnded(new \DateTime())
                        ->setStarted(new \DateTime());

                    $bid->setSlot($slot);
                }

                $bid
                    ->setAgencyValidated(new \DateTime())
                    ->setAdminValidated(new \DateTime());
                $bid->setPublish($this->get('eenov.default_bundle.advert.advert_cloner')->duplicateAdvert($bid->getDraft(), $bid->getPublish()));
                $em->flush();
                $this->get('eenov.default_bundle.session.session')->success(sprintf('Enchère %s validée !', $bid));
            }

            return $this->redirectToRoute('eenov_default_demo_index');
        }

        $choices = [
            'not-started' => 'Pas encore commencée',
            'in-progress' => 'En cours',
            'ended' => 'Terminée',
        ];

        $bids = $em->getRepository(Bid::class)->findBy([], ['title' => 'ASC']);

        $bidChoices = array_combine(array_map(function (Bid $bid) {
            return $bid->getId();
        }, $bids), array_map(function (Bid $bid) {
            return sprintf(
                '[%s] %s %s (%s %s - %s)',
                $bid->isOnline() ? 'EN LIGNE' : 'BROUILLON',
                $bid,
                $bid->getPublish() ? $bid->getPublish()->getCity() : '-',
                mb_strtoupper($bid->getSellerLastname()),
                $bid->getSellerFirstname(),
                $bid->isEnded() ? 'Terminée' : ($bid->isNotStarted() ? 'Pas commencée' : 'En cours')
            );
        }, $bids));

        $bidStatusBuilder = $this->get('form.factory')->createNamedBuilder('advert_status', 'form');
        $bidStatusBuilder->add('bid', 'choice', ['label' => 'Enchère', 'choices' => $bidChoices]);
        $bidStatusBuilder->add('status', 'choice', ['label' => 'Status', 'choices' => $choices]);
        $bidStatusForm = $bidStatusBuilder->getForm();
        if ($bidStatusForm->handleRequest($request)->isSubmitted() && $bidStatusForm->isValid()) {
            $bidId = $bidStatusForm->get('bid')->getData();
            $bid = $em->getRepository(Bid::class)->find($bidId);
            $status = $bidStatusForm->get('status')->getData();
            if ($bid instanceof Bid) {
                $started = new \DateTime();
                $ended = new \DateTime();
                $fourDay = new \DateInterval('P4D');
                $twoDay = new \DateInterval('P2D');
                $oneDay = new \DateInterval('P1D');

                if ('not-started' === $status) {
                    $started->add($twoDay);
                    $ended->add($fourDay);
                } elseif ('in-progress' === $status) {
                    $started->sub($oneDay);
                    $ended->add($oneDay);
                } else {
                    $started->sub($fourDay);
                    $ended->sub($twoDay);
                }

                $bid
                    ->setSellStarted($started)
                    ->setSellEnded($ended);

                $em->flush();
                $this->get('eenov.default_bundle.session.session')->success(sprintf('Statut de %s modifié !', $bid));
            }

            return $this->redirectToRoute('eenov_default_demo_index');
        }

        return [
            'validateEmailForm' => $validateEmailForm->createView(),
            'completeProfileForm' => $completeProfileForm->createView(),
            'validateAccessForm' => $validateAccessForm->createView(),
            'validateBidForm' => $validateBidForm->createView(),
            'bidStatusForm' => $bidStatusForm->createView(),
        ];
    }
}
