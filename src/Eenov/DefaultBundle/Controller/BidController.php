<?php

namespace Eenov\DefaultBundle\Controller;

use EB\DoctrineBundle\Paginator\PaginatorHelper;
use Eenov\DefaultBundle\Entity\Access;
use Eenov\DefaultBundle\Entity\Advert;
use Eenov\DefaultBundle\Entity\Auction;
use Eenov\DefaultBundle\Entity\Bid;
use Eenov\DefaultBundle\Entity\Contact;
use Eenov\DefaultBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class BidController
 *
*
 * @Route("/annonces")
 */
class BidController extends Controller
{
    /**
     * Index
     *
     * @param Request $request
     *
     * @return array
     * @Route("/{page}", requirements={"page":"\d+"}, defaults={"page":1})
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        // Create a filter form
        $form = $this->get('form.factory')->createNamed(null, 'eenov_default_advanced_search', [], ['method' => 'GET', 'csrf_protection' => false]);
        $filters = ($form->handleRequest($request)->isSubmitted() && $form->isValid()) ? $form->getData() : [];
        $this->get('session')->set('search', $filters);
        $filters['isPublished'] = true;

        // Get paginated results
        $em = $this->get('doctrine.orm.default_entity_manager');
        $paginatorHelper = $this->get('eb_paginator_helper');
        $paginatorHelper->setLimit(30);
        $paginator = $em->getRepository(Bid::class)->getPaginator($paginatorHelper, $filters);

        // Si pas de résultat, trouver des alternatives ...
        $morePaginator = null;
        $morePaginatorDistance = 15;
        if (null !== $where = $form->get('where')) {
            if (3 > $paginator->count()) {
                if (null !== $geocoding = $this->get('eenov.default_bundle.geo.geocoder')->getGeocoding($where->getData())) {
                    $paginatorHelper = PaginatorHelper::createEmptyInstance();
                    $paginatorHelper
                        ->setOffset(0)
                        ->setLimit(6);

                    $morePaginator = $em->getRepository(Bid::class)->getPaginatorNear(
                        $paginatorHelper,
                        $geocoding->getPoint(),
                        $morePaginatorDistance,
                        $paginator,
                        $filters
                    );
                }
            }
        }

        return [
            'paginator' => $paginator,
            'filters' => $filters,
            'morePaginator' => $morePaginator,
            'morePaginatorDistance' => $morePaginatorDistance,
        ];
    }

    /**
     * Read
     *
     * @param Request $request Request
     * @param Bid     $bid     Bid
     *
     * @return array
     * @Route("/{bid}-{slug}", requirements={"bid":"\d+"})
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid", options={"read_only":true,"allow_see":true})
     * @Method("GET")
     * @Template()
     */
    public function readAction(Request $request, Bid $bid)
    {
        $em = $this->get('doctrine.orm.default_entity_manager');

        $access = null;
        if ($this->get('security.authorization_checker')->isGranted(User::ROLE_USER)) {
            $access = $em->getRepository(Access::class)->findAccess($bid, $this->getUser());
        }

        return [
            'bid' => $bid,
            'advert' => $request->attributes->get('advert', $bid->getPublish()),
            'access' => $access,
            'accessCount' => $em->getRepository(Access::class)->countValidatedAccessesToBid($bid),
            'auctions' => $bid->isEnded() ? $em->getRepository(Auction::class)->getPaginator(new PaginatorHelper(), ['bid' => $bid]) : [],
            'latestAuction' => $bid->isEnded() ? $em->getRepository(Auction::class)->findLatestAuctionFor($bid) : null,
        ];
    }

    /**
     * Nav search
     *
     * @param Request $request
     *
     * @return array
     * @Template()
     */
    public function _searchAction(Request $request)
    {
        $search = $this->get('session')->get('search', []);
        $form = $this->get('form.factory')->createNamed(null, 'eenov_default_search', $search, ['method' => 'GET', 'csrf_protection' => false]);
        $form->handleRequest($request);

        return [
            'form' => $form->createView(),
            'filters' => $request->query->all(),
        ];
    }

    /**
     * Nav search small
     *
     * @param Request $request
     *
     * @return array
     * @Template()
     */
    public function _searchSmallAction(Request $request)
    {
        $search = $this->get('session')->get('search', []);
        $form = $this->get('form.factory')->createNamed(null, 'eenov_default_search_small', $search, ['method' => 'GET', 'csrf_protection' => false]);
        $form->handleRequest($request);

        return [
            'form' => $form->createView(),
            'filters' => $request->query->all(),
        ];
    }

    /**
     * Contact
     *
     * @param Request $request Request
     * @param Bid     $bid     Bid
     *
     * @return array
     * @Route("/{bid}-{slug}/se-faire-rappeler", requirements={"bid":"\d+"})
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid", options={"read_only":true})
     * @Method("GET|POST")
     * @Template()
     */
    public function contactAction(Request $request, Bid $bid)
    {
        if (null === $bid->getPublish()) {
            throw $this->createNotFoundException();
        }

        $contact = new Contact();
        $contact
            ->setType(Contact::TYPE_BUYER)
            ->setSubject(sprintf('#%u %s', $bid->getId(), $bid->getTitle()));
        if (null !== $user = $this->getUser()) {
            $contact
                ->setEmail($user->getUsername())
                ->setLastname($user->getLastname())
                ->setFirstname($user->getFirstname());
        }

        $form = $this->createForm('eenov_default_bid_contact', $contact);
        if ($form->handleRequest($request)->isValid()) {
            $this->get('eb_email.mailer.mailer')->send('bid_contact', $bid->getAgency()->getEmail(), [
                'bid' => $bid,
                'contact' => $contact,
            ]);
            $this->get('eb_email.mailer.mailer')->send('bid_contact_copy', $this->container->getParameter('email_sender_email'), [
                'bid' => $bid,
                'contact' => $contact,
                'agency' => $bid->getAgency(),
            ]);

            $this->get('eenov.default_bundle.session.session')->success('Votre message a bien été envoyé !');

            return $this->redirectToRoute('eenov_default_bid_read', [
                'bid' => $bid->getId(),
                'slug' => $bid->getSlug(),
            ]);
        }

        if ($request->isXmlHttpRequest()) {
            return $this->render('EenovDefaultBundle:Bid:contactXmlHttpRequest.html.twig', [
                'bid' => $bid,
                'form' => $form->createView(),
            ]);
        }

        return [
            'bid' => $bid,
            'form' => $form->createView(),
        ];
    }

    /**
     * Search
     *
     * @param Request $request
     *
     * @return array
     * @Route("/recherche-avancee")
     * @Method("GET")
     * @Template()
     */
    public function searchAction(Request $request)
    {
        $form = $this->get('form.factory')->createNamed(null, 'eenov_default_advanced_search', [], ['method' => 'GET', 'csrf_protection' => false]);
        $form->handleRequest($request);

        return [
            'form' => $form->createView(),
        ];
    }
}
