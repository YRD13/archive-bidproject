<?php

namespace Eenov\DefaultBundle\Controller;

use Eenov\DefaultBundle\Entity\Post;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class BlogController
 *
*
 * @Route("/blog")
 */
class BlogController extends Controller
{
    /**
     * Index
     *
     * @return array
     * @Route("/{page}", requirements={"page":"\d+"}, defaults={"page":1})
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $paginatorHelper = $this->get('eb_paginator_helper');
        $paginator = $this->get('doctrine.orm.default_entity_manager')->getRepository(Post::class)->getPaginator($paginatorHelper, ['isPublished' => true]);

        return [
            'paginator' => $paginator,
        ];
    }

    /**
     * Read
     *
     * @param Post $post
     *
     * @return array
     * @Route("/{post}-{slug}", requirements={"post":"\d+"})
     * @ParamConverter("post", class="EenovDefaultBundle:Post")
     * @Method("GET")
     * @Template()
     */
    public function readAction(Post $post)
    {
        return [
            'post' => $post,
        ];
    }
}
