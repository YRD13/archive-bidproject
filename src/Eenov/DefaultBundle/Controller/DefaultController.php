<?php

namespace Eenov\DefaultBundle\Controller;

use EB\DoctrineBundle\Paginator\PaginatorHelper;
use Eenov\DefaultBundle\Entity\Agency;
use Eenov\DefaultBundle\Entity\Bid;
use Eenov\DefaultBundle\Entity\Contact;
use Eenov\DefaultBundle\Entity\ContactAgency;
use Eenov\DefaultBundle\Entity\ContactBuyer;
use Eenov\DefaultBundle\Entity\ContactSeller;
use Eenov\DefaultBundle\Entity\Partner;
use Eenov\DefaultBundle\Entity\SubscriptionPack;
use Eenov\DefaultBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * Class DefaultController
 *
*
 */
class DefaultController extends Controller
{
    /**
     * About us
     *
     * @return array
     * @Route("/qui-sommes-nous")
     * @Method("GET")
     * @Template()
     */
    public function aboutUsAction()
    {
        return [];
    }

    /**
     * Offer
     *
     * @param Request $request
     *
     * @return array
     * @Route("/notre-offre-agence")
     * @Method("GET|POST")
     * @Template()
     */
    public function agencyAction(Request $request)
    {
        $form = $this->createForm('eenov_default_contact_agency', $contact = new ContactAgency());
        if ($form->handleRequest($request)->isValid()) {
            $this->get('eb_email.mailer.mailer')->send('contact_agency', $this->container->getParameter('email_contact'), [
                'contact' => $contact,
            ]);
            $this->get('eenov.default_bundle.session.session')->success('Votre message a bien été envoyé !');
            return $this->redirectToRoute('eenov_default_default_agency');
        }
        return [
            'form' => $form->createView(),
            'packs' => SubscriptionPack::getPacks(),
        ];
    }

    /**
     * Buyer
     *
     * @param Request $request
     *
     * @return array
     * @Route("/notre-offre-acquereur")
     * @Method("GET|POST")
     * @Template()
     */
    public function buyerAction(Request $request)
    {
        $form = $this->createForm('eenov_default_contact_buyer', $contact = new ContactBuyer());
        if ($form->handleRequest($request)->isValid()) {
            $this->get('eb_email.mailer.mailer')->send('contact_buyer', $this->container->getParameter('email_contact'), [
                'contact' => $contact,
            ]);

            $this->get('eenov.default_bundle.session.session')->success('Votre message a bien été envoyé !');

            return $this->redirectToRoute('eenov_default_default_buyer');
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * Cgu
     *
     * @return array
     * @Route("/conditions-utilisation-et-confidentialite")
     * @Method("GET")
     * @Template()
     */
    public function cguAction()
    {
        return [];
    }

    /**
     * Contact us
     *
     * @param Request $request
     *
     * @return array|RedirectResponse
     * @Route("/contactez-nous")
     * @Method("GET|POST")
     * @Template()
     */
    public function contactUsAction(Request $request)
    {
        $form = $this->createForm('eenov_default_contact', $contact = new Contact());
        if ($form->handleRequest($request)->isValid()) {
            $this->get('eb_email.mailer.mailer')->send('contact', $this->container->getParameter('email_contact'), [
                'contact' => $contact,
            ]);

            $this->get('eenov.default_bundle.session.session')->success('Votre message a bien été envoyé !');

            return $this->redirectToRoute('eenov_default_default_contactus');
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * FAQ
     *
     * @return array
     * @Route("/foire-aux-questions")
     * @Method("GET")
     * @Template()
     */
    public function faqAction()
    {
        return [];
    }

    /**
     * Imprint
     *
     * @return array
     * @Route("/mentions-legales")
     * @Method("GET")
     * @Template()
     */
    public function imprintAction()
    {
        return [];
    }

    /**
     * Index
     *
     * @return array
     * @Route()
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        return [
            'bids' => $this->get('doctrine.orm.default_entity_manager')->getRepository(Bid::class)->findHomeList(),
            'partners' => $this->get('doctrine.orm.default_entity_manager')->getRepository(Partner::class)->getPaginator(new PaginatorHelper()),
        ];
    }

    /**
     * Login
     *
     * @param Request $request
     *
     * @return array
     * @Route("/connexion")
     * @Method("GET")
     * @Template()
     */
    public function loginAction(Request $request)
    {
        $helper = $this->get('security.authentication_utils');

        if ($request->isXmlHttpRequest()) {
            return $this->render('EenovDefaultBundle:Default:loginXmlHttpRequest.html.twig', [
                'last_username' => $helper->getLastUsername(),
                'error' => $helper->getLastAuthenticationError(),
            ]);
        }

        return [
            'last_username' => $helper->getLastUsername(),
            'error' => $helper->getLastAuthenticationError(),
        ];
    }

    /**
     * Map files
     *
     * @return Response
     * @Route("/bundles/{slug}.css.map")
     * @Route("/bundles/{slug1}.{slug2}.css.map")
     * @Route("/bundles/{slug}.js.map")
     * @Route("/bundles/{slug1}.{slug2}.js.map")
     * @Method("GET")
     */
    public function mapAction()
    {
        return new Response();
    }

    /**
     * Motion
     *
     * @param Request $request
     *
     * @return array
     * @Route("/motion-design")
     * @Method("GET")
     * @Template()
     */
    public function motionAction(Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            return $this->render('EenovDefaultBundle:Default:motionXmlHttpRequest.html.twig');
        }

        return [];
    }

    /**
     * Password
     *
     * @param Request $request
     *
     * @return array
     * @Route("/mot-de-passe-oublie")
     * @Method("GET|POST")
     * @Template()
     */
    public function passwordAction(Request $request)
    {
        $form = $this->createForm('eenov_default_user_password');
        if ($form->handleRequest($request)->isValid()) {
            $username = $form->get('username')->getData();
            if (null !== $user = $this->get('doctrine.orm.default_entity_manager')->getRepository(User::class)->findOneBy(['username' => $username])) {
                $this->get('eb_email.mailer.mailer')->send('password', $user, [
                    'user' => $user,
                ]);
            }

            $this->get('eenov.default_bundle.session.session')->success('Si ce compte existe, vous recevrez un email d\'ici quelques instants avec les instructions pour réinitialiser votre mot de passe.');

            return $this->redirectToRoute('eenov_default_default_password');
        }

        if ($request->isXmlHttpRequest()) {
            return $this->render('EenovDefaultBundle:Default:passwordXmlHttpRequest.html.twig', [
                'form' => $form->createView(),
            ]);
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * Register
     *
     * @param Request $request
     *
     * @return array
     * @Route("/inscription")
     * @Method("GET|POST")
     * @Template()
     */
    public function registerAction(Request $request)
    {
        $form = $this->createForm('eenov_default_user', $user = new User());
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->persist($user);
            $em->flush();

            $token = new UsernamePasswordToken($user, null, 'secured_area', $user->getRoles());
            $this->get('security.token_storage')->setToken($token);

            $this->get('eenov.default_bundle.session.session')->success('Compte créé avec succès. Merci de consulter votre boite email pour passer à l\'étape suivante.');

            return $this->redirectToRoute('eenov_user_user_index');
        }

        if ($request->isXmlHttpRequest()) {
            return $this->render('EenovDefaultBundle:Default:registerXmlHttpRequest.html.twig', [
                'form' => $form->createView(),
            ]);
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * Register Agency
     *
     * @param Request $request
     *
     * @return array
     * @Route("/inscription-agence")
     * @Method("GET|POST")
     * @Template()
     */
    public function registerAgencyAction(Request $request)
    {
        // Save pack
        $this->get('session')->set('pack', $request->query->getInt('pack', 1));

        $owner = new User();
        $owner->setRole(User::ROLE_AGENCY_OWNER);

        $agency = new Agency();
        $agency->setOwner($owner);
        $agency->setCountry('France');

        $form = $this->createForm('eenov_default_agency', $agency);
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($owner);
            $em->persist($agency);
            $em->flush();

            if ($owner->getBirthday() != null)
                $owner->getProfile()->setBirthday($owner->getBirthday());
            if ($owner->getCountry() != null)
                $owner->getProfile()->setCountry($owner->getCountry());

            $em->persist($owner);
            $em->flush();

            $token = new UsernamePasswordToken($owner, null, 'secured_area', $owner->getRoles());
            $this->get('security.token_storage')->setToken($token);

            $this->get('eenov.default_bundle.session.session')->success('Compte créé avec succès. Merci de consulter votre boite email pour passer à l\'étape suivante.');

            return $this->redirectToRoute('eenov_default_subscription_index');
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * Reset
     *
     * @param Request $request Request
     * @param User    $user    User
     * @param string  $key     Key
     *
     * @return array
     * @Route("/mot-de-passe-oublie/{user}-{key}", requirements={"user":"\d+"})
     * @ParamConverter("user", class="EenovDefaultBundle:User")
     * @Method("GET|POST")
     * @Template()
     */
    public function resetAction(Request $request, User $user, $key)
    {
        if ($key !== $user->getSalt()) {
            throw $this->createNotFoundException();
        }

        $form = $this->createForm('eenov_default_user_reset', $user);
        if ($form->handleRequest($request)->isValid()) {
            $this->get('doctrine.orm.default_entity_manager')->flush();

            $this->get('eenov.default_bundle.session.session')->success('Votre mot de passe a été réinitialisé.');

            return $this->redirectToRoute('eenov_default_default_login');
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * Seller
     *
     * @param Request $request
     *
     * @return array
     * @Route("/notre-offre-vendeur")
     * @Method("GET|POST")
     * @Template()
     */
    public function sellerAction(Request $request)
    {
        $form = $this->createForm('eenov_default_contact_seller', $contact = new ContactSeller());
        if ($form->handleRequest($request)->isValid()) {
            $this->get('eb_email.mailer.mailer')->send('contact_seller', $this->container->getParameter('email_contact'), [
                'contact' => $contact,
            ]);

            $this->get('eenov.default_bundle.session.session')->success('Votre message a bien été envoyé !');

            return $this->redirectToRoute('eenov_default_default_seller');
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * Validate
     *
     * @param User   $user User
     * @param string $key  Key
     *
     * @return RedirectResponse
     * @Route("/valider-email-de-{user}-avec-{key}", requirements={"user":"\d+"})
     * @ParamConverter("user", class="EenovDefaultBundle:User")
     * @Method("GET|POST")
     */
    public function validateAction(User $user, $key)
    {
        if ($user->getKey() !== $key) {
            throw $this->createAccessDeniedException();
        }

        $user->setKey(null);
        $this->get('doctrine.orm.default_entity_manager')->flush();

        $this->get('eenov.default_bundle.session.session')->success('Email validé avec succès.');
        // Envoi Email Agence
        $this->get('eb_email.mailer.mailer')->send('email_validation_agency',
            "boris.evan@gmail.com", [
            'user' => $user,
        ]);
        return $this->redirectToRoute('eenov_user_user_index');
    }
}
