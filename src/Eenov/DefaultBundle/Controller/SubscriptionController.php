<?php

namespace Eenov\DefaultBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class SubscriptionController
 *
*
 * @Route("/abonnement")
 */
class SubscriptionController extends Controller
{
    /**
     * Index
     *
     * @param Request $request
     *
     * @return array
     * @Route()
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        return [];
    }
}
