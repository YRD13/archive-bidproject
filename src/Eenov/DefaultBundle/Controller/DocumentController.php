<?php

namespace Eenov\DefaultBundle\Controller;

use Eenov\DefaultBundle\Entity\Access;
use Eenov\DefaultBundle\Entity\Bid;
use Eenov\DefaultBundle\Entity\Document;
use Eenov\DefaultBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

/**
 * Class DocumentController
 *
*
 * @Route("/documents")
 */
class DocumentController extends Controller
{
    /**
     * Download
     *
     * @param Document $document
     *
     * @return BinaryFileResponse
     * @Route("/{document}.{_format}", requirements={"document":"\d+"})
     * @ParamConverter("document", class="EenovDefaultBundle:Document")
     * @Method("GET")
     */
    public function downloadAction(Document $document)
    {
        $authorizationChecker = $this->get('security.authorization_checker');

        // Document is public ?
        if (false === $document->getType()->getIsPublic()) {
            // Must be at least connected
            if (false === $authorizationChecker->isGranted(User::ROLE_USER)) {
                throw $this->createAccessDeniedException();
            }

            // If this is a user
            $user = $this->getUser();
            if ($user instanceof User) {
                if (User::ROLE_USER === $user->getRole()) {
                    $em = $this->get('doctrine.orm.default_entity_manager');
                    if (null === $bid = $em->getRepository(Bid::class)->findBidFromAdvert($document->getAdvert())) {
                        throw $this->createNotFoundException();
                    }
                    if (null === $access = $em->getRepository(Access::class)->findAccess($bid, $user)) {
                        throw $this->createNotFoundException();
                    }
                    if (false === $access->hasBeenValidated() || false === $access->isValid()) {
                        throw $this->createNotFoundException();
                    }
                }
            }
        }

        if (false === $this->get('filesystem')->exists($document->getPath())) {
            throw $this->createNotFoundException();
        }

        return new BinaryFileResponse($document->getPath(), 200, [
            'Content-Type' => 'application/force-download',
            'Cache-Control' => 'no-cache, private',
            'Content-Disposition' => sprintf('attachment; filename="%s"', $document->getFilename()),
            'Content-Transfer-Encoding' => 'binary',
            'Content-Length' => $document->getSize(),
        ]);
    }
}
