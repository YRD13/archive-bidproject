<?php

namespace Eenov\DefaultBundle\Request\ParamConverter;

use Doctrine\Common\Persistence\ManagerRegistry;
use Eenov\DefaultBundle\Entity\Post;
use Eenov\DefaultBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\DoctrineParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

/**
 * Class PostParamConverter
 *
*
 */
class PostParamConverter extends DoctrineParamConverter
{
    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    /**
     * @param AuthorizationCheckerInterface $authorizationChecker Authorization checker
     * @param ManagerRegistry               $registry             Registry
     */
    public function __construct(AuthorizationCheckerInterface $authorizationChecker, ManagerRegistry $registry)
    {
        parent::__construct($registry);
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * {@inheritdoc}
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        if (true === parent::apply($request, $configuration)) {
            $post = $request->attributes->get('post');
            if ($post instanceof Post) {
                // Published
                if (false === $post->getIsPublished() && false === $this->authorizationChecker->isGranted(User::ROLE_ADMIN)) {
                    throw new NotFoundHttpException();
                }

                // Slug
                if (null !== $slug = $request->attributes->get('slug')) {
                    if ($slug !== $post->getSlug()) {
                        throw new NotFoundHttpException();
                    }
                }
            }

            return false;
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(ParamConverter $configuration)
    {
        return
            'post' === $configuration->getName() &&
            parent::supports($configuration);
    }
}
