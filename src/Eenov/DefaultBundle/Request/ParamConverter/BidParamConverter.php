<?php

namespace Eenov\DefaultBundle\Request\ParamConverter;

use Doctrine\Common\Persistence\ManagerRegistry;
use Eenov\DefaultBundle\Entity\Bid;
use Eenov\DefaultBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\DoctrineParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

/**
 * Class BidParamConverter
 *
*
 */
class BidParamConverter extends DoctrineParamConverter
{
    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @param AuthorizationCheckerInterface $authorizationChecker Authorization checker
     * @param TokenStorageInterface         $tokenStorage         Token storage
     * @param ManagerRegistry               $registry             Registry
     */
    public function __construct(AuthorizationCheckerInterface $authorizationChecker, TokenStorageInterface $tokenStorage, ManagerRegistry $registry)
    {
        parent::__construct($registry);
        $this->authorizationChecker = $authorizationChecker;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * {@inheritdoc}
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        $options = $configuration->getOptions();
        $readOnly = array_key_exists('read_only', $options) && true === $options['read_only'];
        $allowSee = array_key_exists('allow_see', $options) && true === $options['allow_see'];

        if (true === parent::apply($request, $configuration)) {
            if (null !== $bid = $request->attributes->get('bid')) {
                if ($bid instanceof Bid) {
                    // Slug verification
                    if (null !== $slug = $request->attributes->get('slug')) {
                        if ($slug !== $bid->getSlug()) {
                            throw new NotFoundHttpException();
                        }
                    }

                    // Admins are OK
                    if ($this->authorizationChecker->isGranted(User::ROLE_ADMIN)) {
                        // Un admin peut demander à accéder au brouillon
                        $request->attributes->set('advert', $bid->getPublish());
                        if (null !== $seeId = $request->query->get('see')) {
                            if (false === $allowSee || $seeId !== $bid->getSeeId()) {
                                throw new NotFoundHttpException();
                            }

                            $request->attributes->set('advert', $bid->getDraft());
                        }

                        return true;
                    }

                    // Agencies are OK for there adverts
                    if ($this->authorizationChecker->isGranted($bid->getAgency()->getRole())) {
                        // Une agence peut demander à accéder à ses brouillons
                        $request->attributes->set('advert', $bid->getPublish());
                        if (null !== $seeId = $request->query->get('see')) {
                            if (false === $allowSee || $seeId !== $bid->getSeeId()) {
                                throw new NotFoundHttpException();
                            }

                            $request->attributes->set('advert', $bid->getDraft());

                            return true;
                        }

                        // Read only - For front controllers
                        if (false === $readOnly) {
                            // The bid must be one of this agency
                            if ($this->authorizationChecker->isGranted($bid->getAgency()->getRole())) {
                                return true;
                            }
                        }
                    }

                    // Else, the bid must be created and validated
                    if ($bid->isOnline()) {
                        return true;
                    }
                }
            }
        }

        throw new NotFoundHttpException();
    }

    /**
     * {@inheritdoc}
     */
    public function supports(ParamConverter $configuration)
    {
        return
            'bid' === $configuration->getName() &&
            parent::supports($configuration);
    }
}
