<?php

namespace Eenov\DefaultBundle\Request\ParamConverter;

use Doctrine\Common\Persistence\ManagerRegistry;
use Eenov\DefaultBundle\Entity\Agency;
use Eenov\DefaultBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\DoctrineParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

/**
 * Class AgencyParamConverter
 *
*
 */
class AgencyParamConverter extends DoctrineParamConverter
{
    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    /**
     * @param AuthorizationCheckerInterface $authorizationChecker Authorization checker
     * @param ManagerRegistry               $registry             Registry
     */
    public function __construct(AuthorizationCheckerInterface $authorizationChecker, ManagerRegistry $registry)
    {
        parent::__construct($registry);
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * {@inheritdoc}
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        if (true === parent::apply($request, $configuration)) {
            if (null !== $agency = $request->attributes->get('agency')) {
                if ($agency instanceof Agency) {
                    // Admins are OK
                    if ($this->authorizationChecker->isGranted(User::ROLE_ADMIN)) {
                        return true;
                    }

                    // Commercials are ok right now ...
                    if ($this->authorizationChecker->isGranted(User::ROLE_COMMERCIAL)) {
                        return true;
                    }

                    // Agencies owners
                    if ($this->authorizationChecker->isGranted($agency->getRole())) {
                        return true;
                    }
                }
            }
        }

        throw new NotFoundHttpException();
    }

    /**
     * {@inheritdoc}
     */
    public function supports(ParamConverter $configuration)
    {
        return
            'agency' === $configuration->getName() &&
            parent::supports($configuration);
    }
}
