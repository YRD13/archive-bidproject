<?php

namespace Eenov\DefaultBundle\Session;

use Symfony\Component\HttpFoundation\Session\Session as BaseSession;

/**
 * Class Session
 *
*
 */
class Session
{
    /**
     * @var BaseSession
     */
    private $session;

    /**
     * @param BaseSession $session
     */
    public function __construct(BaseSession $session)
    {
        $this->session = $session;
    }

    /**
     * Danger
     */
    public function danger()
    {
        $this->addFlash('danger', call_user_func_array('sprintf', func_get_args()));
    }

    /**
     * Info
     */
    public function info()
    {
        $this->addFlash('info', call_user_func_array('sprintf', func_get_args()));
    }

    /**
     * Success
     */
    public function success()
    {
        $this->addFlash('success', call_user_func_array('sprintf', func_get_args()));
    }

    /**
     * Warning
     */
    public function warning()
    {
        $this->addFlash('warning', call_user_func_array('sprintf', func_get_args()));
    }

    /**
     * Add flash
     *
     * @param string $level   Level
     * @param string $message Message
     */
    private function addFlash($level, $message)
    {
        $this->session->getFlashBag()->add($level, $message);
    }
}
