<?php

namespace Eenov\DefaultBundle\Model;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Doc
 *
*
 */
class Doc
{
    const APP_DIRECTORY = '../src/Eenov/DefaultBundle/Resources/public/doc/';
    const PATH_PATTERN = '/bundles/eenovdefault/doc/%s.pdf';

    const TYPE_AUTORISATION_UTILISATION_OXIONEO = 'autorisation-utilisation-oxioneo';
    const TYPE_CGU_OXIONEO = 'cgu-oxioneo';
    const TYPE_MANDAT_AGENCE_OXIONEO = 'mandat-agence-oxioneo';
    const TYPE_MANDAT_AGENCE_OXIONEO_2 = 'mandat-agence-oxioneo-2';
    const TYPE_PLAQUETTE_VENDEUR = 'plaquette-vendeurs';

    /**
     * @var string
     * @Assert\NotNull()
     * @Assert\Type("string")
     * @Assert\Choice(callback="getTypeList")
     */
    private $type;

    /**
     * @var UploadedFile
     * @Assert\NotNull()
     * @Assert\File(mimeTypes={"application/pdf"})
     */
    private $file;

    /**
     * Get Type List
     *
     * @return string[]
     */
    public static function getTypeList()
    {
        return array_keys(self::getTypeNameList());
    }

    /**
     * Get Type List
     *
     * @return string[]
     */
    public static function getTypeNameList()
    {
        return [
            self::TYPE_AUTORISATION_UTILISATION_OXIONEO => 'Autorisation d\'utilisation Oxioneo',
            self::TYPE_CGU_OXIONEO => 'CGU Oxioneo',
            self::TYPE_MANDAT_AGENCE_OXIONEO => 'Mandat Agence Oxioneo',
            self::TYPE_MANDAT_AGENCE_OXIONEO_2 => 'Mandat Agence Oxioneo 2',
            self::TYPE_PLAQUETTE_VENDEUR => 'Plaquette vendeur',
        ];
    }

    /**
     * Get Type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set Type
     *
     * @param string $type
     *
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get File
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set File
     *
     * @param UploadedFile $file
     *
     * @return $this
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;

        return $this;
    }
}
