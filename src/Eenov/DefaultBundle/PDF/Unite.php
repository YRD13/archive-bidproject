<?php

namespace Eenov\DefaultBundle\PDF;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\ProcessBuilder;

/**
 * Class Unite
 *
*
 */
class Unite
{
    /**
     * @var null|string
     */
    private $command;

    /**
     * @param null|string $command
     */
    public function __construct($command = null)
    {
        $this->command = $command;
    }

    /**
     * Get command
     *
     * @return null|string
     */
    public static function getCommand()
    {
        $pdfunite = new Process('which "pdfunite"');
        $pdfunite->run();

        if ($pdfunite->isSuccessful()) {
            return trim($pdfunite->getOutput());
        }

        return null;
    }

    /**
     * @return bool
     */
    public function canWork()
    {
        return null !== $this->command;
    }

    /**
     * @param UploadedFile[] $pdfs
     *
     * @return bool
     */
    public function canMerge(array $pdfs)
    {
        if (!$this->canWork()) {
            return false;
        }

        $mimes = array_map(function (UploadedFile $file) {
            return $file->getClientMimeType();
        }, $pdfs);

        $mimes = array_unique(array_filter($mimes));

        return
            1 === count($mimes) &&
            'application/pdf' === array_shift($mimes);
    }

    /**
     * Merge
     *
     * @param UploadedFile[] $pdfs
     *
     * @return null|UploadedFile
     */
    public function merge(array $pdfs)
    {
        if (!$this->canMerge($pdfs)) {
            return null;
        }

        $builder = new ProcessBuilder([$this->command]);
        foreach ($pdfs as $pdf) {
            $builder->add($pdf->getRealPath());
        }

        $outputFilename = uniqid() . '.pdf';
        $outputPath = sys_get_temp_dir() . DIRECTORY_SEPARATOR . $outputFilename;
        $builder->add($outputPath);

        $merge = $builder->getProcess();
        $merge->run();
        if (!$merge->isSuccessful()) {
            return null;
        }

        return new UploadedFile(
            $outputPath,
            $outputFilename,
            'application/pdf',
            filesize($outputPath),
            null,
            true
        );
    }
}
