<?php

namespace Eenov\DefaultBundle\PDF;

use Doctrine\ORM\EntityManager;
use EB\DoctrineBundle\Converter\StringConverter;
use Eenov\AgencyBundle\Promo\Code;
use Eenov\DefaultBundle\EenovDefaultBundle;
use Eenov\DefaultBundle\Entity\Agency;
use Eenov\DefaultBundle\Entity\Asset;
use Eenov\DefaultBundle\Entity\Bill as BillEntity;
use Eenov\DefaultBundle\Entity\MoneyIn;
use Eenov\DefaultBundle\Entity\Subscription;
use Eenov\DefaultBundle\Entity\SubscriptionPack;
use Knp\Snappy\GeneratorInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Templating\EngineInterface;

/**
 * Class Bill
 *
*
 */
class Bill
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var EngineInterface
     */
    private $engine;

    /**
     * @var GeneratorInterface
     */
    private $generator;

    /**
     * @var Filesystem
     */
    private $fs;

    /**
     * @var StringConverter
     */
    private $stringConverter;

    /**
     * @var Code
     */
    private $code;

    /**
     * @var float
     */
    private $tva;

    /**
     * @var string
     */
    private $condition;

    /**
     * @param EntityManager      $em              Entity manager
     * @param EngineInterface    $engine          Engine
     * @param GeneratorInterface $generator       Generator
     * @param Filesystem         $fs              Filesystem
     * @param StringConverter    $stringConverter String converter
     * @param Code               $code            Code
     * @param float              $tva             TVA
     * @param string             $condition       Condition
     */
    public function __construct(
        EntityManager $em,
        EngineInterface $engine,
        GeneratorInterface $generator,
        Filesystem $fs,
        StringConverter $stringConverter,
        Code $code,
        $tva,
        $condition
    ) {
        $this->em = $em;
        $this->engine = $engine;
        $this->generator = $generator;
        $this->fs = $fs;
        $this->stringConverter = $stringConverter;
        $this->code = $code;
        $this->tva = $tva;
        $this->condition = $condition;
    }

    /**
     * Generate
     *
     * @param Agency         $agency Agency
     * @param null|\DateTime $date   Date
     *
     * @return BillEntity
     */
    public function generate(Agency $agency, \DateTime $date = null)
    {
        // Create a date
        $firstMonthDay = $date ?: new \DateTime();
        $firstMonthDay = EenovDefaultBundle::getFirstMonthDay($firstMonthDay);
        $firstMonthDay->setTime(0, 0, 0);

        // Find last day
        $lastMonthDay = EenovDefaultBundle::getLastMonthDay($firstMonthDay);
        $lastMonthDay->setTime(23, 59, 59);

        $prices = [];

        $subscriptions = $this->em->getRepository(Subscription::class)->findSubscriptionsForAgency($agency, $firstMonthDay);
        foreach ($subscriptions as $subscription) {

            // Prorata pour le premier mois
            $previousMonthFirstDay = EenovDefaultBundle::getFirstPreviousMonthDay($firstMonthDay);
            $previousMonthLastDay = EenovDefaultBundle::getLastPreviousMonthDay($firstMonthDay);
            if ($subscription->getStarted() > $previousMonthFirstDay && $subscription->getStarted() < $previousMonthLastDay) {
                $diff = $subscription->getStarted()->diff($previousMonthLastDay);
                $d = abs($diff->d);
                $prices[] = [
                    'subscription' => sprintf('%s - %u jour%s', $subscription->getName(), $d, $d > 1 ? 's' : ''),
                    'number' => 1,
                    'price' => round(($subscription->getAmount() / (int)$previousMonthFirstDay->format('t')) * $d, 2),
                    'started' => $subscription->getStarted(),
                    'ended' => $previousMonthLastDay,
                ];
            }

            // Prorata pour le dernier mois
            if ($lastMonthDay > $subscription->getEnded()) {
                $diff = $lastMonthDay->diff($subscription->getEnded());
                $d = abs($diff->d);
                $prices[] = [
                    'subscription' => $subscription->getName(),
                    'number' => 1,
                    'price' => round(($subscription->getAmount() / (int)$lastMonthDay->format('t')) * $d, 2),
                    'started' => $firstMonthDay,
                    'ended' => $subscription->getEnded(),
                ];
            } else {
                $prices[] = [
                    'subscription' => $subscription->getName(),
                    'number' => 1,
                    'price' => $subscription->getAmount(),
                    'started' => $firstMonthDay,
                    'ended' => $lastMonthDay,
                ];
            }
        }

        $assets = $this->em->getRepository(Asset::class)->findAssetsForAgency($agency, $firstMonthDay, $lastMonthDay);
        foreach ($assets as $asset) {
            $prices[] = [
                'subscription' => 'Encours financier',
                'number' => 1,
                'price' => $asset->getAmount(),
                'started' => $firstMonthDay,
                'ended' => $lastMonthDay,
            ];
        }

        // Create a fake file in order to create a new ID
        $bill = new BillEntity();
        $bill->setAgency($agency);

        $this->em->persist($bill);
        $this->em->flush();

        // Then create the real file !
        $billLimited = clone $firstMonthDay;
        $billLimited->setDate((int)$billLimited->format('Y'), (int)$billLimited->format('m'), 15);

        $parameters = [
            'css' => file_get_contents(__DIR__ . '/../../DefaultBundle/Resources/public/css/bootstrap.min.css'),
            'logo' => base64_encode(file_get_contents(__DIR__ . '/../../DefaultBundle/Resources/public/img/bid-agency-default-nomargin.png')),
            'prices' => $prices,
            'tva' => $this->tva,
            'agency' => $agency,
            'billNumber' => $bill->getId(),
            'billStarted' => $firstMonthDay,
            'billEnded' => $lastMonthDay,
            'billCreated' => $firstMonthDay,
            'billLimited' => $billLimited,
            'billCondition' => $this->condition,
        ];

        $content = $this->engine->render('EenovAdminBundle::_bill_content.pdf.twig', $parameters);

        $options = [];
        $options['footer-html'] = $this->engine->render('EenovAdminBundle::_bill_footer.pdf.twig', $parameters);

        $path = sys_get_temp_dir() . '/' . uniqid() . '.pdf';
        $this->fs->dumpFile($path, $this->generator->getOutputFromHtml($content, $options));

        $file = new UploadedFile(
            $path,
            sprintf('facture_%s_%s.pdf', $this->stringConverter->underscore((string)$agency), $firstMonthDay->format('m-Y')),
            'application/pdf',
            filesize($path)
        );

        $bill->setFile($file);
        $this->em->flush();

        return $bill;
    }

    /**
     * Generate from money in
     *
     * @param Agency  $agency  Agency
     * @param MoneyIn $moneyIn Money In
     *
     * @return BillEntity
     */
    public function generateFromMoneyIn(Agency $agency, MoneyIn $moneyIn)
    {
        // Create a fake file in order to create a new ID
        $bill = new BillEntity();
        $bill->setAgency($agency);

        $this->em->persist($bill);
        $this->em->flush();

        $pack = SubscriptionPack::getPacks()[$moneyIn->getPack()];

        $price1 = $moneyIn->getSlotCount() * $moneyIn->getPricePerSlot();

        $realHtPricePerSlot = $moneyIn->getCode() ? $this->code->getPrice($moneyIn->getCode(), $pack) : $moneyIn->getPricePerSlot();
        $realHtPrice = $realHtPricePerSlot * $moneyIn->getSlotCount();

        $price2 = $price1 - $realHtPrice;
        $price3 = $realHtPrice;
        $price4 = $pack['vat'] * $realHtPrice / 100;
        $price5 = $moneyIn->getTransactionAmount();

        $parameters = [
            'css' => file_get_contents(__DIR__ . '/../../DefaultBundle/Resources/public/css/bootstrap.min.css'),
            'logo' => base64_encode(file_get_contents(__DIR__ . '/../../DefaultBundle/Resources/public/img/bid-agency-default-nomargin.png')),
            'bill' => $bill,
            'moneyIn' => $moneyIn,
            'agency' => $agency,
            'pack' => $pack,
            'billNumber' => $bill->getId(),
            'price1' => $price1,
            'price2' => $price2,
            'price3' => $price3,
            'price4' => $price4,
            'price5' => $price5,
        ];

        $content = $this->engine->render('EenovAdminBundle::_bill_content_money_in.pdf.twig', $parameters);

        $options = [];
        $options['footer-html'] = $this->engine->render('EenovAdminBundle::_bill_footer.pdf.twig', $parameters);

        $path = sys_get_temp_dir() . '/' . uniqid() . '.pdf';
        $this->fs->dumpFile($path, $this->generator->getOutputFromHtml($content, $options));

        $file = new UploadedFile(
            $path,
            sprintf('facture_%s_%s.pdf', $this->stringConverter->underscore((string)$agency), $bill->getCreated()->format('Ymd-His')),
            'application/pdf',
            filesize($path)
        );

        $bill->setFile($file);
        $this->em->flush();

        return $bill;
    }
}
