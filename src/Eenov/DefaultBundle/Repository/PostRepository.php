<?php

namespace Eenov\DefaultBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use EB\DoctrineBundle\Paginator\PaginatorHelper;
use Eenov\DefaultBundle\Entity\Post;

/**
 * Class PostRepository
 *
*
 */
class PostRepository extends EntityRepository
{
    /**
     * Paginator
     *
     * @param PaginatorHelper $paginatorHelper
     * @param array           $filters
     *
     * @return Post[]|Paginator
     */
    public function getPaginator(PaginatorHelper $paginatorHelper, array $filters = [])
    {
        $qb = $this->createQueryBuilder('a');

        $paginatorHelper
            ->applyLikeFilter($qb, 'title', $filters)
            ->applyEqFilter($qb, 'isPublished', $filters)
            ->applyValidatedFilter($qb, $filters);

        return $paginatorHelper->create($qb, ['created' => 'DESC']);
    }
}
