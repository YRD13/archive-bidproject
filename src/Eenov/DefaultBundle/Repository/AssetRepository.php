<?php

namespace Eenov\DefaultBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use EB\DoctrineBundle\Paginator\PaginatorHelper;
use Eenov\DefaultBundle\Entity\Agency;
use Eenov\DefaultBundle\Entity\Asset;

/**
 * Class AssetRepository
 *
*
 */
class AssetRepository extends EntityRepository
{
    /**
     * Paginator
     *
     * @param PaginatorHelper $paginatorHelper
     * @param array           $filters
     *
     * @return Asset[]|Paginator
     */
    public function getPaginator(PaginatorHelper $paginatorHelper, array $filters = [])
    {
        $qb = $this->createQueryBuilder('a');

        $paginatorHelper
            ->applyEqFilter($qb, 'agency', $filters)
            ->applyLikeFilter($qb, 'date', $filters);

        return $paginatorHelper->create($qb, ['date' => 'ASC']);
    }

    /**
     * findAssetsForAgency
     *
     * @param Agency    $agency Agency
     * @param \DateTime $date   Date
     * @param \DateTime $ended  Date
     *
     * @return Asset[]
     */
    public function findAssetsForAgency($agency, \DateTime $started, \DateTime $ended)
    {
        $qb = $this->createQueryBuilder('a');

        return $qb
            ->andWhere($qb->expr()->eq('a.agency', ':agency'))
            ->setParameter('agency', $agency)
            ->andWhere($qb->expr()->gte('a.date', ':started'))
            ->andWhere($qb->expr()->lt('a.date', ':ended'))
            ->setParameter('started', $started)
            ->setParameter('ended', $ended)
            ->getQuery()
            ->getResult();
    }
}
