<?php

namespace Eenov\DefaultBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use EB\DoctrineBundle\Paginator\PaginatorHelper;
use Eenov\DefaultBundle\Entity\Mandate;

/**
 * Class MandateRepository
 *
*
 */
class MandateRepository extends EntityRepository
{
    /**
     * Paginator
     *
     * @param PaginatorHelper $paginatorHelper
     * @param array           $filters
     *
     * @return Mandate[]|Paginator
     */
    public function getPaginator(PaginatorHelper $paginatorHelper, array $filters = [])
    {
        $qb = $this->createQueryBuilder('a');
        $qb
            ->innerJoin('a.bid', 'b');

        $paginatorHelper
            ->applyLikeFilter($qb, 'buyer', $filters)
            ->applyEqFilter($qb, 'bid', $filters);

        return $paginatorHelper->create($qb, ['created' => 'DESC']);
    }
}
