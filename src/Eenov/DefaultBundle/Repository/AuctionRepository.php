<?php

namespace Eenov\DefaultBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use EB\DoctrineBundle\Paginator\PaginatorHelper;
use Eenov\DefaultBundle\Entity\Auction;
use Eenov\DefaultBundle\Entity\Bid;
use Eenov\DefaultBundle\Entity\User;

/**
 * Class AuctionRepository
 *
*
 */
class AuctionRepository extends EntityRepository
{
    /**
     * Paginator
     *
     * @param PaginatorHelper $paginatorHelper
     * @param array           $filters
     *
     * @return Auction[]|Paginator
     */
    public function getPaginator(PaginatorHelper $paginatorHelper, array $filters = [])
    {
        $qb = $this->createQueryBuilder('a');
        $qb
            ->innerJoin('a.user', 'u')
            ->addSelect('u')
            ->innerJoin('a.bid', 'd')
            ->addSelect('d');

        $paginatorHelper
            ->applyEqFilter($qb, 'bid', $filters);

        return $paginatorHelper->create($qb, ['amount' => 'DESC']);
    }

    /**
     * Find latest auction for an advert
     *
     * @param Bid  $bid  Bid
     * @param User $user User
     *
     * @return Auction|null
     */
    public function findLatestAuctionFor(Bid $bid, User $user = null)
    {
        $qb = $this->createQueryBuilder('a');

        $qb
            ->andWhere($qb->expr()->eq('a.bid', ':bid'))
            ->setParameter('bid', $bid)
            ->andWhere($qb->expr()->eq('a.firm', ':firm'))
            ->setParameter('firm', false);

        if (null !== $user) {
            $qb
                ->andWhere($qb->expr()->eq('a.user', ':user'))
                ->setParameter('user', $user);
        }

        return $qb
            ->addOrderBy('a.created', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Find firm auction for a user and a bid
     *
     * @param User $user User
     * @param Bid  $bid  Bid
     *
     * @return null|Auction
     */
    public function findFirmAuctionFor(User $user, Bid $bid)
    {
        return $this->findOneBy([
            'user' => $user,
            'bid' => $bid,
            'firm' => true,
        ]);
    }

    /**
     * Find not firm auctions for a user and a bid
     *
     * @param User $user User
     * @param Bid  $bid  Bid
     *
     * @return Auction[]
     */
    public function findNonFirmAuctionsFor($user, $bid)
    {
        return $this->findBy([
            'user' => $user,
            'bid' => $bid,
            'firm' => false,
        ]);
    }

    /**
     * Find final auctions
     *
     * @param Bid $bid
     *
     * @return Auction[]
     */
    public function findFinalAuctions(Bid $bid)
    {
        $qb = $this->createQueryBuilder('a');

        $auctions = $qb
            ->innerJoin('a.user', 'u')
            ->addSelect('u')
            ->andWhere($qb->expr()->eq('a.bid', ':bid'))
            ->setParameter('bid', $bid)
            ->addOrderBy('a.amount', 'DESC')
            ->getQuery()
            ->getResult();

        return $auctions;

//        $finalAuctions = [];
//        foreach ($auctions as $auction) {
//            if ($auction instanceof Auction) {
//                if (!isset($finalAuctions[$auction->getUser()->getId()])) {
//                    $finalAuctions[$auction->getUser()->getId()] = $auction;
//                }
//            }
//        }
//
//        return array_values($finalAuctions);
    }
}
