<?php

namespace Eenov\DefaultBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use EB\DoctrineBundle\Paginator\PaginatorHelper;
use Eenov\DefaultBundle\Entity\Agency;
use Eenov\DefaultBundle\Entity\User;

/**
 * Class AgencyRepository
 *
*
 */
class AgencyRepository extends EntityRepository
{
    /**
     * Paginator
     *
     * @param PaginatorHelper $paginatorHelper
     * @param array           $filters
     *
     * @return Agency[]|Paginator
     */
    public function getPaginator(PaginatorHelper $paginatorHelper, array $filters = [])
    {
        $qb = $this->createQueryBuilder('a');
        $qb
            ->leftJoin('a.commercial', 'commercial')
            ->addSelect('commercial')
            ->leftJoin('a.owner', 'owner')
            ->addSelect('owner')
            ->leftJoin('a.responsible', 'responsible')
            ->addSelect('responsible')
            ->leftJoin('a.contact', 'contact')
            ->addSelect('contact');

        $paginatorHelper
            ->applyLikeFilter($qb, 'id', $filters)
            ->applyLikeFilter($qb, 'name', $filters)
            ->applyLikeFilter($qb, 'displayName', $filters)
            ->applyLikeFilter($qb, 'zip', $filters)
            ->applyLikeFilter($qb, 'phone', $filters)
            ->applyLikeFilter($qb, 'email', $filters)
            ->applyEqFilter($qb, 'status', $filters);

        if (null !== $owner = $paginatorHelper->getFilterArgument('owner', $filters)) {
            $qb
                ->andWhere($qb->expr()->orX(
                    $qb->expr()->like('owner.firstname', ':owner'),
                    $qb->expr()->like('owner.lastname', ':owner')
                ))
                ->setParameter('owner', sprintf('%%%s%%', $owner));
        }

        if (null !== $contact = $paginatorHelper->getFilterArgument('responsible', $filters)) {
            $qb
                ->andWhere($qb->expr()->orX(
                    $qb->expr()->like('responsible.firstname', ':responsible'),
                    $qb->expr()->like('responsible.lastname', ':responsible')
                ))
                ->setParameter('responsible', sprintf('%%%s%%', $contact));
        }

        if (null !== $contact = $paginatorHelper->getFilterArgument('contact', $filters)) {
            $qb
                ->andWhere($qb->expr()->orX(
                    $qb->expr()->like('contact.firstname', ':contact'),
                    $qb->expr()->like('contact.lastname', ':contact')
                ))
                ->setParameter('contact', sprintf('%%%s%%', $contact));
        }

        if (null !== $commercial = $paginatorHelper->getFilterArgument('commercial', $filters)) {
            $qb
                ->andWhere($qb->expr()->orX(
                    $qb->expr()->like('commercial.firstname', ':commercial'),
                    $qb->expr()->like('commercial.lastname', ':commercial')
                ))
                ->setParameter('commercial', sprintf('%%%s%%', $commercial));
        }

        if (null !== $commercial = $paginatorHelper->getFilterArgument('commercialObject', $filters)) {
            $qb
                ->andWhere($qb->expr()->eq('a.commercial', ':commercialObject'))
                ->setParameter('commercialObject', $commercial);
        }

        if (null !== $isHidden = $paginatorHelper->getFilterArgument('isHidden', $filters)) {
            $qb
                ->andWhere($qb->expr()->eq('a.hidden', ':hidden'))
                ->setParameter('hidden', $isHidden);
        }

        return $paginatorHelper->create($qb, ['name' => 'ASC']);
    }

    /**
     * Get Commercial First Agency
     *
     * @param User $user
     *
     * @return null|Agency
     */
    public function getCommercialFirstAgency(User $user)
    {
        $qb = $this->createQueryBuilder('a');

        return $qb
            ->leftJoin('a.commercial', 'commercial')
            ->addSelect('commercial')
            ->andWhere($qb->expr()->eq('a.commercial', ':commercial'))
            ->setParameter('commercial', $user)
            ->setFirstResult(0)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Get Commercial Agencies
     *
     * @param User $user
     *
     * @return Agency[]
     */
    public function getCommercialAgencies(User $user)
    {
        $qb = $this->createQueryBuilder('a');

        return $qb
            ->leftJoin('a.commercial', 'commercial')
            ->addSelect('commercial')
            ->andWhere($qb->expr()->eq('a.commercial', ':commercial'))
            ->setParameter('commercial', $user)
            ->getQuery()
            ->getResult();
    }
}
