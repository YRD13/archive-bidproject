<?php

namespace Eenov\DefaultBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use EB\DoctrineBundle\Paginator\PaginatorHelper;
use Eenov\DefaultBundle\Entity\Agency;
use Eenov\DefaultBundle\Entity\Bid;
use Eenov\DefaultBundle\Entity\Visit;

/**
 * Class VisitRepository
 *
*
 */
class VisitRepository extends EntityRepository
{
    /**
     * Paginator
     *
     * @param PaginatorHelper $paginatorHelper
     * @param array           $filters
     *
     * @return Visit[]|Paginator
     */
    public function getPaginator(PaginatorHelper $paginatorHelper, array $filters = [])
    {
        $qb = $this->createQueryBuilder('a');

        $paginatorHelper
            ->applyEqFilter($qb, 'bid', $filters);

        return $paginatorHelper->create($qb, ['date' => 'ASC', 'started' => 'ASC', 'ended' => 'ASC']);
    }

    /**
     * Find visits between
     *
     * @param \DateTime   $start  Start
     * @param \DateTime   $end    End
     * @param null|Bid    $bid    Bid
     * @param null|Agency $agency Agency
     *
     * @return Visit[]
     */
    public function findVisitsBetween(\DateTime $start, \DateTime $end, Bid $bid = null, Agency $agency = null)
    {
        $qb = $this->createQueryBuilder('a');
        $qb
            ->innerJoin('a.bid', 'b')
            ->innerJoin('b.agency', 'c');

        if (null !== $bid) {
            $qb
                ->andWhere($qb->expr()->eq('b', ':bid'))
                ->setParameter('bid', $bid);
        }

        if (null !== $agency) {
            $qb
                ->andWhere($qb->expr()->eq('c', ':agency'))
                ->setParameter('agency', $agency);
        }

        return $qb
            ->andWhere($qb->expr()->gte('a.date', ':start'))
            ->setParameter('start', $start)
            ->andWhere($qb->expr()->lte('a.date', ':end'))
            ->setParameter('end', $end)
            ->getQuery()
            ->getResult();
    }
}
