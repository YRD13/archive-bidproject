<?php

namespace Eenov\DefaultBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use EB\DoctrineBundle\Paginator\PaginatorHelper;
use Eenov\DefaultBundle\Entity\User;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * Class UserRepository
 *
*
 */
class UserRepository extends EntityRepository implements UserProviderInterface
{
    /**
     * Paginator
     *
     * @param PaginatorHelper $paginatorHelper
     * @param array           $filters
     *
     * @return User[]|Paginator
     */
    public function getPaginator(PaginatorHelper $paginatorHelper, array $filters = [])
    {
        $qb = $this->createQueryBuilder('a');
        $qb
            ->leftJoin('a.profile', 'b')
            ->addSelect('b')
            ->leftJoin('b.advertType', 'c')
            ->addSelect('c')
            ->leftJoin('a.ownedAgencies', 'd')
            ->addSelect('d')
            ->leftJoin('a.agencyRepresented', 'e')
            ->addSelect('e')
            ->leftJoin('a.agencyManaged', 'f')
            ->addSelect('f');

        $paginatorHelper
            ->applyLikeFilter($qb, 'id', $filters)
            ->applyLikeFilter($qb, 'firstname', $filters)
            ->applyLikeFilter($qb, 'lastname', $filters)
            ->applyLikeFilter($qb, 'username', $filters)
            ->applyEqFilter($qb, 'role', $filters);

        return $paginatorHelper->create($qb, ['username' => 'ASC']);
    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByUsername($username)
    {
        $qb = $this->createQueryBuilder('a');
        $qb
            ->leftJoin('a.profile', 'profile')
            ->addSelect('profile')
            ->leftJoin('a.ownedAgencies', 'ownedAgency')
            ->addSelect('ownedAgency')
            ->leftJoin('a.agencyRepresented', 'agencyRepresented')
            ->addSelect('agencyRepresented')
            ->leftJoin('a.agencyManaged', 'agencyManaged')
            ->addSelect('agencyManaged')
            ->andWhere($qb->expr()->eq('a.username', ':username'))
            ->setParameter('username', $username);

        if (null === $user = $qb->getQuery()->getOneOrNullResult()) {
            $e = new UsernameNotFoundException();
            $e->setUsername($username);
            throw new $e;
        }

        return $user;
    }

    /**
     * {@inheritdoc}
     */
    public function refreshUser(UserInterface $user)
    {
        if (false === $this->supportsClass(get_class($user))) {
            throw new UnsupportedUserException();
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    /**
     * {@inheritdoc}
     */
    public function supportsClass($class)
    {
        return $class === User::class || is_subclass_of($class, User::class);
    }

    /**
     * Find users created between
     *
     * @param \DateTime $from From
     * @param \DateTime $to   To
     *
     * @return User[]
     */
    public function findUsersCreatedBetween(\DateTime $from, \DateTime $to)
    {
        $qb = $this->createQueryBuilder('a');

        return $qb
            ->andWhere($qb->expr()->eq('a.role', ':role'))
            ->setParameter('role', User::ROLE_USER)
            ->andWhere($qb->expr()->gt('a.created', ':from'))
            ->setParameter('from', $from)
            ->andWhere($qb->expr()->lte('a.created', ':to'))
            ->setParameter('to', $to)
            ->getQuery()
            ->getResult();
    }
}
