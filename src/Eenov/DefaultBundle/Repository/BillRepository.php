<?php

namespace Eenov\DefaultBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use EB\DoctrineBundle\Paginator\PaginatorHelper;
use Eenov\DefaultBundle\Entity\Bill;

/**
 * Class BillRepository
 *
*
 */
class BillRepository extends EntityRepository
{
    /**
     * Paginator
     *
     * @param PaginatorHelper $paginatorHelper
     * @param array           $filters
     *
     * @return Bill[]|Paginator
     */
    public function getPaginator(PaginatorHelper $paginatorHelper, array $filters = [])
    {
        $qb = $this->createQueryBuilder('a');

        $paginatorHelper
            ->applyEqFilter($qb, 'agency', $filters);

        if (null !== $started = $paginatorHelper->getFilterArgument('started', $filters)) {
            if ($started instanceof \DateTime) {
                $started->setTime(0, 0, 0);
                $qb
                    ->andWhere($qb->expr()->gte('a.created', ':started'))
                    ->setParameter('started', $started);
            }
        }

        if (null !== $ended = $paginatorHelper->getFilterArgument('ended', $filters)) {
            if ($started instanceof \DateTime) {
                $ended->setTime(23, 59, 59);
                $qb
                    ->andWhere($qb->expr()->lte('a.created', ':ended'))
                    ->setParameter('ended', $ended);
            }
        }

        return $paginatorHelper->create($qb, ['created' => 'DESC']);
    }
}
