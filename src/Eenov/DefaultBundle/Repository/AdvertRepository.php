<?php

namespace Eenov\DefaultBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use EB\DoctrineBundle\Paginator\PaginatorHelper;
use Eenov\DefaultBundle\Entity\Advert;
use Eenov\DefaultBundle\Entity\AdvertType;
use Eenov\DefaultBundle\Entity\Agency;
use Eenov\DefaultBundle\Entity\User;

/**
 * Class AdvertRepository
 *
*
 */
class AdvertRepository extends EntityRepository
{
    /**
     * Paginator
     *
     * @param PaginatorHelper $paginatorHelper
     * @param array           $filters
     *
     * @return Advert[]|Paginator
     */
    public function getPaginator(PaginatorHelper $paginatorHelper, array $filters = [])
    {
        $qb = $this->createQueryBuilder('a');
        $qb
            ->innerJoin('a.agency', 'b')
            ->innerJoin('a.type', 'c');

        // Timeline
        /*
        $orX = $qb->expr()->orX();
        $now = new \DateTime();
        if (null !== $is = $paginatorHelper->getFilterArgument('isPast', $filters)) {
            if (true === $is) {
                $orX->add($qb->expr()->lt('a.sellEnded', ':now'));
                $qb->setParameter('now', $now);
            } else {
                $orX->add($qb->expr()->gte('a.sellEnded', ':now'));
                $qb->setParameter('now', $now);
            }
        }
        if (null !== $is = $paginatorHelper->getFilterArgument('isPast', $filters)) {
            if (true === $is) {
                $orX->add($qb->expr()->andX(
                    $qb->expr()->lte('a.sellStarted', ':now'),
                    $qb->expr()->gte('a.sellEnded', ':now')
                ));
                $qb->setParameter('now', $now);
            } else {
                $orX->add($qb->expr()->andX(
                    $qb->expr()->gte('a.sellStarted', ':now'),
                    $qb->expr()->lte('a.sellEnded', ':now')
                ));
                $qb->setParameter('now', $now);
            }
        }
        if (null !== $is = $paginatorHelper->getFilterArgument('isPast', $filters)) {
            if (true === $is) {
                $orX->add($qb->expr()->gt('a.sellStarted', ':now'));
                $qb->setParameter('now', $now);
            } else {
                $orX->add($qb->expr()->lte('a.sellStarted', ':now'));
                $qb->setParameter('now', $now);
            }
        }
        $orX->count() && $qb->andWhere($orX);
        */

        // Where
        if (null !== $where = $paginatorHelper->getFilterArgument('where', $filters)) {
            $qb
                ->andWhere($qb->expr()->orX(
                    $qb->expr()->like('a.address', ':where'),
                    $qb->expr()->like('a.zip', ':where'),
                    $qb->expr()->like('a.city', ':where'),
                    $qb->expr()->like('a.title', ':where')
                ))
                ->setParameter('where', sprintf('%%%s%%', $where));
        }

        // IDs
        if (array_key_exists('ids', $filters) && (is_array($filters['ids'])) && (0 !== count($ids = $filters['ids']))) {
            $qb
                ->andWhere($qb->expr()->in('a.id', $ids));
        }

        // Rooms
        // @todo
//        $orX = $qb->expr()->orX();
//        if (true === $paginatorHelper->getFilterArgument('rooms', $filters)) {
//            $orX->add($qb->expr()->eq('a.rooms', 1));
//        }
//        if ($orX->count()) {
//            $qb->andWhere($orX);
//        }

        // Floor min
        if (null !== $shabMin = $paginatorHelper->getFilterArgument('shabMin', $filters)) {
            $qb
                ->andWhere($qb->expr()->gte('a.shab', ':shabMin'))
                ->setParameter('shabMin', $shabMin);
        }

        // Floor max
        if (null !== $shabMax = $paginatorHelper->getFilterArgument('shabMax', $filters)) {
            $qb
                ->andWhere($qb->expr()->lte('a.shab', ':shabMax'))
                ->setParameter('shabMax', $shabMax);
        }

        // Types
        if (array_key_exists('types', $filters) && 0 !== count($filters['types'])) {
            $orX = $qb->expr()->orX();
            foreach ($filters['types'] as $type) {
                if ($type instanceof AdvertType) {
                    $orX->add($qb->expr()->eq('c.id', ':type_' . $type->getId()));
                    $qb->setParameter('type_' . $type->getId(), $type->getId());
                }
            }
            $qb->andWhere($orX);
        }

        // New
        $new = array_key_exists('new', $filters) && true === $filters['new'];
        $old = array_key_exists('old', $filters) && true === $filters['old'];
        if ($new !== $old) {
            $qb
                ->andWhere($qb->expr()->eq('a.new', ':new'))
                ->setParameter('new', $new);
        }

        // Field min
        if (null !== $fieldMin = $paginatorHelper->getFilterArgument('fieldMin', $filters)) {
            $qb
                ->andWhere($qb->expr()->gte('a.field', ':fieldMin'))
                ->setParameter('fieldMin', $fieldMin);
        }

        // Field max
        if (null !== $fieldMax = $paginatorHelper->getFilterArgument('fieldMax', $filters)) {
            $qb
                ->andWhere($qb->expr()->lte('a.field', ':fieldMax'))
                ->setParameter('fieldMax', $fieldMax);
        }

        // Price min
        if (null !== $priceMin = $paginatorHelper->getFilterArgument('priceMin', $filters)) {
            $qb
                ->andWhere($qb->expr()->isNotNull('a.firstPrice'))
                ->andWhere($qb->expr()->gte('a.firstPrice', ':priceMin'))
                ->setParameter('priceMin', $priceMin);
        }

        // Price max
        if (null !== $priceMax = $paginatorHelper->getFilterArgument('priceMax', $filters)) {
            $qb
                ->andWhere($qb->expr()->isNotNull('a.firstPrice'))
                ->andWhere($qb->expr()->lte('a.firstPrice', ':priceMax'))
                ->setParameter('priceMax', $priceMax);
        }

        // Bedrooms
        // @todo
//        $orX = $qb->expr()->orX();
//        if (true === $paginatorHelper->getFilterArgument('bedrooms', $filters)) {
//            $orX->add($qb->expr()->eq('a.bedrooms', 1));
//        }
//        if ($orX->count()) {
//            $qb->andWhere($orX);
//        }

        // Equipments
        if (array_key_exists('equipments', $filters) && 0 !== count($filters['equipments'])) {
            $qb
                ->leftJoin('a.equipments', 'equipmentssearch');

            $orX = $qb->expr()->orX();
            foreach ($filters['equipments'] as $equipment) {
                $orX->add($qb->expr()->eq('equipmentssearch.id', $equipment));
            }
            $qb->andWhere($orX);
        }

        // User
        if (null !== $user = $paginatorHelper->getFilterArgument('user', $filters)) {
            if ($user instanceof User) {
                $qb
                    ->leftJoin('b.owner', 'owner')
                    ->andWhere($qb->expr()->eq('owner.id', ':user_id'))
                    ->setParameter('user_id', $user->getId());
            }
        }

        // Agency
        if (null !== $agency = $paginatorHelper->getFilterArgument('agency', $filters)) {
            if ($agency instanceof Agency) {
                $qb
                    ->andWhere($qb->expr()->eq('b', ':agency'))
                    ->setParameter('agency', $agency);
            } elseif (is_string($agency)) {
                $qb
                    ->andWhere($qb->expr()->like('b.name', ':agency'))
                    ->setParameter('agency', sprintf('%%%s%%', $agency));
            }
        }

        // Agency name
        if (null !== $name = $paginatorHelper->getFilterArgument('agency_name', $filters)) {
            $qb
                ->andWhere($qb->expr()->like('b.name', ':agency_name'))
                ->setParameter('agency_name', sprintf('%%%s%%', $name));
        }

        // Type name
        if (null !== $name = $paginatorHelper->getFilterArgument('type_name', $filters)) {
            $qb
                ->andWhere($qb->expr()->like('c.name', ':type_name'))
                ->setParameter('type_name', sprintf('%%%s%%', $name));
        }

        // First price
        if (null !== $firstPriceMin = $paginatorHelper->getFilterArgument('firstPriceMin', $filters)) {
            $qb
                ->andWhere($qb->expr()->isNotNull('a.firstPrice'))
                ->andWhere($qb->expr()->gte('a.firstPrice', ':firstPriceMin'))
                ->setParameter('firstPriceMin', $firstPriceMin);
        }
        if (null !== $firstPriceMax = $paginatorHelper->getFilterArgument('firstPriceMax', $filters)) {
            $qb
                ->andWhere($qb->expr()->isNotNull('a.firstPrice'))
                ->andWhere($qb->expr()->lte('a.firstPrice', ':firstPriceMax'))
                ->setParameter('firstPriceMax', $firstPriceMax);
        }

        // Estimated price
        if (null !== $estimatedPriceMin = $paginatorHelper->getFilterArgument('estimatedPriceMin', $filters)) {
            $qb
                ->andWhere($qb->expr()->isNotNull('a.estimatedPrice'))
                ->andWhere($qb->expr()->gte('a.estimatedPrice', ':estimatedPriceMin'))
                ->setParameter('estimatedPriceMin', $estimatedPriceMin);
        }
        if (null !== $estimatedPriceMax = $paginatorHelper->getFilterArgument('estimatedPriceMax', $filters)) {
            $qb
                ->andWhere($qb->expr()->isNotNull('a.estimatedPrice'))
                ->andWhere($qb->expr()->lte('a.estimatedPrice', ':estimatedPriceMax'))
                ->setParameter('estimatedPriceMax', $estimatedPriceMax);
        }

        // Dates
        if (null !== $createdFrom = $paginatorHelper->getFilterArgument('created_from', $filters)) {
            if ($createdFrom instanceof \DateTime) {
                $qb
                    ->andWhere($qb->expr()->gte('a.created', ':created_from'))
                    ->setParameter('created_from', $createdFrom);
            }
        }
        if (null !== $createdTo = $paginatorHelper->getFilterArgument('created_to', $filters)) {
            if ($createdTo instanceof \DateTime) {
                $qb
                    ->andWhere($qb->expr()->lte('a.created', ':created_to'))
                    ->setParameter('created_to', $createdTo);
            }
        }
        if (null !== $updatedFrom = $paginatorHelper->getFilterArgument('updated_from', $filters)) {
            if ($updatedFrom instanceof \DateTime) {
                $qb
                    ->andWhere($qb->expr()->gte('a.updated', ':updated_from'))
                    ->setParameter('updated_from', $updatedFrom);
            }
        }
        if (null !== $updatedTo = $paginatorHelper->getFilterArgument('updated_to', $filters)) {
            if ($updatedTo instanceof \DateTime) {
                $qb
                    ->andWhere($qb->expr()->lte('a.updated', ':updated_to'))
                    ->setParameter('updated_to', $updatedTo);
            }
        }

        return $paginatorHelper->create($qb, ['sellStarted' => 'ASC']);
    }
}
