<?php

namespace Eenov\DefaultBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use EB\DoctrineBundle\Paginator\PaginatorHelper;
use Eenov\DefaultBundle\Entity\Agency;
use Eenov\DefaultBundle\Entity\Subscription;

/**
 * Class SubscriptionRepository
 *
*
 */
class SubscriptionRepository extends EntityRepository
{
    /**
     * Paginator
     *
     * @param PaginatorHelper $paginatorHelper
     * @param array           $filters
     *
     * @return Subscription[]|Paginator
     */
    public function getPaginator(PaginatorHelper $paginatorHelper, array $filters = [])
    {
        $qb = $this->createQueryBuilder('a');

        $paginatorHelper
            ->applyEqFilter($qb, 'agency', $filters);

        return $paginatorHelper->create($qb, ['created' => 'ASC']);
    }

    /**
     * findLastSubscriptionsForAgency
     *
     * @param Agency $agency
     *
     * @return Subscription[]
     */
    public function findLastSubscriptionsForAgency(Agency $agency)
    {
        $qb = $this->createQueryBuilder('a');

        return $qb
            ->innerJoin('a.agency', 'b')
            ->andWhere($qb->expr()->eq('b', ':agency'))
            ->setParameter('agency', $agency)
            ->andWhere($qb->expr()->lte('a.started', ':now'))
            ->andWhere($qb->expr()->gte('a.ended', ':now'))
            ->setParameter('now', new \DateTime())
            ->addOrderBy('a.ended', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * findOverlap
     *
     * @param Agency    $agency  Agency
     * @param \DateTime $started Started
     * @param \DateTime $ended   Ended
     *
     * @return null|Subscription
     */
    public function findOverlap(Agency $agency, \DateTime $started, \DateTime $ended)
    {
        $qb = $this->createQueryBuilder('a');

        return $qb
            ->innerJoin('a.agency', 'b')
            ->andWhere($qb->expr()->eq('b', ':agency'))
            ->setParameter('agency', $agency)
            ->andWhere($qb->expr()->orX(
                $qb->expr()->andX(
                    $qb->expr()->gte('a.started', ':started'),
                    $qb->expr()->lte('a.started', ':ended')
                ),
                $qb->expr()->andX(
                    $qb->expr()->gte('a.ended', ':started'),
                    $qb->expr()->lte('a.ended', ':ended')
                )
            ))
            ->setParameter('started', $started)
            ->setParameter('ended', $ended)
            ->addOrderBy('a.started', 'DESC')
            ->setFirstResult(0)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * findOverlap
     *
     * @param Agency    $agency  Agency
     * @param \DateTime $started Started
     * @param \DateTime $ended   Ended
     *
     * @return null|Subscription
     */
    public function findUnlimitedOverlap(Agency $agency, \DateTime $started, \DateTime $ended)
    {
        $qb = $this->createQueryBuilder('a');

        return $qb
            ->innerJoin('a.agency', 'b')
            ->andWhere($qb->expr()->eq('b', ':agency'))
            ->setParameter('agency', $agency)
            ->andWhere($qb->expr()->isNull('a.count'))
            ->andWhere($qb->expr()->orX(
                $qb->expr()->andX(
                    $qb->expr()->gte('a.started', ':started'),
                    $qb->expr()->lte('a.started', ':ended')
                ),
                $qb->expr()->andX(
                    $qb->expr()->gte('a.ended', ':started'),
                    $qb->expr()->lte('a.ended', ':ended')
                )
            ))
            ->setParameter('started', $started)
            ->setParameter('ended', $ended)
            ->addOrderBy('a.started', 'DESC')
            ->setFirstResult(0)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * findSubscriptionsForAgency
     *
     * @param Agency    $agency Agency
     * @param \DateTime $date   Date
     *
     * @return Subscription[]
     */
    public function findSubscriptionsForAgency($agency, $date)
    {
        $qb = $this->createQueryBuilder('a');

        return $qb
            ->andWhere($qb->expr()->eq('a.agency', ':agency'))
            ->setParameter('agency', $agency)
            ->andWhere($qb->expr()->lte('a.started', ':date'))
            ->andWhere($qb->expr()->gte('a.ended', ':date'))
            ->setParameter('date', $date)
            ->getQuery()
            ->getResult();
    }

    /**
     * Count For Agency
     *
     * @param Agency $agency
     *
     * @return int
     */
    public function countForAgency(Agency $agency)
    {
        $qb = $this->createQueryBuilder('subscription');

        return (int)$qb
            ->select($qb->expr()->countDistinct('subscription.id'))
            ->innerJoin('subscription.agency', 'agency')
            ->andWhere($qb->expr()->eq('agency', ':agency'))
            ->setParameter('agency', $agency)
            ->getQuery()
            ->getSingleScalarResult();
    }
}
