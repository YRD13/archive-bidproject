<?php

namespace Eenov\DefaultBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use EB\DoctrineBundle\Paginator\PaginatorHelper;
use Eenov\DefaultBundle\Entity\Agency;
use Eenov\DefaultBundle\Entity\Slot;
use Eenov\DefaultBundle\Entity\Subscription;

/**
 * Class SlotRepository
 *
*
 */
class SlotRepository extends EntityRepository
{
    /**
     * Paginator
     *
     * @param PaginatorHelper $paginatorHelper
     * @param array           $filters
     *
     * @return Slot[]|Paginator
     */
    public function getPaginator(PaginatorHelper $paginatorHelper, array $filters = [])
    {
        $qb = $this->createQueryBuilder('a');
        $qb->leftJoin('a.bid', 'bid');

        $paginatorHelper
            ->applyEqFilter($qb, 'agency', $filters);

        if (null !== $isEnded = $paginatorHelper->getFilterArgument('isEnded', $filters)) {
            if (false === $isEnded) {
                $qb
                    ->andWhere($qb->expr()->gt('a.ended', ':ended'))
                    ->setParameter('ended', new \DateTime());
            }
        }

        if (null !== $agencyOrMother = $paginatorHelper->getFilterArgument('agencyOrMother', $filters)) {
            if ($agencyOrMother instanceof Agency) {
                $orX = $qb->expr()->orX();

                $orX->add($qb->expr()->eq('a.agency', ':agencyx'));
                $qb->setParameter('agencyx', $agencyOrMother);

                $mothers = $agencyOrMother->getMothers();
                foreach ($mothers as $k => $mother) {
                    $orX->add($qb->expr()->eq('a.agency', ':agency' . $k));
                    $qb->setParameter('agency' . $k, $mother);
                }

                $qb->andWhere($orX);
            }
        }

        if (array_key_exists('bid', $filters)) {
            if (null === $filters['bid']) {
                $qb
                    ->andWhere($qb->expr()->isNull('bid.id'));
            } else {
                $qb
                    ->andWhere($qb->expr()->eq('bid.id', ':bid_id'))
                    ->setParameter('bid_id', $filters['bid']->getId());
            }
        }

        return $paginatorHelper->create($qb, ['started' => 'ASC', 'ended' => 'ASC']);
    }

    /**
     * findLastSlotsForSubscription
     *
     * @param Subscription $subscription
     *
     * @return Slot[]
     */
    public function findLastSlotsForSubscription(Subscription $subscription)
    {
        $qb = $this->createQueryBuilder('a');

        return $qb
            ->innerJoin('a.subscription', 'b')
            ->leftJoin('a.bid', 'bid')
            ->andWhere($qb->expr()->eq('b', ':subscription'))
            ->andWhere($qb->expr()->isNull('bid.id'))
            ->setParameter('subscription', $subscription)
            ->andWhere($qb->expr()->lte('a.started', ':now'))
            ->andWhere($qb->expr()->gte('a.ended', ':now'))
            ->setParameter('now', new \DateTime())
            ->addOrderBy('a.ended', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * findLastStandaloneSlots
     *
     * @param Agency $agency
     *
     * @return Slot[]
     */
    public function findLastStandaloneSlots(Agency $agency)
    {
        $qb = $this->createQueryBuilder('a');

        return $qb
            ->leftJoin('a.subscription', 'subscription')
            ->leftJoin('a.bid', 'bid')
            ->andWhere($qb->expr()->eq('a.agency', ':agency'))
            ->setParameter('agency', $agency)
            ->andWhere($qb->expr()->isNull('subscription.id'))
            ->andWhere($qb->expr()->isNull('bid.id'))
            ->andWhere($qb->expr()->lte('a.started', ':now'))
            ->andWhere($qb->expr()->gte('a.ended', ':now'))
            ->setParameter('now', new \DateTime())
            ->addOrderBy('a.ended', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * findUnusedSlotsForAgencyBetween
     *
     * @param Agency    $agency
     * @param \DateTime $start
     * @param \DateTime $end
     *
     * @return Slot[]
     */
    public function findUnusedSlotsForAgencyBetween(Agency $agency, \DateTime $start, \DateTime $end)
    {
        $qb = $this->createQueryBuilder('a');

        return $qb
            ->innerJoin('a.agency', 'b')
            ->addSelect('b')
            ->andWhere($qb->expr()->isNull('a.bid'))
            ->andWhere($qb->expr()->eq('b', ':agency'))
            ->setParameter('agency', $agency)
            ->andWhere($qb->expr()->gte('a.started', ':start'))
            ->setParameter('start', $start)
            ->andWhere($qb->expr()->gte('a.ended', ':end'))
            ->setParameter('end', $end)
            ->getQuery()
            ->getResult();
    }

    /**
     * Count For Agency
     *
     * @param Agency $agency
     *
     * @return int
     */
    public function countForAgency(Agency $agency)
    {
        $qb = $this->createQueryBuilder('slot');

        return (int)$qb
            ->select($qb->expr()->countDistinct('slot.id'))
            ->innerJoin('slot.agency', 'agency')
            ->andWhere($qb->expr()->eq('agency', ':agency'))
            ->setParameter('agency', $agency)
            ->getQuery()
            ->getSingleScalarResult();
    }
}
