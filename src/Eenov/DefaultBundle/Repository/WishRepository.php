<?php

namespace Eenov\DefaultBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use EB\DoctrineBundle\Paginator\PaginatorHelper;
use Eenov\DefaultBundle\Entity\User;
use Eenov\DefaultBundle\Entity\Wish;

/**
 * Class WishRepository
 *
*
 */
class WishRepository extends EntityRepository
{
    /**
     * Paginator
     *
     * @param PaginatorHelper $paginatorHelper
     * @param array           $filters
     *
     * @return Wish[]|Paginator
     */
    public function getPaginator(PaginatorHelper $paginatorHelper, array $filters = [])
    {
        $qb = $this->createQueryBuilder('a');
        $qb
            ->innerJoin('a.user', 'u')
            ->innerJoin('a.bid', 'd')
            ->addSelect('d')
            ->innerJoin('d.publish', 'f')
            ->addSelect('f')
            ->innerJoin('d.slot', 'g');

        $paginatorHelper
            ->applyEqFilter($qb, 'user', $filters)
            ->applyEqFilter($qb, 'bid', $filters);

        return $paginatorHelper->create($qb, ['created' => 'DESC']);
    }

    /**
     * Find wish ids for a user
     *
     * @param User $user
     *
     * @return int[]
     */
    public function findWishIdsFor(User $user)
    {
        $qb = $this->createQueryBuilder('a');

        $qb
            ->innerJoin('a.bid', 'b')
            ->select('b.id')
            ->andWhere($qb->expr()->eq('a.user', ':user'))
            ->setParameter('user', $user);

        $results = $qb
            ->getQuery()
            ->getArrayResult();

        return array_map(function (array $line) {
            return (int)$line['id'];
        }, $results);
    }
}
