<?php

namespace Eenov\DefaultBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use EB\DoctrineBundle\Paginator\PaginatorHelper;
use Eenov\DefaultBundle\Entity\DocumentType;

/**
 * Class DocumentTypeRepository
 *
*
 */
class DocumentTypeRepository extends EntityRepository
{
    /**
     * Paginator
     *
     * @param PaginatorHelper $paginatorHelper
     * @param array           $filters
     *
     * @return DocumentType[]|Paginator
     */
    public function getPaginator(PaginatorHelper $paginatorHelper, array $filters = [])
    {
        $qb = $this->createQueryBuilder('a');

        $paginatorHelper
            ->applyLikeFilter($qb, 'title', $filters)
            ->applyLikeFilter($qb, 'subtitle', $filters)
            ->applyLikeFilter($qb, 'name', $filters);

        return $paginatorHelper->create($qb, ['title' => 'ASC', 'subtitle' => 'ASC', 'name' => 'ASC']);
    }

    /**
     * Get all types ordered
     *
     * @return DocumentType[]
     */
    public function getAllTypesOrdered()
    {
        $qb = $this->createQueryBuilder('a');

        return $qb
            ->addOrderBy('a.order', 'ASC')
            ->addOrderBy('a.title', 'ASC')
            ->addOrderBy('a.subtitle', 'ASC')
            ->addOrderBy('a.name', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
