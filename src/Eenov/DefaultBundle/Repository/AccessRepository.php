<?php

namespace Eenov\DefaultBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use EB\DoctrineBundle\Paginator\PaginatorHelper;
use Eenov\DefaultBundle\Entity\Access;
use Eenov\DefaultBundle\Entity\Agency;
use Eenov\DefaultBundle\Entity\Bid;
use Eenov\DefaultBundle\Entity\User;

/**
 * Class AccessRepository
 *
*
 */
class AccessRepository extends EntityRepository
{
    /**
     * Paginator
     *
     * @param PaginatorHelper $paginatorHelper
     * @param array           $filters
     *
     * @return Access[]|Paginator
     */
    public function getPaginator(PaginatorHelper $paginatorHelper, array $filters = [])
    {
        $qb = $this->createQueryBuilder('a');
        $qb
            ->innerJoin('a.user', 'u')
            ->innerJoin('a.bid', 'd');

        $paginatorHelper
            ->applyLikeFilter($qb, 'id', $filters)
            ->applyEqFilter($qb, 'user', $filters)
            ->applyEqFilter($qb, 'bid', $filters);

        // User name
        if (null !== $userFullname = $paginatorHelper->getFilterArgument('user_fullname', $filters)) {
            $qb
                ->andWhere($qb->expr()->orX(
                    $qb->expr()->like('u.firstname', ':user_fullname'),
                    $qb->expr()->like('u.lastname', ':user_fullname')
                ))
                ->setParameter('user_fullname', sprintf('%%%s%%', $userFullname));
        }

        // Advert title
        if (null !== $advertTitle = $paginatorHelper->getFilterArgument('advert_title', $filters)) {
            $qb
                ->andWhere($qb->expr()->like('d.title', ':advert_title'))
                ->setParameter('advert_title', sprintf('%%%s%%', $advertTitle));
        }

        // Dates
        if (null !== $createdFrom = $paginatorHelper->getFilterArgument('created_from', $filters)) {
            if (is_string($createdFrom)) {
                $qb
                    ->andWhere($qb->expr()->gte('a.created', ':created_from'))
                    ->setParameter('created_from', \DateTime::createFromFormat('d/m/YHis', $createdFrom . '000000'));
            }
        }
        if (null !== $createdTo = $paginatorHelper->getFilterArgument('created_to', $filters)) {
            if (is_string($createdTo)) {
                $qb
                    ->andWhere($qb->expr()->lte('a.created', ':created_to'))
                    ->setParameter('created_to', \DateTime::createFromFormat('d/m/YHis', $createdTo . '235959'));
            }
        }
        if (null !== $dateFrom = $paginatorHelper->getFilterArgument('date_from', $filters)) {
            if (is_string($dateFrom)) {
                $qb
                    ->andWhere($qb->expr()->gte('a.date', ':date_from'))
                    ->setParameter('date_from', \DateTime::createFromFormat('d/m/YHis', $dateFrom . '000000'));
            }
        }
        if (null !== $dateTo = $paginatorHelper->getFilterArgument('date_to', $filters)) {
            if (is_string($dateTo)) {
                $qb
                    ->andWhere($qb->expr()->lte('a.date', ':date_to'))
                    ->setParameter('date_to', \DateTime::createFromFormat('d/m/YHis', $dateTo . '235959'));
            }
        }

        // Agency
        if (null !== $agency = $paginatorHelper->getFilterArgument('agency', $filters)) {
            if ($agency instanceof Agency) {
                $qb
                    ->andWhere($qb->expr()->lte('d.agency', ':agency'))
                    ->setParameter('agency', $agency);
            }
        }

        return $paginatorHelper->create($qb, ['created' => 'DESC']);
    }

    /**
     * Find access
     *
     * @param Bid  $bid  Bid
     * @param User $user User
     *
     * @return null|Access
     */
    public function findAccess(Bid $bid, User $user)
    {
        return $this->findOneBy([
            'bid' => $bid,
            'user' => $user,
        ]);
    }

    /**
     * Find accesses
     *
     * @param User $user
     *
     * @return Access[]
     */
    public function findAccesses(User $user)
    {
        $qb = $this->createQueryBuilder('a');

        return $qb
            ->innerJoin('a.bid', 'b')
            ->addSelect('b')
            ->andWhere($qb->expr()->eq('a.user', ':user'))
            ->setParameter('user', $user)
            ->andWhere($qb->expr()->isNotNull('a.validated'))
            ->andWhere($qb->expr()->isNotNull('b.sellStarted'))
            ->andWhere($qb->expr()->isNotNull('b.sellRealEnded'))
            ->andWhere($qb->expr()->lte('b.sellStarted', ':now'))
            ->andWhere($qb->expr()->gte('b.sellRealEnded', ':now'))
            ->setParameter('now', new \DateTime())
            ->addOrderBy('b.sellStarted', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * Find coming accesses
     *
     * @param User $user
     *
     * @return Access[]
     */
    public function findComingAccesses(User $user)
    {
        $qb = $this->createQueryBuilder('a');

        return $qb
            ->innerJoin('a.bid', 'b')
            ->addSelect('b')
            ->andWhere($qb->expr()->eq('a.user', ':user'))
            ->setParameter('user', $user)
            ->andWhere($qb->expr()->isNotNull('a.validated'))
            ->andWhere($qb->expr()->isNotNull('b.sellStarted'))
            ->andWhere($qb->expr()->gte('b.sellStarted', ':now'))
            ->setParameter('now', new \DateTime())
            ->addOrderBy('b.sellStarted', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * Find finished accesses
     *
     * @param User $user
     *
     * @return Access[]
     */
    public function findFinishedAccesses(User $user)
    {
        $qb = $this->createQueryBuilder('a');

        return $qb
            ->innerJoin('a.bid', 'b')
            ->addSelect('b')
            ->andWhere($qb->expr()->eq('a.user', ':user'))
            ->setParameter('user', $user)
            ->andWhere($qb->expr()->isNotNull('a.validated'))
            ->andWhere($qb->expr()->isNotNull('b.sellRealEnded'))
            ->andWhere($qb->expr()->lte('b.sellRealEnded', ':now'))
            ->setParameter('now', new \DateTime())
            ->addOrderBy('b.sellStarted', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * Find accesses
     *
     * @param Bid $bid
     *
     * @return Access[]
     */
    public function findUsers(Bid $bid)
    {
        $qb = $this->createQueryBuilder('a');

        return $qb
            ->innerJoin('a.bid', 'b')
            ->andWhere($qb->expr()->eq('b', ':bid'))
            ->setParameter('bid', $bid)
            ->andWhere($qb->expr()->isNotNull('a.validated'))
            ->getQuery()
            ->getResult();
    }

    /**
     * Find waiting accesses
     *
     * @param User $user
     *
     * @return Access[]
     */
    public function findWaitingAccesses(User $user)
    {
        $qb = $this->createQueryBuilder('a');

        return $qb
            ->innerJoin('a.bid', 'b')
            ->addSelect('b')
            ->andWhere($qb->expr()->eq('a.user', ':user'))
            ->setParameter('user', $user)
            ->andWhere($qb->expr()->isNull('a.validated'))
            ->andWhere($qb->expr()->isNotNull('b.sellStarted'))
            ->andWhere($qb->expr()->gte('b.sellStarted', ':now'))
            ->setParameter('now', new \DateTime())
            ->addOrderBy('b.sellStarted', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * Count validated accesses to bid
     *
     * @param Bid $bid
     *
     * @return int
     */
    public function countValidatedAccessesToBid(Bid $bid)
    {
        $qb = $this->createQueryBuilder('a');

        return (int)$qb
            ->select($qb->expr()->countDistinct('a.id'))
            ->innerJoin('a.bid', 'b')
            ->andWhere($qb->expr()->eq('b', ':bid'))
            ->setParameter('bid', $bid)
            ->andWhere($qb->expr()->isNotNull('a.validated'))
            ->andWhere($qb->expr()->eq('a.readonly', ':readonly'))
            ->setParameter('readonly', false)
            ->getQuery()
            ->getSingleScalarResult();
    }
}