<?php

namespace Eenov\DefaultBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use EB\DoctrineBundle\Paginator\PaginatorHelper;
use Eenov\DefaultBundle\Entity\Advert;
use Eenov\DefaultBundle\Entity\AdvertType;
use Eenov\DefaultBundle\Entity\Agency;
use Eenov\DefaultBundle\Entity\Bid;
use Eenov\DefaultBundle\Entity\Equipment;
use Eenov\DefaultBundle\Entity\User;
use Eenov\DefaultBundle\ORM\Point;

/**
 * Class BidRepository
 *
*
 */
class BidRepository extends EntityRepository
{
    /**
     * Find adverts between
     *
     * @param \DateTime   $start  Start
     * @param \DateTime   $end    End
     * @param null|User   $user   User
     * @param null|Agency $agency Agency
     *
     * @return Bid[]
     */
    public function findBidsBetween(\DateTime $start, \DateTime $end, User $user = null, Agency $agency = null)
    {
        $qb = $this->createQueryBuilder('a');

        $qb
            ->innerJoin('a.agency', 'b')
            ->innerJoin('b.owner', 'c')
            ->andWhere($qb->expr()->isNotNull('a.sellRealEnded'))
            ->andWhere($qb->expr()->gte('a.sellRealEnded', ':start'))
            ->setParameter('start', $start)
            ->andWhere($qb->expr()->isNotNull('a.sellStarted'))
            ->andWhere($qb->expr()->lte('a.sellStarted', ':end'))
            ->setParameter('end', $end);

        if (null !== $user) {
            $qb
                ->andWhere($qb->expr()->eq('c', ':user'))
                ->setParameter('user', $user);
        }

        if (null !== $agency) {
            $qb
                ->andWhere($qb->expr()->eq('b', ':agency'))
                ->setParameter('agency', $agency);
        }

        return $qb
            ->getQuery()
            ->getResult();
    }

    /**
     * Find bids between
     *
     * @param \DateTime $start
     * @param \DateTime $end
     *
     * @return Bid[]
     */
    public function findBidsEndingBetween(\DateTime $start, \DateTime $end)
    {
        $qb = $this->createQueryBuilder('a');

        return $qb
            ->innerJoin('a.slot', 'slot')
            ->addSelect('slot')
            ->innerJoin('a.publish', 'publish')
            ->addSelect('publish')
            ->andWhere($qb->expr()->isNotNull('a.sellEnded'))
            ->andWhere($qb->expr()->gt('a.sellEnded', ':start'))
            ->setParameter('start', $start)
            ->andWhere($qb->expr()->lte('a.sellEnded', ':end'))
            ->setParameter('end', $end)
            ->getQuery()
            ->getResult();
    }

    /**
     * Find bids really between
     *
     * @param \DateTime $start
     * @param \DateTime $end
     *
     * @return Bid[]
     */
    public function findBidsReallyEndingBetween(\DateTime $start, \DateTime $end)
    {
        $qb = $this->createQueryBuilder('a');

        return $qb
            ->innerJoin('a.slot', 'slot')
            ->addSelect('slot')
            ->innerJoin('a.publish', 'publish')
            ->addSelect('publish')
            ->andWhere($qb->expr()->isNotNull('a.sellRealEnded'))
            ->andWhere($qb->expr()->gt('a.sellRealEnded', ':start'))
            ->setParameter('start', $start)
            ->andWhere($qb->expr()->lte('a.sellRealEnded', ':end'))
            ->setParameter('end', $end)
            ->getQuery()
            ->getResult();
    }

    /**
     * Find bids between
     *
     * @param \DateTime $start
     * @param \DateTime $end
     *
     * @return Bid[]
     */
    public function findBidsStartingBetween(\DateTime $start, \DateTime $end)
    {
        $qb = $this->createQueryBuilder('a');

        return $qb
            ->innerJoin('a.slot', 'slot')
            ->addSelect('slot')
            ->innerJoin('a.publish', 'publish')
            ->addSelect('publish')
            ->andWhere($qb->expr()->isNotNull('a.sellStarted'))
            ->andWhere($qb->expr()->gt('a.sellStarted', ':start'))
            ->setParameter('start', $start)
            ->andWhere($qb->expr()->lte('a.sellStarted', ':end'))
            ->setParameter('end', $end)
            ->getQuery()
            ->getResult();
    }

    /**
     * findHomeList
     *
     * @return Bid[]|Paginator
     */
    public function findHomeList()
    {
        return $this->getPaginator(new PaginatorHelper(15), [
            'isPublished' => true,
        ]);
    }

    /**
     * Paginator
     *
     * @param PaginatorHelper $paginatorHelper
     * @param array           $filters
     *
     * @return Bid[]|Paginator
     */
    public function getPaginator(PaginatorHelper $paginatorHelper, array $filters = [])
    {
        $qb = $this->createQueryBuilder('a');
        $qb
            ->innerJoin('a.agency', 'agency')
            ->addSelect('agency')
            ->innerJoin('a.draft', 'draft')
            ->addSelect('draft')
            ->innerJoin('draft.type', 'draft_type')
            ->addSelect('draft_type')
            ->leftJoin('a.publish', 'publish')
            ->addSelect('publish')
            ->leftJoin('publish.type', 'type')
            ->addSelect('type')
            ->leftJoin('a.slot', 'slot')
            ->addSelect('slot');

        $paginatorHelper
            ->applyLikeFilter($qb, 'id', $filters)
            ->applyLikeFilter($qb, 'title', $filters);

        // Is published
        if (true === $paginatorHelper->getFilterArgument('isPublished', $filters)) {
            $filters['hasSlot'] = true;
            $filters['published'] = true;
        }

        // Is waiting for validation
        if (true === $paginatorHelper->getFilterArgument('isWaitingForValidation', $filters)) {
            $qb->andWhere($qb->expr()->orX(
                $qb->expr()->isNull('slot.id'),
                $qb->expr()->isNull('a.agencyValidated'),
                $qb->expr()->isNull('a.adminValidated')
            ));
        }

        // Slot
        if (true === $paginatorHelper->getFilterArgument('hasSlot', $filters)) {
            $qb->andWhere($qb->expr()->isNotNull('a.slot'));
        }
        if (false === $paginatorHelper->getFilterArgument('hasSlot', $filters)) {
            $qb->andWhere($qb->expr()->isNull('a.slot'));
        }

        // Seller
        if (null !== $seller = $paginatorHelper->getFilterArgument('seller', $filters)) {
            $qb
                ->andWhere($qb->expr()->orX(
                    $qb->expr()->like('a.sellerFirstname', ':seller'),
                    $qb->expr()->like('a.sellerLastname', ':seller')
                ))
                ->setParameter('seller', sprintf('%%%s%%', $seller));
        }

        // Location
        if (null !== $location = $paginatorHelper->getFilterArgument('location', $filters)) {
            $qb
                ->andWhere($qb->expr()->orX(
                    $qb->expr()->like('draft.zip', ':location'),
                    $qb->expr()->like('draft.city', ':location'),
                    $qb->expr()->like('draft.country', ':location')
                ))
                ->setParameter('location', sprintf('%%%s%%', $location));
        }

        // Published
        if (true === $paginatorHelper->getFilterArgument('published', $filters)) {
            $qb->andWhere($qb->expr()->isNotNull('publish.id'));
        }

        // Timeline
        if (null !== $isPast = $paginatorHelper->getFilterArgument('isPast', $filters)) {
            if (false === $isPast) {
                $qb
                    ->andWhere($qb->expr()->gte('a.sellRealEnded', ':now'))
                    ->setParameter('now', new \DateTime());
            }
        }

        // Where
        if (null !== $where = $paginatorHelper->getFilterArgument('where', $filters)) {
            $qb
                ->andWhere($qb->expr()->orX(
                    $qb->expr()->like('publish.address', ':where'),
                    $qb->expr()->like('publish.zip', ':where'),
                    $qb->expr()->like('publish.city', ':where')
                ))
                ->setParameter('where', sprintf('%%%s%%', $where));
        }

        // IDs
        if (array_key_exists('ids', $filters) && (is_array($filters['ids'])) && (0 !== count($ids = $filters['ids']))) {
            $qb
                ->andWhere($qb->expr()->in('a.id', $ids));
        }

        // Rooms Min
        if (null !== $roomsMin = $paginatorHelper->getFilterArgument('roomsMin', $filters)) {
            $qb
                ->andWhere($qb->expr()->gte('publish.rooms', ':roomsMin'))
                ->setParameter('roomsMin', $roomsMin);
        }

        // Rooms Max
        if (null !== $roomsMax = $paginatorHelper->getFilterArgument('roomsMax', $filters)) {
            $qb
                ->andWhere($qb->expr()->lte('publish.rooms', ':roomsMax'))
                ->setParameter('roomsMax', $roomsMax);
        }

        // Types
        if (array_key_exists('types', $filters) && 0 !== count($filters['types'])) {
            $orX = $qb->expr()->orX();
            foreach ($filters['types'] as $type) {
                if ($type instanceof AdvertType) {
                    $orX->add($qb->expr()->eq('type.id', ':type_' . $type->getId()));
                    $qb->setParameter('type_' . $type->getId(), $type->getId());
                }
            }
            $qb->andWhere($orX);
        }

        // New
        $new = array_key_exists('new', $filters) && true === $filters['new'];
        $old = array_key_exists('old', $filters) && true === $filters['old'];
        if ($new !== $old) {
            $qb
                ->andWhere($qb->expr()->eq('publish.new', ':new'))
                ->setParameter('new', $new);
        }

        // Field min
        if (null !== $fieldMin = $paginatorHelper->getFilterArgument('fieldMin', $filters)) {
            $qb
                ->andWhere($qb->expr()->gte('publish.field', ':fieldMin'))
                ->setParameter('fieldMin', $fieldMin);
        }

        // Field max
        if (null !== $fieldMax = $paginatorHelper->getFilterArgument('fieldMax', $filters)) {
            $qb
                ->andWhere($qb->expr()->lte('publish.field', ':fieldMax'))
                ->setParameter('fieldMax', $fieldMax);
        }

        // Price min
        if (null !== $priceMin = $paginatorHelper->getFilterArgument('priceMin', $filters)) {
            $qb
                ->andWhere($qb->expr()->isNotNull('a.firstPrice'))
                ->andWhere($qb->expr()->gte('a.firstPrice', ':priceMin'))
                ->setParameter('priceMin', $priceMin);
        }

        // Price max
        if (null !== $priceMax = $paginatorHelper->getFilterArgument('priceMax', $filters)) {
            $qb
                ->andWhere($qb->expr()->isNotNull('a.firstPrice'))
                ->andWhere($qb->expr()->lte('a.firstPrice', ':priceMax'))
                ->setParameter('priceMax', $priceMax);
        }

        // Shab min
        if (null !== $shabMin = $paginatorHelper->getFilterArgument('shabMin', $filters)) {
            $qb
                ->andWhere($qb->expr()->gte('publish.shab', ':shabMin'))
                ->setParameter('shabMin', $shabMin);
        }

        // Shab max
        if (null !== $shabMax = $paginatorHelper->getFilterArgument('shabMax', $filters)) {
            $qb
                ->andWhere($qb->expr()->lte('publish.shab', ':shabMax'))
                ->setParameter('shabMax', $shabMax);
        }

        // Equipments
        if (isset($filters['equipments']) && 0 !== count($filters['equipments'])) {
            $equipmentIds = array_map(function (Equipment $equipment) {
                return $equipment->getId();
            }, $filters['equipments']->toArray());

            $qb
                ->leftJoin('publish.equipments', 'equipmentssearch')
                ->andWhere($qb->expr()->in('equipmentssearch.id', $equipmentIds));
        }

        // User
        if (null !== $user = $paginatorHelper->getFilterArgument('user', $filters)) {
            if ($user instanceof User) {
                $qb
                    ->leftJoin('agency.owner', 'owner')
                    ->andWhere($qb->expr()->eq('owner.id', ':user_id'))
                    ->setParameter('user_id', $user->getId());
            }
        }

        // Agency
        if (null !== $agency = $paginatorHelper->getFilterArgument('agency', $filters)) {
            if ($agency instanceof Agency) {
                $qb
                    ->andWhere($qb->expr()->eq('agency', ':agency'))
                    ->setParameter('agency', $agency);
            } elseif (is_string($agency)) {
                $qb
                    ->andWhere($qb->expr()->like('agency.name', ':agency'))
                    ->setParameter('agency', sprintf('%%%s%%', $agency));
            }
        }

        // Agency name
        if (null !== $name = $paginatorHelper->getFilterArgument('agency_name', $filters)) {
            $qb
                ->andWhere($qb->expr()->like('agency.name', ':agency_name'))
                ->setParameter('agency_name', sprintf('%%%s%%', $name));
        }

        // Type name
        if (null !== $name = $paginatorHelper->getFilterArgument('type_name', $filters)) {
            $qb
                ->andWhere($qb->expr()->like('type.name', ':type_name'))
                ->setParameter('type_name', sprintf('%%%s%%', $name));
        }

        // First price
        if (null !== $firstPriceMin = $paginatorHelper->getFilterArgument('firstPriceMin', $filters)) {
            $qb
                ->andWhere($qb->expr()->isNotNull('a.firstPrice'))
                ->andWhere($qb->expr()->gte('a.firstPrice', ':firstPriceMin'))
                ->setParameter('firstPriceMin', $firstPriceMin);
        }
        if (null !== $firstPriceMax = $paginatorHelper->getFilterArgument('firstPriceMax', $filters)) {
            $qb
                ->andWhere($qb->expr()->isNotNull('a.firstPrice'))
                ->andWhere($qb->expr()->lte('a.firstPrice', ':firstPriceMax'))
                ->setParameter('firstPriceMax', $firstPriceMax);
        }

        // Estimated price
        if (null !== $estimatedPriceMin = $paginatorHelper->getFilterArgument('estimatedPriceMin', $filters)) {
            $qb
                ->andWhere($qb->expr()->isNotNull('publish.estimatedPrice'))
                ->andWhere($qb->expr()->gte('publish.estimatedPrice', ':estimatedPriceMin'))
                ->setParameter('estimatedPriceMin', $estimatedPriceMin);
        }
        if (null !== $estimatedPriceMax = $paginatorHelper->getFilterArgument('estimatedPriceMax', $filters)) {
            $qb
                ->andWhere($qb->expr()->isNotNull('publish.estimatedPrice'))
                ->andWhere($qb->expr()->lte('publish.estimatedPrice', ':estimatedPriceMax'))
                ->setParameter('estimatedPriceMax', $estimatedPriceMax);
        }

        // Dates
        if (null !== $createdFrom = $paginatorHelper->getFilterArgument('created_from', $filters)) {
            if ($createdFrom instanceof \DateTime) {
                $qb
                    ->andWhere($qb->expr()->gte('publish.created', ':created_from'))
                    ->setParameter('created_from', $createdFrom);
            }
        }
        if (null !== $createdTo = $paginatorHelper->getFilterArgument('created_to', $filters)) {
            if ($createdTo instanceof \DateTime) {
                $qb
                    ->andWhere($qb->expr()->lte('publish.created', ':created_to'))
                    ->setParameter('created_to', $createdTo);
            }
        }
        if (null !== $updatedFrom = $paginatorHelper->getFilterArgument('updated_from', $filters)) {
            if ($updatedFrom instanceof \DateTime) {
                $qb
                    ->andWhere($qb->expr()->gte('publish.updated', ':updated_from'))
                    ->setParameter('updated_from', $updatedFrom);
            }
        }
        if (null !== $updatedTo = $paginatorHelper->getFilterArgument('updated_to', $filters)) {
            if ($updatedTo instanceof \DateTime) {
                $qb
                    ->andWhere($qb->expr()->lte('publish.updated', ':updated_to'))
                    ->setParameter('updated_to', $updatedTo);
            }
        }

        if (null === $by = $paginatorHelper->getOrderBy()) {
            $qb
                ->addSelect('(CASE WHEN a.sellRealEnded > :orderNowDate THEN 1 ELSE 0 END) AS HIDDEN orderIsEnded')
                ->addOrderBy('orderIsEnded', 'DESC')
                ->addSelect('(ABS(UNIX_TIMESTAMP(a.sellStarted) - :orderAbsNowTimestamp)) AS HIDDEN orderAbsTimestamp')
                ->addOrderBy('orderAbsTimestamp', 'ASC')
                ->setParameter('orderAbsNowTimestamp', time())
                ->setParameter('orderNowDate', date('Y-m-d H:i:s'));
        }

        return $paginatorHelper->create($qb, ['sellStarted' => 'ASC']);
    }

    /**
     * Find bid from advert
     *
     * @param Advert $advert
     *
     * @return null|Bid
     */
    public function findBidFromAdvert(Advert $advert)
    {
        $qb = $this->createQueryBuilder('a');

        return $qb
            ->leftJoin('a.draft', 'b')
            ->leftJoin('a.publish', 'c')
            ->orWhere($qb->expr()->eq('b.id', ':id'))
            ->orWhere($qb->expr()->eq('c.id', ':id'))
            ->setParameter('id', $advert->getId())
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Get Paginator Near
     *
     * @param PaginatorHelper $paginatorHelper  Paginator helper
     * @param Point           $point            Point
     * @param int             $km               Kilometers
     * @param Paginator       $currentPaginator Paginator
     * @param array           $filters          Filters
     *
     * @return null|Advert[]|Paginator
     */
    public function getPaginatorNear(PaginatorHelper $paginatorHelper, Point $point, $km = 10, Paginator $currentPaginator, array $filters = [])
    {

        $sql = <<<EOL
SELECT bid.id
FROM Bid bid
INNER JOIN Advert advert ON bid.publish_id = advert.id 
WHERE MBRContains
(
    LineString
    (
        Point(:latitude_min, :longitude_min),
        Point(:latitude_max, :longitude_max)
    ),
    advert.point
)
EOL;

        $kmPerDegree = 111.1;
        $cosRadiansLat = cos(deg2rad($point->getLatitude()));
        $kmPerDegreeForLongitude = $kmPerDegree / $cosRadiansLat;

        $longitudeMin = $point->getLongitude() - ($km / $kmPerDegreeForLongitude);
        $longitudeMax = $point->getLongitude() + ($km / $kmPerDegreeForLongitude);
        $latitudeMin = $point->getLatitude() - ($km / $kmPerDegree);
        $latitudeMax = $point->getLatitude() + ($km / $kmPerDegree);

        $statement = $this->getEntityManager()->getConnection()->prepare($sql);
        $statement->execute([
            'longitude_min' => $longitudeMin,
            'longitude_max' => $longitudeMax,
            'latitude_min' => $latitudeMin,
            'latitude_max' => $latitudeMax,
        ]);

        $currentIds = array_map(function (Bid $bid) {
            return $bid->getId();
        }, iterator_to_array($currentPaginator->getIterator()));

        $ids = array_filter(array_map(function (array $row) use ($currentIds) {
            $id = (int)$row['id'];
            if (in_array($id, $currentIds)) {
                return null;
            }

            return $id;
        }, $statement->fetchAll()));

        if (0 === count($ids)) {
            return null;
        }

        unset($filters['where']);
        $filters['ids'] = $ids;

        return $this->getPaginator($paginatorHelper, $filters);
    }
}
