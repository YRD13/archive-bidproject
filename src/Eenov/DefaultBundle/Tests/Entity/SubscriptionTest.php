<?php

namespace Eenov\DefaultBundle\Tests\Entity;

use Eenov\DefaultBundle\Entity\Agency;
use Eenov\DefaultBundle\Entity\Price;
use Eenov\DefaultBundle\Entity\Subscription;

/**
 * Class SubscriptionTest
 *
*
 */
class SubscriptionTest extends \PHPUnit_Framework_TestCase
{
    public function testPopulate()
    {
        $price = new Price();
        $price
            ->setAmount(0)
            ->setCount(1)
            ->setDuration(1)
            ->setName('test')
            ->setPerSlot(false);

        $subscription = new Subscription();
        $subscription
            ->setAgency(new Agency())
            ->setStarted(\DateTime::createFromFormat('d/m/Y', '01/01/2015'))
            ->populate($price);

        $this->assertEquals(1, $subscription->getSlots()->count());
    }
}
