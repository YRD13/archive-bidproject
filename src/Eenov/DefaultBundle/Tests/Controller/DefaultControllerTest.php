<?php

namespace Eenov\DefaultBundle\Tests\Controller;

use Eenov\DefaultBundle\Entity\Contact;
use Eenov\DefaultBundle\Entity\User;
use Eenov\DefaultBundle\Test\WebTestCase;

/**
 * Class DefaultControllerTest
 *
*
 */
class DefaultControllerTest extends WebTestCase
{
    public function testAboutUsAction()
    {
        $client = static::createClient();
        $client->request('GET', '/qui-sommes-nous');
        $this->assertResponse($client);
    }

    public function testAgencyAction()
    {
        $client = static::createClient();
        $client->request('GET', '/notre-offre-agence');
        $this->assertResponse($client);

        $form = $client->getCrawler()->filterXPath('//*[@id="form-contact"]/div[2]/button')->form();
        $client->enableProfiler();
        $client->submit($form, [
            'eenov_default_contact_agency[name]' => 'Agence',
            'eenov_default_contact_agency[chief]' => 'BOB DILAN',
            'eenov_default_contact_agency[id]' => 15261331,
            'eenov_default_contact_agency[firstname]' => 'John',
            'eenov_default_contact_agency[lastname]' => 'DOE',
            'eenov_default_contact_agency[email]' => 'john.doe@som.com',
            'eenov_default_contact_agency[phone]' => '0123456789',
            'eenov_default_contact_agency[subject]' => 'Subject',
            'eenov_default_contact_agency[message]' => 'Message for oxioneo-immo.com',
        ]);
        $this->assertRedirect($client);
        $this->assertEquals(1, $this->getSwiftmailerMessageCount($client));
        $client->followRedirect();
        $this->assertResponse($client);
    }

    public function testBuyerAction()
    {
        $client = static::createClient();
        $client->request('GET', '/notre-offre-acquereur');
        $this->assertResponse($client);

        $form = $client->getCrawler()->filterXPath('//*[@id="form-contact"]/div/button')->form();
        $client->enableProfiler();
        $client->submit($form, [
            'eenov_default_contact_buyer[firstname]' => 'John',
            'eenov_default_contact_buyer[lastname]' => 'DOE',
            'eenov_default_contact_buyer[email]' => 'john.doe@som.com',
            'eenov_default_contact_buyer[phone]' => '0123456789',
            'eenov_default_contact_buyer[subject]' => 'Subject',
            'eenov_default_contact_buyer[message]' => 'Message for oxioneo-immo.com',
        ]);
        $this->assertRedirect($client);
        $this->assertEquals(1, $this->getSwiftmailerMessageCount($client));
        $client->followRedirect();
        $this->assertResponse($client);
    }

    public function testCguAction()
    {
        $client = static::createClient();
        $client->request('GET', '/conditions-utilisation-et-confidentialite');
        $this->assertResponse($client);
    }

    public function testContactUsAction()
    {
        $client = static::createClient();
        $client->request('GET', '/contactez-nous');
        $this->assertResponse($client);

        $form = $client->getCrawler()->filterXPath('//*[@id="form-contact"]/div[2]/button')->form();
        $client->enableProfiler();
        $client->submit($form, [
            'eenov_default_contact[type]' => Contact::TYPE_BUYER,
            'eenov_default_contact[firstname]' => 'John',
            'eenov_default_contact[lastname]' => 'DOE',
            'eenov_default_contact[email]' => 'john.doe@som.com',
            'eenov_default_contact[phone]' => '0123456789',
            'eenov_default_contact[subject]' => 'Subject',
            'eenov_default_contact[message]' => 'Message for oxioneo-immo.com',
        ]);
        $this->assertRedirect($client);
        $this->assertEquals(1, $this->getSwiftmailerMessageCount($client));
        $client->followRedirect();
        $this->assertResponse($client);
    }

    public function testFaqAction()
    {
        $client = static::createClient();
        $client->request('GET', '/foire-aux-questions');
        $this->assertResponse($client);
    }

    public function testImprintAction()
    {
        $client = static::createClient();
        $client->request('GET', '/mentions-legales');
        $this->assertResponse($client);
    }

    public function testIndexAction()
    {
        $client = static::createClient();
        $client->request('GET', '/');
        $this->assertResponse($client);
    }

    public function testLoginAction()
    {
        $client = static::createClient();
        $client->request('GET', '/connexion');
        $this->assertResponse($client);

        $form = $client->getCrawler()->filterXPath('//*[@id="_submit"]')->form();
        $client->enableProfiler();
        $client->submit($form, [
            '_username' => 'user1@oxioneo.com',
            '_password' => 'aa',
        ]);
        $this->assertRedirect($client);
        $client->followRedirect();
        $this->assertResponse($client);
    }

    public function testLoginXmlHttpRequestAction()
    {
        $client = static::createClient();
        $client->request('GET', '/connexion', [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);
        $this->assertResponse($client);

        $form = $client->getCrawler()->filterXPath('//*[@id="login"]/div[3]/input')->form();
        $client->enableProfiler();
        $client->submit($form, [
            '_username' => 'user1@oxioneo.com',
            '_password' => 'aa',
        ]);
        $this->assertRedirect($client);
        $client->followRedirect();
        $this->assertResponse($client);
    }

    public function testMapAction()
    {
        $client = static::createClient();
        $client->request('GET', '/bundles/test.css.map');
        $this->assertResponse($client);

        $client = static::createClient();
        $client->request('GET', '/bundles/test.test.css.map');
        $this->assertResponse($client);

        $client = static::createClient();
        $client->request('GET', '/bundles/test.js.map');
        $this->assertResponse($client);

        $client = static::createClient();
        $client->request('GET', '/bundles/test.test.js.map');
        $this->assertResponse($client);
    }

    public function testMotionAction()
    {
        $client = static::createClient();
        $client->request('GET', '/motion-design');
        $this->assertResponse($client);
    }

    public function testMotionXmlHttpRequestAction()
    {
        $client = static::createClient();
        $client->request('GET', '/motion-design', [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);
        $this->assertResponse($client);
    }

    public function testPasswordAction()
    {
        $client = static::createClient();
        $client->request('GET', '/mot-de-passe-oublie');
        $this->assertResponse($client);

        $form = $client->getCrawler()->filterXPath('//*[@id="register"]/form/div[2]/button')->form();
        $client->enableProfiler();
        $client->submit($form, [
            'eenov_default_user_password[username]' => 'user1@oxioneo.com',
        ]);
        $this->assertRedirect($client);
        $this->assertEquals(1, $this->getSwiftmailerMessageCount($client));
        $client->followRedirect();
        $this->assertResponse($client);
    }

    public function testPasswordXmlHttpRequestAction()
    {
        $client = static::createClient();
        $client->request('GET', '/mot-de-passe-oublie', [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);
        $this->assertResponse($client);

        $form = $client->getCrawler()->filterXPath('//div/div/div/div[2]/form/div/div[2]/button')->form();
        $client->enableProfiler();
        $client->submit($form, [
            'eenov_default_user_password[username]' => 'user1@oxioneo.com',
        ]);
        $this->assertRedirect($client);
        $this->assertEquals(1, $this->getSwiftmailerMessageCount($client));
        $client->followRedirect();
        $this->assertResponse($client);
    }

    public function testRegisterAction()
    {
        $client = static::createClient();
        $client->request('GET', '/inscription');
        $this->assertResponse($client);

        $form = $client->getCrawler()->filterXPath('//*[@id="register"]/form/div[7]/button')->form();
        $client->enableProfiler();
        $client->submit($form, [
            'eenov_default_user[username]' => 'john.doe@som.com',
            'eenov_default_user[firstname]' => 'John',
            'eenov_default_user[lastname]' => 'DOE',
            'eenov_default_user[rawPassword][first]' => 'thisisatest',
            'eenov_default_user[rawPassword][second]' => 'thisisatest',
        ]);
        $this->assertRedirect($client);
        $this->assertEquals(1, $this->getSwiftmailerMessageCount($client));
        $client->followRedirect();
        $this->assertResponse($client);
    }

    public function testRegisterXmlHttpRequestAction()
    {
        $client = static::createClient();
        $client->request('GET', '/inscription', [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);
        $this->assertResponse($client);

        $form = $client->getCrawler()->filterXPath('//*[@id="register"]/div[7]/button')->form();
        $client->enableProfiler();
        $client->submit($form, [
            'eenov_default_user[username]' => 'john.doe@som.com',
            'eenov_default_user[firstname]' => 'John',
            'eenov_default_user[lastname]' => 'DOE',
            'eenov_default_user[rawPassword][first]' => 'thisisatest',
            'eenov_default_user[rawPassword][second]' => 'thisisatest',
        ]);
        $this->assertRedirect($client);
        $this->assertEquals(1, $this->getSwiftmailerMessageCount($client));
        $client->followRedirect();
        $this->assertResponse($client);
    }

    public function testResetAction()
    {
        $client = static::createClient();

        $user = $client->getContainer()->get('doctrine.orm.default_entity_manager')->getRepository(User::class)->findOneBy(['username' => 'user1@oxioneo.com']);

        $client->request('GET', sprintf('/mot-de-passe-oublie/%u-%s', $user->getId(), $user->getSalt()));
        $this->assertResponse($client);

        $form = $client->getCrawler()->filterXPath('//*[@id="register"]/form/div[3]/button')->form();
        $client->enableProfiler();
        $client->submit($form, [
            'eenov_default_user_reset[rawPassword][first]' => 'thisisanothertest',
            'eenov_default_user_reset[rawPassword][second]' => 'thisisanothertest',
        ]);
        $this->assertRedirect($client);
        $this->assertEquals(1, $this->getSwiftmailerMessageCount($client));
        $client->followRedirect();
        $this->assertResponse($client);
    }

    public function testResetErrorAction()
    {
        $client = static::createClient();
        $client->request('GET', '/mot-de-passe-oublie/1-f6g51hdf1ghdf5g1h45hd');
        $this->assertFalse($client->getResponse()->isSuccessful());
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
    }

    public function testSellerAction()
    {
        $client = static::createClient();
        $client->request('GET', '/notre-offre-vendeur');
        $this->assertResponse($client);

        $form = $client->getCrawler()->filterXPath('//*[@id="form-contact"]/div/button')->form();
        $client->enableProfiler();
        $client->submit($form, [
            'eenov_default_contact_seller[firstname]' => 'John',
            'eenov_default_contact_seller[lastname]' => 'DOE',
            'eenov_default_contact_seller[email]' => 'john.doe@som.com',
            'eenov_default_contact_seller[phone]' => '0123456789',
            'eenov_default_contact_seller[subject]' => 'Subject',
            'eenov_default_contact_seller[message]' => 'Message for oxioneo-immo.com',
        ]);
        $this->assertRedirect($client);
        $this->assertEquals(1, $this->getSwiftmailerMessageCount($client));
        $client->followRedirect();
        $this->assertResponse($client);
    }
}
