<?php

namespace Eenov\DefaultBundle\Tests\Controller;

use Eenov\DefaultBundle\Test\WebTestCase;

/**
 * Class BlogControllerTest
 *
*
 */
class BlogControllerTest extends WebTestCase
{
    public function testIndexAction()
    {
        $client = static::createClient();
        $client->request('GET', '/blog');
        $this->assertResponse($client);
    }

    public function testIndexPageAction()
    {
        $client = static::createClient();
        $client->request('GET', '/blog/1');
        $this->assertResponse($client);
    }

    public function testReadAction()
    {
        $client = static::createClient();
        $client->request('GET', '/blog/1-oxioneo-est-au-salon-rent-2014');
        $this->assertResponse($client);
    }

    public function testReadWrongSlugAction()
    {
        $client = static::createClient();
        $client->request('GET', '/blog/1-oxioneo-eston-rent-2014');
        $this->assertFalse($client->getResponse()->isSuccessful());
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
    }
}
