<?php

namespace Eenov\DefaultBundle\Sms;

use Doctrine\ORM\EntityManager;
use Eenov\DefaultBundle\Entity\Sms;
use Eenov\DefaultBundle\Entity\User;
use Ovh\Api;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * Class OVH
 *
*
 *         https://www.ovhtelecom.fr/sms/
 *         http://www.ovhtelecom.fr/g1639.envoyer-sms-en-php-api-rest-ovh
 */
class OVH
{
    /**
     * @var bool
     */
    private $enabled;

    /**
     * @var Api
     */
    private $api;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param bool                 $enabled Enabled
     * @param Api                  $api     Api
     * @param EntityManager        $em      Manager
     * @param null|LoggerInterface $logger  Logger
     */
    public function __construct($enabled, Api $api, EntityManager $em, LoggerInterface $logger = null)
    {
        $this->enabled = $enabled;
        $this->api = $api;
        $this->em = $em;
        $this->logger = $logger ?: new NullLogger();
    }

    /**
     * Send
     *
     * @param string $message Message
     * @param User[] $users   Users
     *
     * @return bool
     */
    public function send($message, array $users = [])
    {
        // Log

        $this->logger->debug('trying to send a message');
//        if (!$this->enabled) {
//            return true;
//        }

        // Find cells
        $cells = [];
        /** @var User $user */
        foreach ($users as $user){
            if ($user != null){
                if (null !== $cell = $user->getProfile()->getCellphone()) {
                    array_push($cells,$this->cellToInternational($cell));
                }
            }
        }

        // No cell found
        if (0 === count($cells)) {
            $this->logger->warning('no cell found');
            return true;
        }

        $sms = new Sms();
        $sms
            ->setCells($cells)
            ->setMessage($message);

        return $this->sendSms($sms);
    }

    /**
     * @return null|string
     */
    private function getSmsService()
    {
        $this->logger->debug('looking for sms services');
        $services = $this->api->get('/sms/');

        if (isset($services[0])) {
            $this->logger->debug('sms service found', [
                'service' => $services[0],
            ]);

            return $services[0];
        }

        $this->logger->error('no sms services found');

        return null;
    }

    /**
     * Send sms
     *
     * @param Sms $sms
     *
     * @return bool
     */
    private function sendSms(Sms $sms)
    {
        $this->logger->debug('sending sms', [
            'message' => $sms->getMessage(),
            'receivers' => $sms->getCells(),
        ]);

        $result = false;
        if (null !== $service = $this->getSmsService()) {
            try {
                $sms->setOutbox([
                    'charset' => 'UTF-8',
                    'class' => 'phoneDisplay',
                    'coding' => '7bit',
                    'message' => $sms->getMessage(),
                    'noStopClause' => true,
                    'priority' => 'high',
                    'receivers' => $sms->getCells(),
                    'senderForResponse' => true,
                    'validityPeriod' => 2880,
                ]);

                $sms->setInbox($this->api->post(sprintf('/sms/%s/jobs/', $service), $sms->getOutbox()));

                $result = true;
            } catch (\Exception $e) {
                $this->logger->error($e->getMessage());
            }
        }

        $this->em->persist($sms);
        $this->em->flush();

        return $result;
    }

    /**
     * Cell to international
     *
     * @param string $cell
     *
     * @return null|string
     */
    private function cellToInternational($cell)
    {
        if (preg_match('/[0-9]{10}/i', $cell)) {
            return sprintf('+33%s', mb_strcut($cell, 1));
        }

        return null;
    }
}
