<?php

namespace Eenov\DefaultBundle\Command;

use EB\DoctrineBundle\Paginator\PaginatorHelper;
use Eenov\DefaultBundle\Entity\Advert;
use Eenov\DefaultBundle\Entity\Agency;
use Eenov\DefaultBundle\Entity\Search;
use Eenov\DefaultBundle\Entity\Slot;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class EmailDailyCommand
 *
*
 */
class EmailDailyCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('oxioneo:email:daily')
            ->setDescription('Emails journaliés');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->sendSearchesEmails($input, $output);
        $this->sendAgencySlotsEmails($input, $output);

        return 0;
    }

    /**
     * Send agency slots emails
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    private function sendAgencySlotsEmails(InputInterface $input, OutputInterface $output)
    {
        $start = new \DateTime();
        $start->setTime(0, 0, 0);
        $start->add(new \DateInterval('P10D'));

        $end = clone $start;
        $end->setTime(23, 59, 59);

        $em = $this->getContainer()->get('doctrine.orm.default_entity_manager');
        $agencyRepository = $em->getRepository(Agency::class);
        $slotRepository = $em->getRepository(Slot::class);
        $mailer = $this->getContainer()->get('eb_email.mailer.mailer');

        $agencies = $agencyRepository->findBy([], ['id' => 'ASC']);
        foreach ($agencies as $agency) {
            $slots = $slotRepository->findUnusedSlotsForAgencyBetween($agency, $start, $end);
            if (0 !== count($slots)) {
                try {
                    $mailer->send('slots', array_filter([$agency->getEmail(), $agency->getOwner(), $agency->getContact()]), [
                        'agency' => $agency,
                        'slots' => $slots,
                    ]);
                } catch (\Exception $e) {
                }
            }
        }
    }

    /**
     * Send searches emails
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    private function sendSearchesEmails(InputInterface $input, OutputInterface $output)
    {
        $deltaHour = 24;
        $delta = new \DateInterval(sprintf('PT%uH', $deltaHour));

        $started = new \DateTime();
        $started->setTime(2, 0, 0);

        $ended = clone $started;
        $ended->add($delta);

        $limit = 10;
        $offset = 0;

        $em = $this->getContainer()->get('doctrine.orm.default_entity_manager');
        $searchRepository = $em->getRepository(Search::class);
        $advertRepository = $em->getRepository(Advert::class);
        $mailer = $this->getContainer()->get('eb_email.mailer.mailer');

        $paginatorHelper = new PaginatorHelper();

        while (count($searches = $searchRepository->findBy([], ['id' => 'ASC'], $limit, $offset))) {
            foreach ($searches as $search) {
                $paginatorFilter = $search->getFilters();
                $paginatorFilter['updated_from'] = $started;
                $paginatorFilter['updated_to'] = $ended;

                $adverts = $advertRepository->getPaginator($paginatorHelper, $paginatorFilter);
                if ($adverts->count()) {
                    try {
                        $mailer->send('search', $search->getUser(), [
                            'search' => $search,
                            'adverts' => $adverts,
                        ]);
                    } catch (\Exception $e) {
                    }
                }
            }

            $em->clear();
            $offset += $limit;
        }
    }
}
