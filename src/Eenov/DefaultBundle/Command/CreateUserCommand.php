<?php

namespace Eenov\DefaultBundle\Command;

use Eenov\DefaultBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CreateUserCommand
 *
*
 */
class CreateUserCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('oxioneo:user:create')
            ->addArgument('username', InputArgument::REQUIRED)
            ->addArgument('firstname', InputArgument::REQUIRED)
            ->addArgument('lastname', InputArgument::REQUIRED)
            ->addArgument('role', InputArgument::REQUIRED)
            ->addArgument('password', InputArgument::REQUIRED)
            ->setDescription('Création d\'un utilisateur');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $user = new User();
        $user
            ->setUsername($input->getArgument('username'))
            ->setFirstname($input->getArgument('firstname'))
            ->setLastname($input->getArgument('lastname'))
            ->setRole($input->getArgument('role'))
            ->setRawPassword($input->getArgument('password'));

        $violations = $this->getContainer()->get('validator')->validate($user);
        if (0 === $violations->count()) {
            $em = $this->getContainer()->get('doctrine.orm.default_entity_manager');
            $em->persist($user);
            $em->flush();

            return 0;
        }

        $output->writeln(sprintf('<error>%s</error>', $violations));

        return 1;
    }
}
