<?php

namespace Eenov\DefaultBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class EmailTenMinutesCommand
 *
*
 */
class EmailTenMinutesCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('oxioneo:email:ten')
            ->setDescription('Emails de rappel');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logger = new ConsoleLogger($output);

        $this->getContainer()->get('eenov.default_bundle.mailer.mailer')->send(10, $logger);

        return 0;
    }
}
