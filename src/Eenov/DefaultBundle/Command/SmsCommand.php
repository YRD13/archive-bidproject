<?php

namespace Eenov\DefaultBundle\Command;

use Eenov\DefaultBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SmsCommand
 *
*
 */
class SmsCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('oxioneo:sms')
            ->setDescription('SMS')
            ->addArgument('message', InputArgument::REQUIRED, 'Message')
            ->addArgument('cells', InputArgument::REQUIRED | InputArgument::IS_ARRAY, 'Cells');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $message = $input->getArgument('message');
        $cells = $input->getArgument('cells');

        return $this->getContainer()->get('eenov.default_bundle.sms.ovh')->send($message, array_map(function ($cell) {
            $user = new User();
            $user->getProfile()->setCellphone($cell);

            return $user;
        }, $cells)) ? 0 : 1;
    }
}
