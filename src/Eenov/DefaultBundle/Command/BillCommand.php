<?php

namespace Eenov\DefaultBundle\Command;

use Eenov\DefaultBundle\Entity\Agency;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class BillCommand
 *
*
 */
class BillCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('oxioneo:bill')
            ->setDescription('Création des factures pour le mois')
            ->addArgument('date', InputArgument::OPTIONAL, 'Date (Y-m)');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Find date
        $date = new \DateTime();
        if (null !== $dateString = $input->getArgument('date')) {
            if (false === $date = \DateTime::createFromFormat('Y-m', $dateString)) {
                $output->writeln('<error>Invalid date</error>');

                return 1;
            }
        }

        // For all agencies
        $em = $this->getContainer()->get('doctrine.orm.default_entity_manager');
        $agencies = $em->getRepository(Agency::class)->findAll();
        foreach ($agencies as $agency) {
            $this->getContainer()->get('eenov.default_bundle.pdf.bill')->generate($agency, $date);
        }
        $em->flush();

        return 0;
    }
}
