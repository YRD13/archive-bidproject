<?php

namespace Eenov\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EB\DoctrineBundle\Entity\Doctrine\IdTrait;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Class Advert
 *
*
 * @ORM\Entity(repositoryClass="Eenov\DefaultBundle\Repository\VisitRepository")
 * @ORM\Table()
 */
class Visit
{
    use IdTrait;

    /**
     * @var Bid
     * @ORM\ManyToOne(targetEntity="Bid", inversedBy="visits")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     * @Assert\NotNull()
     */
    private $bid;

    /**
     * @var \DateTime
     * @ORM\Column(type="date")
     * @Assert\NotBlank()
     * @Assert\Date()
     */
    private $date;

    /**
     * @var \DateTime
     * @ORM\Column(type="time")
     * @Assert\NotBlank()
     * @Assert\Time()
     */
    private $started;

    /**
     * @var \DateTime
     * @ORM\Column(type="time")
     * @Assert\NotBlank()
     * @Assert\Time()
     */
    private $ended;

    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return sprintf('Le %s de %s à %s', $this->getDate()->format('d/m/Y'), $this->getStarted()->format('H:i'), $this->getEnded()->format('H:i'));
    }

    /**
     * assertIsValid
     *
     * @param ExecutionContextInterface $context
     * @Assert\Callback()
     */
    public function assertIsValid(ExecutionContextInterface $context)
    {
        $start = (int)$this->getStarted()->format('Hi');
        $end = (int)$this->getEnded()->format('Hi');
        if ($start >= $end) {
            $context->buildViolation('L\'heure de fin doit être avant l\'heure de début')->atPath('started')->addViolation();
        }
    }

    /**
     * Get Bid
     *
     * @return Bid
     */
    public function getBid()
    {
        return $this->bid;
    }

    /**
     * Set Bid
     *
     * @param Bid $bid
     *
     * @return Visit
     */
    public function setBid(Bid $bid = null)
    {
        $this->bid = $bid;

        return $this;
    }

    /**
     * Get Date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set Date
     *
     * @param \DateTime $date Date
     *
     * @return Visit
     */
    public function setDate(\DateTime $date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get Ended
     *
     * @return \DateTime
     */
    public function getEnded()
    {
        return $this->ended;
    }

    /**
     * Set Ended
     *
     * @param \DateTime $ended Ended
     *
     * @return Visit
     */
    public function setEnded(\DateTime $ended)
    {
        $this->ended = $ended;

        return $this;
    }

    /**
     * Get Started
     *
     * @return \DateTime
     */
    public function getStarted()
    {
        return $this->started;
    }

    /**
     * Set Started
     *
     * @param \DateTime $started Started
     *
     * @return Visit
     */
    public function setStarted(\DateTime $started)
    {
        $this->started = $started;

        return $this;
    }
}
