<?php

namespace Eenov\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EB\DoctrineBundle\Entity\CreatedInterface;
use EB\DoctrineBundle\Entity\Doctrine\CreatedTrait;
use EB\DoctrineBundle\Entity\Doctrine\IdTrait;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Auction
 *
*
 * @ORM\Entity(repositoryClass="Eenov\DefaultBundle\Repository\AuctionRepository")
 */
class Auction implements CreatedInterface
{
    use IdTrait;
    use CreatedTrait;

    /**
     * @var Bid
     * @ORM\ManyToOne(targetEntity="Bid")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull()
     */
    private $bid;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull()
     */
    private $user;

    /**
     * @var int
     * @ORM\Column(type="integer")
     * @Assert\NotNull()
     * @Assert\Type("int")
     * @Assert\Range(min=0)
     */
    private $amount;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     * @Assert\NotNull()
     */
    private $firm = false;

    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return sprintf('%u par %s le %s', $this->getAmount(), $this->getUser(), $this->getCreated()->format('d/m/Y H:i'));
    }

    /**
     * Get Bid
     *
     * @return Bid
     */
    public function getBid()
    {
        return $this->bid;
    }

    /**
     * Set Bid
     *
     * @param Bid $bid
     *
     * @return Auction
     */
    public function setBid($bid)
    {
        $this->bid = $bid;

        return $this;
    }

    /**
     * Get Amount
     *
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set Amount
     *
     * @param int $amount
     *
     * @return Auction
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get User
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set User
     *
     * @param User $user
     *
     * @return Auction
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get Firm
     *
     * @return bool
     */
    public function getFirm()
    {
        return $this->firm;
    }

    /**
     * Set Firm
     *
     * @param bool $firm
     *
     * @return Auction
     */
    public function setFirm($firm)
    {
        $this->firm = $firm;

        return $this;
    }
}
