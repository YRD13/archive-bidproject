<?php

namespace Eenov\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EB\DoctrineBundle\Entity\CreatedInterface;
use EB\DoctrineBundle\Entity\Doctrine\CreatedTrait;
use EB\DoctrineBundle\Entity\Doctrine\IdTrait;
use EB\DoctrineBundle\Entity\Doctrine\UpdatedTrait;
use EB\DoctrineBundle\Entity\UpdatedInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Sms
 *
*
 * @ORM\Entity(repositoryClass="Eenov\DefaultBundle\Repository\SmsRepository")
 * @ORM\Table()
 */
class Sms implements CreatedInterface, UpdatedInterface
{
    use IdTrait;
    use CreatedTrait;
    use UpdatedTrait;

    /**
     * @var string[]
     * @ORM\Column(type="array")
     * @Assert\NotNull()
     * @Assert\Type("array")
     */
    private $cells;

    /**
     * @var string
     * @ORM\Column(type="string", length=160)
     * @Assert\NotNull()
     * @Assert\Length(max=160)
     */
    private $message;

    /**
     * @var array
     * @ORM\Column(type="array")
     * @Assert\NotNull()
     * @Assert\Type("array")
     */
    private $outbox = [];

    /**
     * @var array
     * @ORM\Column(type="array")
     * @Assert\NotNull()
     * @Assert\Type("array")
     */
    private $inbox = [];

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     * @Assert\NotNull()
     * @Assert\Type("bool")
     */
    private $received = false;

    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return sprintf('SMS envoyé à %s', implode(', ', $this->getCells()));
    }

    /**
     * Get Cells
     *
     * @return string[]
     */
    public function getCells()
    {
        return $this->cells;
    }

    /**
     * Set Cells
     *
     * @param string[] $cells
     *
     * @return Sms
     */
    public function setCells(array $cells)
    {
        $this->cells = $cells;

        return $this;
    }

    /**
     * Get Message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set Message
     *
     * @param string $message
     *
     * @return Sms
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get Outbox
     *
     * @return array
     */
    public function getOutbox()
    {
        return $this->outbox;
    }

    /**
     * Set Outbox
     *
     * @param array $outbox
     *
     * @return Sms
     */
    public function setOutbox(array $outbox)
    {
        $this->outbox = $outbox;

        return $this;
    }

    /**
     * Get Inbox
     *
     * @return array
     */
    public function getInbox()
    {
        return $this->inbox;
    }

    /**
     * Set Inbox
     *
     * @param array $inbox
     *
     * @return Sms
     */
    public function setInbox(array $inbox)
    {
        $this->inbox = $inbox;

        return $this;
    }

    /**
     * Get Received
     *
     * @return bool
     */
    public function getReceived()
    {
        return $this->received;
    }

    /**
     * Set Received
     *
     * @param bool $received
     *
     * @return Sms
     */
    public function setReceived($received)
    {
        $this->received = $received;

        return $this;
    }
}
