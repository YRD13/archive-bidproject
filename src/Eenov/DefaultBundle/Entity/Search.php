<?php

namespace Eenov\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EB\DoctrineBundle\Entity\CreatedInterface;
use EB\DoctrineBundle\Entity\Doctrine\CreatedTrait;
use EB\DoctrineBundle\Entity\Doctrine\IdTrait;
use EB\DoctrineBundle\Entity\Doctrine\UpdatedTrait;
use EB\DoctrineBundle\Entity\UpdatedInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Search
 *
*
 * @ORM\Entity()
 * @ORM\Table()
 */
class Search implements CreatedInterface, UpdatedInterface
{
    use IdTrait;
    use CreatedTrait;
    use UpdatedTrait;

    /**
     * @var array
     * @ORM\Column(type="array")
     * @Assert\NotBlank()
     * @Assert\Type("array")
     */
    private $filters;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @Assert\NotNull()
     */
    private $user;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     * @Assert\NotNull()
     */
    private $isSentByEmail = true;

    /**
     * @param null|User $user    User
     * @param array     $filters Filters
     */
    public function __construct(User $user = null, array $filters = [])
    {
        $this
            ->setUser($user)
            ->setFilters($filters);
    }

    /**
     * Get Filters
     *
     * @return array
     */
    public function getFilters()
    {
        return $this->filters;
    }

    /**
     * Set Filters
     *
     * @param array $filters Filters
     *
     * @return Search
     */
    public function setFilters(array $filters)
    {
        $this->filters = array_filter($filters, function ($value) {
            return null !== $value;
        });

        return $this;
    }

    /**
     * Get IsSentByEmail
     *
     * @return bool
     */
    public function getIsSentByEmail()
    {
        return $this->isSentByEmail;
    }

    /**
     * Set IsSentByEmail
     *
     * @param bool $isSentByEmail
     *
     * @return Search
     */
    public function setIsSentByEmail($isSentByEmail)
    {
        $this->isSentByEmail = $isSentByEmail;

        return $this;
    }

    /**
     * Get User
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set User
     *
     * @param User $user User
     *
     * @return Search
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }
}
