<?php

namespace Eenov\DefaultBundle\Entity;

/**
 * Class SubscriptionPack
 *
*
 */
class SubscriptionPack
{
    const PACK_ONE = 'one';
    const PACK_START = 'start';
    const PACK_MAX = 'max';

    /**
     * Get Packs
     *
     * @return array
     */
    public static function getPacks()
    {
        return [
            self::PACK_ONE => [
                'id' => self::PACK_ONE,
                'slots' => 1,
                'pricePerSlot' => 99,
                'duration' => 30,
                'vat' => 20,
            ],
            self::PACK_START => [
                'id' => self::PACK_START,
                'slots' => 3,
                'pricePerSlot' => 79,
                'duration' => 60,
                'vat' => 20,
            ],
            self::PACK_MAX => [
                'id' => self::PACK_MAX,
                'slots' => 10,
                'pricePerSlot' => 49,
                'duration' => 90,
                'vat' => 20,
            ],
        ];
    }
}
