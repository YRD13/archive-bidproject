<?php

namespace Eenov\DefaultBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Contact
 *
*
 */
class Contact
{
    const TYPE_AGENCY = 'AGENCY';
    const TYPE_BUYER = 'BUYER';
    const TYPE_SELLER = 'SELLER';

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $type;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(min=2)
     */
    private $firstname;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(min=2)
     */
    private $lastname;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    /**
     * @var null|string
     * @Assert\Length(min=10, max=10)
     */
    private $phone;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(min=5)
     */
    private $subject;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(min=5)
     */
    private $message;

    /**
     * Get type list
     *
     * @return string[]
     */
    public static function getTypeList()
    {
        return [
            self::TYPE_BUYER,
            self::TYPE_SELLER,
            self::TYPE_AGENCY,
        ];
    }

    /**
     * Get type name list
     *
     * @return string[]
     */
    public static function getTypeNameList()
    {
        return [
            self::TYPE_BUYER => 'Acheteur',
            self::TYPE_SELLER => 'Vendeur',
            self::TYPE_AGENCY => 'Agence',
        ];
    }

    /**
     * Get type name
     *
     * @return string
     */
    public function getTypeName()
    {
        return self::getTypeNameList()[$this->getType()];
    }

    /**
     * Get Email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set Email
     *
     * @param string $email Email
     *
     * @return Contact
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get Firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set Firstname
     *
     * @param string $firstname Firstname
     *
     * @return Contact
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get Lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set Lastname
     *
     * @param string $lastname Lastname
     *
     * @return Contact
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get Message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set Message
     *
     * @param string $message Message
     *
     * @return Contact
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get Phone
     *
     * @return null|string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set Phone
     *
     * @param null|string $phone Phone
     *
     * @return Contact
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get Subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set Subject
     *
     * @param string $subject Subject
     *
     * @return Contact
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get Type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set Type
     *
     * @param string $type Type
     *
     * @return Contact
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }
}
