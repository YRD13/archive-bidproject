<?php

namespace Eenov\DefaultBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use EB\DoctrineBundle\Entity\CreatedInterface;
use EB\DoctrineBundle\Entity\Doctrine\CreatedTrait;
use EB\DoctrineBundle\Entity\Doctrine\FileReadableTrait;
use EB\DoctrineBundle\Entity\Doctrine\FileTrait;
use EB\DoctrineBundle\Entity\Doctrine\FileVersionableTrait;
use EB\DoctrineBundle\Entity\Doctrine\IdTrait;
use EB\DoctrineBundle\Entity\Doctrine\UpdatedTrait;
use EB\DoctrineBundle\Entity\FileReadableInterface;
use EB\DoctrineBundle\Entity\FileVersionableInterface;
use EB\DoctrineBundle\Entity\UpdatedInterface;
use Eenov\DefaultBundle\Entity\Doctrine\AddressTrait;
use Eenov\DefaultBundle\Entity\Doctrine\CondoTrait;
use Eenov\DefaultBundle\Entity\Doctrine\CoordinatesTrait;
use Eenov\DefaultBundle\Entity\Doctrine\EstateTrait;
use Eenov\DefaultBundle\Entity\Doctrine\ExposureTrait;
use Eenov\DefaultBundle\Entity\Doctrine\HeatingSystemTrait;
use Eenov\DefaultBundle\Entity\Doctrine\HeatingTypeTrait;
use Eenov\DefaultBundle\Entity\Doctrine\InternetTrait;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Class Advert
 *
*
 * @ORM\Entity(repositoryClass="Eenov\DefaultBundle\Repository\AdvertRepository")
 * @ORM\Table()
 */
class Advert implements CreatedInterface, UpdatedInterface, FileReadableInterface, FileVersionableInterface
{
    use IdTrait;
    use CreatedTrait;
    use UpdatedTrait;
    use FileTrait;
    use FileReadableTrait;
    use FileVersionableTrait;
    use CoordinatesTrait;
    use AddressTrait;
    use EstateTrait;
    use HeatingSystemTrait;
    use HeatingTypeTrait;
    use ExposureTrait;
    use InternetTrait;
    use CondoTrait;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     * @Assert\NotNull()
     */
    private $new = false;

    /**
     * @var AdvertType
     * @ORM\ManyToOne(targetEntity="AdvertType")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull()
     */
    private $type;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     */
    private $mandateAgency;

    /**
     * @var string
     * @ORM\Column(type="text")
     * @Assert\NotNull()
     * @Assert\Length(min=10)
     */
    private $description;

    /**
     * @var float
     * @ORM\Column(type="float")
     * @Assert\NotNull()
     */
    private $shab;

    /**
     * @var float
     * @ORM\Column(type="float")
     * @Assert\NotNull()
     */
    private $field;

    /**
     * @var float
     * @ORM\Column(type="float")
     * @Assert\NotNull()
     */
    private $livingRoom;

    /**
     * @var int
     * @ORM\Column(type="integer")
     * @Assert\NotNull()
     * @Assert\Type("int")
     * @Assert\GreaterThanOrEqual(0)
     */
    private $rooms;

    /**
     * @var int
     * @ORM\Column(type="integer")
     * @Assert\NotNull()
     * @Assert\Type("int")
     * @Assert\GreaterThanOrEqual(0)
     */
    private $bedrooms;

    /**
     * @var int
     * @ORM\Column(type="integer")
     * @Assert\NotNull()
     * @Assert\Type("int")
     * @Assert\GreaterThanOrEqual(0)
     */
    private $bathrooms;

    /**
     * @var int
     * @ORM\Column(type="integer")
     * @Assert\NotNull()
     * @Assert\Type("int")
     * @Assert\GreaterThanOrEqual(0)
     */
    private $showerrooms;

    /**
     * @var int
     * @ORM\Column(type="integer")
     * @Assert\NotNull()
     * @Assert\Type("int")
     * @Assert\GreaterThanOrEqual(0)
     */
    private $floor;

    /**
     * @var int
     * @ORM\Column(type="integer")
     * @Assert\NotNull()
     * @Assert\Type("int")
     * @Assert\GreaterThanOrEqual(1000)
     */
    private $year;

    /**
     * @var null|int
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Type("int")
     * @Assert\GreaterThanOrEqual(0)
     */
    private $dpe;

    /**
     * @var null|int
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Type("int")
     * @Assert\GreaterThanOrEqual(0)
     */
    private $ges;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Type("int")
     */
    private $toilets;

    /**
     * @var Around[]|ArrayCollection
     * @ORM\ManyToMany(targetEntity="Around")
     * @ORM\JoinTable(name="AdvertAround")
     */
    private $arounds;

    /**
     * @var Equipment[]|ArrayCollection
     * @ORM\ManyToMany(targetEntity="Equipment")
     * @ORM\JoinTable(name="AdvertEquipment")
     */
    private $equipments;

    /**
     * @var Image[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="Image", mappedBy="advert", cascade={"persist","remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"created"="ASC"})
     */
    private $images;

    /**
     * @var Document[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="Document", mappedBy="advert", cascade={"persist","remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"created"="ASC"})
     */
    private $documents;

    /**
     * @var null|AdvertGround
     * @ORM\OneToOne(targetEntity="AdvertGround", inversedBy="advert", cascade={"persist","remove"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $ground;

    /**
     * @var null|AdvertBuilding
     * @ORM\OneToOne(targetEntity="AdvertBuilding", inversedBy="advert", cascade={"persist","remove"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $building;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     * @Assert\NotNull()
     */
    private $displayAddress = true;

    /**
     * {@inheritdoc}
     */
    public function __construct()
    {
        $this->arounds = new ArrayCollection();
        $this->equipments = new ArrayCollection();
        $this->images = new ArrayCollection();
        $this->documents = new ArrayCollection();
    }

    /**
     * {@inheritdoc}
     */
    public function getCacheKey()
    {
        return 'bida';
    }

    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return sprintf('Annonce %u', $this->getId());
    }

    /**
     * Add arounds
     *
     * @param Around $around
     *
     * @return Advert
     */
    public function addAround(Around $around)
    {
        if (false === $this->arounds->contains($around)) {
            $this->arounds[] = $around;
        }

        return $this;
    }

    /**
     * Add document
     *
     * @param Document $document
     *
     * @return Subscription
     */
    public function addDocument(Document $document)
    {
        if (false === $this->documents->contains($document)) {
            $this->documents->add($document);
            $document->setAdvert($this);
        }

        return $this;
    }

    /**
     * Add equipments
     *
     * @param Equipment $equipment
     *
     * @return Advert
     */
    public function addEquipment(Equipment $equipment)
    {
        if (false === $this->equipments->contains($equipment)) {
            $this->equipments[] = $equipment;
        }

        return $this;
    }

    /**
     * Add image
     *
     * @param Image $image
     *
     * @return Subscription
     */
    public function addImage(Image $image)
    {
        if (false === $this->images->contains($image)) {
            $this->images->add($image);
            $image->setAdvert($this);
        }

        return $this;
    }

    /**
     * Assert is valid
     *
     * @param ExecutionContextInterface $context
     * @Assert\Callback()
     */
    public function assertIsValid(ExecutionContextInterface $context)
    {
        if (null === $this->getAddress()) {
            $context->buildViolation('Vous devez renseigner votre adresse')->atPath('address')->addViolation();
        }
        if (null === $this->getZip()) {
            $context->buildViolation('Vous devez renseigner votre code postal')->atPath('zip')->addViolation();
        }
        if (null === $this->getCity()) {
            $context->buildViolation('Vous devez renseigner votre ville')->atPath('city')->addViolation();
        }
        if (null === $this->getCountry()) {
            $context->buildViolation('Vous devez renseigner votre pays')->atPath('country')->addViolation();
        }
        $context->getViolations()->addAll($context->getValidator()->validate($this->getFile(), [new Assert\Image()]));
        if (null !== $this->getYear() && $this->getYear() > (int)date('Y')) {
            $context->buildViolation('L\'année de construction doit être supérieure à l\'année en cours')->atPath('year')->addViolation();
        }

        // Condo validation
        if ($this->getIsCondo()) {
            if (null === $this->getCondoLots()) {
                $context->buildViolation('Vous devez renseigner le nombre de lots de la copropriété')->atPath('condoLots')->addViolation();
            }
            if (null === $this->getCondoCharges()) {
                $context->buildViolation('Vous devez renseigner la valeur des charges de la copropriété')->atPath('condoCharges')->addViolation();
            }
            if (null === $this->getCondoChargesRecurrence()) {
                $context->buildViolation('Vous devez renseigner la récurrence des charges de la copropriété')->atPath('condoChargesRecurrence')->addViolation();
            }
            if (null === $this->getCondoHasOngoingProceedings()) {
                $context->buildViolation('Vous devez renseigner l\'existence de procédure en cours avec le syndic de coproprité')->atPath('condoHasOngoingProceedings')->addViolation();
            }
        }
    }

    /**
     * Get around sorted Ids
     *
     * @return int[]
     */
    public function getAroundSortedIds()
    {
        $ids = array_map(function (Around $around) {
            return $around->getId();
        }, $this->getArounds()->toArray());

        sort($ids);

        return $ids;
    }

    /**
     * Get arounds
     *
     * @return Around[]|ArrayCollection
     */
    public function getArounds()
    {
        return $this->arounds;
    }

    /**
     * Get bathrooms
     *
     * @return int
     */
    public function getBathrooms()
    {
        return $this->bathrooms;
    }

    /**
     * Set bathrooms
     *
     * @param int $bathrooms
     *
     * @return Advert
     */
    public function setBathrooms($bathrooms)
    {
        $this->bathrooms = $bathrooms;

        return $this;
    }

    /**
     * Get bedrooms
     *
     * @return int
     */
    public function getBedrooms()
    {
        return $this->bedrooms;
    }

    /**
     * Set bedrooms
     *
     * @param int $bedrooms
     *
     * @return Advert
     */
    public function setBedrooms($bedrooms)
    {
        $this->bedrooms = $bedrooms;

        return $this;
    }

    /**
     * Get Building
     *
     * @return AdvertBuilding
     */
    public function getBuilding()
    {
        return $this->building;
    }

    /**
     * Set Building
     *
     * @param AdvertBuilding $building
     *
     * @return Advert
     */
    public function setBuilding(AdvertBuilding $building = null)
    {
        $this->building = $building;
        if (null !== $building) {
            $building->setAdvert($this);
        }

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Advert
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get DisplayAddress
     *
     * @return bool
     */
    public function getDisplayAddress()
    {
        return $this->displayAddress;
    }

    /**
     * Set DisplayAddress
     *
     * @param bool $displayAddress
     *
     * @return Advert
     */
    public function setDisplayAddress($displayAddress)
    {
        $this->displayAddress = $displayAddress;

        return $this;
    }

    /**
     * Get document sorted Ids
     *
     * @return int[]
     */
    public function getDocumentSortedIds()
    {
        $ids = array_map(function (Document $document) {
            return $document->getId();
        }, $this->getDocuments()->toArray());

        sort($ids);

        return $ids;
    }

    /**
     * Get documents
     *
     * @return Document[]|ArrayCollection
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * Get documents of type
     *
     * @param DocumentType $type
     *
     * @return Document[]
     */
    public function getDocumentsOfType(DocumentType $type)
    {
        $documents = [];
        foreach ($this->getDocuments() as $document) {
            if ($document->getType()->getId() === $type->getId()) {
                $documents[] = $document;
            }
        }

        return $documents;
    }

    /**
     * Get dpe
     *
     * @return null|int
     */
    public function getDpe()
    {
        return $this->dpe;
    }

    /**
     * Set dpe
     *
     * @param null|int $dpe
     *
     * @return Advert
     */
    public function setDpe($dpe)
    {
        $this->dpe = $dpe;

        return $this;
    }

    /**
     * Get DPE letter
     *
     * @return null|string
     */
    public function getDpeLetter()
    {
        if (null === $this->dpe) {
            return null;
        }

        if ($this->dpe > 450) {
            return 'G';
        } elseif ($this->dpe > 330) {
            return 'F';
        } elseif ($this->dpe > 230) {
            return 'E';
        } elseif ($this->dpe > 150) {
            return 'D';
        } elseif ($this->dpe > 90) {
            return 'C';
        } elseif ($this->dpe > 50) {
            return 'B';
        }

        return 'A';
    }

    /**
     * Get equipment sorted Ids
     *
     * @return int[]
     */
    public function getEquipmentSortedIds()
    {
        $ids = array_map(function (Equipment $equipment) {
            return $equipment->getId();
        }, $this->getEquipments()->toArray());

        sort($ids);

        return $ids;
    }

    /**
     * Get equipments
     *
     * @return Equipment[]|ArrayCollection
     */
    public function getEquipments()
    {
        return $this->equipments;
    }

    /**
     * Get Field
     *
     * @return float
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * Set Field
     *
     * @param float $field Field
     *
     * @return Advert
     */
    public function setField($field)
    {
        $this->field = $field;

        return $this;
    }

    /**
     * Get floor
     *
     * @return int
     */
    public function getFloor()
    {
        return $this->floor;
    }

    /**
     * Set floor
     *
     * @param int $floor
     *
     * @return Advert
     */
    public function setFloor($floor)
    {
        $this->floor = $floor;

        return $this;
    }

    /**
     * Get ges
     *
     * @return null|int
     */
    public function getGes()
    {
        return $this->ges;
    }

    /**
     * Set ges
     *
     * @param null|int $ges
     *
     * @return Advert
     */
    public function setGes($ges = null)
    {
        $this->ges = $ges;

        return $this;
    }

    /**
     * Retourne la lettre du GES
     *
     * @return null|string
     */
    public function getGesLetter()
    {
        if (null === $this->ges) {
            return null;
        }

        if ($this->ges > 80) {
            return 'G';
        } elseif ($this->ges > 55) {
            return 'F';
        } elseif ($this->ges > 35) {
            return 'E';
        } elseif ($this->ges > 20) {
            return 'D';
        } elseif ($this->ges > 10) {
            return 'C';
        } elseif ($this->ges > 5) {
            return 'B';
        }

        return 'A';
    }

    /**
     * Get Ground
     *
     * @return null|AdvertGround
     */
    public function getGround()
    {
        return $this->ground;
    }

    /**
     * Set Ground
     *
     * @param null|AdvertGround $ground
     *
     * @return Advert
     */
    public function setGround(AdvertGround $ground = null)
    {
        $this->ground = $ground;
        if (null !== $ground) {
            $ground->setAdvert($this);
        }

        return $this;
    }

    /**
     * Get image sorted Ids
     *
     * @return int[]
     */
    public function getImageSortedIds()
    {
        $ids = array_map(function (Image $image) {
            return $image->getId();
        }, $this->getImages()->toArray());

        sort($ids);

        return $ids;
    }

    /**
     * Get images
     *
     * @return Image[]|ArrayCollection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Get livingRoom
     *
     * @return \float
     */
    public function getLivingRoom()
    {
        return $this->livingRoom;
    }

    /**
     * Set livingRoom
     *
     * @param \float $livingRoom
     *
     * @return Advert
     */
    public function setLivingRoom($livingRoom)
    {
        $this->livingRoom = $livingRoom;

        return $this;
    }

    /**
     * Get mandateAgency
     *
     * @return string
     */
    public function getMandateAgency()
    {
        return $this->mandateAgency;
    }

    /**
     * Set mandateAgency
     *
     * @param string $mandateAgency
     *
     * @return Advert
     */
    public function setMandateAgency($mandateAgency)
    {
        $this->mandateAgency = $mandateAgency;

        return $this;
    }

    /**
     * Get New
     *
     * @return bool
     */
    public function getNew()
    {
        return $this->new;
    }

    /**
     * Set New
     *
     * @param bool $new
     *
     * @return Advert
     */
    public function setNew($new)
    {
        $this->new = $new;

        return $this;
    }

    /**
     * Get rooms
     *
     * @return int
     */
    public function getRooms()
    {
        return $this->rooms;
    }

    /**
     * Set rooms
     *
     * @param int $rooms
     *
     * @return Advert
     */
    public function setRooms($rooms)
    {
        $this->rooms = $rooms;

        return $this;
    }

    /**
     * Get shab
     *
     * @return \float
     */
    public function getShab()
    {
        return $this->shab;
    }

    /**
     * Set shab
     *
     * @param \float $shab
     *
     * @return Advert
     */
    public function setShab($shab)
    {
        $this->shab = $shab;

        return $this;
    }

    /**
     * Get Showerrooms
     *
     * @return int
     */
    public function getShowerrooms()
    {
        return $this->showerrooms;
    }

    /**
     * Set Showerrooms
     *
     * @param int $showerrooms
     *
     * @return Advert
     */
    public function setShowerrooms($showerrooms)
    {
        $this->showerrooms = $showerrooms;

        return $this;
    }

    /**
     * Get toilets
     *
     * @return int
     */
    public function getToilets()
    {
        return $this->toilets;
    }

    /**
     * Set toilets
     *
     * @param int $toilets
     *
     * @return Advert
     */
    public function setToilets($toilets)
    {
        $this->toilets = $toilets;

        return $this;
    }

    /**
     * Get Type
     *
     * @return AdvertType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set Type
     *
     * @param AdvertType $type
     *
     * @return Advert
     */
    public function setType(AdvertType $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get Year
     *
     * @return mixed
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set Year
     *
     * @param mixed $year Year
     *
     * @return Advert
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Remove around
     *
     * @param Around $around
     *
     * @return Advert
     */
    public function removeAround(Around $around)
    {
        if (true === $this->arounds->contains($around)) {
            $this->arounds->removeElement($around);
        }

        return $this;
    }

    /**
     * Remove document
     *
     * @param Document $document
     *
     * @return Advert
     */
    public function removeDocument(Document $document)
    {
        if (true === $this->documents->contains($document)) {
            $this->documents->removeElement($document);
            $document->setAdvert(null);
        }

        return $this;
    }

    /**
     * Remove equipment
     *
     * @param Equipment $equipment
     *
     * @return Advert
     */
    public function removeEquipment(Equipment $equipment)
    {
        if (true === $this->equipments->contains($equipment)) {
            $this->equipments->removeElement($equipment);
        }

        return $this;
    }

    /**
     * Remove image
     *
     * @param Image $image
     *
     * @return Advert
     */
    public function removeImage(Image $image)
    {
        if (true === $this->images->contains($image)) {
            $this->images->removeElement($image);
            $image->setAdvert(null);
        }

        return $this;
    }
}
