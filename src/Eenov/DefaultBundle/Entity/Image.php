<?php

namespace Eenov\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EB\DoctrineBundle\Entity\CreatedInterface;
use EB\DoctrineBundle\Entity\Doctrine\CreatedTrait;
use EB\DoctrineBundle\Entity\Doctrine\FileReadableTrait;
use EB\DoctrineBundle\Entity\Doctrine\FileTrait;
use EB\DoctrineBundle\Entity\Doctrine\IdTrait;
use EB\DoctrineBundle\Entity\Doctrine\UpdatedTrait;
use EB\DoctrineBundle\Entity\FileReadableInterface;
use EB\DoctrineBundle\Entity\UpdatedInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Class Image
 *
*
 * @ORM\Entity()
 * @ORM\Table()
 */
class Image implements CreatedInterface, UpdatedInterface, FileReadableInterface
{
    use IdTrait;
    use CreatedTrait;
    use UpdatedTrait;
    use FileTrait;
    use FileReadableTrait;

    /**
     * @var Advert
     * @ORM\ManyToOne(targetEntity="Advert", inversedBy="images")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     * @Assert\NotNull()
     */
    private $advert;

    /**
     * @var ImageType
     * @ORM\ManyToOne(targetEntity="ImageType")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull()
     */
    private $type;

    /**
     * {@inheritdoc}
     */
    public function getCacheKey()
    {
        return 'image';
    }

    /**
     * assertIsValid
     *
     * @param ExecutionContextInterface $context
     * @Assert\Callback()
     */
    public function assertIsValid(ExecutionContextInterface $context)
    {
        $context->getViolations()->addAll($context->getValidator()->validate($this->getFile(), [new Assert\Image()]));
    }

    /**
     * Get Advert
     *
     * @return Advert
     */
    public function getAdvert()
    {
        return $this->advert;
    }

    /**
     * Set Advert
     *
     * @param Advert $advert
     *
     * @return Image
     */
    public function setAdvert(Advert $advert = null)
    {
        $this->advert = $advert;

        return $this;
    }

    /**
     * Get Type
     *
     * @return ImageType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set Type
     *
     * @param ImageType $type
     *
     * @return Image
     */
    public function setType(ImageType $type = null)
    {
        $this->type = $type;

        return $this;
    }
}
