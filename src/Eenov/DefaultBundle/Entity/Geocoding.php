<?php

namespace Eenov\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EB\DoctrineBundle\Entity\CreatedInterface;
use EB\DoctrineBundle\Entity\Doctrine\CreatedTrait;
use EB\DoctrineBundle\Entity\Doctrine\IdTrait;
use Eenov\DefaultBundle\Entity\Doctrine\CoordinatesTrait;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Geocoding
 *
*
 * @ORM\Entity()
 * @UniqueEntity(fields={"search"}, errorPath="search")
 */
class Geocoding implements CreatedInterface
{
    use IdTrait;
    use CoordinatesTrait;
    use CreatedTrait;

    /**
     * @var string
     * @ORM\Column(unique=true)
     * @Assert\NotNull()
     * @Assert\Type("string")
     * @Assert\Length(min=3, max=255)
     */
    private $search;

    /**
     * Get Search
     *
     * @return string
     */
    public function getSearch()
    {
        return $this->search;
    }

    /**
     * Set Search
     *
     * @param string $search
     *
     * @return $this
     */
    public function setSearch($search)
    {
        $this->search = $search;

        return $this;
    }
}
