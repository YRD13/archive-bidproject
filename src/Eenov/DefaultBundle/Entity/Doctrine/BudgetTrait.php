<?php

namespace Eenov\DefaultBundle\Entity\Doctrine;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class BudgetTrait
 *
*
 */
trait BudgetTrait
{
    /**
     * @var null|string
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Choice(callback="getBudgetList")
     */
    private $budget;

    /**
     * Get estate list
     *
     * @return string[]
     */
    public static function getBudgetList()
    {
        return [
            BudgetInterface::BUDGET_0_50000,
            BudgetInterface::BUDGET_50000_100000,
            BudgetInterface::BUDGET_100000_150000,
            BudgetInterface::BUDGET_150000_200000,
            BudgetInterface::BUDGET_200000_250000,
            BudgetInterface::BUDGET_250000_300000,
            BudgetInterface::BUDGET_300000_350000,
            BudgetInterface::BUDGET_350000_400000,
            BudgetInterface::BUDGET_400000_500000,
            BudgetInterface::BUDGET_500000_600000,
            BudgetInterface::BUDGET_MORE_600000,
        ];
    }

    /**
     * Get estate name list
     *
     * @return string[]
     */
    public static function getBudgetNameList()
    {
        return [
            BudgetInterface::BUDGET_0_50000 => 'Moins de 50 000',
            BudgetInterface::BUDGET_50000_100000 => 'Entre 50 000 et 100 000',
            BudgetInterface::BUDGET_100000_150000 => 'Entre 100 000 et 150 000',
            BudgetInterface::BUDGET_150000_200000 => 'Entre 150 000 et 200 000',
            BudgetInterface::BUDGET_200000_250000 => 'Entre 200 000 et 250 000',
            BudgetInterface::BUDGET_250000_300000 => 'Entre 250 000 et 300 000',
            BudgetInterface::BUDGET_300000_350000 => 'Entre 300 000 et 350 000',
            BudgetInterface::BUDGET_350000_400000 => 'Entre 350 000 et 400 000',
            BudgetInterface::BUDGET_400000_500000 => 'Entre 400 000 et 500 000',
            BudgetInterface::BUDGET_500000_600000 => 'Entre 500 000 et 600 000',
            BudgetInterface::BUDGET_MORE_600000 => 'Plus de 600 000',
        ];
    }

    /**
     * Get Budget
     *
     * @return null|string
     */
    public function getBudget()
    {
        return $this->budget;
    }

    /**
     * Set Budget
     *
     * @param null|string $budget
     *
     * @return $this
     */
    public function setBudget($budget = null)
    {
        $this->budget = $budget;

        return $this;
    }

    /**
     * Get Budget Name
     *
     * @return null|string
     */
    public function getBudgetName()
    {
        if (null === $this->getBudget()) {
            return null;
        }

        return self::getBudgetNameList()[$this->getBudget()];
    }
}
