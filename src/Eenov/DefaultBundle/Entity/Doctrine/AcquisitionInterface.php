<?php

namespace Eenov\DefaultBundle\Entity\Doctrine;

/**
 * Interface AcquisitionInterface
 *
*
 */
interface AcquisitionInterface
{
    const ACQUISITION_FIRST = 'FIRST';
    const ACQUISITION_INVESTMENT = 'INVESTMENT';
}
