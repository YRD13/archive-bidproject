<?php

namespace Eenov\DefaultBundle\Entity\Doctrine;

/**
 * Interface EstateInterface
 *
*
 */
interface EstateInterface
{
    const ESTATE_NEW_CONDITION = 'NEW_CONDITION';
    const ESTATE_RENOVATED_BY_ARCHITECT = 'RENOVATED_BY_ARCHITECT';
    const ESTATE_TO_REFRESH = 'TO_REFRESH';
    const ESTATE_TO_RENOVATE = 'TO_RENOVATE';
    const ESTATE_WORK_REQUIRED = 'WORK_REQUIRED';
    const ESTATE_GOOD = 'ESTATE_GOOD';
}
