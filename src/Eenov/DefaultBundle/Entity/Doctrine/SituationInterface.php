<?php

namespace Eenov\DefaultBundle\Entity\Doctrine;

/**
 * Interface SituationInterface
 *
*
 */
interface SituationInterface
{
    const SITUATION_CDD = 'CDD';
    const SITUATION_CDI = 'CDI';
    const SITUATION_OTHER = 'OTHER';
    const SITUATION_UNEMPLOYED = 'UNEMPLOYED';
}
