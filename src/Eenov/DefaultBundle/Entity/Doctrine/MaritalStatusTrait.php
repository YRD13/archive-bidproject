<?php

namespace Eenov\DefaultBundle\Entity\Doctrine;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class MaritalStatusTrait
 *
*
 */
trait MaritalStatusTrait
{
    /**
     * @var null|string
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Choice(callback="getMaritalStatusList")
     */
    private $maritalStatus;

    /**
     * Get maritalStatus list
     *
     * @return string[]
     */
    public static function getMaritalStatusList()
    {
        return [
            MaritalStatusInterface::MARITAL_STATUS_MARRIED,
            MaritalStatusInterface::MARITAL_STATUS_PACS,
            MaritalStatusInterface::MARITAL_STATUS_COMCUBINAGE,
            MaritalStatusInterface::MARITAL_STATUS_SINGLE,
        ];
    }

    /**
     * Get maritalStatus name list
     *
     * @return string[]
     */
    public static function getMaritalStatusNameList()
    {
        return [
            MaritalStatusInterface::MARITAL_STATUS_MARRIED => 'Marié',
            MaritalStatusInterface::MARITAL_STATUS_PACS => 'Pacsé',
            MaritalStatusInterface::MARITAL_STATUS_COMCUBINAGE => 'En comcubinage',
            MaritalStatusInterface::MARITAL_STATUS_SINGLE => 'Célibataire',
        ];
    }

    /**
     * Get MaritalStatus
     *
     * @return null|string
     */
    public function getMaritalStatus()
    {
        return $this->maritalStatus;
    }

    /**
     * Set MaritalStatus
     *
     * @param null|string $maritalStatus
     *
     * @return $this
     */
    public function setMaritalStatus($maritalStatus = null)
    {
        $this->maritalStatus = $maritalStatus;

        return $this;
    }

    /**
     * Get MaritalStatus Name
     *
     * @return null|string
     */
    public function getMaritalStatusName()
    {
        if (null === $this->getMaritalStatus()) {
            return null;
        }

        return self::getMaritalStatusNameList()[$this->getMaritalStatus()];
    }
}
