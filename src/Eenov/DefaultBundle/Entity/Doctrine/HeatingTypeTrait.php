<?php

namespace Eenov\DefaultBundle\Entity\Doctrine;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class HeatingTypeTrait
 *
*
 */
trait HeatingTypeTrait
{
    /**
     * @var null|string
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Choice(callback="getHeatingTypeList")
     */
    private $heatingType = HeatingTypeInterface::HEATING_TYPE_NONE;

    /**
     * Get heatingType list
     *
     * @return string[]
     */
    public static function getHeatingTypeList()
    {
        return [
            HeatingTypeInterface::HEATING_TYPE_NONE,
            HeatingTypeInterface::HEATING_TYPE_GAS,
            HeatingTypeInterface::HEATING_TYPE_ELECTRICAL,
            HeatingTypeInterface::HEATING_TYPE_GEOTHERMAL,
            HeatingTypeInterface::HEATING_TYPE_SOLAR,
            HeatingTypeInterface::HEATING_TYPE_REVERSIBLE,
            HeatingTypeInterface::HEATING_TYPE_WOOD,
            HeatingTypeInterface::HEATING_TYPE_FUEL_OIL,
            HeatingTypeInterface::HEATING_TYPE_HEAT_PUMP,
        ];
    }

    /**
     * Get heatingType name list
     *
     * @return string[]
     */
    public static function getHeatingTypeNameList()
    {
        return [
            HeatingTypeInterface::HEATING_TYPE_NONE => 'Aucun chauffage',
            HeatingTypeInterface::HEATING_TYPE_GAS => 'Au gaz',
            HeatingTypeInterface::HEATING_TYPE_ELECTRICAL => 'Électrique',
            HeatingTypeInterface::HEATING_TYPE_GEOTHERMAL => 'Géothermal',
            HeatingTypeInterface::HEATING_TYPE_SOLAR => 'Solaire',
            HeatingTypeInterface::HEATING_TYPE_REVERSIBLE => 'Climatisation réversible',
            HeatingTypeInterface::HEATING_TYPE_WOOD => 'Au bois',
            HeatingTypeInterface::HEATING_TYPE_FUEL_OIL => 'Fioul',
            HeatingTypeInterface::HEATING_TYPE_HEAT_PUMP => 'Pompe à chaleur',
        ];
    }

    /**
     * Get HeatingType
     *
     * @return null|string
     */
    public function getHeatingType()
    {
        return $this->heatingType;
    }

    /**
     * Set HeatingType
     *
     * @param null|string $heatingType
     *
     * @return $this
     */
    public function setHeatingType($heatingType = null)
    {
        $this->heatingType = $heatingType;

        return $this;
    }

    /**
     * Get HeatingType Name
     *
     * @return null|string
     */
    public function getHeatingTypeName()
    {
        if (null === $this->getHeatingType()) {
            return null;
        }

        return self::getHeatingTypeNameList()[$this->getHeatingType()];
    }
}
