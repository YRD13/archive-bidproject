<?php

namespace Eenov\DefaultBundle\Entity\Doctrine;

/**
 * Interface MaritalStatusInterface
 *
*
 */
interface MaritalStatusInterface
{
    const MARITAL_STATUS_COMCUBINAGE = 'COMCUBINAGE';
    const MARITAL_STATUS_MARRIED = 'MARRIED';
    const MARITAL_STATUS_PACS = 'PACS';
    const MARITAL_STATUS_SINGLE = 'SINGLE';
}
