<?php

namespace Eenov\DefaultBundle\Entity\Doctrine;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ExposureTrait
 *
*
 */
trait ExposureTrait
{
    /**
     * @var null|string
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Choice(callback="getExposureList")
     */
    private $exposure;

    /**
     * Get exposure list
     *
     * @return string[]
     */
    public static function getExposureList()
    {
        return [
            ExposureInterface::EXPOSURE_N,
            ExposureInterface::EXPOSURE_S,
            ExposureInterface::EXPOSURE_E,
            ExposureInterface::EXPOSURE_O,
            ExposureInterface::EXPOSURE_NE,
            ExposureInterface::EXPOSURE_NO,
            ExposureInterface::EXPOSURE_SE,
            ExposureInterface::EXPOSURE_SO,
            ExposureInterface::EXPOSURE_EO,
            ExposureInterface::EXPOSURE_NS,
        ];
    }

    /**
     * Get exposure name list
     *
     * @return string[]
     */
    public static function getExposureNameList()
    {
        return [
            ExposureInterface::EXPOSURE_N => 'Nord',
            ExposureInterface::EXPOSURE_S => 'Sud',
            ExposureInterface::EXPOSURE_E => 'Est',
            ExposureInterface::EXPOSURE_O => 'Ouest',
            ExposureInterface::EXPOSURE_NE => 'Nord Est',
            ExposureInterface::EXPOSURE_NO => 'Nord Ouest',
            ExposureInterface::EXPOSURE_SE => 'Sud Est',
            ExposureInterface::EXPOSURE_SO => 'Sud Ouest',
            ExposureInterface::EXPOSURE_EO => 'Est Ouest',
            ExposureInterface::EXPOSURE_NS => 'Nord Sud',
        ];
    }

    /**
     * Get Exposure
     *
     * @return null|string
     */
    public function getExposure()
    {
        return $this->exposure;
    }

    /**
     * Set Exposure
     *
     * @param null|string $exposure
     *
     * @return $this
     */
    public function setExposure($exposure = null)
    {
        $this->exposure = $exposure;

        return $this;
    }

    /**
     * Get Exposure Name
     *
     * @return null|string
     */
    public function getExposureName()
    {
        if (null === $this->getExposure()) {
            return null;
        }

        return self::getExposureNameList()[$this->getExposure()];
    }
}
