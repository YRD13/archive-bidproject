<?php

namespace Eenov\DefaultBundle\Entity\Doctrine;

use Doctrine\ORM\Mapping as ORM;
use Eenov\DefaultBundle\ORM\Point;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Trait CoordinatesTrait
 *
*
 */
trait CoordinatesTrait
{
    /**
     * @var Point
     * @ORM\Column(type="point")
     * @Assert\NotNull(message="Les coordonnées doivent être renseignées")
     */
    private $point;

    /**
     * Get Point
     *
     * @return Point
     */
    public function getPoint()
    {
        return $this->point;
    }

    /**
     * Set Point
     *
     * @param mixed $point Point
     *
     * @return $this
     */
    public function setPoint($point)
    {
        $this->point = $point;

        return $this;
    }
}
