<?php

namespace Eenov\DefaultBundle\Entity\Doctrine;

/**
 * Interface FundingInterface
 *
*
 */
interface FundingInterface
{
    const FUNDING_BORROWING = 'BORROWING';
    const FUNDING_CASH = 'CASH';
}
