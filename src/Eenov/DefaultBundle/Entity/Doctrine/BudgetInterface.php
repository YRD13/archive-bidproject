<?php

namespace Eenov\DefaultBundle\Entity\Doctrine;

/**
 * Interface BudgetInterface
 *
*
 */
interface BudgetInterface
{
    const BUDGET_0_50000 = '0_50000';
    const BUDGET_50000_100000 = '50000_100000';
    const BUDGET_100000_150000 = '100000_150000';
    const BUDGET_150000_200000 = '150000_200000';
    const BUDGET_200000_250000 = '200000_250000';
    const BUDGET_250000_300000 = '250000_300000';
    const BUDGET_300000_350000 = '300000_350000';
    const BUDGET_350000_400000 = '350000_400000';
    const BUDGET_400000_500000 = '400000_500000';
    const BUDGET_500000_600000 = '500000_600000';
    const BUDGET_MORE_600000 = 'MORE_600000';
}
