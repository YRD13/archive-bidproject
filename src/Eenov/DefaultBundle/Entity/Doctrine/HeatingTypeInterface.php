<?php

namespace Eenov\DefaultBundle\Entity\Doctrine;

/**
 * Interface HeatingTypeInterface
 *
*
 */
interface HeatingTypeInterface
{
    const HEATING_TYPE_NONE = 'NONE';
    const HEATING_TYPE_ELECTRICAL = 'ELECTRICAL';
    const HEATING_TYPE_GAS = 'GAS';
    const HEATING_TYPE_GEOTHERMAL = 'GEOTHERMAL';
    const HEATING_TYPE_REVERSIBLE = 'REVERSIBLE';
    const HEATING_TYPE_SOLAR = 'SOLAR';
    const HEATING_TYPE_WOOD = 'WOOD';
    const HEATING_TYPE_FUEL_OIL = 'FUEL_OIL';
    const HEATING_TYPE_HEAT_PUMP = "HEAT_PUMP";
}
