<?php

namespace Eenov\DefaultBundle\Entity\Doctrine;

/**
 * Interface BuyerInterface
 *
*
 */
interface BuyerInterface
{
    const BUYER_STATUS_OWNER = 'OWNER';
    const BUYER_STATUS_TENANT = 'TENANT';
}
