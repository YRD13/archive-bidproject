<?php

namespace Eenov\DefaultBundle\Entity\Doctrine;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class AcquisitionTrait
 *
*
 */
trait AcquisitionTrait
{
    /**
     * @var null|string
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Choice(callback="getAcquisitionList")
     */
    private $acquisition;

    /**
     * Get acquisition list
     *
     * @return string[]
     */
    public static function getAcquisitionList()
    {
        return [
            AcquisitionInterface::ACQUISITION_FIRST,
            AcquisitionInterface::ACQUISITION_INVESTMENT,
        ];
    }

    /**
     * Get acquisition name list
     *
     * @return string[]
     */
    public static function getAcquisitionNameList()
    {
        return [
            AcquisitionInterface::ACQUISITION_FIRST => 'Première aquisition',
            AcquisitionInterface::ACQUISITION_INVESTMENT => 'Investissement',
        ];
    }

    /**
     * Get Acquisition
     *
     * @return null|string
     */
    public function getAcquisition()
    {
        return $this->acquisition;
    }

    /**
     * Set Acquisition
     *
     * @param null|string $acquisition
     *
     * @return $this
     */
    public function setAcquisition($acquisition = null)
    {
        $this->acquisition = $acquisition;

        return $this;
    }

    /**
     * Get Acquisition Name
     *
     * @return null|string
     */
    public function getAcquisitionName()
    {
        if (null === $this->getAcquisition()) {
            return null;
        }

        return self::getAcquisitionNameList()[$this->getAcquisition()];
    }
}
