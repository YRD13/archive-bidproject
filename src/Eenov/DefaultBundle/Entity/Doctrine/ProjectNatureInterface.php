<?php

namespace Eenov\DefaultBundle\Entity\Doctrine;

/**
 * Interface ProjectNatureInterface
 *
*
 */
interface ProjectNatureInterface
{
    const PROJECT_NATURE_PRINCIPAL_RESIDENCE = 'PRINCIPAL_RESIDENCE';
    const PROJECT_NATURE_SECOND_HOME = 'SECOND_HOME';
}
