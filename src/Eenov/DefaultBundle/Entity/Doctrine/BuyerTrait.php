<?php

namespace Eenov\DefaultBundle\Entity\Doctrine;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class BuyerTrait
 *
*
 */
trait BuyerTrait
{
    /**
     * @var null|string
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Choice(callback="getBuyerStatusList")
     */
    private $buyerStatus;

    /**
     * @var null|\DateTime
     * @ORM\Column(type="date", nullable=true)
     * @Assert\Date()
     */
    private $buyerStatusDate;

    /**
     * Get buyer list
     *
     * @return string[]
     */
    public static function getBuyerStatusList()
    {
        return [
            BuyerInterface::BUYER_STATUS_OWNER,
            BuyerInterface::BUYER_STATUS_TENANT,
        ];
    }

    /**
     * Get buyer name list
     *
     * @return string[]
     */
    public static function getBuyerStatusNameList()
    {
        return [
            BuyerInterface::BUYER_STATUS_OWNER => 'Propriétaire',
            BuyerInterface::BUYER_STATUS_TENANT => 'Locataire',
        ];
    }

    /**
     * Get BuyerStatus
     *
     * @return null|string
     */
    public function getBuyerStatus()
    {
        return $this->buyerStatus;
    }

    /**
     * Set BuyerStatus
     *
     * @param null|string $buyerStatus
     *
     * @return $this
     */
    public function setBuyerStatus($buyerStatus)
    {
        $this->buyerStatus = $buyerStatus;

        return $this;
    }

    /**
     * Get Buyer Name
     *
     * @return null|string
     */
    public function getBuyerStatusName()
    {
        if (null === $this->getBuyerStatus()) {
            return null;
        }

        return self::getBuyerStatusNameList()[$this->getBuyerStatus()];
    }

    /**
     * Get BuyerStatusDate
     *
     * @return null|int
     */
    public function getBuyerStatusDate()
    {
        return $this->buyerStatusDate;
    }

    /**
     * Set BuyerStatusDate
     *
     * @param null|int $buyerStatusDate
     *
     * @return $this
     */
    public function setBuyerStatusDate($buyerStatusDate = null)
    {
        $this->buyerStatusDate = $buyerStatusDate;

        return $this;
    }
}
