<?php

namespace Eenov\DefaultBundle\Entity\Doctrine;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Trait CondoTrait
 *
*
 */
trait CondoTrait
{
    /**
     * @var bool
     * @ORM\Column(type="boolean")
     * @Assert\NotNull()
     * @Assert\Type("bool")
     */
    private $isCondo = false;

    /**
     * @var null|int
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Type("int")
     * @Assert\GreaterThan(0)
     */
    private $condoLots;

    /**
     * @var null|float
     * @ORM\Column(type="float", nullable=true)
     * @Assert\Type("float")
     * @Assert\GreaterThanOrEqual(0)
     */
    private $condoCharges;

    /**
     * @var null|string
     * @ORM\Column(length=20, nullable=true)
     * @Assert\Type("string")
     * @Assert\Length(max=20)
     * @Assert\Choice(callback="getCondoChargesRecurrenceList")
     */
    private $condoChargesRecurrence;

    /**
     * @var null|bool
     * @ORM\Column(type="boolean", nullable=true)
     * @Assert\Type("bool")
     */
    private $condoHasOngoingProceedings;

    /**
     * Get Condo Charges Recurrence List
     *
     * @return string[]
     */
    public static function getCondoChargesRecurrenceList()
    {
        return array_keys(self::getCondoChargesRecurrenceNameList());
    }

    /**
     * Get Condo Charges Recurrence Name List
     *
     * @return string[]
     */
    public static function getCondoChargesRecurrenceNameList()
    {
        return [
            CondoInterface::CONDO_CHARGE_RECURRENCE_MONTHLY => 'Charges mensuelles',
            CondoInterface::CONDO_CHARGE_RECURRENCE_QUARTERLY => 'Charges trimestrielles',
            CondoInterface::CONDO_CHARGE_RECURRENCE_YEARLY => 'Charges annuelles',
        ];
    }

    /**
     * Get Condo Charges Recurrence Name
     *
     * @return null|string
     */
    public function getCondoChargesRecurrenceName()
    {
        if (null !== $key = $this->getCondoChargesRecurrence()) {
            return self::getCondoChargesRecurrenceNameList()[$key];
        }

        return null;
    }

    /**
     * Get IsCondo
     *
     * @return boolean
     */
    public function getIsCondo()
    {
        return $this->isCondo;
    }

    /**
     * Set IsCondo
     *
     * @param boolean $isCondo
     *
     * @return $this
     */
    public function setIsCondo($isCondo)
    {
        $this->isCondo = $isCondo;

        return $this;
    }

    /**
     * Get CondoLots
     *
     * @return null|int
     */
    public function getCondoLots()
    {
        return $this->condoLots;
    }

    /**
     * Set CondoLots
     *
     * @param null|int $condoLots
     *
     * @return $this
     */
    public function setCondoLots($condoLots)
    {
        $this->condoLots = $condoLots;

        return $this;
    }

    /**
     * Get CondoCharges
     *
     * @return null|float
     */
    public function getCondoCharges()
    {
        return $this->condoCharges;
    }

    /**
     * Set CondoCharges
     *
     * @param null|float $condoCharges
     *
     * @return $this
     */
    public function setCondoCharges($condoCharges)
    {
        $this->condoCharges = $condoCharges;

        return $this;
    }

    /**
     * Get CondoChargesRecurrence
     *
     * @return null|string
     */
    public function getCondoChargesRecurrence()
    {
        return $this->condoChargesRecurrence;
    }

    /**
     * Set CondoChargesRecurrence
     *
     * @param null|string $condoChargesRecurrence
     *
     * @return $this
     */
    public function setCondoChargesRecurrence($condoChargesRecurrence)
    {
        $this->condoChargesRecurrence = $condoChargesRecurrence;

        return $this;
    }

    /**
     * Get CondoHasOngoingProceedings
     *
     * @return null|bool
     */
    public function getCondoHasOngoingProceedings()
    {
        return $this->condoHasOngoingProceedings;
    }

    /**
     * Set CondoHasOngoingProceedings
     *
     * @param null|bool $condoHasOngoingProceedings
     *
     * @return $this
     */
    public function setCondoHasOngoingProceedings($condoHasOngoingProceedings)
    {
        $this->condoHasOngoingProceedings = $condoHasOngoingProceedings;

        return $this;
    }
}
