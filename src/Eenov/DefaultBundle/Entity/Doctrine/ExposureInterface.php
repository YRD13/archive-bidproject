<?php

namespace Eenov\DefaultBundle\Entity\Doctrine;

/**
 * Interface ExposureInterface
 *
*
 */
interface ExposureInterface
{
    const EXPOSURE_E = 'E';
    const EXPOSURE_N = 'N';
    const EXPOSURE_NE = 'NE';
    const EXPOSURE_NO = 'NO';
    const EXPOSURE_O = 'O';
    const EXPOSURE_S = 'S';
    const EXPOSURE_SE = 'SE';
    const EXPOSURE_SO = 'SO';
    const EXPOSURE_EO = 'EO';
    const EXPOSURE_NS = 'NS';
}
