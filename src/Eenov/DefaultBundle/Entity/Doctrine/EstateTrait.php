<?php

namespace Eenov\DefaultBundle\Entity\Doctrine;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class EstateTrait
 *
*
 */
trait EstateTrait
{
    /**
     * @var null|string
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Choice(callback="getEstateList")
     */
    private $estate;

    /**
     * Get estate list
     *
     * @return string[]
     */
    public static function getEstateList()
    {
        return [
            EstateInterface::ESTATE_NEW_CONDITION,
            EstateInterface::ESTATE_RENOVATED_BY_ARCHITECT,
            EstateInterface::ESTATE_TO_REFRESH,
            EstateInterface::ESTATE_WORK_REQUIRED,
            EstateInterface::ESTATE_TO_RENOVATE,
            EstateInterface::ESTATE_GOOD,
        ];
    }

    /**
     * Get estate name list
     *
     * @return string[]
     */
    public static function getEstateNameList()
    {
        return [
            EstateInterface::ESTATE_NEW_CONDITION => 'État neuf',
            EstateInterface::ESTATE_RENOVATED_BY_ARCHITECT => 'Rénové par un architecte',
            EstateInterface::ESTATE_TO_REFRESH => 'À rafraichir',
            EstateInterface::ESTATE_WORK_REQUIRED => 'Travaux à prévoir',
            EstateInterface::ESTATE_TO_RENOVATE => 'À rénover',
            EstateInterface::ESTATE_GOOD => 'Bon état général',
        ];
    }

    /**
     * Get Estate
     *
     * @return null|string
     */
    public function getEstate()
    {
        return $this->estate;
    }

    /**
     * Set Estate
     *
     * @param null|string $estate
     *
     * @return $this
     */
    public function setEstate($estate = null)
    {
        $this->estate = $estate;

        return $this;
    }

    /**
     * Get Estate Name
     *
     * @return null|string
     */
    public function getEstateName()
    {
        if (null === $this->getEstate()) {
            return null;
        }

        return self::getEstateNameList()[$this->getEstate()];
    }
}
