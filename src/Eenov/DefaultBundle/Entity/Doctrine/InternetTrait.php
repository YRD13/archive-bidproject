<?php

namespace Eenov\DefaultBundle\Entity\Doctrine;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class InternetTrait
 *
*
 */
trait InternetTrait
{
    /**
     * @var null|string
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Choice(callback="getInternetList")
     */
    private $internet;

    /**
     * Get internet list
     *
     * @return string[]
     */
    public static function getInternetList()
    {
        return [
            InternetInterface::INTERNET_LESS_56,
            InternetInterface::INTERNET_56_512,
            InternetInterface::INTERNET_512_2000,
            InternetInterface::INTERNET_2000_8000,
            InternetInterface::INTERNET_8000_20000,
            InternetInterface::INTERNET_MORE_20000,
        ];
    }

    /**
     * Get internet name list
     *
     * @return string[]
     */
    public static function getInternetNameList()
    {
        return [
            InternetInterface::INTERNET_LESS_56 => '< 56 Kb',
            InternetInterface::INTERNET_56_512 => 'De 56 Kb à 512 Kb',
            InternetInterface::INTERNET_512_2000 => 'De 512 Kb à 2 Mb',
            InternetInterface::INTERNET_2000_8000 => 'De 2 Mb à 8 Mb',
            InternetInterface::INTERNET_8000_20000 => 'De 8 Mb à 20 Mb',
            InternetInterface::INTERNET_MORE_20000 => 'Plus 20 Mb',
        ];
    }

    /**
     * Get Internet
     *
     * @return null|string
     */
    public function getInternet()
    {
        return $this->internet;
    }

    /**
     * Set Internet
     *
     * @param null|string $internet
     *
     * @return $this
     */
    public function setInternet($internet = null)
    {
        $this->internet = $internet;

        return $this;
    }

    /**
     * Get Internet Name
     *
     * @return null|string
     */
    public function getInternetName()
    {
        if (null === $this->getInternet()) {
            return null;
        }

        return self::getInternetNameList()[$this->getInternet()];
    }
}
