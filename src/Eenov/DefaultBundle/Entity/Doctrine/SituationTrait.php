<?php

namespace Eenov\DefaultBundle\Entity\Doctrine;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class SituationTrait
 *
*
 */
trait SituationTrait
{
    /**
     * @var null|string
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Choice(callback="getSituationList")
     */
    private $situation;

    /**
     * @var null|\DateTime
     * @ORM\Column(type="date", nullable=true)
     * @Assert\Date()
     */
    private $situationDate;

    /**
     * Get situation list
     *
     * @return string[]
     */
    public static function getSituationList()
    {
        return [
            SituationInterface::SITUATION_CDI,
            SituationInterface::SITUATION_CDD,
            SituationInterface::SITUATION_UNEMPLOYED,
            SituationInterface::SITUATION_OTHER,
        ];
    }

    /**
     * Get situation name list
     *
     * @return string[]
     */
    public static function getSituationNameList()
    {
        return [
            SituationInterface::SITUATION_CDI => 'CDI',
            SituationInterface::SITUATION_CDD => 'CDD',
            SituationInterface::SITUATION_UNEMPLOYED => 'Sans emploi',
            SituationInterface::SITUATION_OTHER => 'Autre',
        ];
    }

    /**
     * Get Situation
     *
     * @return null|string
     */
    public function getSituation()
    {
        return $this->situation;
    }

    /**
     * Set Situation
     *
     * @param null|string $situation
     *
     * @return $this
     */
    public function setSituation($situation = null)
    {
        $this->situation = $situation;

        return $this;
    }

    /**
     * Get Situation Name
     *
     * @return null|string
     */
    public function getSituationName()
    {
        if (null === $this->getSituation()) {
            return null;
        }

        return self::getSituationNameList()[$this->getSituation()];
    }

    /**
     * Get SituationDate
     *
     * @return null|int
     */
    public function getSituationDate()
    {
        return $this->situationDate;
    }

    /**
     * Set SituationDate
     *
     * @param null|int $situationDate
     *
     * @return $this
     */
    public function setSituationDate($situationDate = null)
    {
        $this->situationDate = $situationDate;

        return $this;
    }
}
