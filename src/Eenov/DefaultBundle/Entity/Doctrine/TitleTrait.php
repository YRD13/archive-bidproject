<?php

namespace Eenov\DefaultBundle\Entity\Doctrine;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class TitleTrait
 *
*
 */
trait TitleTrait
{
    /**
     * @var null|string
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Choice(callback="getTitleList")
     */
    private $title;

    /**
     * Get title list
     *
     * @return string[]
     */
    public static function getTitleList()
    {
        return [
            TitleInterface::TITLE_MR,
            TitleInterface::TITLE_MISS,
            TitleInterface::TITLE_MS,
        ];
    }

    /**
     * Get title name list
     *
     * @return string[]
     */
    public static function getTitleNameList()
    {
        return [
            TitleInterface::TITLE_MR => 'Monsieur',
            TitleInterface::TITLE_MISS => 'Mademoiselle',
            TitleInterface::TITLE_MS => 'Madame',
        ];
    }

    /**
     * Get Title
     *
     * @return null|string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set Title
     *
     * @param null|string $title
     *
     * @return $this
     */
    public function setTitle($title = null)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get Title Name
     *
     * @return null|string
     */
    public function getTitleName()
    {
        if (null === $this->getTitle()) {
            return null;
        }

        return self::getTitleNameList()[$this->getTitle()];
    }
}
