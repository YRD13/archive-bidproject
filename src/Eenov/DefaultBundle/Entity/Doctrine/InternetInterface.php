<?php

namespace Eenov\DefaultBundle\Entity\Doctrine;

/**
 * Interface InternetInterface
 *
*
 */
interface InternetInterface
{
    const INTERNET_2000_8000 = '2000_8000';
    const INTERNET_512_2000 = '512_2000';
    const INTERNET_56_512 = '56_512';
    const INTERNET_8000_20000 = '8000_20000';
    const INTERNET_LESS_56 = 'LESS_56';
    const INTERNET_MORE_20000 = 'MORE_20000';
}
