<?php

namespace Eenov\DefaultBundle\Entity\Doctrine;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class HeatingSystemTrait
 *
*
 */
trait HeatingSystemTrait
{
    /**
     * @var null|string
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Choice(callback="getHeatingSystemList")
     */
    private $heatingSystem;

    /**
     * Get heatingSystem list
     *
     * @return string[]
     */
    public static function getHeatingSystemList()
    {
        return [
            HeatingSystemInterface::HEATING_SYSTEM_SINGLE,
            HeatingSystemInterface::HEATING_SYSTEM_COLLECTIVE,
            HeatingSystemInterface::HEATING_SYSTEM_CONVECTION,
            HeatingSystemInterface::HEATING_SYSTEM_CONVECTOR,
            HeatingSystemInterface::HEATING_SYSTEM_GROUND,
            HeatingSystemInterface::HEATING_SYSTEM_INSERTS,
            HeatingSystemInterface::HEATING_SYSTEM_RADIANT,
            HeatingSystemInterface::HEATING_SYSTEM_RADIATOR,
        ];
    }

    /**
     * Get heatingSystem name list
     *
     * @return string[]
     */
    public static function getHeatingSystemNameList()
    {
        return [
            HeatingSystemInterface::HEATING_SYSTEM_SINGLE => 'Individuel',
            HeatingSystemInterface::HEATING_SYSTEM_COLLECTIVE => 'Collectif',
            HeatingSystemInterface::HEATING_SYSTEM_CONVECTION => 'Air pulsé',
            HeatingSystemInterface::HEATING_SYSTEM_CONVECTOR => 'Convecteur',
            HeatingSystemInterface::HEATING_SYSTEM_GROUND => 'Au sol',
            HeatingSystemInterface::HEATING_SYSTEM_INSERTS => 'Inserts',
            HeatingSystemInterface::HEATING_SYSTEM_RADIANT => 'Radiant',
            HeatingSystemInterface::HEATING_SYSTEM_RADIATOR => 'Radiateur',
        ];
    }

    /**
     * Get HeatingSystem
     *
     * @return null|string
     */
    public function getHeatingSystem()
    {
        return $this->heatingSystem;
    }

    /**
     * Set HeatingSystem
     *
     * @param null|string $heatingSystem
     *
     * @return $this
     */
    public function setHeatingSystem($heatingSystem = null)
    {
        $this->heatingSystem = $heatingSystem;

        return $this;
    }

    /**
     * Get HeatingSystem Name
     *
     * @return null|string
     */
    public function getHeatingSystemName()
    {
        if (null === $this->getHeatingSystem()) {
            return null;
        }

        return self::getHeatingSystemNameList()[$this->getHeatingSystem()];
    }
}
