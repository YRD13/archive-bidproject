<?php

namespace Eenov\DefaultBundle\Entity\Doctrine;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ProjectNatureTrait
 *
*
 */
trait ProjectNatureTrait
{
    /**
     * @var null|string
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Choice(callback="getProjectNatureList")
     */
    private $projectNature;

    /**
     * Get projectNature list
     *
     * @return string[]
     */
    public static function getProjectNatureList()
    {
        return [
            ProjectNatureInterface::PROJECT_NATURE_PRINCIPAL_RESIDENCE,
            ProjectNatureInterface::PROJECT_NATURE_SECOND_HOME,
        ];
    }

    /**
     * Get projectNature name list
     *
     * @return string[]
     */
    public static function getProjectNatureNameList()
    {
        return [
            ProjectNatureInterface::PROJECT_NATURE_PRINCIPAL_RESIDENCE => 'Résidence principale',
            ProjectNatureInterface::PROJECT_NATURE_SECOND_HOME => 'Résidence secondaire',
        ];
    }

    /**
     * Get ProjectNature
     *
     * @return null|string
     */
    public function getProjectNature()
    {
        return $this->projectNature;
    }

    /**
     * Set ProjectNature
     *
     * @param null|string $projectNature
     *
     * @return $this
     */
    public function setProjectNature($projectNature = null)
    {
        $this->projectNature = $projectNature;

        return $this;
    }

    /**
     * Get ProjectNature Name
     *
     * @return null|string
     */
    public function getProjectNatureName()
    {
        if (null === $this->getProjectNature()) {
            return null;
        }

        return self::getProjectNatureNameList()[$this->getProjectNature()];
    }
}
