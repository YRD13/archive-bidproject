<?php

namespace Eenov\DefaultBundle\Entity\Doctrine;

/**
 * Interface HeatingSystemInterface
 *
*
 */
interface HeatingSystemInterface
{
    const HEATING_SYSTEM_COLLECTIVE = 'COLLECTIVE';
    const HEATING_SYSTEM_CONVECTION = 'CONVECTION';
    const HEATING_SYSTEM_CONVECTOR = 'CONVECTOR';
    const HEATING_SYSTEM_GROUND = 'GROUND';
    const HEATING_SYSTEM_INSERTS = 'INSERTS';
    const HEATING_SYSTEM_RADIANT = 'RADIANT';
    const HEATING_SYSTEM_RADIATOR = 'RADIATOR';
    const HEATING_SYSTEM_SINGLE = 'SINGLE';
}
