<?php

namespace Eenov\DefaultBundle\Entity\Doctrine;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class FundingTrait
 *
*
 */
trait FundingTrait
{
    /**
     * @var null|string
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Choice(callback="getFundingList")
     */
    private $funding;

    /**
     * Get funding list
     *
     * @return string[]
     */
    public static function getFundingList()
    {
        return [
            FundingInterface::FUNDING_BORROWING,
            FundingInterface::FUNDING_CASH,
        ];
    }

    /**
     * Get funding name list
     *
     * @return string[]
     */
    public static function getFundingNameList()
    {
        return [
            FundingInterface::FUNDING_BORROWING => 'Emprunt',
            FundingInterface::FUNDING_CASH => 'Comptant',
        ];
    }

    /**
     * Get Funding
     *
     * @return null|string
     */
    public function getFunding()
    {
        return $this->funding;
    }

    /**
     * Set Funding
     *
     * @param null|string $funding
     *
     * @return $this
     */
    public function setFunding($funding = null)
    {
        $this->funding = $funding;

        return $this;
    }

    /**
     * Get Funding Name
     *
     * @return null|string
     */
    public function getFundingName()
    {
        if (null === $this->getFunding()) {
            return null;
        }

        return self::getFundingNameList()[$this->getFunding()];
    }
}
