<?php

namespace Eenov\DefaultBundle\Entity\Doctrine;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Trait PriceTrait
 *
*
 */
trait PriceTrait
{
    /**
     * @var string
     * @ORM\Column(type="string", length=50)
     * @Assert\NotNull()
     * @Assert\Length(min="3", max="50")
     */
    private $name;

    /**
     * @var int
     * @ORM\Column(type="integer")
     * @Assert\NotNull()
     * @Assert\GreaterThan(0)
     */
    private $duration;

    /**
     * @var float
     * @ORM\Column(type="float")
     * @Assert\NotBlank()
     * @Assert\GreaterThanOrEqual(0)
     */
    private $amount;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     * @Assert\NotNull()
     */
    private $perSlot = false;

    /**
     * @var null|int
     * @ORM\Column(name="slot_count", type="integer", nullable=true)
     */
    private $count;

    /**
     * Get Amount
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set Amount
     *
     * @param float $amount Amount
     *
     * @return $this
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get Count
     *
     * @return int|null
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * Set Count
     *
     * @param int|null $count
     *
     * @return $this
     */
    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }

    /**
     * Get Duration
     *
     * @return int
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set Duration
     *
     * @param int $duration
     *
     * @return $this
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get PerSlot
     *
     * @return bool
     */
    public function getPerSlot()
    {
        return $this->perSlot;
    }

    /**
     * Set PerSlot
     *
     * @param bool $perSlot
     *
     * @return $this
     */
    public function setPerSlot($perSlot)
    {
        $this->perSlot = $perSlot;

        return $this;
    }
}
