<?php

namespace Eenov\DefaultBundle\Entity\Doctrine;

/**
 * Interface CondoInterface
 *
*
 */
interface CondoInterface
{
    const CONDO_CHARGE_RECURRENCE_MONTHLY = 'MONTHLY';
    const CONDO_CHARGE_RECURRENCE_QUARTERLY = 'QUARTERLY';
    const CONDO_CHARGE_RECURRENCE_YEARLY = 'YEARLY';
}
