<?php

namespace Eenov\DefaultBundle\Entity\Doctrine;

/**
 * Interface TitleInterface
 *
*
 */
interface TitleInterface
{
    const TITLE_MISS = 'MISS';
    const TITLE_MR = 'MR';
    const TITLE_MS = 'MS';
}
