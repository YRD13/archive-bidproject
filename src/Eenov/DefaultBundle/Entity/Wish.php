<?php

namespace Eenov\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EB\DoctrineBundle\Entity\CreatedInterface;
use EB\DoctrineBundle\Entity\Doctrine\CreatedTrait;
use EB\DoctrineBundle\Entity\Doctrine\IdTrait;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Wish
 *
*
 * @ORM\Entity(repositoryClass="Eenov\DefaultBundle\Repository\WishRepository")
 * @ORM\Table()
 * @UniqueEntity(fields={"user","bid"})
 */
class Wish implements CreatedInterface
{
    use IdTrait;
    use CreatedTrait;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @Assert\NotNull()
     */
    private $user;

    /**
     * @var Bid
     * @ORM\ManyToOne(targetEntity="Bid")
     * @Assert\NotNull()
     */
    private $bid;

    /**
     * @param null|User $user User
     */
    public function __construct(User $user = null)
    {
        $this->setUser($user);
    }

    /**
     * Get Bid
     *
     * @return Bid
     */
    public function getBid()
    {
        return $this->bid;
    }

    /**
     * Set Bid
     *
     * @param Bid $bid Bid
     *
     * @return Wish
     */
    public function setBid($bid)
    {
        $this->bid = $bid;

        return $this;
    }

    /**
     * Get similar search
     *
     * @return array
     */
    public function getSimilarSearch()
    {
        return [
            'where' => sprintf(
                '%s %s',
                $this->getBid()->getPublish()->getZip(),
                $this->getBid()->getPublish()->getCity()
            ),
        ];
    }

    /**
     * Get User
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set User
     *
     * @param User $user User
     *
     * @return Search
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }
}
