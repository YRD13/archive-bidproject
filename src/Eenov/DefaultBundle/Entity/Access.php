<?php

namespace Eenov\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EB\DoctrineBundle\Entity\CreatedInterface;
use EB\DoctrineBundle\Entity\Doctrine\CreatedTrait;
use EB\DoctrineBundle\Entity\Doctrine\IdTrait;
use EB\DoctrineBundle\Entity\Doctrine\UpdatedTrait;
use EB\DoctrineBundle\Entity\Doctrine\ValidatedTrait;
use EB\DoctrineBundle\Entity\UpdatedInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Class Access
 * Cette classe représente la demande d'accès à une vente formulée
 * par un utilisateur. Elle contient aussi la réponse de l'agence.
 *
*
 * @ORM\Entity(repositoryClass="Eenov\DefaultBundle\Repository\AccessRepository")
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="ask",columns={"user_id","bid_id"})})
 * @UniqueEntity(fields={"user","bid"})
 */
class Access implements CreatedInterface, UpdatedInterface
{
    use IdTrait;
    use CreatedTrait;
    use UpdatedTrait;
    use ValidatedTrait;

    /**
     * @var Bid
     * @ORM\ManyToOne(targetEntity="Bid", inversedBy="accesses")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull()
     */
    private $bid;

    /**
     * @var null|Mandate
     * @ORM\OneToOne(targetEntity="Mandate", inversedBy="access")
     */
    private $mandate;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull(message="Cet utilisateur n'existe pas")
     */
    private $user;

    /**
     * @var null|string
     * @ORM\Column(type="text", nullable=true)
     */
    private $userComment;

    /**
     * @var null|string
     * @ORM\Column(type="text", nullable=true)
     */
    private $agencyComment;

    /**
     * @var boolean
     * @ORM\Column(name="is_readonly", type="boolean")
     * @Assert\NotNull()
     */
    private $readonly = false;

    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return sprintf('Accès de %s à %s', $this->getUser(), $this->getBid());
    }

    /**
     * Assert is valid
     *
     * @param ExecutionContextInterface $context
     * @Assert\Callback()
     */
    public function assertIsValid(ExecutionContextInterface $context)
    {
        if ($this->hasBeenValidated() && $this->isValid()) {
            if (null === $this->getMandate()) {
                $context->buildViolation('Vous devez sélectionner le mandat associé')->atPath('mandate')->addViolation();
            }
        }

        if($this->getReadonly()) {
            if (null !== $this->getMandate()) {
                $context->buildViolation('Vous ne devez pas sélectionner de mandat pour un compte en lecture seule')->atPath('mandate')->addViolation();
            }
        }
    }

    /**
     * Get AgencyComment
     *
     * @return null|string
     */
    public function getAgencyComment()
    {
        return $this->agencyComment;
    }

    /**
     * Set AgencyComment
     *
     * @param null|string $agencyComment
     *
     * @return Access
     */
    public function setAgencyComment($agencyComment)
    {
        $this->agencyComment = $agencyComment;

        return $this;
    }

    /**
     * Get Bid
     *
     * @return Bid
     */
    public function getBid()
    {
        return $this->bid;
    }

    /**
     * Set Bid
     *
     * @param Bid $bid
     *
     * @return Access
     */
    public function setBid(Bid $bid)
    {
        $this->bid = $bid;

        return $this;
    }

    /**
     * Get Mandate
     *
     * @return null|Mandate
     */
    public function getMandate()
    {
        return $this->mandate;
    }

    /**
     * Set Mandate
     *
     * @param null|Mandate $mandate
     *
     * @return Access
     */
    public function setMandate(Mandate $mandate = null)
    {
        $this->mandate = $mandate;

        return $this;
    }

    /**
     * Get Readonly
     *
     * @return bool
     */
    public function getReadonly()
    {
        return $this->readonly;
    }

    /**
     * Set Readonly
     *
     * @param bool $readonly
     *
     * @return Access
     */
    public function setReadonly($readonly)
    {
        $this->readonly = $readonly;

        return $this;
    }

    /**
     * Get User
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set User
     *
     * @param User $user
     *
     * @return Access
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get UserComment
     *
     * @return null|string
     */
    public function getUserComment()
    {
        return $this->userComment;
    }

    /**
     * Set UserComment
     *
     * @param null|string $userComment
     *
     * @return Access
     */
    public function setUserComment($userComment)
    {
        $this->userComment = $userComment;

        return $this;
    }
}
