<?php

namespace Eenov\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EB\DoctrineBundle\Entity\CreatedInterface;
use EB\DoctrineBundle\Entity\Doctrine\CreatedTrait;
use EB\DoctrineBundle\Entity\Doctrine\FileTrait;
use EB\DoctrineBundle\Entity\Doctrine\IdTrait;
use EB\DoctrineBundle\Entity\Doctrine\UpdatedTrait;
use EB\DoctrineBundle\Entity\FileInterface;
use EB\DoctrineBundle\Entity\UpdatedInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Class Mandate
 * Cette classe représente la demande d'accès au format PDF.
 *
*
 * @ORM\Entity(repositoryClass="Eenov\DefaultBundle\Repository\MandateRepository")
 */
class Mandate implements CreatedInterface, UpdatedInterface, FileInterface
{
    use IdTrait;
    use CreatedTrait;
    use UpdatedTrait;
    use FileTrait;

    /**
     * @var Bid
     * @ORM\ManyToOne(targetEntity="Bid", inversedBy="accesses")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull()
     */
    private $bid;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotNull()
     * @Assert\Length(max=255)
     */
    private $buyer;

    /**
     * @var null|string
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotNull()
     * @Assert\Length(max=255)
     * @Assert\Email()
     */
    private $buyerEmail;

    /**
     * @var null|Access
     * @ORM\OneToOne(targetEntity="Access", mappedBy="mandate")
     */
    private $access;

    /**
     * @var \DateTime
     * @ORM\Column(type="date")
     * @Assert\NotNull()
     * @Assert\Date()
     */
    private $visited;

    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return (string)$this->getBuyer();
    }

    /**
     * assertIsValid
     *
     * @param ExecutionContextInterface $context
     * @Assert\Callback()
     */
    public function assertIsValid(ExecutionContextInterface $context)
    {
        if (null === $this->getId() && null === $this->getFile()) {
            $context->buildViolation('Vous devez choisir un fichier')->atPath('file')->addViolation();
        }
    }

    /**
     * Get Bid
     *
     * @return Bid
     */
    public function getBid()
    {
        return $this->bid;
    }

    /**
     * Set Bid
     *
     * @param Bid $bid
     *
     * @return Mandate
     */
    public function setBid(Bid $bid)
    {
        $this->bid = $bid;

        return $this;
    }

    /**
     * Get Buyer
     *
     * @return string
     */
    public function getBuyer()
    {
        return $this->buyer;
    }

    /**
     * Set Buyer
     *
     * @param string $buyer
     *
     * @return Mandate
     */
    public function setBuyer($buyer)
    {
        $this->buyer = $buyer;

        return $this;
    }

    /**
     * Get BuyerEmail
     *
     * @return null|string
     */
    public function getBuyerEmail()
    {
        return $this->buyerEmail;
    }

    /**
     * Set BuyerEmail
     *
     * @param null|string $buyerEmail
     *
     * @return $this
     */
    public function setBuyerEmail($buyerEmail)
    {
        $this->buyerEmail = $buyerEmail;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getCacheKey()
    {
        return 'mandate';
    }

    /**
     * Get Access
     *
     * @return null|Access
     */
    public function getAccess()
    {
        return $this->access;
    }

    /**
     * Set Access
     *
     * @param null|Access $access
     *
     * @return Mandate
     */
    public function setAccess(Access $access = null)
    {
        if (null === $access && null !== $this->access) {
            $this->access->setMandate(null);
        }
        $this->access = $access;
        if (null !== $access) {
            $access->setMandate($this);
        }

        return $this;
    }

    /**
     * Get Visited
     *
     * @return \DateTime
     */
    public function getVisited()
    {
        return $this->visited;
    }

    /**
     * Set Visited
     *
     * @param \DateTime $visited
     *
     * @return Mandate
     */
    public function setVisited(\DateTime $visited = null)
    {
        $this->visited = $visited;

        return $this;
    }
}
