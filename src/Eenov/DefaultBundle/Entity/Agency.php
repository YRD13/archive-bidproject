<?php

namespace Eenov\DefaultBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use EB\DoctrineBundle\Entity\CreatedInterface;
use EB\DoctrineBundle\Entity\Doctrine\CreatedTrait;
use EB\DoctrineBundle\Entity\Doctrine\FileReadableTrait;
use EB\DoctrineBundle\Entity\Doctrine\FileTrait;
use EB\DoctrineBundle\Entity\Doctrine\FileVersionableTrait;
use EB\DoctrineBundle\Entity\Doctrine\IdTrait;
use EB\DoctrineBundle\Entity\Doctrine\SlugTrait;
use EB\DoctrineBundle\Entity\Doctrine\UpdatedTrait;
use EB\DoctrineBundle\Entity\FileReadableInterface;
use EB\DoctrineBundle\Entity\FileVersionableInterface;
use EB\DoctrineBundle\Entity\SlugInterface;
use EB\DoctrineBundle\Entity\UpdatedInterface;
use Eenov\DefaultBundle\Entity\Doctrine\AddressTrait;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Class Agency
 *
*
 * @ORM\Entity(repositoryClass="Eenov\DefaultBundle\Repository\AgencyRepository")
 * @ORM\Table()
 */
class Agency implements CreatedInterface, UpdatedInterface, SlugInterface, FileReadableInterface, FileVersionableInterface
{
    const TYPE_EURL = 'EURL';
    const TYPE_SARL = 'SARL';
    const TYPE_SAS = 'SAS';

    use IdTrait;
    use CreatedTrait;
    use UpdatedTrait;
    use AddressTrait;
    use SlugTrait;
    use FileTrait;
    use FileReadableTrait;
    use FileVersionableTrait;

    /**
     * @var null|User
     * @ORM\ManyToOne(targetEntity="User")
     */
    private $commercial;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="ownedAgencies", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     * @Assert\NotNull()
     */
    private $owner;

    /**
     * @var null|User
     * @ORM\OneToOne(targetEntity="User", inversedBy="agencyManaged", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private $responsible;

    /**
     * @var null|User
     * @ORM\OneToOne(targetEntity="User", inversedBy="agencyRepresented", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private $contact;

    /**
     * @var string
     * @ORM\Column(type="string", length=45)
     * @Assert\NotBlank()
     * @Assert\Length(max=45)
     * @Assert\Type("string")
     */
    private $name;

    /**
     * @var null|string
     * @ORM\Column(type="string", length=45, nullable=true)
     * @Assert\Length(max=45)
     * @Assert\Type("string")
     */
    private $displayName;

    /**
     * @var string
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     * @Assert\Length(max=100)
     * @Assert\Email()
     */
    private $email;

    /**
     * @var string
     * @ORM\Column(type="string", length=20)
     * @Assert\NotBlank()
     * @Assert\Length(max=20)
     */
    private $phone;

    /**
     * @var null|string
     * @ORM\Column(type="string", length=20, nullable=true)
     * @Assert\Length(max=20)
     * @Assert\Choice(callback="getTypeList")
     */
    private $type;

    /**
     * @var null|int
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Type("int")
     */
    private $siret;

    /**
     * @var null|string
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\Length(max=100)
     */
    private $rcs;

    /**
     * @var null|string
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\Length(max=100)
     */
    private $cardNumber;

    /**
     * @var null|string
     * @ORM\Column(type="string", length=200, nullable=true)
     * @Assert\Length(max=200)
     * @Assert\Url()
     */
    private $website;

    /**
     * @var null|Agency
     * @ORM\ManyToOne(targetEntity="Agency", inversedBy="daughters")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $mother;

    /**
     * @var Agency[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="Agency", mappedBy="mother")
     */
    private $daughters;

    /**
     * @var Legal[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="Legal", mappedBy="agency")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $legals;

    /**
     * {@inheritdoc}
     */
    public function __construct()
    {
        $this->daughters = new ArrayCollection();
        $this->legals = new ArrayCollection();
    }

    /**
     * Get type list
     *
     * @return string[]
     */
    public static function getTypeList()
    {
        return [
            self::TYPE_SARL,
            self::TYPE_SAS,
            self::TYPE_EURL,
        ];
    }

    /**
     * Get type name list
     *
     * @return string[]
     */
    public static function getTypeNameList()
    {
        return [
            self::TYPE_SARL => 'SARL',
            self::TYPE_SAS => 'SAS',
            self::TYPE_EURL => 'EURL',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return (string)$this->getName();
    }

    /**
     * Add daughter
     *
     * @param Agency $agency
     *
     * @return Agency
     */
    public function addDaughter(Agency $agency)
    {
        if (false === $this->daughters->contains($agency)) {
            $this->daughters->add($agency);
            $agency->setMother($this);
        }

        return $this;
    }

    /**
     * Add legal
     *
     * @param Legal $legal
     *
     * @return Agency
     */
    public function addLegal(Legal $legal)
    {
        if (false === $this->legals->contains($legal)) {
            $this->legals->add($legal);
            $legal->setAgency($this);
        }

        return $this;
    }

    /**
     * assertIsValid
     *
     * @param ExecutionContextInterface $context
     * @Assert\Callback()
     */
    public function assertIsValid(ExecutionContextInterface $context)
    {
        if (null === $this->getId() && null === $this->getFile()) {
            $context->buildViolation('Vous devez choisir un logo')->atPath('file')->addViolation();
        }
        $context->getViolations()->addAll($context->getValidator()->validate($this->getFile(), [new Assert\Image()]));

        if (null !== $commercial = $this->getCommercial()) {
            if (User::ROLE_COMMERCIAL !== $commercial->getRole()) {
                $context->buildViolation('Le commercial de l\'agence doit avoir le rôle correspondant.')->atPath('commercial')->addViolation();
            }
        }

        if (null !== $owner = $this->getOwner()) {
            if (User::ROLE_AGENCY_OWNER !== $owner->getRole()) {
                $context->buildViolation('Le propriétaire de l\'agence doit avoir le rôle correspondant.')->atPath('owner')->addViolation();
            }
        }

        if (null !== $responsible = $this->getResponsible()) {
            if (User::ROLE_AGENCY_RESPONSIBLE !== $responsible->getRole()) {
                $context->buildViolation('Le responsable de l\'agence doit avoir le rôle correspondant.')->atPath('responsible')->addViolation();
            }
        }

        if (null !== $contact = $this->getContact()) {
            if (User::ROLE_AGENCY_CONTACT !== $contact->getRole()) {
                $context->buildViolation('Le contact de l\'agence doit avoir le rôle correspondant.')->atPath('contact')->addViolation();
            }
        }

        if (null === $this->getAddress()) {
            $context->buildViolation('Vous devez renseigner votre adresse')->atPath('address')->addViolation();
        }
        if (null === $this->getZip()) {
            $context->buildViolation('Vous devez renseigner votre code postal')->atPath('zip')->addViolation();
        }
        if (null === $this->getCity()) {
            $context->buildViolation('Vous devez renseigner votre ville')->atPath('city')->addViolation();
        }
        if (null === $this->getCountry()) {
            $context->buildViolation('Vous devez renseigner votre pays')->atPath('country')->addViolation();
        }

        if (null !== $this->getId() && null !== $mother = $this->getMother()) {
            if (in_array($this->getId(), $this->getMotherIds())) {
                $context->buildViolation('Vous ne pouvez pas choisir cette agence comme parent (redondance)')->atPath('mother')->addViolation();
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getCacheKey()
    {
        return 'agency';
    }

    /**
     * Get CardNumber
     *
     * @return mixed
     */
    public function getCardNumber()
    {
        return $this->cardNumber;
    }

    /**
     * Set CardNumber
     *
     * @param mixed $cardNumber CardNumber
     *
     * @return Agency
     */
    public function setCardNumber($cardNumber)
    {
        $this->cardNumber = $cardNumber;

        return $this;
    }

    /**
     * Get Commercial
     *
     * @return null|User
     */
    public function getCommercial()
    {
        return $this->commercial;
    }

    /**
     * Set Commercial
     *
     * @param null|User $commercial
     *
     * @return Agency
     */
    public function setCommercial(User $commercial = null)
    {
        $this->commercial = $commercial;

        return $this;
    }

    /**
     * Get Responsible
     *
     * @return null|User
     */
    public function getResponsible()
    {
        return $this->responsible;
    }

    /**
     * Set Responsible
     *
     * @param null|User $responsible
     *
     * @return Agency
     */
    public function setResponsible(User $responsible = null)
    {
        $this->responsible = $responsible;

        return $this;
    }

    /**
     * Get Contact
     *
     * @return null|User
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set Contact
     *
     * @param null|User $contact
     *
     * @return Agency
     */
    public function setContact(User $contact = null)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get daughters
     *
     * @return Agency[]|ArrayCollection
     */
    public function getDaughters()
    {
        return $this->daughters;
    }

    /**
     * getChildrenIds
     *
     * @return int[]
     */
    public function getDaughtersIds()
    {
        $ids = [];
        foreach ($this->getDaughters() as $daughter) {
            $ids = array_merge($ids, [$daughter->getId()], $daughter->getDaughtersIds());
        }

        return $ids;
    }

    /**
     * Get Email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set Email
     *
     * @param string $email
     *
     * @return Agency
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get Legals
     *
     * @return Legal[]|ArrayCollection
     */
    public function getLegals()
    {
        return $this->legals;
    }

    /**
     * Get mother
     *
     * @return null|Agency
     */
    public function getMother()
    {
        return $this->mother;
    }

    /**
     * Set mother
     *
     * @param null|Agency $mother
     *
     * @return Agency
     */
    public function setMother(Agency $mother = null)
    {
        $this->mother = $mother;
        if (null !== $mother) {
            $mother->addDaughter($this);
        }

        return $this;
    }

    /**
     * Get Mother Ids
     *
     * @return int[]
     */
    public function getMotherIds()
    {
        return array_map(function (Agency $agency) {
            return $agency->getId();
        }, $this->getMothers());
    }

    /**
     * Get Mothers
     *
     * @return Agency[]
     */
    public function getMothers()
    {
        if (null === $mother = $this->getMother()) {
            return [];
        }

        return array_merge([$mother], $mother->getMothers());
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Agency
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get DisplayName
     *
     * @return null|string
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }

    /**
     * Set DisplayName
     *
     * @param null|string $displayName
     *
     * @return $this
     */
    public function setDisplayName($displayName)
    {
        $this->displayName = $displayName;

        return $this;
    }

    /**
     * Get Owner
     *
     * @return User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set Owner
     *
     * @param User $owner
     *
     * @return Agency
     */
    public function setOwner(User $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Agency
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get Rcs
     *
     * @return null|string
     */
    public function getRcs()
    {
        return $this->rcs;
    }

    /**
     * Set Rcs
     *
     * @param null|string $rcs Rcs
     *
     * @return Agency
     */
    public function setRcs($rcs)
    {
        $this->rcs = $rcs;

        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole()
    {
        return sprintf(User::_ROLE_AGENCY, $this->getId());
    }

    /**
     * Get Siret
     *
     * @return null|int
     */
    public function getSiret()
    {
        return $this->siret;
    }

    /**
     * Set Siret
     *
     * @param null|int $siret
     *
     * @return Agency
     */
    public function setSiret($siret)
    {
        $this->siret = $siret;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getStringToSlug()
    {
        return $this->getName();
    }

    /**
     * Get Type
     *
     * @return null|string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set Type
     *
     * @param null|string $type
     *
     * @return Agency
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type name
     *
     * @return string
     */
    public function getTypeName()
    {
        return self::getTypeNameList()[$this->getType()];
    }

    /**
     * Get Website
     *
     * @return null|string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set Website
     *
     * @param null|string $website
     *
     * @return Agency
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Remove daughter
     *
     * @param Agency $agency
     *
     * @return Agency
     */
    public function removeDaughter(Agency $agency)
    {
        if (true === $this->daughters->contains($agency)) {
            $this->daughters->removeElement($agency);
            $agency->setMother(null);
        }

        return $this;
    }

    /**
     * Remove legal
     *
     * @param Legal $legal
     *
     * @return Agency
     */
    public function removeLegal(Legal $legal)
    {
        if (true === $this->legals->contains($legal)) {
            $this->legals->removeElement($legal);
            $legal->setAgency(null);
        }

        return $this;
    }
}
