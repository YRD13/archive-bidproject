<?php

namespace Eenov\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EB\DoctrineBundle\Entity\CreatedInterface;
use EB\DoctrineBundle\Entity\Doctrine\CreatedTrait;
use EB\DoctrineBundle\Entity\Doctrine\IdTrait;
use EB\DoctrineBundle\Entity\Doctrine\UpdatedTrait;
use EB\DoctrineBundle\Entity\UpdatedInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class MoneyIn
 *
*
 * @ORM\Entity()
 */
class MoneyIn implements CreatedInterface, UpdatedInterface
{
    use IdTrait;
    use CreatedTrait;
    use UpdatedTrait;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull()
     */
    private $user;

    /**
     * @var string
     * @ORM\Column()
     * @Assert\NotNull()
     * @Assert\Type("string")
     * @Assert\Length(max=255)
     */
    private $pack;

    /**
     * @var null|string
     * @ORM\Column(nullable=true)
     * @Assert\Type("string")
     * @Assert\Length(max=255)
     */
    private $code;

    /**
     * @var int
     * @ORM\Column(type="integer")
     * @Assert\NotNull()
     * @Assert\Type("int")
     * @Assert\GreaterThan(0)
     */
    private $slotCount;

    /**
     * @var float
     * @ORM\Column(type="float")
     * @Assert\NotNull()
     * @Assert\Type("float")
     * @Assert\GreaterThan(0)
     */
    private $pricePerSlot;

    /**
     * @var int
     * @ORM\Column(type="integer")
     * @Assert\NotNull()
     * @Assert\Type("int")
     * @Assert\GreaterThan(0)
     */
    private $slotDurationInDays;

    /**
     * @var null|\DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\DateTime()
     */
    private $validated;

    /**
     * @var null|\DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\DateTime()
     */
    private $canceled;

    /**
     * @var null|\DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\DateTime()
     */
    private $errored;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Type("int")
     */
    private $transactionId;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     * @Assert\Type("float")
     */
    private $transactionAmount;

    /**
     * @var string
     * @ORM\Column(nullable=true)
     * @Assert\Type("string")
     */
    private $transactionMessage;

    /**
     * Get User
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set User
     *
     * @param User $user
     *
     * @return $this
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get Pack
     *
     * @return string
     */
    public function getPack()
    {
        return $this->pack;
    }

    /**
     * Set Pack
     *
     * @param string $pack
     *
     * @return $this
     */
    public function setPack($pack)
    {
        $this->pack = $pack;

        return $this;
    }

    /**
     * Get Code
     *
     * @return null|string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set Code
     *
     * @param null|string $code
     *
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get SlotCount
     *
     * @return int
     */
    public function getSlotCount()
    {
        return $this->slotCount;
    }

    /**
     * Set SlotCount
     *
     * @param int $slotCount
     *
     * @return $this
     */
    public function setSlotCount($slotCount)
    {
        $this->slotCount = $slotCount;

        return $this;
    }

    /**
     * Get PricePerSlot
     *
     * @return float
     */
    public function getPricePerSlot()
    {
        return $this->pricePerSlot;
    }

    /**
     * Set PricePerSlot
     *
     * @param float $pricePerSlot
     *
     * @return $this
     */
    public function setPricePerSlot($pricePerSlot)
    {
        $this->pricePerSlot = $pricePerSlot;

        return $this;
    }

    /**
     * Get SlotDurationInDays
     *
     * @return int
     */
    public function getSlotDurationInDays()
    {
        return $this->slotDurationInDays;
    }

    /**
     * Set SlotDurationInDays
     *
     * @param int $slotDurationInDays
     *
     * @return $this
     */
    public function setSlotDurationInDays($slotDurationInDays)
    {
        $this->slotDurationInDays = $slotDurationInDays;

        return $this;
    }

    /**
     * Get Validated
     *
     * @return null|\DateTime
     */
    public function getValidated()
    {
        return $this->validated;
    }

    /**
     * Set Validated
     *
     * @param null|\DateTime $validated
     *
     * @return $this
     */
    public function setValidated(\DateTime $validated = null)
    {
        $this->validated = $validated;

        return $this;
    }

    /**
     * Get Canceled
     *
     * @return null|\DateTime
     */
    public function getCanceled()
    {
        return $this->canceled;
    }

    /**
     * Set Canceled
     *
     * @param null|\DateTime $canceled
     *
     * @return $this
     */
    public function setCanceled(\DateTime $canceled = null)
    {
        $this->canceled = $canceled;

        return $this;
    }

    /**
     * Get Errored
     *
     * @return null|\DateTime
     */
    public function getErrored()
    {
        return $this->errored;
    }

    /**
     * Set Errored
     *
     * @param null|\DateTime $errored
     *
     * @return $this
     */
    public function setErrored(\DateTime $errored = null)
    {
        $this->errored = $errored;

        return $this;
    }

    /**
     * Get TransactionId
     *
     * @return int
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }

    /**
     * Set TransactionId
     *
     * @param int $transactionId
     *
     * @return $this
     */
    public function setTransactionId($transactionId)
    {
        $this->transactionId = $transactionId;

        return $this;
    }

    /**
     * Get TransactionAmount
     *
     * @return float
     */
    public function getTransactionAmount()
    {
        return $this->transactionAmount;
    }

    /**
     * Set TransactionAmount
     *
     * @param float $transactionAmount
     *
     * @return $this
     */
    public function setTransactionAmount($transactionAmount)
    {
        $this->transactionAmount = $transactionAmount;

        return $this;
    }

    /**
     * Get TransactionMessage
     *
     * @return string
     */
    public function getTransactionMessage()
    {
        return $this->transactionMessage;
    }

    /**
     * Set TransactionMessage
     *
     * @param string $transactionMessage
     *
     * @return $this
     */
    public function setTransactionMessage($transactionMessage)
    {
        $this->transactionMessage = $transactionMessage;

        return $this;
    }
}
