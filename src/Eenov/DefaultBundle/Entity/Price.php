<?php

namespace Eenov\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EB\DoctrineBundle\Entity\Doctrine\IdTrait;
use Eenov\DefaultBundle\Entity\Doctrine\PriceTrait;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Price
 *
*
 * @ORM\Entity(repositoryClass="Eenov\DefaultBundle\Repository\PriceRepository")
 * @ORM\Table()
 * @UniqueEntity(fields={"name"})
 */
class Price
{
    use IdTrait;
    use PriceTrait;

    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return (string)$this->getName();
    }
}
