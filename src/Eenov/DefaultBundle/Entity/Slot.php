<?php

namespace Eenov\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EB\DoctrineBundle\Entity\CreatedInterface;
use EB\DoctrineBundle\Entity\Doctrine\CreatedTrait;
use EB\DoctrineBundle\Entity\Doctrine\IdTrait;
use EB\DoctrineBundle\Entity\Doctrine\UpdatedTrait;
use EB\DoctrineBundle\Entity\LoggableInterface;
use EB\DoctrineBundle\Entity\UpdatedInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Slot
 *
*
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Eenov\DefaultBundle\Repository\SlotRepository")
 */
class Slot implements CreatedInterface, UpdatedInterface, LoggableInterface
{
    use IdTrait;
    use CreatedTrait;
    use UpdatedTrait;

    /**
     * @var Agency
     * @ORM\ManyToOne(targetEntity="Agency")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     * @Assert\NotNull()
     */
    private $agency;

    /**
     * @var null|Subscription
     * @ORM\ManyToOne(targetEntity="Subscription", inversedBy="slots")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $subscription;

    /**
     * @var \DateTime
     * @ORM\Column(type="date")
     * @Assert\NotNull()
     * @Assert\Date()
     */
    private $started;

    /**
     * @var \DateTime
     * @ORM\Column(type="date")
     * @Assert\NotNull()
     * @Assert\Date()
     */
    private $ended;

    /**
     * @var null|Bid
     * @ORM\OneToOne(targetEntity="Bid", mappedBy="slot")
     */
    private $bid;

    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return sprintf('Slot de %s allant du %s au %s', $this->getAgency(), $this->getStarted()->format('d/m/y'), $this->getEnded()->format('d/m/y'));
    }

    /**
     * Get Agency
     *
     * @return Agency
     */
    public function getAgency()
    {
        return $this->agency;
    }

    /**
     * Set Agency
     *
     * @param Agency $agency Agency
     *
     * @return Slot
     */
    public function setAgency(Agency $agency)
    {
        $this->agency = $agency;

        return $this;
    }

    /**
     * Get Bid
     *
     * @return null|Bid
     */
    public function getBid()
    {
        return $this->bid;
    }

    /**
     * Set Bid
     *
     * @param null|Bid $bid
     *
     * @return Slot
     */
    public function setBid(Bid $bid = null)
    {
        $this->bid = $bid;

        return $this;
    }

    /**
     * Get Ended
     *
     * @return \DateTime
     */
    public function getEnded()
    {
        return $this->ended;
    }

    /**
     * Set Ended
     *
     * @param \DateTime $ended Ended
     *
     * @return Slot
     */
    public function setEnded($ended)
    {
        $this->ended = $ended;

        return $this;
    }

    /**
     * Get Started
     *
     * @return \DateTime
     */
    public function getStarted()
    {
        return $this->started;
    }

    /**
     * Set Started
     *
     * @param \DateTime $started Started
     *
     * @return Slot
     */
    public function setStarted($started)
    {
        $this->started = $started;

        return $this;
    }

    /**
     * Get Subscription
     *
     * @return null|Subscription
     */
    public function getSubscription()
    {
        return $this->subscription;
    }

    /**
     * Set Subscription
     *
     * @param null|Subscription $subscription
     *
     * @return Slot
     */
    public function setSubscription(Subscription $subscription = null)
    {
        $this->subscription = $subscription;

        return $this;
    }
}
