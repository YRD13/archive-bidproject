<?php

namespace Eenov\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EB\DoctrineBundle\Entity\Doctrine\IdTrait;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class AdvertGround
 *
*
 * @ORM\Entity()
 * @ORM\Table()
 */
class AdvertGround
{
    use IdTrait;

    const SERVICING_ELECTRICITY = 'SERVICING_ELECTRICITY';
    const SERVICING_GAS = 'SERVICING_GAS';
    const SERVICING_MAINS_DRAINAGE = 'SERVICING_MAINS_DRAINAGE';
    const SERVICING_PHONE = 'SERVICING_PHONE';
    const SERVICING_WATER = 'SERVICING_WATER';

    /**
     * @var Advert
     * @ORM\OneToOne(targetEntity="Advert", mappedBy="ground")
     * @Assert\NotNull()
     */
    private $advert;

    /**
     * @var string
     * @ORM\Column(type="text")
     * @Assert\NotNull()
     * @Assert\Type("string")
     */
    private $pluZoning;

    /**
     * @var float
     * @ORM\Column(type="float")
     * @Assert\NotNull()
     * @Assert\Type("float")
     */
    private $frontageLength;

    /**
     * @var float
     * @ORM\Column(type="float")
     * @Assert\NotNull()
     * @Assert\Type("float")
     */
    private $fieldDepth;

    /**
     * @var string
     * @ORM\Column(type="string", length=100)
     * @Assert\NotNull()
     * @Assert\Length(max=100)
     */
    private $soilNature;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     * @Assert\NotNull()
     */
    private $serviced = false;

    /**
     * @var string[]
     * @ORM\Column(type="array")
     */
    private $servicingTypes = [];

    /**
     * getServicingTypesList
     *
     * @return string[]
     */
    public static function getServicingTypesList()
    {
        return [
            self::SERVICING_ELECTRICITY,
            self::SERVICING_GAS,
            self::SERVICING_MAINS_DRAINAGE,
            self::SERVICING_PHONE,
            self::SERVICING_WATER,
        ];
    }

    /**
     * getServicingTypesNameList
     *
     * @return string[]
     */
    public static function getServicingTypesNameList()
    {
        return [
            self::SERVICING_ELECTRICITY => 'Électricité',
            self::SERVICING_GAS => 'Gaz',
            self::SERVICING_MAINS_DRAINAGE => 'Tout à l\'égout',
            self::SERVICING_PHONE => 'Téléphone',
            self::SERVICING_WATER => 'Eau',
        ];
    }

    /**
     * Get Advert
     *
     * @return Advert
     */
    public function getAdvert()
    {
        return $this->advert;
    }

    /**
     * Set Advert
     *
     * @param Advert $advert
     *
     * @return AdvertGround
     */
    public function setAdvert(Advert $advert)
    {
        $this->advert = $advert;

        return $this;
    }

    /**
     * Get FieldDepth
     *
     * @return float
     */
    public function getFieldDepth()
    {
        return $this->fieldDepth;
    }

    /**
     * Set FieldDepth
     *
     * @param float $fieldDepth
     *
     * @return AdvertGround
     */
    public function setFieldDepth($fieldDepth)
    {
        $this->fieldDepth = $fieldDepth;

        return $this;
    }

    /**
     * Get FrontageLength
     *
     * @return float
     */
    public function getFrontageLength()
    {
        return $this->frontageLength;
    }

    /**
     * Set FrontageLength
     *
     * @param float $frontageLength
     *
     * @return AdvertGround
     */
    public function setFrontageLength($frontageLength)
    {
        $this->frontageLength = $frontageLength;

        return $this;
    }

    /**
     * Get PluZoning
     *
     * @return string
     */
    public function getPluZoning()
    {
        return $this->pluZoning;
    }

    /**
     * Set PluZoning
     *
     * @param string $pluZoning
     *
     * @return AdvertGround
     */
    public function setPluZoning($pluZoning)
    {
        $this->pluZoning = $pluZoning;

        return $this;
    }

    /**
     * Get Serviced
     *
     * @return bool
     */
    public function getServiced()
    {
        return $this->serviced;
    }

    /**
     * Set Serviced
     *
     * @param bool $serviced
     *
     * @return AdvertGround
     */
    public function setServiced($serviced)
    {
        $this->serviced = $serviced;

        return $this;
    }

    /**
     * Get ServicingTypes
     *
     * @return string[]
     */
    public function getServicingTypes()
    {
        return $this->servicingTypes;
    }

    /**
     * Set ServicingTypes
     *
     * @param string[] $servicingTypes
     *
     * @return AdvertGround
     */
    public function setServicingTypes(array $servicingTypes)
    {
        $this->servicingTypes = $servicingTypes;

        return $this;
    }

    /**
     * Get SoilNature
     *
     * @return string
     */
    public function getSoilNature()
    {
        return $this->soilNature;
    }

    /**
     * Set SoilNature
     *
     * @param string $soilNature
     *
     * @return AdvertGround
     */
    public function setSoilNature($soilNature)
    {
        $this->soilNature = $soilNature;

        return $this;
    }
}
