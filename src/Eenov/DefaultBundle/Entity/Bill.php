<?php

namespace Eenov\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EB\DoctrineBundle\Entity\CreatedInterface;
use EB\DoctrineBundle\Entity\Doctrine\CreatedTrait;
use EB\DoctrineBundle\Entity\Doctrine\FileTrait;
use EB\DoctrineBundle\Entity\Doctrine\IdTrait;
use EB\DoctrineBundle\Entity\Doctrine\UpdatedTrait;
use EB\DoctrineBundle\Entity\FileInterface;
use EB\DoctrineBundle\Entity\UpdatedInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Class Bill
 *
*
 * @ORM\Entity(repositoryClass="Eenov\DefaultBundle\Repository\BillRepository")
 * @ORM\Table()
 */
class Bill implements CreatedInterface, UpdatedInterface, FileInterface
{
    use IdTrait;
    use CreatedTrait;
    use UpdatedTrait;
    use FileTrait;

    /**
     * @var Agency
     * @ORM\ManyToOne(targetEntity="Agency", inversedBy="legals")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull()
     */
    private $agency;

    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return sprintf('Facture #%s', $this->getId());
    }

    /**
     * {@inheritdoc}
     */
    public function getCacheKey()
    {
        return 'bill';
    }

    /**
     * assertIsValid
     *
     * @param ExecutionContextInterface $context
     * @Assert\Callback()
     */
    public function assertIsValid(ExecutionContextInterface $context)
    {
        if (null === $this->getId() && null === $this->getFile()) {
            $context->buildViolation('Vous devez sélectionner un fichier')->atPath('file')->addViolation();
        }
    }

    /**
     * Get Agency
     *
     * @return Agency
     */
    public function getAgency()
    {
        return $this->agency;
    }

    /**
     * Set Agency
     *
     * @param Agency $agency
     *
     * @return $this
     */
    public function setAgency(Agency $agency = null)
    {
        $this->agency = $agency;

        return $this;
    }
}
