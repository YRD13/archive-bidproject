<?php

namespace Eenov\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EB\DoctrineBundle\Entity\Doctrine\IdTrait;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class AdvertBuilding
 *
*
 * @ORM\Entity()
 * @ORM\Table()
 */
class AdvertBuilding
{
    use IdTrait;

    /**
     * @var Advert
     * @ORM\OneToOne(targetEntity="Advert", mappedBy="building")
     * @Assert\NotNull()
     */
    private $advert;

    /**
     * Get Advert
     *
     * @return Advert
     */
    public function getAdvert()
    {
        return $this->advert;
    }

    /**
     * Set Advert
     *
     * @param Advert $advert
     *
     * @return AdvertBuilding
     */
    public function setAdvert(Advert $advert)
    {
        $this->advert = $advert;

        return $this;
    }
}
