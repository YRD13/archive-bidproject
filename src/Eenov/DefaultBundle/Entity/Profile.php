<?php

namespace Eenov\DefaultBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use EB\DoctrineBundle\Entity\Doctrine\IdTrait;
use EB\DoctrineBundle\Entity\Doctrine\UpdatedTrait;
use EB\DoctrineBundle\Entity\UpdatedInterface;
use Eenov\DefaultBundle\Entity\Doctrine\AcquisitionTrait;
use Eenov\DefaultBundle\Entity\Doctrine\AddressTrait;
use Eenov\DefaultBundle\Entity\Doctrine\BudgetTrait;
use Eenov\DefaultBundle\Entity\Doctrine\BuyerTrait;
use Eenov\DefaultBundle\Entity\Doctrine\FundingTrait;
use Eenov\DefaultBundle\Entity\Doctrine\MaritalStatusTrait;
use Eenov\DefaultBundle\Entity\Doctrine\ProjectNatureTrait;
use Eenov\DefaultBundle\Entity\Doctrine\SituationTrait;
use Eenov\DefaultBundle\Entity\Doctrine\TitleTrait;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Profile
 *
*
 * @ORM\Entity()
 * @ORM\Table()
 */
class Profile implements UpdatedInterface
{
    use IdTrait;
    use UpdatedTrait;
    use FundingTrait;
    use AcquisitionTrait;
    use BuyerTrait;
    use SituationTrait;
    use TitleTrait;
    use ProjectNatureTrait;
    use MaritalStatusTrait;
    use AddressTrait;
    use BudgetTrait;

    /**
     * @var null|string
     * @ORM\Column(type="string", length=20, nullable=true)
     * @Assert\Length(max=20)
     */
    private $phone;

    /**
     * @var null|string
     * @ORM\Column(type="string", length=20, nullable=true)
     * @Assert\Length(max=20)
     */
    private $cellphone;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\DateTime()
     */
    private $birthday;

    /**
     * @var null|string
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\Length(min=1,max=100)
     */
    private $placeOfBirth;

    /**
     * @var null|int
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Type(type="integer")
     * @Assert\GreaterThanOrEqual(0)
     */
    private $childCount;

    /**
     * @var null|AdvertType
     * @ORM\ManyToOne(targetEntity="AdvertType")
     */
    private $advertType;

    /**
     * @var AdvertType[]|ArrayCollection
     * @ORM\ManyToMany(targetEntity="AdvertType")
     * @ORM\JoinTable(name="ProfileAdvertType")
     */
    private $advertTypes;

    /**
     * @var null|Bank
     * @ORM\ManyToOne(targetEntity="Bank")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $bank;

    /**
     * @var null|float
     * @ORM\Column(type="float", nullable=true)
     * @Assert\Type(type="numeric")
     */
    private $loan;

    /**
     * @var null|int
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Type("int")
     * @Assert\GreaterThanOrEqual(0)
     */
    private $loanDuration;

    /**
     * @var null|int
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Type("int")
     * @Assert\GreaterThanOrEqual(0)
     */
    private $contribution;

    /**
     * @var null|int
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Type("int")
     * @Assert\GreaterThanOrEqual(0)
     */
    private $collateral;

    /**
     * @var null|int
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Type("int")
     * @Assert\GreaterThanOrEqual(0)
     */
    private $bridgingLoan;

    /**
     * @var null|string
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\Type("string")
     * @Assert\Length(max=100)
     */
    private $job;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Type("int")
     * @Assert\GreaterThanOrEqual(0)
     */
    private $incomes;

    /**
     * @var null|int
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Type("int")
     * @Assert\GreaterThanOrEqual(0)
     */
    private $previousIncomes;

    /**
     * @var null|bool
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $sellBeforeBuying = false;

    /**
     * @var null|\DateTime
     * @ORM\Column(type="date", nullable=true)
     * @Assert\Date()
     */
    private $sellSinceDate;

    /**
     * @var Seller[]|ArrayCollection
     * @ORM\ManyToMany(targetEntity="Seller")
     * @ORM\JoinTable(name="ProfileSeller")
     */
    private $sellers;

    /**
     * {@inheritdoc}
     */
    public function __construct()
    {
        $this->advertTypes = new ArrayCollection();
        $this->sellers = new ArrayCollection();
    }

    /**
     * Add advertType
     *
     * @param AdvertType $advertType
     *
     * @return Profile
     */
    public function addAdvertType(AdvertType $advertType)
    {
        if (false === $this->advertTypes->contains($advertType)) {
            $this->advertTypes->add($advertType);
        }

        return $this;
    }

    /**
     * Add seller
     *
     * @param Seller $seller
     *
     * @return Profile
     */
    public function addSeller(Seller $seller)
    {
        if (false === $this->sellers->contains($seller)) {
            $this->sellers->add($seller);
        }

        return $this;
    }

    /**
     * Get AdvertType
     *
     * @return null|AdvertType
     */
    public function getAdvertType()
    {
        return $this->advertType;
    }

    /**
     * Set AdvertType
     *
     * @param null|AdvertType $advertType
     *
     * @return Profile
     */
    public function setAdvertType(AdvertType $advertType = null)
    {
        $this->advertType = $advertType;

        return $this;
    }

    /**
     * Get AdvertTypes
     *
     * @return AdvertType[]|ArrayCollection
     */
    public function getAdvertTypes()
    {
        return $this->advertTypes;
    }

    /**
     * Get Bank
     *
     * @return Bank|null
     */
    public function getBank()
    {
        return $this->bank;
    }

    /**
     * Set Bank
     *
     * @param Bank|null $bank Bank
     *
     * @return Profile
     */
    public function setBank(Bank $bank = null)
    {
        $this->bank = $bank;

        return $this;
    }

    /**
     * Get Birthday
     *
     * @return null|\DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set Birthday
     *
     * @param null|\DateTime $birthday
     *
     * @return Profile
     */
    public function setBirthday(\DateTime $birthday = null)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get BridgingLoan
     *
     * @return int|null
     */
    public function getBridgingLoan()
    {
        return $this->bridgingLoan;
    }

    /**
     * Set BridgingLoan
     *
     * @param int|null $bridgingLoan BridgingLoan
     *
     * @return Profile
     */
    public function setBridgingLoan($bridgingLoan)
    {
        $this->bridgingLoan = $bridgingLoan;

        return $this;
    }

    /**
     * Get Cellphone
     *
     * @return null|string
     */
    public function getCellphone()
    {
        return $this->cellphone;
    }

    /**
     * Set Cellphone
     *
     * @param null|string $cellphone
     *
     * @return Profile
     */
    public function setCellphone($cellphone)
    {
        $this->cellphone = $cellphone;

        return $this;
    }

    /**
     * Get ChildCount
     *
     * @return int|null
     */
    public function getChildCount()
    {
        return $this->childCount;
    }

    /**
     * Set ChildCount
     *
     * @param int|null $childCount
     *
     * @return Profile
     */
    public function setChildCount($childCount = null)
    {
        $this->childCount = $childCount;

        return $this;
    }

    /**
     * Get Collateral
     *
     * @return int|null
     */
    public function getCollateral()
    {
        return $this->collateral;
    }

    /**
     * Set Collateral
     *
     * @param int|null $collateral Collateral
     *
     * @return Profile
     */
    public function setCollateral($collateral)
    {
        $this->collateral = $collateral;

        return $this;
    }

    /**
     * Get Contribution
     *
     * @return int|null
     */
    public function getContribution()
    {
        return $this->contribution;
    }

    /**
     * Set Contribution
     *
     * @param int|null $contribution Contribution
     *
     * @return Profile
     */
    public function setContribution($contribution)
    {
        $this->contribution = $contribution;

        return $this;
    }

    /**
     * Get Job
     *
     * @return null|string
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * Set Job
     *
     * @param null|string $job
     *
     * @return Profile
     */
    public function setJob($job)
    {
        $this->job = $job;

        return $this;
    }

    /**
     * Get Incomes
     *
     * @return int
     */
    public function getIncomes()
    {
        return $this->incomes;
    }

    /**
     * Set Incomes
     *
     * @param int $incomes Incomes
     *
     * @return Profile
     */
    public function setIncomes($incomes)
    {
        $this->incomes = $incomes;

        return $this;
    }

    /**
     * Get Loan
     *
     * @return float|null
     */
    public function getLoan()
    {
        return $this->loan;
    }

    /**
     * Set Loan
     *
     * @param float|null $loan Loan
     *
     * @return Profile
     */
    public function setLoan($loan)
    {
        $this->loan = $loan;

        return $this;
    }

    /**
     * Get LoanDuration
     *
     * @return int|null
     */
    public function getLoanDuration()
    {
        return $this->loanDuration;
    }

    /**
     * Set LoanDuration
     *
     * @param int|null $loanDuration LoanDuration
     *
     * @return Profile
     */
    public function setLoanDuration($loanDuration)
    {
        $this->loanDuration = $loanDuration;

        return $this;
    }

    /**
     * Get Phone
     *
     * @return null|string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set Phone
     *
     * @param null|string $phone
     *
     * @return Profile
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get PlaceOfBirth
     *
     * @return null|string
     */
    public function getPlaceOfBirth()
    {
        return $this->placeOfBirth;
    }

    /**
     * Set PlaceOfBirth
     *
     * @param null|string $placeOfBirth
     *
     * @return Profile
     */
    public function setPlaceOfBirth($placeOfBirth = null)
    {
        $this->placeOfBirth = $placeOfBirth;

        return $this;
    }

    /**
     * Get PreviousIncomes
     *
     * @return int|null
     */
    public function getPreviousIncomes()
    {
        return $this->previousIncomes;
    }

    /**
     * Set PreviousIncomes
     *
     * @param int|null $previousIncomes PreviousIncomes
     *
     * @return Profile
     */
    public function setPreviousIncomes($previousIncomes)
    {
        $this->previousIncomes = $previousIncomes;

        return $this;
    }

    /**
     * Get SellBeforeBuying
     *
     * @return bool|null
     */
    public function getSellBeforeBuying()
    {
        return $this->sellBeforeBuying;
    }

    /**
     * Set SellBeforeBuying
     *
     * @param bool|null $sellBeforeBuying SellBeforeBuying
     *
     * @return Profile
     */
    public function setSellBeforeBuying($sellBeforeBuying)
    {
        $this->sellBeforeBuying = $sellBeforeBuying;

        return $this;
    }

    /**
     * Get SellSinceDate
     *
     * @return int|null
     */
    public function getSellSinceDate()
    {
        return $this->sellSinceDate;
    }

    /**
     * Set SellSinceDate
     *
     * @param int|null $sellSinceDate
     *
     * @return Profile
     */
    public function setSellSinceDate($sellSinceDate)
    {
        $this->sellSinceDate = $sellSinceDate;

        return $this;
    }

    /**
     * Get Sellers
     *
     * @return ArrayCollection|Seller[]
     */
    public function getSellers()
    {
        return $this->sellers;
    }

    /**
     * @return bool
     */
    public function isValid()
    {
        return
            (
                null !== $this->getPhone() ||
                null !== $this->getCellphone()
            ) &&
            null !== $this->getTitle() &&
            null !== $this->getBirthday() &&
            null !== $this->getPlaceOfBirth() &&
            null !== $this->getAddress() &&
            null !== $this->getZip() &&
            null !== $this->getCity() &&
            null !== $this->getCountry() &&
            null !== $this->getChildCount() &&
            null !== $this->getMaritalStatus() &&
            null !== $this->getBuyerStatus() &&
            null !== $this->getBuyerStatusDate() &&
            null !== $this->getAdvertType() &&
            null !== $this->getBudget() &&
            null !== $this->getJob() &&
            null !== $this->getSituation() &&
            null !== $this->getSituationDate() &&
//            null !== $this->getIncomes() &&
//            null !== $this->getPreviousIncomes() &&
            null !== $this->getProjectNature() &&
            null !== $this->getAcquisition() &&
            0 !== $this->getAdvertTypes()->count() &&
            null !== $this->getSellBeforeBuying() &&
            (
                false === $this->getSellBeforeBuying() ||
                (
                    null !== $this->getSellSinceDate() &&
                    0 !== $this->getSellers()->count()
                )
            )
//            null !== $this->getFunding() &&
//            (
//                FundingInterface::FUNDING_CASH === $this->getFunding() ||
//                (
//                    null !== $this->getBank() &&
//                    null !== $this->getLoan() &&
//                    null !== $this->getLoanDuration() &&
//                    null !== $this->getContribution() &&
//                    null !== $this->getCollateral() &&
//                    null !== $this->getBridgingLoan()
//                )
//            )
            ;
    }

    /**
     * Remove advertType
     *
     * @param AdvertType $advertType
     *
     * @return Profile
     */
    public function removeAdvertType(AdvertType $advertType)
    {
        if (true === $this->advertTypes->contains($advertType)) {
            $this->advertTypes->removeElement($advertType);
        }

        return $this;
    }

    /**
     * Remove seller
     *
     * @param Seller $seller
     *
     * @return Profile
     */
    public function removeSeller(Seller $seller)
    {
        if (true === $this->sellers->contains($seller)) {
            $this->sellers->removeElement($seller);
        }

        return $this;
    }
}
