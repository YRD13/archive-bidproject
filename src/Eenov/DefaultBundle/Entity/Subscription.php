<?php

namespace Eenov\DefaultBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use EB\DoctrineBundle\Entity\CreatedInterface;
use EB\DoctrineBundle\Entity\Doctrine\CreatedTrait;
use EB\DoctrineBundle\Entity\Doctrine\IdTrait;
use EB\DoctrineBundle\Entity\Doctrine\UpdatedTrait;
use EB\DoctrineBundle\Entity\LoggableInterface;
use EB\DoctrineBundle\Entity\UpdatedInterface;
use Eenov\DefaultBundle\Constraint\Overlap;
use Eenov\DefaultBundle\Entity\Doctrine\PriceTrait;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Subscription
 *
*
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Eenov\DefaultBundle\Repository\SubscriptionRepository")
 * @Overlap()
 */
class Subscription implements CreatedInterface, UpdatedInterface, LoggableInterface
{
    use IdTrait;
    use CreatedTrait;
    use UpdatedTrait;
    use PriceTrait;

    /**
     * @var Agency
     * @ORM\ManyToOne(targetEntity="Agency")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     * @Assert\NotNull()
     */
    private $agency;

    /**
     * @var Slot[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="Slot", mappedBy="subscription", cascade={"persist","remove"})
     * @ORM\OrderBy({"started"="DESC"})
     */
    private $slots;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     * @Assert\NotNull()
     * @Assert\DateTime()
     */
    private $started;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     * @Assert\NotNull()
     * @Assert\DateTime()
     */
    private $ended;

    /**
     * {@inheritdoc}
     */
    public function __construct()
    {
        $this->slots = new ArrayCollection();
    }

    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return sprintf('%s pour %s', $this->getName(), $this->getAgency());
    }

    /**
     * Add slot
     *
     * @param Slot $slot
     *
     * @return Subscription
     */
    public function addSlot(Slot $slot)
    {
        if (false === $this->slots->contains($slot)) {
            $this->slots->add($slot);
            $slot->setSubscription($this);
        }

        return $this;
    }

    /**
     * Get Agency
     *
     * @return Agency
     */
    public function getAgency()
    {
        return $this->agency;
    }

    /**
     * Set Agency
     *
     * @param Agency $agency
     *
     * @return Subscription
     */
    public function setAgency(Agency $agency)
    {
        $this->agency = $agency;

        return $this;
    }

    /**
     * Get Ended
     *
     * @return \DateTime
     */
    public function getEnded()
    {
        return $this->ended;
    }

    /**
     * Set Ended
     *
     * @param \DateTime $ended
     *
     * @return Subscription
     */
    public function setEnded(\DateTime $ended)
    {
        $this->ended = $ended;
        $this->ended->setTime(23, 59, 59);

        return $this;
    }

    /**
     * Get Slots
     *
     * @return ArrayCollection|Slot[]
     */
    public function getSlots()
    {
        return $this->slots;
    }

    /**
     * Get Started
     *
     * @return \DateTime
     */
    public function getStarted()
    {
        return $this->started;
    }

    /**
     * Set Started
     *
     * @param \DateTime $started
     *
     * @return Subscription
     */
    public function setStarted(\DateTime $started)
    {
        $this->started = $started;
        $this->started->setTime(0, 0, 0);

        return $this;
    }

    /**
     * Populate
     *
     * @param Price $price
     *
     * @return Subscription
     */
    public function populate(Price $price)
    {
        $this
            ->setName($price->getName())
            ->setDuration($price->getDuration())
            ->setAmount($price->getAmount())
            ->setPerSlot($price->getPerSlot())
            ->setCount($price->getCount());

        if (null !== $this->getStarted()) {
            $finalEnded = clone $this->getStarted();
            $finalEnded->add(new \DateInterval(sprintf('P%uM', $price->getDuration())));
            $this->setEnded($finalEnded);

            if (null !== $price->getCount()) {
                $started = clone $this->getStarted();

                $month = new \DateInterval('P1M');
                do {
                    $ended = clone $started;
                    $ended->add($month);

                    for ($i = 0; $i < $this->getCount(); $i++) {
                        $slot = new Slot();
                        $slot
                            ->setAgency($this->getAgency())
                            ->setStarted($started)
                            ->setEnded($ended)
                            ->setSubscription($this);

                        $this->addSlot($slot);
                    }

                    $started = clone $started;
                    $started->add($month);

                    $startedMidnight = clone $started;
                    $startedMidnight->setTime(23, 59, 59);
                } while ($startedMidnight < $finalEnded);
            }
        }

        return $this;
    }

    /**
     * Remove slot
     *
     * @param Slot $slot
     *
     * @return Subscription
     */
    public function removeSlot(Slot $slot)
    {
        if (true === $this->slots->contains($slot)) {
            $this->slots->removeElement($slot);
            $slot->setSubscription(null);
        }

        return $this;
    }
}
