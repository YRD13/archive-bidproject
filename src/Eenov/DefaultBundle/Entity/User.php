<?php

namespace Eenov\DefaultBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use EB\DoctrineBundle\Entity\CreatedInterface;
use EB\DoctrineBundle\Entity\Doctrine\CreatedTrait;
use EB\DoctrineBundle\Entity\Doctrine\IdTrait;
use EB\DoctrineBundle\Entity\Doctrine\UpdatedTrait;
use EB\DoctrineBundle\Entity\Doctrine\UserAdvancedTrait;
use EB\DoctrineBundle\Entity\Doctrine\UserLoginTrait;
use EB\DoctrineBundle\Entity\Doctrine\UserPasswordDateTrait;
use EB\DoctrineBundle\Entity\Doctrine\UserTrait;
use EB\DoctrineBundle\Entity\UpdatedInterface;
use EB\DoctrineBundle\Entity\UserInterface;
use EB\DoctrineBundle\Entity\UserLoginInterface;
use EB\DoctrineBundle\Entity\UserPasswordDateInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface as CoreUserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Class User
 *
*
 * @ORM\Entity(repositoryClass="Eenov\DefaultBundle\Repository\UserRepository")
 * @ORM\Table()
 * @UniqueEntity(fields={"username"})
 */
class User implements UserInterface, EquatableInterface, AdvancedUserInterface, UserLoginInterface, UserPasswordDateInterface, CreatedInterface, UpdatedInterface, \Serializable
{
    use IdTrait;
    use UserTrait {
        setUsername as defaultSetUsername;
    }
    use UserAdvancedTrait;
    use UserLoginTrait;
    use UserPasswordDateTrait;
    use CreatedTrait;
    use UpdatedTrait;

    // Global roles
    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_AGENCY_OWNER = 'ROLE_AGENCY_OWNER';
    const ROLE_AGENCY_RESPONSIBLE = 'ROLE_AGENCY_RESPONSIBLE';
    const ROLE_AGENCY_CONTACT = 'ROLE_AGENCY_CONTACT';
    const ROLE_COMMERCIAL = 'ROLE_COMMERCIAL';
    const ROLE_MODERATOR = 'ROLE_MODERATOR';
    const ROLE_SUPER_COMMERCIAL = 'ROLE_SUPER_COMMERCIAL';
    const ROLE_USER = 'ROLE_USER';

    // These roles are generated using the agency ID
    const _ROLE_AGENCY = 'ROLE_AGENCY_%u';

    /**
     * @var string
     * @ORM\Column(type="string", length=32)
     * @Assert\NotNull()
     * @Assert\Choice(callback="getRoleList")
     */
    private $role = self::ROLE_USER;

    /**
     * @var null|string
     * @ORM\Column(name="emailKey", type="string", length=32, nullable=true)
     */
    private $key;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(min="3", max="255")
     */
    private $firstname;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(min="3", max="255")
     */
    private $lastname;

    /**
     * @var Profile
     * @ORM\OneToOne(targetEntity="Profile", cascade={"persist","remove"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @Assert\NotBlank()
     */
    private $profile;

    /**
     * @var Agency[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="Agency", mappedBy="owner")
     */
    private $ownedAgencies;

    /**
     * @var null|Agency
     * @ORM\OneToOne(targetEntity="Agency", mappedBy="responsible")
     */
    private $agencyManaged;

    /**
     * @var null|Agency
     * @ORM\OneToOne(targetEntity="Agency", mappedBy="contact")
     */
    private $agencyRepresented;

    /**
     * @var User[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="User", mappedBy="accountant")
     */
    private $commercials;

    /**
     * @var null|User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="commercials")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $accountant;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $hasNewsletter = true;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\DateTime()
     */
    private $birthday;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(min="3", max="255")
     */
    private $country;

    /**
     * {@inheritdoc}
     */
    public function __construct($role = self::ROLE_USER)
    {
        $this->ownedAgencies = new ArrayCollection();
        $this->profile = new Profile();
        $this
            ->setRole($role)
            ->generateNewKey();
        $this->birthday = new \DateTime('1990-01-01');
        $this->country = 'France';
    }

    /**
     * Get role list
     *
     * @return string[]
     */
    public static function getRoleList()
    {
        return [
            self::ROLE_USER,
            self::ROLE_AGENCY_OWNER,
            self::ROLE_AGENCY_RESPONSIBLE,
            self::ROLE_AGENCY_CONTACT,
            self::ROLE_MODERATOR,
            self::ROLE_ADMIN,
            self::ROLE_COMMERCIAL,
            self::ROLE_SUPER_COMMERCIAL,
        ];
    }

    /**
     * Get role name list
     *
     * @return string[]
     */
    public static function getRoleNameList()
    {
        return [
            self::ROLE_USER => 'Utilisateur',
            self::ROLE_AGENCY_OWNER => 'Propriétaire d\'agence',
            self::ROLE_AGENCY_RESPONSIBLE => 'Responsable d\'agence',
            self::ROLE_AGENCY_CONTACT => 'Contact d\'agence',
            self::ROLE_MODERATOR => 'Modérateur',
            self::ROLE_ADMIN => 'Administrateur',
            self::ROLE_COMMERCIAL => 'Commercial',
            self::ROLE_SUPER_COMMERCIAL => 'Dir. commercial',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return (string)$this->getFullName();
    }

    /**
     * Returns the user roles (overwrite)
     *
     * @return array The roles
     */
    public function getRoles()
    {
        $roles = [$this->getRole()];

        foreach ($this->getOwnedAgencies() as $ownedAgency) {
            $roles[] = $ownedAgency->getRole();
        }

        $agencies = $this->getResponsibleAgencies();
        array_map(function (Agency $agency) use (&$roles) {
            $roles[] = $agency->getRole();
        }, $agencies);

        if (null !== $this->getAgencyRepresented()) {
            $roles[] = $this->getAgencyRepresented()->getRole();
        }

        return $roles;
    }

    /**
     * Get Responsible Agencies
     *
     * @return Agency[]
     */
    public function getResponsibleAgencies()
    {
        if (self::ROLE_AGENCY_RESPONSIBLE !== $this->getRole()) {
            return [];
        }

        $agencies = [];
        if (null !== $agency = $this->getAgencyManaged()) {
            $this->addDaugthersAgencies($agencies, $agency);
        }

        return $agencies;
    }

    /**
     * Add Daugthers Agencies
     *
     * @param array  $agencies Agencies
     * @param Agency $agency   Agency
     */
    private function addDaugthersAgencies(array &$agencies, Agency $agency)
    {
        $agencies[] = $agency;
        foreach ($agency->getDaughters() as $daugther) {
            $this->addDaugthersAgencies($agencies, $daugther);
        }
    }

    /**
     * Add commercial
     *
     * @param User $commercial
     *
     * @return $this
     */
    public function addCommercial(User $commercial)
    {
        if (false === $this->commercials->contains($commercial)) {
            $this->commercials->add($commercial);
            $commercial->setAccountant($this);
        }

        return $this;
    }

    /**
     * Add OwnedAgency
     *
     * @param Agency $agency
     *
     * @return User
     */
    public function addOwnedAgency(Agency $agency)
    {
        if (false === $this->ownedAgencies->contains($agency)) {
            $this->ownedAgencies->add($agency);
            $agency->setOwner($this);
        }

        return $this;
    }

    /**
     * assertIsValid
     *
     * @param ExecutionContextInterface $context
     * @Assert\Callback()
     */
    public function assertIsValid(ExecutionContextInterface $context)
    {
        if (null === $this->getId() && null === $this->getRawPassword()) {
            $context->buildViolation('Vous devez choisir un mot de passe')->atPath('rawPassword')->addViolation();
        }
    }

    /**
     * Generate new key
     *
     * @return User
     */
    public function generateNewKey()
    {
        $this->setKey(md5(uniqid() . time()));

        return $this;
    }

    /**
     * Get Accountant
     *
     * @return null|User
     */
    public function getAccountant()
    {
        return $this->accountant;
    }

    /**
     * Set Accountant
     *
     * @param null|User $accountant
     *
     * @return User
     */
    public function setAccountant(User $accountant = null)
    {
        $this->accountant = $accountant;

        return $this;
    }

    /**
     * Get Agency
     *
     * @return null|Agency
     */
    public function getAgency()
    {
        if (self::ROLE_AGENCY_OWNER === $this->getRole()) {
            return $this->getOwnedAgencies()->first() ?: null;
        }
        if (self::ROLE_AGENCY_RESPONSIBLE === $this->getRole()) {
            return $this->getAgencyManaged();
        }
        if (self::ROLE_AGENCY_CONTACT === $this->getRole()) {
            return $this->getAgencyRepresented();
        }

        return null;
    }

    /**
     * Get AgencyManaged
     *
     * @return null|Agency
     */
    public function getAgencyManaged()
    {
        return $this->agencyManaged;
    }

    /**
     * Set AgencyManaged
     *
     * @param null|Agency $agencyManaged
     *
     * @return User
     */
    public function setAgencyManaged(Agency $agencyManaged = null)
    {
        $this->agencyManaged = $agencyManaged;
        if (null !== $agencyManaged) {
            $agencyManaged->setResponsible($this);
        }

        return $this;
    }

    /**
     * Get AgencyRepresented
     *
     * @return Agency|null
     */
    public function getAgencyRepresented()
    {
        return $this->agencyRepresented;
    }

    /**
     * Set AgencyRepresented
     *
     * @param Agency|null $agencyRepresented
     *
     * @return User
     */
    public function setAgencyRepresented(Agency $agencyRepresented = null)
    {
        $this->agencyRepresented = $agencyRepresented;
        if (null !== $agencyRepresented) {
            $agencyRepresented->setContact($this);
        }

        return $this;
    }

    /**
     * Get Commercials
     *
     * @return User[]|ArrayCollection
     */
    public function getCommercials()
    {
        return $this->commercials;
    }

    /**
     * Get Firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set Firstname
     *
     * @param string $firstname
     *
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get fullName
     *
     * @return string
     */
    public function getFullName()
    {
        return sprintf('%s %s', mb_strtoupper($this->getLastname()), $this->getFirstname());
    }

    /**
     * Get entirename
     *
     * @return string
     */
    public function getEntirename()
    {
        return sprintf('[%s] %s', $this->getRoleName(), $this->getFullName());
    }

    /**
     * Get HasNewsletter
     *
     * @return bool
     */
    public function getHasNewsletter()
    {
        return $this->hasNewsletter;
    }

    /**
     * Set HasNewsletter
     *
     * @param bool $hasNewsletter
     *
     * @return User
     */
    public function setHasNewsletter($hasNewsletter)
    {
        $this->hasNewsletter = $hasNewsletter;

        return $this;
    }

    /**
     * Get Key
     *
     * @return null|string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * Set Key
     *
     * @param null|string $key
     *
     * @return User
     */
    public function setKey($key)
    {
        $this->key = $key;

        return $this;
    }

    /**
     * Get Lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set Lastname
     *
     * @param string $lastname
     *
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get OwnedAgencies
     *
     * @return Agency[]|ArrayCollection
     */
    public function getOwnedAgencies()
    {
        return $this->ownedAgencies;
    }

    /**
     * Get Profile
     *
     * @return Profile
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * Set Profile
     *
     * @param Profile $profile
     *
     * @return User
     */
    public function setProfile(Profile $profile = null)
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * Get Role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set Role
     *
     * @param string $role
     *
     * @return User
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role name
     *
     * @return string
     */
    public function getRoleName()
    {
        return self::getRoleNameList()[$this->getRole()];
    }

    /**
     * {@inheritdoc}
     */
    public function isEqualTo(CoreUserInterface $user)
    {
        return false;
    }

    /**
     * Remove commercial
     *
     * @param User $commercial
     *
     * @return User
     */
    public function removeCommercial(User $commercial)
    {
        if (true === $this->commercials->contains($commercial)) {
            $this->commercials->removeElement($commercial);
            $commercial->setAccountant(null);
        }

        return $this;
    }

    /**
     * Remove OwnedAgency
     *
     * @param Agency $agency
     *
     * @return User
     */
    public function removeOwnedAgency(Agency $agency)
    {
        if (true === $this->ownedAgencies->contains($agency)) {
            $this->ownedAgencies->removeElement($agency);
            $agency->setOwner(null);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function serialize()
    {
        return serialize([
            $this->getId(),
            $this->getUsername(),
            $this->getSalt(),
            $this->getPassword(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function unserialize($serialized)
    {
        list(
            $this->id,
            $this->username,
            $this->salt,
            $this->password
            ) = unserialize($serialized);
    }

    /**
     * {@inheritdoc}
     */
    public function setUsername($username)
    {
        if ($username !== $this->getUsername()) {
            $this->generateNewKey();
        }

        return $this->defaultSetUsername($username);
    }

    /**
     * Get Birthday
     *
     * @return null|\DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set Birthday
     *
     * @param null|\DateTime $birthday
     *
     * @return User
     */
    public function setBirthday(\DateTime $birthday = null)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get Country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set Country
     *
     * @param string $country
     *
     * @return User
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }
}
