<?php

namespace Eenov\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EB\DoctrineBundle\Entity\CreatedInterface;
use EB\DoctrineBundle\Entity\Doctrine\CreatedTrait;
use EB\DoctrineBundle\Entity\Doctrine\FileTrait;
use EB\DoctrineBundle\Entity\Doctrine\IdTrait;
use EB\DoctrineBundle\Entity\Doctrine\UpdatedTrait;
use EB\DoctrineBundle\Entity\FileInterface;
use EB\DoctrineBundle\Entity\UpdatedInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Document
 *
*
 * @ORM\Entity()
 * @ORM\Table()
 */
class Document implements CreatedInterface, UpdatedInterface, FileInterface
{
    use IdTrait;
    use CreatedTrait;
    use UpdatedTrait;
    use FileTrait;

    /**
     * @var Advert
     * @ORM\ManyToOne(targetEntity="Advert", inversedBy="documents")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     * @Assert\NotNull()
     */
    private $advert;

    /**
     * @var DocumentType
     * @ORM\ManyToOne(targetEntity="DocumentType")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     * @Assert\NotNull()
     */
    private $type;

    /**
     * {@inheritdoc}
     */
    public function getCacheKey()
    {
        return 'document';
    }

    /**
     * Get Advert
     *
     * @return Advert
     */
    public function getAdvert()
    {
        return $this->advert;
    }

    /**
     * Set Advert
     *
     * @param Advert $advert
     *
     * @return Document
     */
    public function setAdvert(Advert $advert = null)
    {
        $this->advert = $advert;

        return $this;
    }

    /**
     * Get Type
     *
     * @return DocumentType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set Type
     *
     * @param DocumentType $type
     *
     * @return Document
     */
    public function setType(DocumentType $type)
    {
        $this->type = $type;

        return $this;
    }
}
