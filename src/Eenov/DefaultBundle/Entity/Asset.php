<?php

namespace Eenov\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EB\DoctrineBundle\Entity\CreatedInterface;
use EB\DoctrineBundle\Entity\Doctrine\CreatedTrait;
use EB\DoctrineBundle\Entity\Doctrine\IdTrait;
use EB\DoctrineBundle\Entity\Doctrine\UpdatedTrait;
use EB\DoctrineBundle\Entity\LoggableInterface;
use EB\DoctrineBundle\Entity\UpdatedInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Class Asset
 *
*
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Eenov\DefaultBundle\Repository\AssetRepository")
 */
class Asset implements CreatedInterface, UpdatedInterface, LoggableInterface
{
    use IdTrait;
    use CreatedTrait;
    use UpdatedTrait;

    /**
     * @var Agency
     * @ORM\ManyToOne(targetEntity="Agency")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull()
     */
    private $agency;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     * @Assert\NotNull()
     * @Assert\DateTime()
     */
    private $date;

    /**
     * @var float
     * @ORM\Column(type="float")
     * @Assert\NotNull()
     * @Assert\NotEqualTo(value=0)
     */
    private $amount;

    /**
     * @var null|string
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     */
    private $comment;

    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return sprintf('Encours financier de %s pour %s le %s', $this->getAmount(), $this->getAgency(), $this->getDate()->format('m/Y'));
    }

    /**
     * assertIsValid
     *
     * @param ExecutionContextInterface $context
     * @Assert\Callback()
     */
    public function assertIsValid(ExecutionContextInterface $context)
    {
        $now = new \DateTime();

        $dateYear = (int)$this->getDate()->format('Y');
        $dateMonth = (int)$this->getDate()->format('m');
        $currentYear = (int)$now->format('Y');
        $currentMonth = (int)$now->format('m');
        if ($currentYear > $dateYear) {
            $context->buildViolation('Vous devez choisir une année à venir')->atPath('date')->addViolation();
        }
        if ($currentYear === $dateYear && $dateMonth <= $currentMonth) {
            $context->buildViolation('Vous devez choisir un mois à venir')->atPath('date')->addViolation();
        }
    }

    /**
     * Get Agency
     *
     * @return Agency
     */
    public function getAgency()
    {
        return $this->agency;
    }

    /**
     * Set Agency
     *
     * @param Agency $agency
     *
     * @return Asset
     */
    public function setAgency(Agency $agency)
    {
        $this->agency = $agency;

        return $this;
    }

    /**
     * Get Amount
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set Amount
     *
     * @param float $amount
     *
     * @return Asset
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get Comment
     *
     * @return null|string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set Comment
     *
     * @param null|string $comment
     *
     * @return Asset
     */
    public function setComment($comment = null)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get Date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set Date
     *
     * @param \DateTime $date
     *
     * @return Asset
     */
    public function setDate(\DateTime $date)
    {
        $this->date = $date;

        return $this;
    }
}
