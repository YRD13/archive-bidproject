<?php

namespace Eenov\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EB\DoctrineBundle\Entity\CreatedInterface;
use EB\DoctrineBundle\Entity\Doctrine\CreatedTrait;
use EB\DoctrineBundle\Entity\Doctrine\IdTrait;
use EB\DoctrineBundle\Entity\Doctrine\UpdatedTrait;
use EB\DoctrineBundle\Entity\LoggableInterface;
use EB\DoctrineBundle\Entity\UpdatedInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class DocumentBase
 *
*
 * @ORM\Entity(repositoryClass="Eenov\DefaultBundle\Repository\DocumentTypeRepository")
 * @ORM\Table()
 * @UniqueEntity(fields={"title","subtitle","name"}, errorPath="title", message="Ce trio titre/sous-titre/nom est déjà utilisé")
 */
class DocumentType implements CreatedInterface, UpdatedInterface, LoggableInterface
{
    use IdTrait;
    use CreatedTrait;
    use UpdatedTrait;

    /**
     * @var null|int
     * @ORM\Column(name="order_key", type="integer")
     * @Assert\NotNull()
     */
    private $order;

    /**
     * @var string
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     * @Assert\Length(max=100)
     */
    private $title;

    /**
     * @var null|string
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\Length(max=100)
     */
    private $subtitle;

    /**
     * @var string
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     * @Assert\Length(max=100)
     */
    private $name;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $isPublic;

    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        if (null !== $this->getSubtitle()) {
            return sprintf('%s > %s > %s', $this->getTitle(), $this->getSubtitle(), $this->getName());
        }

        return sprintf('%s > %s', $this->getTitle(), $this->getName());
    }

    /**
     * Get IsPublic
     *
     * @return bool
     */
    public function getIsPublic()
    {
        return $this->isPublic;
    }

    /**
     * Set IsPublic
     *
     * @param bool $isPublic
     *
     * @return DocumentType
     */
    public function setIsPublic($isPublic)
    {
        $this->isPublic = $isPublic;

        return $this;
    }

    /**
     * Get Name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set Name
     *
     * @param string $name
     *
     * @return DocumentType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get Order
     *
     * @return null|int
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set Order
     *
     * @param null|int $order
     *
     * @return DocumentType
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get Subtitle
     *
     * @return null|string
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * Set Subtitle
     *
     * @param null|string $subtitle
     *
     * @return DocumentType
     */
    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;

        return $this;
    }

    /**
     * Get Title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set Title
     *
     * @param string $title
     *
     * @return DocumentType
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }
}
