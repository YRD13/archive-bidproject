<?php

namespace Eenov\DefaultBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use EB\DoctrineBundle\Entity\CreatedInterface;
use EB\DoctrineBundle\Entity\Doctrine\CreatedTrait;
use EB\DoctrineBundle\Entity\Doctrine\FileTrait;
use EB\DoctrineBundle\Entity\Doctrine\IdTrait;
use EB\DoctrineBundle\Entity\Doctrine\SlugTrait;
use EB\DoctrineBundle\Entity\Doctrine\UpdatedTrait;
use EB\DoctrineBundle\Entity\FileInterface;
use EB\DoctrineBundle\Entity\SlugInterface;
use EB\DoctrineBundle\Entity\UpdatedInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Class Bid
 *
*
 * @ORM\Entity(repositoryClass="Eenov\DefaultBundle\Repository\BidRepository")
 * @ORM\Table()
 */
class Bid implements CreatedInterface, UpdatedInterface, SlugInterface, FileInterface
{
    use IdTrait;
    use CreatedTrait;
    use UpdatedTrait;
    use SlugTrait;
    use FileTrait;

    /**
     * @var string
     * @ORM\Column(type="string", length=60)
     * @Assert\NotBlank()
     * @Assert\Length(max=60)
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     * @Assert\Length(max=100)
     */
    private $sellerFirstname;

    /**
     * @var string
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     * @Assert\Length(max=100)
     */
    private $sellerLastname;

    /**
     * @var null|string
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\Length(max=100)
     * @Assert\Email()
     */
    private $sellerEmail;

    /**
     * @var null|string
     * @ORM\Column(type="string", length=20, nullable=true)
     * @Assert\Length(max=20)
     */
    private $sellerPhone;

    /**
     * @var null|string
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\Length(max=100)
     */
    private $negotiatorName;

    /**
     * @var null|Slot
     * @ORM\OneToOne(targetEntity="Slot", inversedBy="bid", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $slot;

    /**
     * @var null|Advert
     * @ORM\OneToOne(targetEntity="Advert")
     */
    private $publish;

    /**
     * @var Advert
     * @ORM\OneToOne(targetEntity="Advert", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     * @Assert\NotNull()
     */
    private $draft;

    /**
     * @var Access[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="Access", mappedBy="bid", cascade={"remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"created"="ASC"})
     */
    private $accesses;

    /**
     * @var Visit[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="Visit", mappedBy="bid", cascade={"persist","remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"date"="ASC","started"="ASC"})
     */
    private $visits;

    /**
     * @var Agency
     * @ORM\ManyToOne(targetEntity="Agency")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     * @Assert\NotNull()
     */
    private $agency;

    /**
     * @var bool
     * @ORM\Column(name="is_locked", type="boolean")
     * @Assert\NotNull()
     */
    private $lock = false;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     * @Assert\NotNull()
     */
    private $allowFirmAuction = true;

    /**
     * @var null|\DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\DateTime()
     */
    private $mandateDate;

    /**
     * @var null|\DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\DateTime()
     */
    private $sellStarted;

    /**
     * @var null|\DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\DateTime()
     */
    private $sellEnded;

    /**
     * @var null|\DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\DateTime()
     */
    private $sellRealEnded;

    /**
     * @var null|int
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Type("int")
     * @Assert\GreaterThan(0)
     */
    private $firstPrice;

    /**
     * @var null|int
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Type("int")
     * @Assert\GreaterThan(0)
     */
    private $estimatedPrice;

    /**
     * @var null|int
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Type("int")
     * @Assert\GreaterThan(0)
     */
    private $stepPrice;

    /**
     * @var null|\DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\DateTime()
     */
    private $adminValidated;

    /**
     * @var null|\DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\DateTime()
     */
    private $agencyValidated;

    /**
     * @var null|int
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Type("int")
     * @Assert\GreaterThan(0)
     */
    private $honorairePrice;

    /**
     * @var null|int
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Type("int")
     * @Assert\GreaterThan(0)
     */
    private $honorairePriceStart;

    /**
     * @var null|int
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Type("int")
     * @Assert\GreaterThan(0)
     */
    private $priceStartOld;
    /**
     * @var null|int
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Type("int")
     * @Assert\GreaterThan(0)
     */
    private $priceEstimatedOld;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $honoraireForSeller;

    /**
     * @var null|int
     * @ORM\Column(type="integer", nullable=true,options={"default" : 12})
     * @Assert\Type("int")
     * @Assert\GreaterThan(0)
     */
    private $hourStart;

    /**
     * {@inheritdoc}
     */
    public function __construct()
    {
        $this->accesses = new ArrayCollection();
        $this->visits = new ArrayCollection();
    }

    /**
     * Calculate step price
     *
     * @param null|int $firstPrice
     *
     * @return null|int
     */
    public function calculateStepPrice($firstPrice,$estimatedPrice)
    {
        if (null === $firstPrice OR null === $estimatedPrice) {
            return null;
        }

        $diff = $estimatedPrice - $firstPrice;

        if ($diff <= 5000)
        {
            $price = 1000;
        }
        elseif ($diff >= 5000 && $diff <= 20000)
        {
            $price = 1500;
        }
        elseif ($diff >= 20000 && $diff <= 50000)
        {
            $price = 2000;
        }
        elseif ($diff >= 50000 && $diff <= 100000)
        {
            $price = 5000;
        }
        elseif ($diff >= 100000 && $diff <= 150000)
        {
            $price = 8000;
        }
        elseif ($diff >= 150000 && $diff <= 200000)
        {
            $price = 10000;
        }
        elseif ($diff >= 200000 && $diff <= 300000)
        {
            $price = 15000;
        }
        elseif ($diff >= 300000 && $diff <= 500000)
        {
            $price = 20000;
        }
        elseif ($diff >= 500000)
        {
            $price = 30000;
        }

        return $price;

    }

    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return (string)$this->getTitle();
    }

    /**
     * @return string
     */
    public function getFullTitle()
    {
        return sprintf('%s - %s', mb_strtoupper($this->getSellerLastname()), $this->getTitle());
    }

    /**
     * Add access
     *
     * @param Access $access
     *
     * @return Bid
     */
    public function addAccess(Access $access)
    {
        if (false === $this->accesses->contains($access)) {
            $this->accesses->add($access);
        }

        return $this;
    }

    /**
     * Add visit
     *
     * @param Visit $visit
     *
     * @return Bid
     */
    public function addVisit(Visit $visit)
    {
        if (false === $this->visits->contains($visit)) {
            $this->visits->add($visit);
            $visit->setBid($this);
        }

        return $this;
    }

    /**
     * @param ExecutionContextInterface $context
     * @Assert\Callback()
     */
    public function assertIsValid(ExecutionContextInterface $context)
    {
        if (null !== $sellStarted = $this->getSellStarted()) {
            if (null !== $sellEnded = $this->getSellEnded()) {
                $diff = 24 * 60 * 60;
                
                if (($sellEnded->getTimestamp() - $sellStarted->getTimestamp()) !== $diff) {
                    $context->buildViolation('La date de début doit être au moins un jour avant la date de fin')->atPath('sellStarted')->addViolation();
                }
            }
        }

        if (null !== $firstPrice = $this->getFirstPrice()) {
            if (null !== $estimatedPrice = $this->getEstimatedPrice()) {
                if ($firstPrice > $estimatedPrice) {
                    $context->buildViolation('Le premier prix doit être inférieur au prix de présentation')->atPath('firstPrice')->addViolation();
                }
            }
        }
    }

    /**
     * Get Accesses
     *
     * @return Access[]|ArrayCollection
     */
    public function getAccesses()
    {
        return $this->accesses;
    }

    /**
     * Get AdminValidated
     *
     * @return null|\DateTime
     */
    public function getAdminValidated()
    {
        return $this->adminValidated;
    }

    /**
     * Set AdminValidated
     *
     * @param null|\DateTime $adminValidated
     *
     * @return Bid
     */
    public function setAdminValidated(\DateTime $adminValidated = null)
    {
        $this->adminValidated = $adminValidated;

        return $this;
    }

    /**
     * Get agency
     *
     * @return Agency
     */
    public function getAgency()
    {
        return $this->agency;
    }

    /**
     * Set agency
     *
     * @param Agency $agency
     *
     * @return Bid
     */
    public function setAgency(Agency $agency)
    {
        $this->agency = $agency;

        return $this;
    }

    /**
     * Get AgencyValidated
     *
     * @return null|\DateTime
     */
    public function getAgencyValidated()
    {
        return $this->agencyValidated;
    }

    /**
     * Set AgencyValidated
     *
     * @param null|\DateTime $agencyValidated
     *
     * @return Bid
     */
    public function setAgencyValidated(\DateTime $agencyValidated = null)
    {
        $this->agencyValidated = $agencyValidated;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getCacheKey()
    {
        return 'bid';
    }

    /**
     * Get Draft
     *
     * @return Advert
     */
    public function getDraft()
    {
        return $this->draft;
    }

    /**
     * Set Draft
     *
     * @param Advert $draft
     *
     * @return Bid
     */
    public function setDraft(Advert $draft)
    {
        $this->draft = $draft;

        return $this;
    }

    /**
     * Get EstimatedPrice
     *
     * @return null|int
     */
    public function getEstimatedPrice()
    {
        return $this->estimatedPrice;
    }

    /**
     * Set EstimatedPrice
     *
     * @param null|int $estimatedPrice
     *
     * @return Bid
     */
    public function setEstimatedPrice($estimatedPrice = null)
    {
        $this->estimatedPrice = $estimatedPrice;

        return $this;
    }

    /**
     * Get FirstPrice
     *
     * @return null|int
     */
    public function getFirstPrice()
    {
        return $this->firstPrice;
    }

    /**
     * Set FirstPrice
     *
     * @param null|int $firstPrice
     *
     * @return Bid
     */
    public function setFirstPrice($firstPrice = null)
    {
        $this->firstPrice = $firstPrice;


        return $this;
    }

    /**
     * Get Lock
     *
     * @return bool
     */
    public function getLock()
    {
        return $this->lock;
    }

    /**
     * Get HonorairePrice
     *
     * @return null|int
     */
    public function getHonorairePrice(){
        return $this->honorairePrice;
    }

    /**
     * Get HonorairePriceStart
     *
     * @return null|int
     */
    public function getHonorairePriceStart(){
        return $this->honorairePriceStart;
    }
    /**
     * Get GetPriceStartOld
     *
     * @return null|int
     */
    public function getPriceStartOld(){
        return $this->priceStartOld;
    }
    /**
     * Get GetPriceStartOld
     *
     * @return null|int
     */
    public function getPriceEstimatedOld(){
        return $this->priceEstimatedOld;
    }
    /**
     * Set PriceStartOld
     *
     * @param null|int $priceStartOld
     *
     * @return Bid
     */
    public function setPriceStartOld($priceStartOld = null){

        $this->priceStartOld = $priceStartOld;

        return $this;
    }

    /**
     * Set PriceEstimatedOld
     *
     * @param null|int $priceEstimatedOld
     *
     * @return Bid
     */
    public function setPriceEstimatedOld($priceEstimatedOld = null){

        $this->priceEstimatedOld = $priceEstimatedOld;

        return $this;
    }
    /**
     * Set HonorairePrice
     *
     * @param null|int $honorairePrice
     *
     * @return Bid
     */
    public function setHonorairePrice($honorairePrice = null){

        $this->honorairePrice = $honorairePrice;

        return $this;
    }

    /**
     * Set HonorairePriceStart
     *
     * @param null|int $honorairePriceStart
     *
     * @return Bid
     */
    public function setHonorairePriceStart($honorairePriceStart = null){

        $this->honorairePriceStart = $honorairePriceStart;

        return $this;
    }

    /**
     * Set Lock
     *
     * @param bool $lock
     *
     * @return Bid
     */
    public function setLock($lock)
    {
        $this->lock = $lock;

        return $this;
    }

    /**
     * Get Publish
     *
     * @return Advert|null
     */
    public function getPublish()
    {
        return $this->publish;
    }

    /**
     * Set Publish
     *
     * @param Advert|null $publish Publish
     *
     * @return Bid
     */
    public function setPublish(Advert $publish = null)
    {
        $this->publish = $publish;

        return $this;
    }

    /**
     * getSeeId
     *
     * @return string
     */
    public function getSeeId()
    {
        return md5(date('dmY') . $this->getId());
    }

    /**
     * Get MandateDate
     *
     * @return null|\DateTime
     */
    public function getMandateDate()
    {
        return $this->mandateDate;
    }

    /**
     * Set MandateDate
     *
     * @param null|\DateTime $mandateDate
     *
     * @return Bid
     */
    public function setMandateDate(\DateTime $mandateDate = null)
    {
        $this->mandateDate = $mandateDate;

        return $this;
    }

    /**
     * Get SellEnded
     *
     * @return null|\DateTime
     */
    public function getSellEnded()
    {
        return $this->sellEnded;
    }

    /**
     * Set SellEnded
     *
     * @param null|\DateTime $sellEnded SellEnded
     *
     * @return Bid
     */
    public function setSellEnded(\DateTime $sellEnded = null)
    {
        $this->sellEnded = $sellEnded;
        if (null !== $sellEnded) {
            $sellEnded->setTime($this->hourStart, 0, 0);
        }
        $this->setSellRealEnded($sellEnded);

        return $this;
    }

    /**
     * Get SellRealEnded
     *
     * @return null|\DateTime
     */
    public function getSellRealEnded()
    {
        return $this->sellRealEnded;
    }

    /**
     * Set SellRealEnded
     *
     * @param null|\DateTime $sellRealEnded
     *
     * @return Bid
     */
    public function setSellRealEnded(\DateTime $sellRealEnded = null)
    {
        $this->sellRealEnded = $sellRealEnded;

        return $this;
    }

    /**
     * Get SellStarted
     *
     * @return null|\DateTime
     */
    public function getSellStarted()
    {
        return $this->sellStarted;
    }

    /**
     * Set SellStarted
     *
     * @param null|\DateTime $sellStarted
     *
     * @return Bid
     */
    public function setSellStarted(\DateTime $sellStarted = null)
    {
        $this->sellStarted = $sellStarted;
        if (null !== $sellStarted ) {
            $sellStarted->setTime($this->hourStart, 0, 0);
        }

        return $this;
    }

    /**
     * Get SellerEmail
     *
     * @return null|string
     */
    public function getSellerEmail()
    {
        return $this->sellerEmail;
    }

    /**
     * Set SellerEmail
     *
     * @param null|string $sellerEmail
     *
     * @return Bid
     */
    public function setSellerEmail($sellerEmail)
    {
        $this->sellerEmail = $sellerEmail;

        return $this;
    }

    /**
     * Get SellerPhone
     *
     * @return null|string
     */
    public function getSellerPhone()
    {
        return $this->sellerPhone;
    }

    /**
     * Set SellerPhone
     *
     * @param null|string $sellerPhone
     *
     * @return Bid
     */
    public function setSellerPhone($sellerPhone)
    {
        $this->sellerPhone = $sellerPhone;

        return $this;
    }

    /**
     * Get SellerFirstname
     *
     * @return string
     */
    public function getSellerFirstname()
    {
        return $this->sellerFirstname;
    }

    /**
     * Set SellerFirstname
     *
     * @param string $sellerFirstname
     *
     * @return Bid
     */
    public function setSellerFirstname($sellerFirstname)
    {
        $this->sellerFirstname = $sellerFirstname;

        return $this;
    }

    /**
     * Get SellerLastname
     *
     * @return string
     */
    public function getSellerLastname()
    {
        return $this->sellerLastname;
    }

    /**
     * Set SellerLastname
     *
     * @param string $sellerLastname
     *
     * @return Bid
     */
    public function setSellerLastname($sellerLastname)
    {
        $this->sellerLastname = $sellerLastname;

        return $this;
    }

    /**
     * Get NegotiatorName
     *
     * @return null|string
     */
    public function getNegotiatorName()
    {
        return $this->negotiatorName;
    }

    /**
     * Set NegotiatorName
     *
     * @param null|string $negotiatorName
     *
     * @return $this
     */
    public function setNegotiatorName($negotiatorName = null)
    {
        $this->negotiatorName = $negotiatorName;

        return $this;
    }

    /**
     * Get Slot
     *
     * @return null|Slot
     */
    public function getSlot()
    {
        return $this->slot;
    }

    /**
     * Set Slot
     *
     * @param null|Slot $slot
     *
     * @return Bid
     */
    public function setSlot(Slot $slot = null)
    {
        $this->slot = $slot;

        return $this;
    }

    /**
     * Get StepPrice
     *
     * @return null|int
     */
    public function getStepPrice()
    {
        return $this->stepPrice;
    }

    /**
     * Set StepPrice
     *
     * @param null|int $stepPrice
     *
     * @return Bid
     */
    public function setStepPrice($stepPrice = null)
    {
        $this->stepPrice = $stepPrice;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getStringToSlug()
    {
        return (string)$this->getTitle();
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Bid
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get Visits
     *
     * @return Visit[]|ArrayCollection
     */
    public function getVisits()
    {
        return $this->visits;
    }

    /**
     * isEnded
     *
     * @return bool
     */
    public function isEnded()
    {
        return $this->getSellRealEnded() ? ($this->getSellRealEnded()->getTimestamp() < time()) : false;
    }

    /**
     * isInProgress
     *
     * @return bool
     */
    public function isInProgress()
    {
        return
            false === $this->isEnded() &&
            false === $this->isNotStarted();
    }

    /**
     * isNotStarted
     *
     * @return bool
     */
    public function isNotStarted()
    {
        return $this->getSellStarted() ? ($this->getSellStarted()->getTimestamp() > time()) : false;
    }

    /**
     * isOnline
     *
     * @return bool
     */
    public function isOnline()
    {
        return
            null !== $this->getPublish() &&
            null !== $this->getSlot();
    }

    /**
     * Needs validation
     *
     * @return bool
     */
    public function needsValidation()
    {
        if (null === $slot = $this->getSlot()) {
            return false;
        }

        if (null === $this->getAdminValidated()) {
            return true;
        }

        if ($slot->getAgency()->getId() !== $this->getAgency()->getId()) {
            if (null === $this->getAgencyValidated()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Remove access
     *
     * @param Access $access
     *
     * @return Bid
     */
    public function removeAccess(Access $access)
    {
        if (true === $this->accesses->contains($access)) {
            $this->accesses->removeElement($access);
        }

        return $this;
    }

    /**
     * Remove visit
     *
     * @param Visit $visit
     *
     * @return Bid
     */
    public function removeVisit(Visit $visit)
    {
        if (true === $this->visits->contains($visit)) {
            $this->visits->removeElement($visit);
            $visit->setBid(null);
        }

        return $this;
    }

    /**
     * Get AllowFirmAuction
     *
     * @return bool
     */
    public function getAllowFirmAuction()
    {
        return $this->allowFirmAuction;
    }

    /**
     * Set AllowFirmAuction
     *
     * @param bool $allowFirmAuction
     *
     * @return Bid
     */
    public function setAllowFirmAuction($allowFirmAuction)
    {
        $this->allowFirmAuction = $allowFirmAuction;

        return $this;
    }

    /**
     * Get HonoraireForSeller
     *
     * @return bool
     */
    public function getHonoraireForSeller(){

        return $this->honoraireForSeller;

    }

    /**
     * Set HonoraireForSeller
     *
     * @param bool $honoraireForSeller
     *
     * @return Bid
     */
    public function setHonoraireForSeller($honoraireForSeller){
        $this->honoraireForSeller = $honoraireForSeller;
        return $this;
    }


    /**
     * Get getHourStart
     *
     * @return integer
     */
    public function getHourStart(){

        return $this->hourStart;

    }

    /**
     * Set setHourStart
     *
     * @param integer $hourStart
     *
     * @return Bid
     */
    public function setHourStart($hourStart){
        $this->hourStart = $hourStart;
        return $this;
    }
}
