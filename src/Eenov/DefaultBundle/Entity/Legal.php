<?php

namespace Eenov\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EB\DoctrineBundle\Entity\CreatedInterface;
use EB\DoctrineBundle\Entity\Doctrine\CreatedTrait;
use EB\DoctrineBundle\Entity\Doctrine\FileTrait;
use EB\DoctrineBundle\Entity\Doctrine\IdTrait;
use EB\DoctrineBundle\Entity\Doctrine\UpdatedTrait;
use EB\DoctrineBundle\Entity\FileInterface;
use EB\DoctrineBundle\Entity\UpdatedInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Class Legal
 *
*
 * @ORM\Entity(repositoryClass="Eenov\DefaultBundle\Repository\LegalRepository")
 * @ORM\Table()
 */
class Legal implements CreatedInterface, UpdatedInterface, FileInterface
{
    const TYPE_CONTRACT = 'CONTRACT';
    const TYPE_RIB = 'RIB';

    use IdTrait;
    use CreatedTrait;
    use UpdatedTrait;
    use FileTrait;

    /**
     * @var string
     * @ORM\Column(type="string", length=20)
     * @Assert\NotNull()
     * @Assert\Length(max=20)
     * @Assert\Choice(callback="getTypeList")
     */
    private $type;

    /**
     * @var Agency
     * @ORM\ManyToOne(targetEntity="Agency", inversedBy="legals")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull()
     */
    private $agency;

    /**
     * @return string[]
     */
    public static function getTypeList()
    {
        return [
            self::TYPE_CONTRACT,
            self::TYPE_RIB,
        ];
    }

    /**
     * @return string[]
     */
    public static function getTypeNameList()
    {
        return [
            self::TYPE_CONTRACT => 'Contrat',
            self::TYPE_RIB => 'RIB',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return sprintf('%s - %s', $this->getTypeName(), $this->getFilename());
    }

    /**
     * {@inheritdoc}
     */
    public function getCacheKey()
    {
        return 'legal';
    }

    /**
     * assertIsValid
     *
     * @param ExecutionContextInterface $context
     * @Assert\Callback()
     */
    public function assertIsValid(ExecutionContextInterface $context)
    {
        if (null === $this->getId() && null === $this->getFile()) {
            $context->buildViolation('Vous devez sélectionner un fichier')->atPath('file')->addViolation();
        }
    }

    /**
     * Get Agency
     *
     * @return Agency
     */
    public function getAgency()
    {
        return $this->agency;
    }

    /**
     * Set Agency
     *
     * @param Agency $agency
     *
     * @return Legal
     */
    public function setAgency(Agency $agency = null)
    {
        $this->agency = $agency;

        return $this;
    }

    /**
     * Get Type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set Type
     *
     * @param string $type
     *
     * @return Legal
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getTypeName()
    {
        return self::getTypeNameList()[$this->getType()];
    }
}
