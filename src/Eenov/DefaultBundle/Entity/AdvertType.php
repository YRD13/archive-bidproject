<?php

namespace Eenov\DefaultBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EB\DoctrineBundle\Entity\CreatedInterface;
use EB\DoctrineBundle\Entity\Doctrine\CreatedTrait;
use EB\DoctrineBundle\Entity\Doctrine\IdTrait;
use EB\DoctrineBundle\Entity\Doctrine\UpdatedTrait;
use EB\DoctrineBundle\Entity\LoggableInterface;
use EB\DoctrineBundle\Entity\UpdatedInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class AdvertType
 *
*
 * @ORM\Entity(repositoryClass="Eenov\DefaultBundle\Repository\AdvertTypeRepository")
 * @ORM\Table()
 * @UniqueEntity(fields={"name"})
 */
class AdvertType implements CreatedInterface, UpdatedInterface, LoggableInterface
{
    use IdTrait;
    use CreatedTrait;
    use UpdatedTrait;

    /**
     * @var string
     * @ORM\Column(type="string", length=100, unique=true)
     * @Assert\NotBlank()
     * @Assert\Length(max=100)
     */
    private $name;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     * @Assert\NotNull()
     */
    private $isGround = false;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     * @Assert\NotNull()
     */
    private $isBuilding = false;

    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return (string)$this->getName();
    }

    /**
     * Get Name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set Name
     *
     * @param string $name
     *
     * @return AdvertType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get IsBuilding
     *
     * @return bool
     */
    public function getIsBuilding()
    {
        return $this->isBuilding;
    }

    /**
     * Set IsBuilding
     *
     * @param bool $isBuilding
     *
     * @return AdvertType
     */
    public function setIsBuilding($isBuilding)
    {
        $this->isBuilding = $isBuilding;

        return $this;
    }

    /**
     * Get IsGround
     *
     * @return bool
     */
    public function getIsGround()
    {
        return $this->isGround;
    }

    /**
     * Set IsGround
     *
     * @param bool $isGround
     *
     * @return AdvertType
     */
    public function setIsGround($isGround)
    {
        $this->isGround = $isGround;

        return $this;
    }
}
