<?php

namespace Eenov\DefaultBundle\ORM;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

/**
 * Class PointType
 *
*
 */
class PointType extends Type
{
    const POINT = 'point';

    /**
     * {@inheritdoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (!$value) {
            return null;
        }

        return pack('xxxxcLdd', '0', 1, $value->getLatitude(), $value->getLongitude());
    }

    /**
     * {@inheritdoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value == '') {
            return null;
        }

        $data = unpack('x/x/x/x/corder/Ltype/dlat/dlon', $value);

        return new Point($data['lat'], $data['lon']);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return self::POINT;
    }

    /**
     * {@inheritdoc}
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return self::POINT;
    }
}
