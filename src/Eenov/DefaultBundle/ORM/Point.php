<?php

namespace Eenov\DefaultBundle\ORM;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Point
 *
*
 */
class Point
{
    /**
     * @var null|float
     * @Assert\NotNull()
     */
    private $latitude;

    /**
     * @var null|float
     * @Assert\NotNull()
     */
    private $longitude;

    /**
     * @param null|float $latitude  Latitude
     * @param null|float $longitude Longitude
     */
    public function __construct($latitude = null, $longitude = null)
    {
        $this->latitude = $latitude;
        $this->longitude = $longitude;
    }

    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return (string)sprintf('POINT(%f %f)', $this->getLatitude(), $this->getLongitude());
    }

    /**
     * Get Latitude
     *
     * @return null|float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set Latitude
     *
     * @param null|float $latitude
     *
     * @return Point
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get Longitude
     *
     * @return null|float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set Longitude
     *
     * @param null|float $longitude
     *
     * @return Point
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }
}
