<?php

namespace Eenov\DefaultBundle\Mailer;

use Doctrine\ORM\EntityManager;
use EB\EmailBundle\Mailer\Mailer as DefaultMailer;
use Eenov\DefaultBundle\Entity\Access;
use Eenov\DefaultBundle\Entity\Bid;
use Eenov\DefaultBundle\Entity\User;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * Class Mailer
 *
*
 */
class Mailer
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var DefaultMailer
     */
    private $mailer;

    /**
     * @var string
     */
    private $emailTechnical;

    /**
     * @param EntityManager $em             Manager
     * @param DefaultMailer $mailer         Mailer
     * @param string        $emailTechnical Email technical
     */
    function __construct(EntityManager $em, DefaultMailer $mailer, $emailTechnical)
    {
        $this->em = $em;
        $this->mailer = $mailer;
        $this->emailTechnical = $emailTechnical;
    }

    /**
     * Send
     *
     * @param int                  $deltaMin Delta min
     * @param null|LoggerInterface $logger   Logger
     *
     * @return int
     */
    public function send($deltaMin, LoggerInterface $logger = null)
    {
        $logger = $logger ?: new NullLogger();

        // "Bientôt le début ..."

        $delta = new \DateInterval(sprintf('PT%uM', $deltaMin));
        $count = 0;

        /** @var \DateInterval[] $intervals */
        $intervals = [
            'dans 5 jours' => new \DateInterval('P5D'), // 5 jours avant
            'dans 4 jours' => new \DateInterval('P4D'), // 4 jours avant
            'dans 3 jours' => new \DateInterval('P3D'), // 3 jours avant
            'dans 2 jours' => new \DateInterval('P2D'), // 2 jours avant
            'demain' => new \DateInterval('P1D'), // 1 jour avant
            'dans 2 heures' => new \DateInterval('PT2H'), // 2 heures avant
            'dans 30 minutes' => new \DateInterval('PT30M'), // 30 minutes avant
            'dans 10 minutes' => new \DateInterval('PT10M'), // 10 minutes avant
        ];
        foreach ($intervals as $text => $interval) {
            $from = new \DateTime();
            $from->add($interval);

            $hour = (int)$from->format('H');
            $minute = (int)$from->format('i');
            $minute = $minute - ($minute % $deltaMin);
            $from->setTime($hour, $minute, 0);

            $to = clone $from;
            $to->add($delta);

            $logger->info(sprintf('%s : %s > %s', $text, $from->format('d/m/Y H\hi'), $to->format('d/m/Y H\hi')));

            $count += $this->sendBefore($logger, $from, $to, $text);
        }

        // "Bientôt la fin ..."

        /** @var \DateInterval[] $intervals */
        $intervals = [
            '2 heures' => new \DateInterval('PT2H'), // 2 heures avant la fin
            '30 minutes' => new \DateInterval('PT30M'), // 30 minutes avant la fin
            '20 minutes' => new \DateInterval('PT20M'), // 20 minutes avant la fin
            '10 minutes' => new \DateInterval('PT10M'), // 10 minutes avant la fin
        ];
        foreach ($intervals as $text => $interval) {
            $from = new \DateTime();
            $from->add($interval);

            $hour = (int)$from->format('H');
            $minute = (int)$from->format('i');
            $minute = $minute - ($minute % $deltaMin);
            $from->setTime($hour, $minute, 0);

            $to = clone $from;
            $to->add($delta);

            $logger->info(sprintf('%s : %s > %s', $text, $from->format('d/m/Y H\hi'), $to->format('d/m/Y H\hi')));

            $count += $this->sendDuring($logger, $from, $to, $text);
        }

        // "C'est fini ..."

        /** @var \DateInterval[] $intervals */
        $intervals = [
            '20 minutes' => new \DateInterval('PT20M'), // 20 minutes après la fin
        ];
        foreach ($intervals as $text => $interval) {
            $from = new \DateTime();
            $from->sub($interval);

            $hour = (int)$from->format('H');
            $minute = (int)$from->format('i');
            $minute = $minute - ($minute % $deltaMin);
            $from->setTime($hour, $minute, 0);

            $to = clone $from;
            $to->add($delta);

            $logger->info(sprintf('%s : %s > %s', $text, $from->format('d/m/Y H\hi'), $to->format('d/m/Y H\hi')));

            $count += $this->sendAfter($logger, $from, $to, $text);
        }

        // "Inscription ..."
        $intervals = [
            '1 heure' => new \DateInterval('PT1H'), // 1 heure après la fin
        ];
        foreach ($intervals as $text => $interval) {
            $from = new \DateTime();
            $from->sub($interval);

            $hour = (int)$from->format('H');
            $minute = (int)$from->format('i');
            $minute = $minute - ($minute % $deltaMin);
            $from->setTime($hour, $minute, 0);

            $to = clone $from;
            $to->add($delta);

            $logger->info(sprintf('%s : %s > %s', $text, $from->format('d/m/Y H\hi'), $to->format('d/m/Y H\hi')));

            $count += $this->sendSubscription($logger, $from, $to, $text);
        }

        return $count;
    }

    /**
     * Send before
     *
     * @param LoggerInterface $logger Logger
     * @param \DateTime       $from   From
     * @param \DateTime       $to     To
     * @param string          $text   Text to add after time
     *
     * @return int
     */
    private function sendBefore(LoggerInterface $logger, \DateTime $from, \DateTime $to, $text)
    {
        $count = 0;
        $bids = $this->em->getRepository(Bid::class)->findBidsStartingBetween($from, $to);
        $logger->info(sprintf('  • %u bid(s) found', count($bids)));

        foreach ($bids as $bid) {
            if ($bid->isEnded()) {
                continue;
            }

            $logger->info(sprintf('  • Processing mails for bid %u (%s)', $bid->getId(), $bid->getFullTitle()));

            $accesses = $this->em->getRepository(Access::class)->findUsers($bid);
            foreach ($accesses as $access) {
                try {
                    $logger->info(sprintf('    • Processing mail for %s', $access->getUser()));

                    $this->mailer->send('auction_recall', $access->getUser(), [
                        'bid' => $access->getBid(),
                        'text' => $text,
                    ]);
                    $count++;
                } catch (\Exception $e) {
                    $logger->error($e->getMessage());
                }
            }
        }

        return $count;
    }

    /**
     * Send during
     *
     * @param LoggerInterface $logger Logger
     * @param \DateTime       $from   From
     * @param \DateTime       $to     To
     * @param string          $text   Text
     *
     * @return int
     */
    private function sendDuring(LoggerInterface $logger, \DateTime $from, \DateTime $to, $text)
    {
        $count = 0;
        $bids = $this->em->getRepository(Bid::class)->findBidsEndingBetween($from, $to);
        $logger->info(sprintf('  • %u bid(s) found', count($bids)));

        foreach ($bids as $bid) {
            if ($bid->isEnded()) {
                continue;
            }

            $logger->info(sprintf('  • Processing mails for bid %u (%s)', $bid->getId(), $bid->getFullTitle()));

            $accesses = $this->em->getRepository(Access::class)->findUsers($bid);
            foreach ($accesses as $access) {
                try {
                    $logger->info(sprintf('    • Processing mail for %s', $access->getUser()));

                    $this->mailer->send('auction_end', $access->getUser(), [
                        'bid' => $bid,
                        'text' => $text,
                    ]);
                    $count++;
                } catch (\Exception $e) {
                    $logger->error($e->getMessage());
                }
            }
        }

        return $count;
    }

    /**
     * Send after
     *
     * @param LoggerInterface $logger Logger
     * @param \DateTime       $from   From
     * @param \DateTime       $to     To
     * @param string          $text   Text
     *
     * @return int
     */
    private function sendAfter(LoggerInterface $logger, \DateTime $from, \DateTime $to, $text)
    {
        $count = 0;
        $bids = $this->em->getRepository(Bid::class)->findBidsReallyEndingBetween($from, $to);
        $logger->info(sprintf('  • %u bid(s) found', count($bids)));

        foreach ($bids as $bid) {
            if (!$bid->isEnded()) {
                continue;
            }

            $logger->info(sprintf('  • Processing mails for bid %u (%s)', $bid->getId(), $bid->getFullTitle()));

            $accesses = $this->em->getRepository(Access::class)->findUsers($bid);
            foreach ($accesses as $access) {
                try {
                    $logger->info(sprintf('    • Processing mail for %s', $access->getUser()));

                    $this->mailer->send('auction_ended', $access->getUser(), [
                        'bid' => $bid,
                        'text' => $text,
                    ]);
                    $count++;
                } catch (\Exception $e) {
                    $logger->error($e->getMessage());
                }
            }
        }

        return $count;
    }

    /**
     * Send subscription
     *
     * @param LoggerInterface $logger Logger
     * @param \DateTime       $from   From
     * @param \DateTime       $to     To
     * @param string          $text   Text
     *
     * @return int
     */
    private function sendSubscription(LoggerInterface $logger, \DateTime $from, \DateTime $to, $text)
    {
        $count = 0;
        $users = $this->em->getRepository(User::class)->findUsersCreatedBetween($from, $to);
        $logger->info(sprintf('  • %u user(s) found', count($users)));

        foreach ($users as $user) {
            $logger->info(sprintf('  • Processing mails for user %u (%s)', $user->getId(), $user->getFullName()));

            try {
                $logger->info(sprintf('    • Processing mail for %s', $user->getUsername()));

                $this->mailer->send('user_created', $user->getUsername(), [
                    'user' => $user,
                ]);
                $count++;
            } catch (\Exception $e) {
                $logger->error($e->getMessage());
            }
        }

        return $count;
    }
}
