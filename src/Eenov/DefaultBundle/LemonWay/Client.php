<?php

namespace Eenov\DefaultBundle\LemonWay;

use Doctrine\ORM\EntityManager;
use Eenov\AgencyBundle\Promo\Code;
use Eenov\DefaultBundle\Entity\Agency;
use Eenov\DefaultBundle\Entity\MoneyIn;
use Eenov\DefaultBundle\Entity\Slot;
use Eenov\DefaultBundle\Entity\SubscriptionPack;
use Eenov\DefaultBundle\Entity\User;
use LemonWay\LemonWayAPI;
use LemonWay\Models\Wallet;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * Class Client
 *
*
 */
class Client
{
    /**
     * @var LemonWayAPI
     */
    private $lemonWayApi;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var Code
     */
    private $code;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param LemonWayAPI          $lemonWayApi Lemon Way API
     * @param EntityManager        $em          Entity Manager
     * @param Code                 $code        Code
     * @param null|LoggerInterface $logger      Logger
     */
    public function __construct(LemonWayAPI $lemonWayApi, EntityManager $em, Code $code, LoggerInterface $logger = null)
    {
        $this->lemonWayApi = $lemonWayApi;
        $this->em = $em;
        $this->code = $code;
        $this->logger = $logger ?: new NullLogger();
    }

    /**
     * Get Or Create Wallet
     *
     * @param User $user
     *
     * @return bool
     */
    public function getOrCreateWallet(User $user)
    {

        try {
            $getWalletDetailsResponse = $this->lemonWayApi->GetWalletDetails([
                'wallet' => $user->getId(),
            ]);

            if (!isset($getWalletDetailsResponse->lwError)) {
                return true;
            }

            $this->logger->error($getWalletDetailsResponse->lwError->MSG);
        } catch (\Exception $e) {

            $this->logger->error($e->getMessage());
        }

        try {
            $registerWalletDetailsResponse = $this->lemonWayApi->RegisterWallet([
                'wallet' => $user->getId(),
                'clientMail' => $user->getUsername(),
                'clientTitle' => Wallet::UNKNOWN,
                'clientFirstName' => $user->getFirstname(),
                'clientLastName' => $user->getLastname(),
                'companyName' => $user->getAgency()->getName(),
                'payerOrBeneficiary'=>'1',
                'ctry'=>strtoupper(substr($user->getProfile()->getCountry(),0,3)),
                'birthdate'=>$user->getProfile()->getBirthday()->format('dd/mm/YYYY'),
                'nationality'=>strtoupper(substr($user->getProfile()->getCountry(),0,3))
            ]);

            if (!isset($registerWalletDetailsResponse->lwError)) {
                return true;
            }

            $this->logger->error($registerWalletDetailsResponse->lwError->MSG);
        } catch (\Exception $e) {

            $this->logger->error($e->getMessage());
        }

        return false;
    }

    /**
     * Prepare Money In
     *
     * @param MoneyIn $moneyIn   Money IN
     * @param string  $returnUrl Return URL
     * @param string  $cancelUrl Cancel URL
     * @param string  $errorUrl  Error URL
     * @param string  $cssUrl    CSS URL
     *
     * @return string
     */
    public function prepareMoneyIn(MoneyIn $moneyIn, $returnUrl, $cancelUrl, $errorUrl, $cssUrl)
    {
        try {
            $price = $moneyIn->getPricePerSlot() * $moneyIn->getSlotCount();

            if (null !== $moneyIn->getCode()) {
                $price = $this->code->getPrice($moneyIn->getCode(), SubscriptionPack::getPacks()[$moneyIn->getPack()]) * $moneyIn->getSlotCount();
            }

            $vat = SubscriptionPack::getPacks()[$moneyIn->getPack()]['vat'];
            $price = $price + ($vat / 100 * $price);
            $com = 0;

            $responseMoneyIn = $this->lemonWayApi->MoneyInWebInit([
                'wkToken' => $moneyIn->getId(),
                'wallet' => $moneyIn->getUser()->getId(),
                'amountTot' => (string)number_format($price, 2, '.', ''),
                'amountCom' => (string)number_format($com, 2, '.', ''),
                'comment' => 'This is a comment ...',
                'returnUrl' => urlencode($returnUrl),
                'cancelUrl' => urlencode($cancelUrl),
                'errorUrl' => urlencode($errorUrl),
                'autoCommission' => \LemonWay\Models\Wallet::NO_AUTO_COMMISSION,
            ]);

            if (isset($responseMoneyIn->lwError)) {
                $this->logger->error($responseMoneyIn->lwError->CODE . ' -> ' . $responseMoneyIn->lwError->MSG);

                return null;
            }

            return sprintf(
                '%s?moneyInToken=%s&p=%s&lang=%s',
                $this->lemonWayApi->config->wkUrl,
                urlencode($responseMoneyIn->lwXml->MONEYINWEB->TOKEN),
                urlencode($cssUrl),
                urlencode($this->lemonWayApi->config->lang)
            );
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }

        return null;
    }

    /**
     * Validate And Fill
     *
     * @param MoneyIn $moneyIn
     * @param int     $transactionId      Transaction ID
     * @param Agency  $agency             Agency
     * @param int     $duration           Duration
     * @param string  $transactionAmount  Transaction Amount
     * @param string  $transactionComment Transaction Comment
     *
     * @return bool
     */
    public function validateAndFill(MoneyIn $moneyIn, Agency $agency, $duration, $transactionId, $transactionAmount, $transactionComment)
    {
        try {
            $getMoneyInTransDetailsResponse = $this->lemonWayApi->GetMoneyInTransDetails([
                'transactionId' => $transactionId,
            ]);

            if (isset($getMoneyInTransDetailsResponse->lwError)) {
                return false;
            }

            $moneyIn
                ->setTransactionId($transactionId)
                ->setTransactionAmount($transactionAmount)
                ->setTransactionMessage($transactionComment)
                ->setValidated(new \DateTime());

            for ($i = 0; $i < $moneyIn->getSlotCount(); $i++) {
                $started = new \DateTime();

                $ended = new \DateTime();
                $ended->add(new \DateInterval(sprintf('P%uD', $duration)));

                $slot = new Slot();
                $slot
                    ->setAgency($agency)
                    ->setStarted($started)
                    ->setEnded($ended);

                $this->em->persist($slot);
            }
            $this->em->flush();

            return true;
        } catch (\Exception $e) {
        }

        return false;
    }
}
