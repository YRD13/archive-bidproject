<?php

namespace Eenov\DefaultBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Eenov\DefaultBundle\Entity\Advert;
use Eenov\DefaultBundle\Entity\Auction;

/**
 * Class DoctrineAuctionEventListener
 *
*
 */
class DoctrineAuctionEventListener
{
    /**
     * @param LifecycleEventArgs $args
     */
    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if ($entity instanceof Auction) {
            $max = new \DateTime();
            $max->add(new \DateInterval('PT4M'));
            if($entity->getBid()->getSellRealEnded()) {
                if ($max > $entity->getBid()->getSellRealEnded()) {
                    $entity->getBid()->setSellRealEnded($max);

                    $mdt = $args->getEntityManager()->getClassMetadata(Advert::class);
                    $args->getEntityManager()->getUnitOfWork()->recomputeSingleEntityChangeSet($mdt, $entity->getBid());
                }
            }
        }
    }
}
