<?php

namespace Eenov\DefaultBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Eenov\DefaultBundle\Entity\Access;
use Eenov\DefaultBundle\Entity\Agency;
use Eenov\DefaultBundle\Entity\Asset;
use Eenov\DefaultBundle\Entity\Auction;
use Eenov\DefaultBundle\Entity\Bid;
use Eenov\DefaultBundle\Entity\Mandate;
use Eenov\DefaultBundle\Entity\MoneyIn;
use Eenov\DefaultBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DoctrineEmailEventListener
 *
*
 */
class DoctrineEmailEventListener
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var array
     */
    private $entities = [];

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * postFlush
     *
     * @param PostFlushEventArgs $args
     */
    public function postFlush(PostFlushEventArgs $args)
    {
        if (0 < count($this->entities)) {
            foreach ($this->entities as $entity) {
                $args->getEntityManager()->persist($entity);
            }
            $this->entities = [];
            $args->getEntityManager()->flush();
        }
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        // User registration
        if ($entity instanceof User) {
            $this->container->get('eb_email.mailer.mailer')->send('register', $entity, [
                'user' => $entity,
            ]);
        }

        // Access asked
        if ($entity instanceof Access) {
            // Oxioneo
            $this->container->get('eb_email.mailer.mailer')->send('access', $this->getOxioneoEmails(), [
                'bid' => $entity->getBid(),
            ]);

            // Agencies
            $this->container->get('eb_email.mailer.mailer')->send('access', $this->getAgencyEmails($entity->getBid()->getAgency()), [
                'bid' => $entity->getBid(),
            ]);
        }

        // Auction created
        if ($entity instanceof Auction) {
            // Oxioneo
            $this->container->get('eb_email.mailer.mailer')->send('auction', $this->getOxioneoEmails(), [
                'bid' => $entity->getBid(),
            ]);

            // Agencies
            $this->container->get('eb_email.mailer.mailer')->send('auction', $this->getAgencyEmails($entity->getBid()->getAgency()), [
                'bid' => $entity->getBid(),
            ]);

            // Users
            $accesses = $args->getEntityManager()->getRepository(Access::class)->findUsers($entity->getBid());
            foreach ($accesses as $access) {
                $this->container->get('eb_email.mailer.mailer')->send('auction', $access->getUser(), [
                    'bid' => $access->getBid(),
                ]);
            }
        }

        // Mandate created
        if ($entity instanceof Mandate) {
            if (null !== $buyerEmail = $entity->getBuyerEmail()) {
                $this->container->get('eb_email.mailer.mailer')->send('user_created', $buyerEmail, [
                    'mandate' => $entity,
                ]);
            }
        }

        // Agency created
        if ($entity instanceof Agency) {
            $this->container->get('eb_email.mailer.mailer')->send('agency_created', $this->getOxioneoEmails(), [
                'agency' => $entity,
            ]);
        }

        // MoneyIn created
        if ($entity instanceof MoneyIn) {
            $this->container->get('eb_email.mailer.mailer')->send('money_in_created', $this->getOxioneoEmails(), [
                'moneyIn' => $entity,
            ]);
        }
    }

    /**
     * @param PreUpdateEventArgs $args
     */
    public function preUpdate(PreUpdateEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof User) {
            if ($args->hasChangedField('key') && null !== $args->getNewValue('key')) {
                $this->container->get('eb_email.mailer.mailer')->send('email_validation', $entity, [
                    'user' => $entity,
                ]);
            }

            if ($args->hasChangedField('password')) {
                $this->container->get('eb_email.mailer.mailer')->send('password_update', $entity, [
                    'user' => $entity,
                ]);
            }
        }

        if ($entity instanceof Access) {
            if ($args->hasChangedField('validated') && $args->getNewValue('validated')) {
                $this->container->get('eb_email.mailer.mailer')->send('access_validated', $entity->getUser(), [
                    'bid' => $entity->getBid(),
                ]);
            }

            if ($args->hasChangedField('invalidated') && $args->getNewValue('invalidated')) {
                $this->container->get('eb_email.mailer.mailer')->send('access_invalidated', $entity->getUser(), [
                    'bid' => $entity->getBid(),
                ]);
            }
        }

        if ($entity instanceof Bid) {
            // Première validation, un slot est pris
            if ($args->hasChangedField('slot') && !$args->getOldValue('slot') && $args->getNewValue('slot')) {
                $this->container->get('eb_email.mailer.mailer')->send('bid_admin_validate', $this->getOxioneoEmails(), [
                    'bid' => $entity,
                ]);

                $this->container->get('eb_email.mailer.mailer')->send('bid_agency_validate', $this->getAgencyEmails($entity->getAgency()), [
                    'bid' => $entity,
                ]);

                // Si c'est un tarif par slot, on crée une facture
                if (null !== $subscription = $entity->getSlot()->getSubscription()) {
                    if ($subscription->getPerSlot()) {
                        $asset = new Asset();
                        $asset
                            ->setAgency($entity->getSlot()->getAgency())
                            ->setAmount(-1 * $subscription->getAmount())
                            ->setComment(sprintf('Validation de l\'annonce "%s"', $entity))
                            ->setDate(new \DateTime());

                        $this->entities[] = $asset;
                    }
                }
            }

            // La validation de l'administrateur est remise à zéro
            if ($args->hasChangedField('adminValidated') && $args->getOldValue('adminValidated') && !$args->getNewValue('adminValidated')) {
                $this->container->get('eb_email.mailer.mailer')->send('bid_admin_validate', $this->getOxioneoEmails(), [
                    'bid' => $entity,
                ]);
            }

            // La validation de l'agence est remise à zéro
            if ($args->hasChangedField('agencyValidated') && $args->getOldValue('agencyValidated') && !$args->getNewValue('agencyValidated')) {
                $this->container->get('eb_email.mailer.mailer')->send('bid_agency_validate', $this->getAgencyEmails($entity->getAgency()), [
                    'bid' => $entity,
                ]);
            }

            // L'annonce est validée pour la mise en ligne
            if (
                ($args->hasChangedField('adminValidated') && $args->getNewValue('adminValidated') && $entity->getAgencyValidated()) ||
                ($args->hasChangedField('agencyValidated') && $args->getNewValue('agencyValidated') && $entity->getAdminValidated())
            ) {
                if (null !== $email = $entity->getSellerEmail()) {
                    $this->container->get('eb_email.mailer.mailer')->send('bid_validated_to_agency', $this->getAgencyEmails($entity->getAgency()), [
                        'bid' => $entity,
                    ]);
                    $this->container->get('eb_email.mailer.mailer')->send('bid_validated_to_seller', $email, [
                        'bid' => $entity,
                    ]);
                }
            }
        }

        if ($entity instanceof MoneyIn) {
            $this->container->get('eb_email.mailer.mailer')->send('money_in_updated', $this->getOxioneoEmails(), [
                'moneyIn' => $entity,
            ]);
        }
    }

    /**
     * Get Oxioneo emails
     *
     * @return string[]
     */
    private function getOxioneoEmails()
    {
        return [
            $this->container->getParameter('email_technical'),
            $this->container->getParameter('email_contact'),
        ];
    }

    /**
     * Get agency emails
     *
     * @param Agency $agency
     *
     * @return string[]
     */
    private function getAgencyEmails(Agency $agency)
    {
        $emails = [
            $agency->getEmail(),
            $agency->getOwner()->getUsername(),
            $agency->getResponsible() ? $agency->getResponsible()->getUsername() : null,
            $agency->getContact() ? $agency->getContact()->getUsername() : null,
        ];

        while (null !== $parent = $agency->getMother()) {
            if ($parent instanceof Agency) {
                $emails = array_merge($emails, [
                    $parent->getEmail(),
                    $parent->getOwner()->getUsername(),
                    $parent->getContact() ? $parent->getContact()->getUsername() : null,
                ]);
            }

            $agency = $parent;
        }

        return array_filter(array_unique($emails));
    }
}
