<?php

namespace Eenov\DefaultBundle\Test;

use Eenov\DefaultBundle\DataFixtures\ORM as Fixtures;
use Eenov\DefaultBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase as BaseWebTestCase;
use Symfony\Bundle\SwiftmailerBundle\DataCollector\MessageDataCollector;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * Class WebTestCase
 *
*
 */
class WebTestCase extends BaseWebTestCase
{
    /**
     * {@inheritdoc}
     */
    protected static function createClient()
    {
        return parent::createClient([
            'environment' => 'test',
            'debug' => true,
        ]);
    }

    /**
     * Login
     *
     * @param Client $client   Client
     * @param string $username Username
     */
    protected function login(Client $client, $username)
    {
        if (null !== $user = $client->getContainer()->get('doctrine.orm.default_entity_manager')->getRepository(User::class)->findOneBy(['username' => $username])) {
            if ($user instanceof User) {
                $session = $client->getContainer()->get('session');

                $firewall = 'secured_area';
                $token = new UsernamePasswordToken($user, null, $firewall, $user->getRoles());
                $session->set('_security_' . $firewall, serialize($token));
                $session->save();

                $cookie = new Cookie($session->getName(), $session->getId());
                $client->getCookieJar()->set($cookie);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        // Backup generated database
        if (file_exists($path = static::getDatabaseFilePath())) {
            copy($path, static::getDatabaseBackupFilePath());
        }

        parent::setUp();
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown()
    {
        // Reload previous database
        if (file_exists($path = static::getDatabaseBackupFilePath())) {
            rename($path, static::getDatabaseFilePath());
        }

        parent::tearDown();
    }

    /**
     * @return string
     */
    private static function getDatabaseBackupFilePath()
    {
        return sprintf('%s.cache', static::getDatabaseFilePath());
    }

    /**
     * @return string
     */
    private static function getDatabaseFilePath()
    {
        return __DIR__ . '/../../../../app/cache/test/test.db';
    }

    /**
     * Assert redirect
     *
     * @param Client $client      Client
     * @param bool   $successfull Successfull
     * @param int    $code        Code
     * @param string $type        Type
     */
    protected function assertResponse(Client $client, $successfull = true, $code = 200, $type = 'text/html; charset=UTF-8')
    {
        $this->assertEquals($successfull, $client->getResponse()->isSuccessful(), $client->getResponse()->getContent());
        $this->assertEquals($code, $client->getResponse()->getStatusCode());
        $this->assertEquals($type, $client->getResponse()->headers->get('Content-Type'));
    }

    /**
     * Assert redirect
     *
     * @param Client $client Client
     */
    protected function assertRedirect(Client $client)
    {
        $this->assertTrue($client->getResponse()->isRedirection(), $client->getResponse()->getContent());
    }

    /**
     * getSwiftmailerMessageCount
     *
     * @param Client $client
     *
     * @return int|null
     */
    protected function getSwiftmailerMessageCount(Client $client)
    {
        if (false !== $profile = $client->getProfile()) {
            $collector = $profile->getCollector('swiftmailer');
            if ($collector instanceof MessageDataCollector) {
                return $collector->getMessageCount();
            }
        }

        return null;
    }

    /**
     * Create file
     *
     * @param $name
     *
     * @return UploadedFile
     */
    public function createFile($name)
    {
        $mime = 'image/jpg';
        $source = sprintf(__DIR__ . '/../Resources/private/test/%s', $name);
        $target = sys_get_temp_dir() . '/' . $name;
        copy($source, $target);

        return new UploadedFile($target, $name, $mime, null, null, true);
    }
}
