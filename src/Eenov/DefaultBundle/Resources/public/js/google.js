var $canvas = $('#bid-map-canvas[data-latitude][data-longitude][data-icon][data-latitude-id][data-longitude-id]');
if (oxioneo && 0 < $canvas.length) {
    // Marker position and design
    var latitude = $canvas.data('latitude');
    var $latitude = $($canvas.data('latitude-id'));
    var longitude = $canvas.data('longitude');
    var $longitude = $($canvas.data('longitude-id'));
    var icon = $canvas.data('icon');
    var latLng = null;
    var map = null;
    var marker = null;

    // Load script
    $(document.body).append($('<script type="text/javascript">').attr('src', oxioneo.google_map_script_uri + '&callback=initializeGoogleMap'));

    // Google tools
    function initializeGoogleMap() {
        // Create map and marker
        latLng = new google.maps.LatLng(latitude, longitude);
        map = new google.maps.Map(document.getElementById('bid-map-canvas'), {center: latLng, zoom: 10});
        marker = new google.maps.Marker({
            position: latLng,
            map: map,
            animation: google.maps.Animation.DROP,
            draggable: true,
            icon: icon
        });

        // Listen for drag events
        function updatePositionUsingMarker() {
            $latitude.val(marker.position.lat());
            $longitude.val(marker.position.lng());
        };
        google.maps.event.addListener(marker, 'dragend', updatePositionUsingMarker);

        // Geocode ?
        if ($canvas.data('geocode-ids')) {
            var $listeners = $($canvas.data('geocode-ids'));
            var geocoder = new google.maps.Geocoder();
            var geocode = function () {
                // Create one address
                var address = '';
                $.each($listeners, function () {
                    address += $(this).val() + ' ';
                });

                // Geocode !
                geocoder.geocode({address: address, region: 'fr'}, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        map.setCenter(results[0].geometry.location);
                        marker.setPosition(results[0].geometry.location);
                        updatePositionUsingMarker();

                        if (marker.getAnimation() != null) {
                            marker.setAnimation(null);
                        } else {
                            marker.setAnimation(google.maps.Animation.DROP);
                        }
                    }
                });
            };

            // Fire geocode in each change
            if ($canvas.data('geocode-button-id')) {
                $($canvas.data('geocode-button-id')).click(function (e) {
                    e.preventDefault();
                    geocode();
                });
            } else {
                $listeners.change(geocode);
            }
        }
    }
}
