$(document).ready(function () {
    // Small nav search
    var $searchBar = $('#searchbar');
    $('.second-nav', '#header').on('click', '.search button', function (e) {
        e.preventDefault();
        if ($searchBar.hasClass('closed')) {
            $searchBar.toggleClass('open', true);
            $searchBar.toggleClass('closed', false);
            $(this).toggleClass('active', true);
        } else {
            $searchBar.toggleClass('open', false);
            $searchBar.toggleClass('closed', true);
            $(this).toggleClass('active', false);
        }
    });

    $searchBar.on('click', '.search-advanced.submit', function (e) {
        e.preventDefault();
        $(this).parents('form').attr('action', $(this).attr('href')).submit();
    });

    $(document.body).on('click', '.save-search.submit', function (e) {
        e.preventDefault();
        $(this).parents('form').attr('action', $(this).attr('href')).attr('method', 'POST').submit();
    });

    // Profile conditions
    $('select[data-hide]').each(function () {
        var hide = $(this).data('hide');
        $(this)
            .on('change', function () {
                var val = $(this).val();
                if (hide.hasOwnProperty(val)) {
                    $.each(hide[val], function (key, value) {
                        var $item = $('#' + value);
                        $item
                            .val('')
                            .find('input[type=checkbox]')
                            .prop('checked', false);
                        $item
                            .closest('.form-group')
                            .hide();
                    });
                } else {
                    if ($(this).data('hide-reset')) {
                        $.each($(this).data('hide-reset'), function (key, value) {
                            var $item = $('#' + value);
                            $item
                                .closest('.form-group')
                                .show();
                        });
                    }
                }
            });
    }).change();

    // Search
    $(document.body).on('change', '#eenov_user_search_isSentByEmail', function () {
        $(this).parents('form').submit();
    });

    // All wishlist button
    $(document.body).on('click', '.wishlist[data-token][data-uri]', function (e) {
        e.preventDefault();
        var $link = $(this);
        $.post($(this).data('uri'), {_token: $(this).data('token')}, function () {
            if ($link.data('delete')) {
                $link.parents($link.data('delete')).remove();
            } else {
                $link.toggleClass('active pulse');
            }
        });
    });

    // Modal loaded using Ajax
    $(document.body).on('click', '*[data-popup]', function (e) {
        e.preventDefault();
        $.get($(this).data('popup'), function (data) {
            $(data).modal('show').on('hidden.bs.modal', function () {
                $(this).remove();
            });
        });
    });


    // Bouton deploiement options recherche site
    $('#btnplus').click(function () {
        if ($(this).parent().parent().parent().find('div.bloc_plus_filtres').hasClass('open_filtres')) {
            $('#btnplus').parent().parent().parent().find('div.bloc_plus_filtres').hide();
            $('#btnplus').parent().parent().parent().find('div.bloc_plus_filtres').removeClass('open_filtres');
        } else {
            $('#btnplus').parent().parent().parent().find('div.bloc_plus_filtres').show();
            $('#btnplus').parent().parent().parent().find('div.bloc_plus_filtres').addClass('open_filtres');
        }
    });

    // Bouton deploiement options recherche page annonces
    $('#btnplus_bid').click(function () {
        if ($(this).parent().parent().parent().find('div.bloc_plus_filtres_bid').hasClass('open_filtres')) {
            $('#btnplus_bid').parent().parent().parent().find('div.bloc_plus_filtres_bid').hide();
            $('#btnplus_bid').parent().parent().parent().find('div.bloc_plus_filtres_bid').removeClass('open_filtres');
        } else {
            $('#btnplus_bid').parent().parent().parent().find('div.bloc_plus_filtres_bid').show();
            $('#btnplus_bid').parent().parent().parent().find('div.bloc_plus_filtres_bid').addClass('open_filtres');
        }
    });

    // mm
    $('.mm').each(function () {
        var $mm = $(this),
            mms = $mm.data('mms').split(','),
            $mmLabel = $mm.find('.mm-label'),
            valueSuffix = $mm.data('mm-value-suffix'),
            labelDefault = $mmLabel.data('mm-label-default'),
            labelSuffix = $mmLabel.data('mm-label-suffix'),
            $mmChoices = $mm.find('.mm-choices'),
            $mmChoicesChoice,
            $mmDropdown = $mm.find('.mm-dropdown'),
            $currentInput,
            $mmMinInput = $mm.find('.mm-min-input'),
            $mmMaxInput = $mm.find('.mm-max-input'),
            avoidNextWindowEvent = false,
            getMin = function () {
                return parseInt($mmMinInput.val(), 10);
            },
            getMax = function () {
                return parseInt($mmMaxInput.val(), 10);
            },
            closeDropdown = function () {
                $mmDropdown.removeClass('mm-dropdown-visible');
            },
            openDropdown = function () {
                $mmDropdown.addClass('mm-dropdown-visible');
            },
            toggleDropdown = function () {
                $mmDropdown.toggleClass('mm-dropdown-visible');
            },
            addChoice = function (text, value) {
                return $('<button type="button" class="btn btn-default btn-block mm-choice"></button>')
                    .addClass('mm-choice')
                    .text(text)
                    .data('mm-set', value)
                    .appendTo($mmChoices);
            },
            fixPrice = function (price) {
                price = parseInt(price, 10);
                if (price > 1000) {
                    return parseInt(price / 1000, 10) + 'k';
                }

                return price;
            },
            getValueSuffix = function () {
                return valueSuffix ? ' ' + valueSuffix : getLabelSuffix();
            },
            getLabelSuffix = function () {
                return labelSuffix ? ' ' + labelSuffix : '';
            },
            updateLabel = function () {
                var min = getMin(),
                    minLabel = fixPrice(min),
                    max = getMax(),
                    maxLabel = fixPrice(max),
                    label;

                if (min && max) {
                    label = minLabel + ' à ' + maxLabel + getLabelSuffix();
                } else if (min) {
                    label = minLabel + getLabelSuffix() + ' min'
                } else if (max) {
                    label = maxLabel + getLabelSuffix() + ' max'
                } else {
                    label = labelDefault;
                }

                $mmLabel.text(label);
            },
            mouseup = function (e) {
                if (avoidNextWindowEvent) {
                    avoidNextWindowEvent = false;
                    return;
                }
                if (!$mm.is(e.target) && $mm.has(e.target).length === 0) {
                    closeDropdown();
                }
            },
            showAllChoices = function () {
                $mmChoicesChoice.show();
            },
            hideChoicesBelow = function (min) {
                var hiddenCount = 0;
                $mmChoicesChoice.each(function () {
                    var value = $(this).data('mm-set');
                    if (value) {
                        if (value <= min) {
                            $(this).hide();
                            hiddenCount++;
                        } else {
                            $(this).show();
                        }
                    }
                });
                if (hiddenCount >= $mmChoicesChoice.length - 1) {
                    $mmChoicesChoice.hide();
                }
            };

        updateLabel();

        addChoice('Aucun');
        mms.forEach(function (value) {
            addChoice(value + getValueSuffix(), value);
        });
        $mmChoicesChoice = $mmChoices.find('.mm-choice');

        $mm
            .on('focus', '.mm-min-input', function () {
                $currentInput = $mmMinInput;
                showAllChoices();
                $mmChoices.addClass('mm-choices-visible mm-choices-min');
                $mmChoices.removeClass('mm-choices-max');
            })
            .on('keydown', '.mm-min-input', function (e) {
                var w = e.keyCode || e.which || 0;
                if (13 === w) {
                    updateLabel();
                    $mmMaxInput.focus();
                    e.preventDefault();
                }
            })
            .on('focus', '.mm-max-input', function () {
                $currentInput = $mmMaxInput;
                if (getMin()) {
                    hideChoicesBelow(getMin());
                }
                $mmChoices.addClass('mm-choices-visible mm-choices-max');
                $mmChoices.removeClass('mm-choices-min');
            })
            .on('keydown', '.mm-max-input', function (e) {
                var w = e.keyCode || e.which || 0;
                if (13 === w) {
                    updateLabel();
                    closeDropdown();
                    e.preventDefault();
                }
            })
            .on('mousedown', '.mm-choice', function (e) {
                var value;

                if ($currentInput) {
                    value = $(this).data('mm-set');
                    $currentInput.val(value);
                    $mmChoices.removeClass('mm-choices-visible');
                    updateLabel();

                    if ($currentInput.hasClass('mm-min-input')) {
                        if (value) {
                            hideChoicesBelow(value);
                        }
                        if (value > getMax()) {
                            $mmMaxInput.val('');
                        }

                        avoidNextWindowEvent = true;
                        $mmMaxInput.focus();
                    } else {
                        showAllChoices();
                    }
                }

                e.preventDefault();
            })
            .on('click', '.mm-label', function () {
                toggleDropdown();
                $mmMinInput.focus();
            })
        ;

        $(window.document).on('mouseup', mouseup);

    });
});
