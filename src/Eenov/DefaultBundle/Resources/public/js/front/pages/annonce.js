$(document).ready(function(){
    $('a','#tabs').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    var $photos = $('#photos');
    if($photos.length) {
        $('.slider-nav img', '#photos').tooltip();

        initSlider('.slider-for', '.slider-nav');
    }
});

function initSlider(slider, navbar) {
    $(slider).slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        asNavFor: navbar,
        autoplay: true,
        autoplaySpeed: 8000
    });

    var l = $(navbar).find('img').length,
        sts = l < 6 ? 3 : 5;

    $(navbar).slick({
        slidesToShow:  sts,
        slidesToScroll: 1,
        centerMode: true,
        asNavFor: slider,
        arrows: true,
        focusOnSelect: true
    });
}
