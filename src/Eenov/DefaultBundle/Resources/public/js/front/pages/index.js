$(document).ready(function () {

    var $sliders = $('ul.slides', '#home-slider'),
        timeout = 5000,
        duration = 1000,
        count = 0,
        max = 0;

    function runTimer($slides, current) {
        current = current <= 0 ? $slides.length : current;
        if (current === $slides.length) {
            $slides.last().animate({opacity: 1}, duration, function () {
                $slides.css('opacity', 1);
            });
        } else {
            $($slides.get(current)).animate({opacity: 0}, duration);
        }

        setTimeout(function () {
            runTimer($slides, --current);
        }, timeout);
    }

    $sliders.each(function () {
        var $slides = $(this).children('li');
        setTimeout(function () {
            runTimer($slides, $slides.length - 1);
        }, timeout);
    });

    $('.slider', '#bids').each(function () {
        count = $(this).find('.slide').length;
        max = count > 5 ? 5 : count - 1;

        $(this).slick({
            centerPadding: '0',
            slidesToShow: max,
            responsive: [
                {
                    breakpoint: 10000,
                    settings: {
                        slidesToShow: Math.min(max, 5)
                    }
                },
                {
                    breakpoint: 1600,
                    settings: {
                        slidesToShow: Math.min(max, 4)
                    }
                },
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: Math.min(max, 3)
                    }
                },
                {
                    breakpoint: 900,
                    settings: {
                        slidesToShow: Math.min(max, 2)
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        arrows: false,
                        slidesToShow: Math.min(max, 1)
                    }
                }
            ]
        });
    });

    $('.slider', '#partners').each(function () {
        count = $(this).find('.slide').length;
        max = count > 6 ? 6 : count - 1;

        $(this).slick({
            centerMode: true,
            centerPadding: 0,
            responsive: [
                {
                    breakpoint: 10000,
                    settings: {
                        slidesToShow: Math.min(max, 6)
                    }
                },
                {
                    breakpoint: 1600,
                    settings: {
                        slidesToShow: Math.min(max, 5)
                    }
                },
                {
                    breakpoint: 1325,
                    settings: {
                        slidesToShow: Math.min(max, 4)
                    }
                },
                {
                    breakpoint: 1050,
                    settings: {
                        slidesToShow: Math.min(max, 3)
                    }
                },
                {
                    breakpoint: 775,
                    settings: {
                        slidesToShow: Math.min(max, 2)
                    }
                },
                {
                    breakpoint: 500,
                    settings: {
                        arrows: false,
                        slidesToShow: Math.min(max, 1)
                    }
                }
            ]
        });
    });

    var $photos = $('#photos');
    if ($photos.length) {
        $('.slider-nav img', '#photos').tooltip();

        initSlider('.slider-for', '.slider-nav');
    }
});

function initSlider(slider, navbar) {
    $(slider).slick({
        autoplay: true,
        autoplaySpeed: 3500, 
    });

    var l = $(navbar).find('img').length,
        sts = l < 6 ? 3 : 5;


}
