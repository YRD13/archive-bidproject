jQuery(document).ready(function () {
    Metronic.setAssetsPath('/bundles/eenovdefault/lib/metronic_v3.2.0/');
    Metronic.setGlobalImgPath('global/img/');
    Metronic.init(); // init metronic core components
    Layout.init(); // init layout

    jQuery("a.eenov-modal").live("click", function () {

        if (jQuery(this).data('title') && jQuery(this).data('content')) {

            getEenovTempModal(jQuery(this).data('title'), jQuery(this).data('content'), jQuery(this).attr('href'));

            jQuery('#temp-eenov-modal').modal('show');
            return false;
        }
    });

    // Datepicker
    $('.datepicker').datepicker({
        language: 'fr'
    });
    // @todo issue http://gitlab.oxioneo-immo.com/eenov/oxioneo/issues/250
    // new Date((new Date()).getTime() + (45 * 24 * 60 * 60 * 1000))
    $('.datepicker-45').datepicker({
        language: 'fr',
        startDate: new Date()
    });
    $('.timepicker').timepicker({
        autoclose: true,
        minuteStep: 5,
        showSeconds: false,
        showMeridian: false
    });

    // Started vs Ended
    $('form .started input').on('changeDate', function () {
        var date = $(this).datepicker('getDate'),
            start = new Date(),
            end = new Date(),
            $ended = $('form .ended input');

        start.setTime(date.getTime());
        start.setDate(date.getDate() + 1);

        end.setTime(date.getTime());
        end.setDate(date.getDate() + 1);

        $ended.datepicker('setStartDate', start);
        $ended.datepicker('setEndDate', end);
    }).each(function () {
        var val = $(this).datepicker('getDate');
        if (val && val.getTime && val.getTime()) {
            $(this).datepicker('setDate', val);
        }
    });

    // Firstprice vs Estimatedprice
    var $estimatedPrice = $('form .estimatedprice input');
    $('form .firstprice').on('change', 'input', function () {
        if (1 === $estimatedPrice.length) {
            if (!$estimatedPrice.val() || ($estimatedPrice.val() < $(this).val())) {
                $estimatedPrice.val($(this).val());
            }
            $estimatedPrice.attr('min', $(this).val());
        }
    });

    // Calendar
    $('.calendar[data-calendar-api]').each(function () {
        var conf = {
            ignoreTimezone: true,
            events: {
                url: $(this).data('calendar-api')
            },
            header: {
                left: 'title',
                center: 'month,agendaWeek,agendaDay',
                right: 'today prev,next'
            }
        };

        if ($(this).data('calendar-date')) {
            conf.defaultDate = $(this).data('calendar-date');
        }

        $(this).fullCalendar(conf);
    });
});

function getEenovTempModal(title, content, href) {

    if (jQuery('body #temp-eenov-modal').length < 1) {

        var modal = '';

        modal = '<div class="modal fade" id="temp-eenov-modal" tabindex="-1" role="dialog">';
        modal += '<div class="modal-dialog">';
        modal += '<div class="modal-content">';
        modal += '<div class="modal-header">';
        modal += '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">{{ "Fermer"|trans }}</span></button>';
        modal += '<h4 class="modal-title">' + title + '</h4>';
        modal += '</div>';
        modal += '<div class="modal-body">';
        modal += '<div class="text-center">' + content + '</div>';
        modal += '</div>';
        modal += '<div class="modal-footer">';
        modal += '<button type="button" class="btn default" data-dismiss="modal">{{ "Annuler"|trans }}</button>';
        modal += '<a href="' + href + '" title="{{ "Continuer"|trans }}" type="button" class="btn blue">{{ "Continuer"|trans }}</a>';
        modal += '</div>';
        modal += '</div>';
        modal += '</div>';
        modal += '</div>';

        jQuery('body').append(modal);

    } else {

        jQuery('body #temp-eenov-modal h4.modal-title').html(title);
        jQuery('body #temp-eenov-modal .modal-body .text-center').html(content);
        jQuery('body #temp-eenov-modal .btn.blue').attr('href', href);
    }
}
