countdown.resetLabels();
countdown.setLabels(
    ' milliseconde| seconde| minute| heure| jour| semaine| mois| année| décennie| siècle| millénaire',
    ' millisecondes| secondes| minutes| heures| jours| semaines| mois| années| décennies| siècles| millénaires',
    ' et ',
    ', ',
    'maintenant'
);

$(document).ready(function () {
    // Slider
    $.supersized({
        slide_interval: 5000,
        transition: 1,
        transition_speed: 700,
        slide_links: false,
        slides: window['supersizedImages'] || {}
    });

    // Show and hide details panel
    $('#auction-room').on('click', '.details a.toggle', function (e) {
        e.preventDefault();
        $(this).parent().toggleClass('open closed')
    });

    // Reload
    var $inner = $('#inner[data-uri]');

    function reloadInner() {
        $.ajax({
            url: $inner.data('uri'),
            success: function (html) {
                $inner.html(html);
                reloadTimespan();
            },
            cache: false
        });
    }

    if ($inner.length) {
        setInterval(reloadInner, 15000);
        reloadInner();
    }

    function reloadTimespan() {
        var now = new Date(),
            $timespan = $('#bid-ended-timespan');

        if ($timespan.length) {
            if (now <= bidEnded) {
                $timespan.html(countdown(null, bidEnded, countdown.HOURS | countdown.MINUTES | countdown.SECONDS).toString());
            } else {
                window.location.reload();
            }
        }
    }

    setInterval(reloadTimespan, 1000);
});
