$(document).ready(function () {
    // Metronic
    Metronic.setAssetsPath('/bundles/eenovdefault/lib/metronic_v3.2.0/');
    Metronic.setGlobalImgPath('global/img/');
    Metronic.init();
    Layout.init();

    // Toasts
    if (flashes && toastr) {
        $.each(flashes, function (type, texts) {
            $.each(texts, function (k, text) {
                toastr[type](text);
            });
        });
    }

    // Select2
    $('select').each(function () {
        $(this).select2({
            allowClear: !$(this).prop('required')
        });
    });

    // Datepicker
    $('.datepicker').datepicker({
        language: 'fr'
    });
    // @todo issue http://gitlab.oxioneo-immo.com/eenov/oxioneo/issues/250
    // new Date((new Date()).getTime() + (45 * 24 * 60 * 60 * 1000))
    $('.datepicker-45').datepicker({
        language: 'fr',
        startDate: new Date()
    });
    $('.timepicker').timepicker({
        autoclose: true,
        minuteStep: 5,
        showSeconds: false,
        showMeridian: false
    });

    // Started vs Ended
    $('form .started input').on('changeDate', function () {
        var date = $(this).datepicker('getDate'),
            start = new Date(),
            end = new Date(),
            $ended = $('form .ended input');

        start.setTime(date.getTime());
        start.setDate(date.getDate() + 1);

        end.setTime(date.getTime());
        end.setDate(date.getDate() + 1);

        $ended.datepicker('setStartDate', start);
        $ended.datepicker('setEndDate', end);
    }).each(function () {
        var val = $(this).datepicker('getDate');
        if (val && val.getTime && val.getTime()) {
            $(this).datepicker('setDate', val);
        }
    });

    // Firstprice vs Estimatedprice
    var $estimatedPrice = $('form .estimatedprice input');
    $('form .firstprice').on('change', 'input', function () {
        if (1 === $estimatedPrice.length) {
            if (!$estimatedPrice.val() || ($estimatedPrice.val() < $(this).val())) {
                $estimatedPrice.val($(this).val());
            }
            $estimatedPrice.attr('min', $(this).val());
        }
    });

    // Wysiwyg
    $('.wysiwyg').summernote({height: 400});

    // Calendar
    $('.calendar[data-calendar-api]').each(function () {
        var conf = {
            ignoreTimezone: true,
            events: {
                url: $(this).data('calendar-api')
            },
            header: {
                left: 'title',
                center: 'month,agendaWeek,agendaDay',
                right: 'today prev,next'
            }
        };

        if ($(this).data('calendar-date')) {
            conf.defaultDate = $(this).data('calendar-date');
        }

        $(this).fullCalendar(conf);
    });

    // Create new agency
    var $selectorOwner = $('#selector-owner');
    $selectorOwner.on('click', '#create-owner-button,#select-owner-button', function (e) {
        e.preventDefault();
        $(this).closest('ul').children('li').removeClass('active');
        $(this).parent().addClass('active');
        $selectorOwner.find('select').select2('val', '');
        $selectorOwner.children('div').hide();
        $selectorOwner.children($(this).attr('href')).show();
    }).on('click', 'li.disabled a', function (e) {
        e.preventDefault();
    });

    var $selectorContact = $('#selector-contact');
    $selectorContact.on('click', '#empty-contact-button,#create-contact-button,#select-contact-button', function (e) {
        e.preventDefault();
        $(this).closest('ul').children('li').removeClass('active');
        $(this).parent().addClass('active');
        $selectorContact.find('#eenov_admin_agency_isContactEmpty').prop('checked', 'empty-contact-button' === $(this).attr('id'));
        $selectorContact.find('select').select2('val', '');
        $selectorContact.children('div').hide();
        $selectorContact.children($(this).attr('href')).show();
    }).on('click', 'li.disabled a', function (e) {
        e.preventDefault();
    });

    // Toggle with checkbox
    $('[data-check-will-toggle]').each(function () {
        var targets = $(this).data('check-will-toggle'),
            $targets = $(targets),
            $firstInput = $(this).find('input[type=checkbox]').first();

        $(this).on('change', 'input[type=checkbox]', function () {
            $targets.toggle($(this).prop('checked'));
        });
        if ($firstInput.length) {
            $targets.toggle($firstInput.prop('checked'));
        }
    });

});
