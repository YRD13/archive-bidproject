<?php

namespace Eenov\DefaultBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class EenovDefaultBundle
 *
*
 */
class EenovDefaultBundle extends Bundle
{
    /**
     * getLastMonthDay
     *
     * @param \DateTime $date
     *
     * @return \DateTime
     */
    static function getLastMonthDay(\DateTime $date)
    {
        $new = clone $date;
        $new->setDate((int)$new->format('Y'), (int)$new->format('m'), (int)$new->format('t'));

        return $new;
    }

    /**
     * getLastNextMonthDay
     *
     * @param \DateTime $date
     *
     * @return \DateTime
     */
    static function getLastNextMonthDay(\DateTime $date)
    {
        $new = clone $date;
        $new->setDate((int)$new->format('Y'), 1 + (int)$new->format('m'), 1);
        $new->setDate((int)$new->format('Y'), (int)$new->format('m'), (int)$new->format('t'));

        return $new;
    }

    /**
     * getLastPreviousMonthDay
     *
     * @param \DateTime $date
     *
     * @return \DateTime
     */
    static function getLastPreviousMonthDay(\DateTime $date)
    {
        $new = clone $date;
        $new->setDate((int)$new->format('Y'), -1 + (int)$new->format('m'), 1);
        $new->setDate((int)$new->format('Y'), (int)$new->format('m'), (int)$new->format('t'));

        return $new;
    }

    /**
     * getFirstMonthDay
     *
     * @param \DateTime $date
     *
     * @return \DateTime
     */
    static function getFirstMonthDay(\DateTime $date)
    {
        $new = clone $date;
        $new->setDate((int)$new->format('Y'), (int)$new->format('m'), 1);

        return $new;
    }

    /**
     * getFirstNextMonthDay
     *
     * @param \DateTime $date
     *
     * @return \DateTime
     */
    static function getFirstNextMonthDay(\DateTime $date)
    {
        $new = clone $date;
        $new->setDate((int)$new->format('Y'), 1 + (int)$new->format('m'), 1);

        return $new;
    }

    /**
     * getFirstPreviousMonthDay
     *
     * @param \DateTime $date
     *
     * @return \DateTime
     */
    static function getFirstPreviousMonthDay(\DateTime $date)
    {
        $new = clone $date;
        $new->setDate((int)$new->format('Y'), -1 + (int)$new->format('m'), 1);

        return $new;
    }
}
