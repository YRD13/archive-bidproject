<?php

namespace Eenov\AgencyBundle\EventListener;

use Eenov\AgencyBundle\Controller\AbstractAgencyController;
use Eenov\AgencyBundle\Controller\AgencySlotController;
use Eenov\DefaultBundle\Advert\SlotDealer;
use Eenov\DefaultBundle\Entity\Agency;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

/**
 * Class AgencySlotEventListener
 *
*
 */
class AgencySlotEventListener
{
    /**
     * @var SlotDealer
     */
    private $slotDealer;

    /**
     * @param SlotDealer $slotDealer
     */
    public function __construct(SlotDealer $slotDealer)
    {
        $this->slotDealer = $slotDealer;
    }

    /**
     * On kernel.controller
     *
     * @param FilterControllerEvent $event
     */
    public function onKernelController(FilterControllerEvent $event)
    {
        if ($event->isMasterRequest()) {
            $request = $event->getRequest();
            if (null === $agency = $request->attributes->get('agency')) {
                return;
            }
            if (!$agency instanceof Agency) {
                return;
            }
            if (null === $controllerCallable = $event->getController()) {
                return;
            }
            if (!is_array($controllerCallable)) {
                return;
            }
            $controller = $controllerCallable[0];
            if (!$controller instanceof AbstractAgencyController) {
                return;
            }
            if ($this->slotDealer->isAllowed($agency)) {
                return;
            }
            if ($controller instanceof AgencySlotController && 'indexAction' !== $controllerCallable[1]) {
                return;
            }

            $controllerCallable[1] = 'firstAction';

            $event->setController($controllerCallable);
        }
    }
}
