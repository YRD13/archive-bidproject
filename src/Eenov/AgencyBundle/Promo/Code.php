<?php

namespace Eenov\AgencyBundle\Promo;

use Symfony\Component\ExpressionLanguage\ExpressionLanguage;

/**
 * Class Code
 *
*
 */
class Code
{
    public static $codes = [

        'PROPRIETES_PRIVEES' => '"one" == pack["id"] ? pack["pricePerSlot"] - 0 : ("start" == pack["id"] ? pack["pricePerSlot"] - 0 : pack["pricePerSlot"])',
        'CIMMENCHERES' => '"one" == pack["id"] ? pack["pricePerSlot"] - 0 : ("start" == pack["id"] ? pack["pricePerSlot"] - 0 : pack["pricePerSlot"])',
        'LANCEMENT_NOUVELLE_OFFRE' => 'round(pack["pricePerSlot"] - (0 / 0 * pack["pricePerSlot"]))',
        'FNAIM69' => '"one" == pack["id"] ? pack["pricePerSlot"] - 20 : ("start" == pack["id"] ? pack["pricePerSlot"] - 20 : pack["pricePerSlot"])',

    ];

    /**
     * @var ExpressionLanguage
     */
    private $el;

    /**
     * @param ExpressionLanguage $el
     */
    public function __construct(ExpressionLanguage $el)
    {
        $this->el = $el;

        $this->el->register('round', function ($str) {
            return sprintf('(is_numeric(%1$s) ? round(%1$s) : %1$s)', $str);
        }, function ($arguments, $str) {
            return is_numeric($str) ? round($str) : $str;
        });
    }

    /**
     * Has Code
     *
     * @param string $code
     *
     * @return bool
     */
    public function hasCode($code)
    {
        return isset(self::$codes[$code]);
    }

    /**
     * Get Price
     *
     * @param string $code Code
     * @param array  $pack Pack
     *
     * @return float
     */
    public function getPrice($code, array $pack)
    {
        if ($this->hasCode($code)) {
            return (float)$this->el->evaluate(self::$codes[$code], ['pack' => $pack]);
        }

        return (float)$pack['pricePerSlot'];
    }
}
