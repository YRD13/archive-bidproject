<?php

namespace Eenov\AgencyBundle\Form;

use Doctrine\ORM\EntityRepository;
use Eenov\DefaultBundle\Entity\Agency;
use Eenov\DefaultBundle\Entity\User;
use Eenov\DefaultBundle\Form\FormNameTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

/**
 * Class AgencyType
 *
*
 */
class AgencyType extends AbstractType
{
    use FormNameTrait;

    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    /**
     * @param AuthorizationCheckerInterface $authorizationChecker
     */
    public function __construct(AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('file', 'file', [
                'label' => 'Logo',
                'required' => false,
            ])
            ->add('name', 'text', [
                'label' => 'Nom',
            ])
            ->add('displayName', 'text', [
                'label' => 'Nom commercial (affiché sur le site)',
                'required' => false,
            ])
            ->add('email', 'email', [
                'label' => 'Email',
            ])
            ->add('phone', 'text', [
                'label' => 'Téléphone',
                'required' => false,
            ])
            ->add('address', 'textarea', [
                'label' => 'Adresse',
                'required' => false,
            ])
            ->add('zip', 'text', [
                'label' => 'Code postal',
                'required' => false,
            ])
            ->add('city', 'text', [
                'label' => 'Ville',
                'required' => false,
            ])
            ->add('country', 'text', [
                'label' => 'Pays',
                'required' => false,
            ])
            ->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
                $agency = $event->getData();
                if ($agency instanceof Agency) {
                    if ($this->authorizationChecker->isGranted(User::ROLE_COMMERCIAL)) {
                        $event->getForm()->add('mother', 'entity', [
                            'class' => Agency::class,
                            'label' => 'Agence mère (visible car vous êtes commercial de cette structure)',
                            'query_builder' => function (EntityRepository $er) use ($agency) {
                                $qb = $er->createQueryBuilder('agency');

                                if (null !== $agency->getId()) {
                                    $qb
                                        ->andWhere($qb->expr()->neq('agency.id', ':id'))
                                        ->setParameter('id', $agency->getId());
                                }

                                return $qb;
                            },
                            'required' => false,
                        ]);
                    }
                }
            });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Agency::class,
        ]);
    }
}
