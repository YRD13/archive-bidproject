<?php

namespace Eenov\AgencyBundle\Form\Transformer;

use Doctrine\ORM\EntityManager;
use Eenov\DefaultBundle\Entity\User;
use Symfony\Component\Form\DataTransformerInterface;

/**
 * Class UserIdTransformer
 *
*
 */
class UserIdTransformer implements DataTransformerInterface
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function transform($value)
    {
        if ($value instanceof User) {
            return $value->getId();
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function reverseTransform($value)
    {
        if (is_numeric($value)) {
            return $this->em->getRepository(User::class)->find(abs((int)$value));
        }

        return null;
    }
}
