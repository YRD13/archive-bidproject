<?php

namespace Eenov\AgencyBundle\Form;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Eenov\DefaultBundle\Entity\Access;
use Eenov\DefaultBundle\Entity\Mandate;
use Eenov\DefaultBundle\Form\FormNameTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\SubmitButton;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AccessValidateType
 *
*
 */
class AccessValidateType extends AbstractType
{
    use FormNameTrait;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('refuse', 'submit', [
                'label' => 'Refuser',
            ])
            ->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
                $access = $event->getData();

                if ($access instanceof Access) {
                    if (false === $access->hasBeenValidated() || false === $access->isValid()) {
                        $event
                            ->getForm()
                            ->add('mandate', 'entity', [
                                'label' => 'Mandat de participation acquéreur',
                                'class' => Mandate::class,
                                'query_builder' => function (EntityRepository $er) use ($access) {
                                    $qb = $er->createQueryBuilder('a');

                                    return $qb
                                        ->leftJoin('a.access', 'b')
                                        ->andWhere($qb->expr()->eq('a.bid', ':bid'))
                                        ->setParameter('bid', $access->getBid())
                                        ->andWhere($qb->expr()->isNull('b.id'));
                                },
                            ])
                            ->add('accept', 'submit', [
                                'label' => 'Accepter',
                            ]);
                    }
                }
            })
            ->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
                $access = $event->getData();

                if ($access instanceof Access) {
                    // Accept
                    if ($event->getForm()->has('accept')) {
                        if (null !== $accept = $event->getForm()->get('accept')) {
                            if ($accept instanceof SubmitButton) {
                                if ($accept->isClicked()) {
                                    $access
                                        ->validate();
                                }
                            }
                        }
                    }

                    // Refuse
                    if (null !== $refuse = $event->getForm()->get('refuse')) {
                        if ($refuse instanceof SubmitButton) {
                            if ($refuse->isClicked()) {
                                $access
                                    ->invalidate()
                                    ->setMandate(null);
                            }
                        }
                    }
                }
            });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Access::class,
        ]);
    }
}
