<?php

namespace Eenov\AgencyBundle\Form;

use Eenov\DefaultBundle\Entity\User;
use Eenov\DefaultBundle\Form\FormNameTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class UserType
 *
*
 */
class UserType extends AbstractType
{
    use FormNameTrait;

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'eenov_default_user';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('phone', 'text', [
                'label' => 'Téléphone',
                'required' => false,
                'mapped' => false,
            ])
            ->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
                $user = $event->getData();
                if ($user instanceof User) {
                    $event->getForm()->get('phone')->setData($user->getProfile()->getPhone());
                }
            })
            ->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
                $user = $event->getData();
                if ($user instanceof User) {
                    $user->getProfile()->setPhone($event->getForm()->get('phone')->getData());
                }
            });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
