<?php

namespace Eenov\AgencyBundle\Form;

use Eenov\DefaultBundle\Entity\Bid;
use Eenov\DefaultBundle\Form\FormNameTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class BidType
 *
*
 */
class BidType extends AbstractType
{
    use FormNameTrait;

    /**
     * @var string
     */
    private $unityMoney;

    /**
     * @param string $unityMoney
     */
    public function __construct($unityMoney)
    {
        $this->unityMoney = $unityMoney;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $choices = [
            '1' => 'Vendeur',
            '0' => 'Acquéreur'
        ];
        $choicesHour = [
            '0' => '00h00',
            '1' => '1h00',
            '2' => '2h00',
            '3' => '3h00',
            '4' => '4h00',
            '5' => '5h00',
            '6' => '6h00',
            '7' => '7h00',
            '8' => '8h00',
            '9' => '9h00',
            '10' => '10h00',
            '11' => '11h00',
            '12' => '12h00',
            '13' => '13h00',
            '14' => '14h00',
            '15' => '15h00',
            '16' => '16h00',
            '17' => '17h00',
            '18' => '18h00',
            '19' => '19h00',
            '20' => '20h00',
            '21' => '21h00',
            '22' => '22h00',
            '23' => '23h00',





        ];

        $builder
            ->add('sellerFirstname', 'text', [
                'label' => 'Prénom du vendeur',
            ])
            ->add('sellerLastname', 'text', [
                'label' => 'Nom du vendeur',
            ])
            ->add('sellerEmail', 'email', [
                'label' => 'Email du vendeur',
                'required' => false,
            ])
            ->add('sellerPhone', 'text', [
                'label' => 'Téléphone du vendeur',
                'required' => false,
            ])
            ->add('negotiatorName', 'text', [
                'label' => 'Nom du négociateur',
                'required' => false,
            ])
            ->add('file', 'pdfmerge', [
                'label' => 'Mandat de recherche d\'acquéreur',
                'required' => false,
            ])
            ->add('mandateDate', 'datepicker', [
                'label' => 'Date de première mise en vente',
                'required' => false,
            ])
            ->add('title', 'text', [
                'label' => 'Titre principal de l\'annonce',
            ])
            ->add('draft', 'eenov_agency_advert', [
                'label' => 'Brouillon',
            ])
            ->add('sellStarted', 'datepicker', [
                'label' => 'Début de l\'enchère',
                'required' => false,
                'attr' => [
                    'class' => 'datepicker-45',
                ],
            ])
            ->add('sellEnded', 'datepicker', [
                'label' => 'Fin de l\'enchère',
                'required' => false,
                'attr' => [
                    'class' => 'datepicker-45',
                ],
            ])
            ->add('priceStartOld', 'integer', [
//              'label' => sprintf('Première offre possible (%s)', $this->unityMoney),
                'label' => sprintf(' Prix de départ hors frais d\'agence (%s)',$this->unityMoney),
                'required' => false,
                'disabled'=>true,
                'attr' => [
                    'min' => 0,
                ],
            ])
            ->add('firstPrice', 'integer', [
                // 'label' => sprintf('Première offre possible (%s)', $this->unityMoney),

                'label' => sprintf('Prix de départ des offres frais d\'agence inclus (prix communiqué sur les portails d’annonces) (%s)',$this->unityMoney),
                'required' => false,
                'attr' => [
                    'min' => 0,
                ],
            ])
            ->add('priceEstimatedOld', 'integer', [
//                'label' => sprintf('Prix de présentation (%s)', $this->unityMoney),
                'label' => sprintf('Prix idéal vendeur hors frais d\'agence (%s)', $this->unityMoney),
                'required' => false,
                'disabled'=>true,
                'attr' => [
                    'min' => 0,
                ],
            ])
            ->add('estimatedPrice', 'integer', [
                // 'label' => sprintf('Prix de présentation (%s)', $this->unityMoney),
                'label' => sprintf('Prix idéal vendeur frais d\'agence inclus (%s)', $this->unityMoney),
                'required' => false,
                'attr' => [
                    'min' => 0,
                ],
            ])
            ->add('allowFirmAuction', 'checkbox', [
                'label' => 'Autoriser les enchères uniques ?',
                'disabled' => true,
                'read_only' => true,
                'required' => false,
            ])
            ->add('hourStart', 'choice', [
                'label' => 'Départ et fin des offres',
                'choices' => $choicesHour,
                'expanded' => false,
                'required' => false,
            ])
            ->add('honorairePrice', 'integer', [
//                'label' => sprintf('Prix de présentation (%s)', $this->unityMoney),
                'label' => sprintf('Honoraires calculés sur le prix idéal vendeur  (%s)', $this->unityMoney),
                'required' => false,
                'attr' => [
                    'min' => 0,
                ],
            ])
            ->add('honorairePrice2', 'integer', [
//                'label' => sprintf('Prix de présentation (%s)', $this->unityMoney),
                'mapped'=>false,
                'placeholder'=>'Indiquez ici le montant',
                'required' => false,
                'label'=>false,
                'attr' => [
                    'min' => 0,
                ]
            ])
            ->add('honoraireForSeller', 'choice', [
                'choices' => $choices,
                'expanded' => false,
                'required' => false,
                'label' => 'Indiquez à qui incombe la charge des honoraires d\'agence'])

//            ->add('honoraireCalculation', 'choice', array(
//                'mapped'=>false,
//                'required' => false,
//                'multiple'=>false,
//                'choices'   => array('0' => 'Valeur', '1' => 'En pourcentage %'),
//                'preferred_choices' => array('0'),
//                'label' => 'Indiquez la méthode de calcul des honoraires'
//            ))
            ->add('honoraireCalculation', 'integer', array(
                'mapped'=>false,
                'required' => false,
                'label' => 'Valeur'
            ))

            ->add('honoraireCalculation2', 'integer', array(
                'mapped'=>false,
                'required' => false,
                'label' => 'Pourcentage'
            ))


//            ->add('honorairePricePourcentage', 'integer', [
//                'mapped' => false,
//                'label' => 'Veuillez indiquer ici le montant des honoraires (en pourcentage %)',
//                'required' => false,
//                'attr' => [
//                    'min' => 0,
//                ],
//            ])
            ->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
                $bid = $event->getData();
                if ($bid instanceof Bid) {
                    $bid->setSellRealEnded($bid->getSellEnded());
                }
            });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Bid::class,
            'cascade_validation' => true,
        ]);
    }
}
