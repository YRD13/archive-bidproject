<?php

namespace Eenov\AgencyBundle\Form;

use Doctrine\ORM\EntityManager;
use Eenov\AgencyBundle\Form\Transformer\UserIdTransformer;
use Eenov\DefaultBundle\Entity\Access;
use Eenov\DefaultBundle\Form\FormNameTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AccessType
 *
*
 */
class AccessType extends AbstractType
{
    use FormNameTrait;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                $builder->create('user', 'integer', [
                    'label' => 'Identifiant de l\'utilisateur',
                ])->addModelTransformer(new UserIdTransformer($this->em))
            );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Access::class,
        ]);
    }
}
