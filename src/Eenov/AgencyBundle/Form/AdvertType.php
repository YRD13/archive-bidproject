<?php

namespace Eenov\AgencyBundle\Form;

use Eenov\DefaultBundle\Entity\Advert;
use Eenov\DefaultBundle\Entity\AdvertType as EntityAdvertType;
use Eenov\DefaultBundle\Entity\Around;
use Eenov\DefaultBundle\Entity\Equipment;
use Eenov\DefaultBundle\Form\FormNameTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AdvertType
 *
*
 */
class AdvertType extends AbstractType
{
    use FormNameTrait;

    /**
     * @var string
     */
    private $unitySurface;

    /**
     * @param string $unitySurface
     */
    public function __construct($unitySurface)
    {
        $this->unitySurface = $unitySurface;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('point', 'eenov_default_point', [
                'cascade_validation' => true,
            ])
            ->add('address', 'textarea', [
                'label' => 'Adresse',
            ])
            ->add('zip', 'text', [
                'label' => 'Code postal',
            ])
            ->add('city', 'text', [
                'label' => 'Ville',
            ])
            ->add('country', 'text', [
                'label' => 'Pays',
            ])
            ->add('type', 'entity', [
                'label' => 'Type de bien',
                'class' => EntityAdvertType::class,
            ])
            ->add('mandateAgency', 'text', [
                'label' => 'Numéro de mandat agence',
                'required' => false,
            ])
            ->add('description', 'textarea', [
                'label' => 'Description',
            ])
            ->add('shab', 'number', [
                'label' => sprintf('Surface habitable (%s)', $this->unitySurface),
            ])
            ->add('field', 'number', [
                'label' => sprintf('Surface terrain (%s)', $this->unitySurface),
            ])
            ->add('livingRoom', 'number', [
                'label' => sprintf('Surface séjour (%s)', $this->unitySurface),
            ])
            ->add('rooms', 'integer', [
                'label' => 'Nombre de pièces',
                'attr' => [
                    'min' => 0,
                ],
            ])
            ->add('bedrooms', 'integer', [
                'label' => 'Nombre de chambres',
                'required' => false,
                'attr' => [
                    'min' => 0,
                ],
            ])
            ->add('bathrooms', 'integer', [
                'label' => 'Nombre de salles de bains',
                'required' => false,
                'attr' => [
                    'min' => 0,
                ],
            ])
            ->add('showerrooms', 'integer', [
                'label' => 'Nombre de salles d\'eau',
                'required' => false,
                'attr' => [
                    'min' => 0,
                ],
            ])
            ->add('floor', 'integer', [
                'label' => 'Étage',
                'required' => false,
                'attr' => [
                    'min' => 0,
                ],
            ])
            ->add('year', 'integer', [
                'label' => 'Année de construction',
                'required' => false,
                'attr' => [
                    'min' => 1000,
                    'max' => (int)date('Y'),
                ],
            ])
            ->add('estate', 'choice', [
                'label' => 'État',
                'choices' => Advert::getEstateNameList(),
            ])
            ->add('heatingSystem', 'choice', [
                'label' => 'Nature du chauffage',
                'choices' => Advert::getHeatingSystemNameList(),
            ])
            ->add('heatingType', 'choice', [
                'label' => 'Chauffage',
                'choices' => Advert::getHeatingTypeNameList(),
            ])
            ->add('exposure', 'choice', [
                'label' => 'Exposition',
                'choices' => Advert::getExposureNameList(),
            ])
            ->add('internet', 'choice', [
                'label' => 'Débit connexion internet',
                'choices' => Advert::getInternetNameList(),
                'required' => false,
            ])
            ->add('dpe', 'integer', [
                'label' => 'Diagnostic DPE',
                'attr' => [
                    'min' => 0,
                ],
                'required' => false,
            ])
            ->add('ges', 'integer', [
                'label' => 'Diagnostic GES',
                'attr' => [
                    'min' => 0,
                ],
                'required' => false,
            ])
            ->add('toilets', 'integer', [
                'label' => 'Nombre de WC',
                'required' => false,
            ])
            ->add('arounds', 'entity', [
                'label' => 'Aux alentours',
                'class' => Around::class,
                'required' => false,
                'expanded' => true,
                'multiple' => true,
            ])
            ->add('equipments', 'entity', [
                'label' => 'Équipements',
                'class' => Equipment::class,
                'required' => true,
                'expanded' => true,
                'multiple' => true,
            ])
            ->add('displayAddress', 'checkbox', [
                'label' => 'Afficher l\'adresse en front',
                'required' => false,
            ])
            ->add('isCondo', 'checkbox', [
                'label' => 'Ce bien est en copropriété',
                'required' => false,
            ])
            ->add('condoLots', 'integer', [
                'label' => 'Nombre de lots total dans la copropriété',
                'required' => false,
            ])
            ->add('condoCharges', 'number', [
                'label' => 'Montant des charges de la copropriété',
                'required' => false,
            ])
            ->add('condoChargesRecurrence', 'choice', [
                'label' => 'Récurrence des charges dans la copropriété',
                'required' => false,
                'choices' => Advert::getCondoChargesRecurrenceNameList(),
            ])
            ->add('condoHasOngoingProceedings', 'checkbox', [
                'label' => 'Existence de procédure en cours avec le syndic de copropriété',
                'required' => false,
            ])
            ->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
                $advert = $event->getData();
                if ($advert instanceof Advert) {
                    if (null === $advert->getType() || false === $advert->getType()->getIsGround()) {
                        $advert->setGround(null);
                    }
                    if (null === $advert->getType() || false === $advert->getType()->getIsBuilding()) {
                        $advert->setBuilding(null);
                    }
                    if (!$advert->getIsCondo()) {
                        $advert
                            ->setCondoLots(null)
                            ->setCondoCharges(null)
                            ->setCondoChargesRecurrence(null)
                            ->setCondoHasOngoingProceedings(null);
                    }

                    $event->setData($advert);
                }
            });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Advert::class,
        ]);
    }
}
