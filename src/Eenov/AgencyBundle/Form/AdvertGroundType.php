<?php

namespace Eenov\AgencyBundle\Form;

use Eenov\DefaultBundle\Entity\AdvertGround;
use Eenov\DefaultBundle\Form\FormNameTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AdvertGroundType
 *
*
 */
class AdvertGroundType extends AbstractType
{
    use FormNameTrait;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('pluZoning', 'text', [
                'label' => 'Zonage PLU',
            ])
            ->add('frontageLength', 'number', [
                'label' => 'Longueur de la façade (m)',
            ])
            ->add('fieldDepth', 'number', [
                'label' => 'Profondeur du terrain (m)',
            ])
            ->add('soilNature', 'text', [
                'label' => 'Nature du sol',
            ])
            ->add('serviced', 'checkbox', [
                'label' => 'Viabilisé',
                'required' => false,
            ])
            ->add('servicingTypes', 'choice', [
                'label' => 'Types de viabilisation',
                'choices' => AdvertGround::getServicingTypesNameList(),
                'multiple' => true,
                'expanded' => true,
                'required' => false,
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AdvertGround::class,
        ]);
    }
}
