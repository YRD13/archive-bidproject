<?php

namespace Eenov\AgencyBundle\Form;

use Eenov\DefaultBundle\Entity\AdvertBuilding;
use Eenov\DefaultBundle\Form\FormNameTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AdvertBuildingType
 *
*
 */
class AdvertBuildingType extends AbstractType
{
    use FormNameTrait;

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AdvertBuilding::class,
        ]);
    }
}
