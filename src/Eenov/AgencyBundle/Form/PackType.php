<?php

namespace Eenov\AgencyBundle\Form;

use Eenov\AgencyBundle\Promo\Code;
use Eenov\DefaultBundle\Form\FormNameTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Class PackType
 *
*
 */
class PackType extends AbstractType
{
    use FormNameTrait;

    /**
     * @var Code
     */
    private $code;

    /**
     * @param Code $code
     */
    public function __construct(Code $code)
    {
        $this->code = $code;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('code', 'text', [
                'label' => 'Code promotionnel',
                'required' => false,
            ])
            ->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
                if (null !== $code = $event->getForm()->get('code')->getData()) {
                    if (!$this->code->hasCode($code)) {
                        $event->getForm()->addError(new FormError('Ce code est invalide.'));
                    }
                }
            });
    }
}
