<?php

namespace Eenov\AgencyBundle\DependencyInjection;

use Eenov\AgencyBundle\Form as AgencyForm;
use Eenov\DefaultBundle\DependencyInjection\AbstractExtension;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Parameter;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class EenovDefaultExtension
 *
*
 */
class EenovAgencyExtension extends AbstractExtension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $forms = [
            AgencyForm\AccessType::class => [new Reference('doctrine.orm.default_entity_manager')],
            AgencyForm\AccessValidateType::class => [new Reference('doctrine.orm.default_entity_manager')],
            AgencyForm\AdvertBuildingType::class => [],
            AgencyForm\AdvertGroundType::class => [],
            AgencyForm\AdvertType::class => [new Parameter('unity_surface')],
            AgencyForm\AgencyType::class => [new Reference('security.authorization_checker')],
            AgencyForm\BidDatesType::class => [],
            AgencyForm\BidType::class => [new Parameter('unity_money')],
            AgencyForm\MandateType::class => [],
            AgencyForm\PackType::class => [new Reference('eenov.agency_bundle.promo.code')],
            AgencyForm\UserType::class => [],
            AgencyForm\VisitsType::class => [],
            AgencyForm\VisitType::class => [],
        ];
        $this->addForms($container, $forms);

        $loader = new Loader\XmlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('event_listener.xml');
        $loader->load('promo.xml');
    }
}
