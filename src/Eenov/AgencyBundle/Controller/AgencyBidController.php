<?php

namespace Eenov\AgencyBundle\Controller;

use Eenov\DefaultBundle\Entity\AdvertBuilding;
use Eenov\DefaultBundle\Entity\AdvertGround;
use Eenov\DefaultBundle\Entity\Agency;
use Eenov\DefaultBundle\Entity\Bid;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AgencyBidController
 *
*
 * @Route("/{agency}/annonces", requirements={"agency":"\d+"})
 */
class AgencyBidController extends AbstractAgencyController
{
    /**
     * Index
     *
     * @param Agency $agency Agency
     *
     * @return array
     * @Route("/{page}", requirements={"page":"\d+"}, defaults={"page":1})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Agency $agency)
    {
        // Find filtered results
        $paginatorHelper = $this->get('eb_paginator_helper');
        $paginator = $this->get('doctrine.orm.default_entity_manager')->getRepository(Bid::class)->getPaginator($paginatorHelper, [
            'agency' => $agency,
        ]);

        return [
            'agency' => $agency,
            'paginator' => $paginator,
            'slot' => $this->get('eenov.default_bundle.advert.slot_dealer')->findAvailableSlotFor($agency),
        ];
    }

    /**
     * Building
     *
     * @param Request $request Request
     * @param Agency  $agency  Agency
     * @param Bid     $bid     Bid
     *
     * @return array|RedirectResponse
     * @Route("/{bid}/immeuble", requirements={"bid":"\d+"})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @Method("GET|POST")
     * @Template()
     */
    public function buildingAction(Request $request, Agency $agency, Bid $bid)
    {
        $building = $bid->getDraft()->getBuilding() ?: new AdvertBuilding();
        $bid->getDraft()->setBuilding($building);
        $form = $this->createForm('eenov_agency_advert_building', $building);
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->persist($building);
            $em->flush();

            $this->get('eenov.default_bundle.session.session')->success('Annonce mise à jour avec succès');

            return $this->redirectToRoute('eenov_agency_agencybid_building', [
                'agency' => $agency->getId(),
                'bid' => $bid->getId(),
            ]);
        }

        return [
            'agency' => $agency,
            'bid' => $bid,
            'form' => $form->createView(),
        ];
    }

    /**
     * Create
     *
     * @param Request $request
     * @param Agency  $agency Agency
     *
     * @return array|RedirectResponse
     * @Route("/creer")
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @Method("GET|POST")
     * @Template()
     */
    public function createAction(Request $request, Agency $agency)
    {
        $bid = new Bid();
        $bid->setAgency($agency);
        $form = $this->createForm('eenov_agency_bid', $bid);
        if ($form->handleRequest($request)->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->get('doctrine.orm.default_entity_manager');
                $em->persist($bid);
                $em->flush();

                $this->get('eenov.default_bundle.session.session')->success('Annonce créée avec succès');

                return $this->redirectToRoute('eenov_agency_agencybid_update', [
                    'agency' => $agency->getId(),
                    'bid' => $bid->getId(),
                ]);
            } else {
                $form->addError(new FormError('Une erreur est survenue. L\'enchère n\'a pas été sauvegardée. Vous trouverez plus de détails sur les erreurs au niveau de chaque champ du formulaire.'));
            }
        }

        return [
            'agency' => $agency,
            'form' => $form->createView(),
        ];
    }

    /**
     * Daughter
     *
     * @param Agency $agency   Agency
     * @param Agency $daughter Daughter
     *
     * @return array
     * @Route("/agence-{daughter}/{page}", requirements={"daughter":"\d+","page":"\d+"}, defaults={"page":1})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @ParamConverter("daughter", class="EenovDefaultBundle:Agency")
     * @Method("GET")
     * @Template()
     */
    public function daughterAction(Agency $agency, Agency $daughter)
    {
        $ids = $this->getUser()->getOwnedAgencies()->map(function (Agency $agency) {
            return $agency->getId();
        })->toArray();
        if (false === in_array($daughter->getId(), $ids)) {
            throw $this->createNotFoundException();
        }

        // Find filtered results
        $paginatorHelper = $this->get('eb_paginator_helper');
        $paginator = $this->get('doctrine.orm.default_entity_manager')->getRepository(Bid::class)->getPaginator($paginatorHelper, [
            'agency' => $daughter,
        ]);

        return [
            'agency' => $agency,
            'daughter' => $daughter,
            'paginator' => $paginator,
        ];
    }

    /**
     * Delete
     *
     * @param Request $request Request
     * @param Agency  $agency  Agency
     * @param Bid     $bid     Bid
     *
     * @return array|RedirectResponse
     * @Route("/{bid}/supprimer", requirements={"bid":"\d+"})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @Method("GET|POST")
     * @Template()
     */
    public function deleteAction(Request $request, Agency $agency, Bid $bid)
    {
        $form = $this->createForm('form');
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->remove($bid);
            $em->flush();

            return $this->redirectToRoute('eenov_agency_agencybid_index', [
                'agency' => $agency->getId(),
            ]);
        }

        return [
            'agency' => $agency,
            'bid' => $bid,
            'form' => $form->createView(),
        ];
    }

    /**
     * File
     *
     * @param Agency $agency Agency
     * @param Bid    $bid    Bid
     *
     * @return BinaryFileResponse
     * @Route("/{bid}/mandat.{_format}", requirements={"bid":"\d+"})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @Method("GET")
     */
    public function fileAction(Agency $agency, Bid $bid)
    {
        if ($agency->getId() !== $bid->getAgency()->getId()) {
            throw $this->createNotFoundException();
        }
        if (null === $bid->getPath()) {
            throw $this->createNotFoundException();
        }

        return new BinaryFileResponse($bid->getPath(), 200, [
            'Content-Type' => 'application/force-download',
            'Cache-Control' => 'no-cache, private',
            'Content-Disposition' => sprintf('attachment; filename="%s"', $bid->getFilename()),
            'Content-Transfer-Encoding' => 'binary',
            'Content-Length' => $bid->getSize(),
        ]);
    }

    /**
     * Ground
     *
     * @param Request $request Request
     * @param Agency  $agency  Agency
     * @param Bid     $bid     Bid
     *
     * @return array|RedirectResponse
     * @Route("/{bid}/terrain", requirements={"bid":"\d+"})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @Method("GET|POST")
     * @Template()
     */
    public function groundAction(Request $request, Agency $agency, Bid $bid)
    {
        $ground = $bid->getDraft()->getGround() ?: new AdvertGround();
        $bid->getDraft()->setGround($ground);
        $form = $this->createForm('eenov_agency_advert_ground', $ground);
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->persist($ground);
            $em->flush();

            $this->get('eenov.default_bundle.session.session')->success('Annonce mise à jour avec succès');

            return $this->redirectToRoute('eenov_agency_agencybid_ground', [
                'agency' => $agency->getId(),
                'bid' => $bid->getId(),
            ]);
        }

        return [
            'agency' => $agency,
            'bid' => $bid,
            'form' => $form->createView(),
        ];
    }

    /**
     * Publish
     *
     * @param Request $request Request
     * @param Agency  $agency  Agency
     * @param Bid     $bid     Bid
     *
     * @return array|RedirectResponse
     * @Route("/{bid}/publier", requirements={"bid":"\d+"})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @Method("GET|POST")
     * @Template()
     */
    public function publishAction(Request $request, Agency $agency, Bid $bid)
    {
        $form = null;
        $motherValidationForm = null;
        $revalidateForm = null;
        if (null === $slot = $bid->getSlot()) {
            if (null !== $slot = $this->get('eenov.default_bundle.advert.slot_dealer')->findAvailableSlotFor($agency)) {
                $form = $this->createForm('form');
                if ($form->handleRequest($request)->isValid()) {
                    $bid->setSlot($slot);
                    $this->get('doctrine.orm.default_entity_manager')->flush();

                    $this->get('eenov.default_bundle.session.session')->success('Votre annonce a été soumise a validation');

                    return $this->redirectToRoute('eenov_agency_agencybid_publish', [
                        'agency' => $agency->getId(),
                        'bid' => $bid->getId(),
                    ]);
                }
            }
        } else {
            if (false === $bid->needsValidation()) {
                $revalidateForm = $this->createForm('form');
                if ($revalidateForm->handleRequest($request)->isValid()) {
                    $bid->setAgencyValidated(null)->setAdminValidated(null);
                    $this->get('doctrine.orm.default_entity_manager')->flush();

                    $this->get('eenov.default_bundle.session.session')->success('Votre annonce a été de nouveau soumise a validation');

                    return $this->redirectToRoute('eenov_agency_agencybid_publish', [
                        'agency' => $agency->getId(),
                        'bid' => $bid->getId(),
                    ]);
                }
            }

            $ids = $this->getUser()->getOwnedAgencies()->map(function (Agency $agency) {
                return $agency->getId();
            })->toArray();
            $ids[] = $this->getUser()->getAgencyManaged() ? $this->getUser()->getAgencyManaged()->getId() : null;
            foreach ($this->getUser()->getResponsibleAgencies() as $managedAgency) {
                $ids[] = $managedAgency->getId();
            }
            $ids = array_filter($ids);

            if (in_array($slot->getAgency()->getId(), $ids)) {
                if (null === $bid->getAgencyValidated()) {
                    $motherValidationForm = $this->createForm('form');
                    if ($motherValidationForm->handleRequest($request)->isValid()) {
                        $bid->setAgencyValidated(new \DateTime());
                        $this->get('eenov.default_bundle.advert.advert_cloner')->progress($bid);
                        $this->get('doctrine.orm.default_entity_manager')->flush();

                        $this->get('eenov.default_bundle.session.session')->success('Annonce validée avec succès');

                        return $this->redirectToRoute('eenov_agency_agencybid_publish', [
                            'agency' => $agency->getId(),
                            'bid' => $bid->getId(),
                        ]);
                    }
                }
            }
        }

        return [
            'agency' => $agency,
            'bid' => $bid,
            'slot' => $slot,
            'form' => $form ? $form->createView() : null,
            'motherValidationForm' => $motherValidationForm ? $motherValidationForm->createView() : null,
            'revalidateForm' => $revalidateForm ? $revalidateForm->createView() : null,
        ];
    }

    /**
     * Update
     *
     * @param Request $request Request
     * @param Agency  $agency  Agency
     * @param Bid     $bid     Bid
     *
     * @return array|RedirectResponse
     * @Route("/{bid}/modifier", requirements={"bid":"\d+"})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @Method("GET|POST")
     * @Template()
     */
    public function updateAction(Request $request, Agency $agency, Bid $bid)
    {
        $form = $this->createForm('eenov_agency_bid', $bid);
        if ($form->handleRequest($request)->isSubmitted()) {
            if ($form->isValid()) {
                $heureDebut=$form->get('hourStart')->getData();
                if($heureDebut == null ){
                    $heureDebut = 12;
                }

                $bid->setHourStart($heureDebut);
                $bid->setSellStarted( $form->get('sellStarted')->getData());
                $bid->setSellEnded( $form->get('sellEnded')->getData());


                /*$DepartOffre =$form->get('priceStartOld')->getData();
                $DepartOffreEstimated =$form->get('priceEstimatedOld')->getData();*/

                $DepartOffre =$form->get('firstPrice')->getData();
                $DepartOffreEstimated =$form->get('estimatedPrice')->getData();

                $honoraireCalculation =floatval(str_replace(",", ".", $form->get('honoraireCalculation')->getData()));
                $honoraireCalculation2 =floatval(str_replace(",", ".", $form->get('honoraireCalculation2')->getData()));

                if ($honoraireCalculation !="" && $honoraireCalculation2 == "") {
                    $bid->setHonorairePrice($honoraireCalculation);

                    $bid->setFirstPrice($DepartOffre);
                    $bid->setPriceStartOld($DepartOffre - $honoraireCalculation);
                    $bid->setEstimatedPrice($DepartOffreEstimated);
                    $bid->setPriceEstimatedOld($DepartOffreEstimated-$honoraireCalculation);
                }elseif ($honoraireCalculation =="" && $honoraireCalculation2 != ""){
                    $honoraireMontant_first = round($DepartOffre*$honoraireCalculation2/100);
                    $honoraireMontant_estimated = round($DepartOffreEstimated*$honoraireCalculation2/100);
                    $bid->setHonorairePrice($honoraireMontant_estimated);
                    $bid->setFirstPrice($DepartOffre);
                    $bid->setPriceStartOld($DepartOffre - $honoraireMontant_first);
                    $bid->setEstimatedPrice($DepartOffreEstimated);
                    $bid->setPriceEstimatedOld($DepartOffreEstimated - $honoraireMontant_estimated);

                }

                $bid->setStepPrice($bid->calculateStepPrice($bid->getFirstPrice(), $bid->getEstimatedPrice()));
                $this->get('doctrine.orm.default_entity_manager')->flush();

                $this->get('eenov.default_bundle.session.session')->success('Annonce mise à jour avec succès');

                return $this->redirectToRoute('eenov_agency_agencybid_update', [
                    'agency' => $agency->getId(),
                    'bid' => $bid->getId(),
                ]);
            } else {
                $form->addError(new FormError('Une erreur est survenue. L\'enchère n\'a pas été sauvegardée. Vous trouverez plus de détails sur les erreurs au niveau de chaque champ du formulaire.'));
            }
        }

        return [
            'agency' => $agency,
            'bid' => $bid,
            'form' => $form->createView(),
        ];
    }

    /**
     * Dates
     *
     * @param Request $request Request
     * @param Agency  $agency  Agency
     * @param Bid     $bid     Bid
     *
     * @return array|RedirectResponse
     * @Route("/{bid}/dates", requirements={"bid":"\d+"})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @Method("GET|POST")
     * @Template()
     */
    public function datesAction(Request $request, Agency $agency, Bid $bid)
    {
        $form = $this->createForm('eenov_agency_bid_dates', $bid, ['validation_groups' => ['Dates']]);
        if ($form->handleRequest($request)->isValid()) {
            $this->get('doctrine.orm.default_entity_manager')->flush();

            $this->get('eenov.default_bundle.session.session')->success('Dates mises à jour avec succès');

            return $this->redirectToRoute('eenov_agency_agencybid_dates', [
                'agency' => $agency->getId(),
                'bid' => $bid->getId(),
            ]);
        }

        return [
            'agency' => $agency,
            'bid' => $bid,
            'form' => $form->createView(),
        ];
    }

    /**
     * Text
     *
     * @param Agency $agency Agency
     * @param Bid    $bid    Bid
     *
     * @return array|RedirectResponse
     * @Route("/{bid}/texte-annonce", requirements={"bid":"\d+"})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @Method("GET")
     * @Template()
     */
    public function textAction(Agency $agency, Bid $bid)
    {
        return [
            'agency' => $agency,
            'bid' => $bid,
        ];
    }
}
