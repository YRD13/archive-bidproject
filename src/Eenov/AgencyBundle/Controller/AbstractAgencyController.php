<?php

namespace Eenov\AgencyBundle\Controller;

use Eenov\DefaultBundle\Entity\Agency;
use Eenov\DefaultBundle\Entity\SubscriptionPack;
use Eenov\DefaultBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AbstractAgencyController
 *
*
 */
abstract class AbstractAgencyController extends Controller
{
    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return parent::getUser();
    }

    /**
     * First
     *
     * @param Request $request Request
     * @param Agency  $agency  Agency
     *
     * @return array
     */
    public function firstAction(Request $request, Agency $agency)
    {
        return $this->render('EenovAgencyBundle::first.html.twig', [
            'agency' => $agency,
            'packs' => SubscriptionPack::getPacks(),
        ]);
    }
}
