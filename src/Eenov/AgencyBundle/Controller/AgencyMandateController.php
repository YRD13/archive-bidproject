<?php

namespace Eenov\AgencyBundle\Controller;

use Eenov\DefaultBundle\Entity\Agency;
use Eenov\DefaultBundle\Entity\Bid;
use Eenov\DefaultBundle\Entity\Mandate;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AgencyMandateController
 *
*
 * @Route("/{agency}/annonces/{bid}/mandats", requirements={"agency":"\d+","bid":"\d+"})
 */
class AgencyMandateController extends AbstractAgencyController
{
    /**
     * Create
     *
     * @param Request $request Request
     * @param Agency  $agency  Agency
     * @param Bid     $bid     Bid
     *
     * @return array
     * @Route("/creer")
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @Method("GET|POST")
     * @Template()
     */
    public function createAction(Request $request, Agency $agency, Bid $bid)
    {
        $mandate = new Mandate();
        $mandate->setBid($bid);
        $form = $this->createForm('eenov_agency_mandate', $mandate);
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->persist($mandate);
            $em->flush();

            $this->get('eenov.default_bundle.session.session')->success('Demande sauvegardée');

            return $this->redirectToRoute('eenov_agency_agencymandate_index', [
                'agency' => $agency->getId(),
                'bid' => $bid->getId(),
            ]);
        }

        return [
            'bid' => $bid,
            'agency' => $agency,
            'form' => $form->createView(),
        ];
    }

    /**
     * Delete
     *
     * @param Agency  $agency  Agency
     * @param Bid     $bid     Bid
     * @param Mandate $mandate Access
     *
     * @return array|RedirectResponse
     * @Route("/{mandate}/supprimer", requirements={"mandate":"\d+"})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @ParamConverter("mandate", class="EenovDefaultBundle:Mandate")
     * @Method("GET|POST")
     */
    public function deleteAction(Agency $agency, Bid $bid, Mandate $mandate)
    {
        $em = $this->get('doctrine.orm.default_entity_manager');
        $em->remove($mandate);
        $em->flush();

        $this->get('eenov.default_bundle.session.session')->success('Demande supprimée');

        return $this->redirectToRoute('eenov_agency_agencymandate_index', [
            'agency' => $agency->getId(),
            'bid' => $bid->getId(),
        ]);
    }

    /**
     * File
     *
     * @param Agency  $agency  Agency
     * @param Bid     $bid     Bid
     * @param Mandate $mandate Access
     *
     * @return BinaryFileResponse
     * @Route("/{mandate}/mandat.{_format}", requirements={"mandate":"\d+"})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @ParamConverter("mandate", class="EenovDefaultBundle:Mandate")
     * @Method("GET")
     */
    public function fileAction(Agency $agency, Bid $bid, Mandate $mandate)
    {
        if ($agency->getId() !== $bid->getAgency()->getId()) {
            throw $this->createNotFoundException();
        }
        if ($bid->getId() !== $mandate->getBid()->getId()) {
            throw $this->createNotFoundException();
        }
        if (null === $mandate->getPath()) {
            throw $this->createNotFoundException();
        }

        return new BinaryFileResponse($mandate->getPath(), 200, [
            'Content-Type' => 'application/force-download',
            'Cache-Control' => 'no-cache, private',
            'Content-Disposition' => sprintf('attachment; filename="%s"', $mandate->getFilename()),
            'Content-Transfer-Encoding' => 'binary',
            'Content-Length' => $mandate->getSize(),
        ]);
    }

    /**
     * Index
     *
     * @param Agency $agency Agency
     * @param Bid    $bid    Bid
     *
     * @return array
     * @Route("/{page}", requirements={"page":"\d+"}, defaults={"page":1})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Agency $agency, Bid $bid)
    {
        // Find filtered results
        $paginatorHelper = $this->get('eb_paginator_helper');
        $paginator = $this->get('doctrine.orm.default_entity_manager')->getRepository(Mandate::class)->getPaginator($paginatorHelper, ['bid' => $bid]);

        return [
            'bid' => $bid,
            'agency' => $agency,
            'paginator' => $paginator,
        ];
    }
}
