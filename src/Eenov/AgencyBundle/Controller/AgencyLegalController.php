<?php

namespace Eenov\AgencyBundle\Controller;

use Eenov\DefaultBundle\Entity\Agency;
use Eenov\DefaultBundle\Entity\Legal;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

/**
 * Class AgencyLegalController
 *
*
 * @Route("/{agency}/documents", requirements={"agency":"\d+"})
 */
class AgencyLegalController extends AbstractAgencyController
{
    /**
     * Index
     *
     * @param Agency $agency Agency
     *
     * @return array
     * @Route("/{page}", requirements={"page":"\d+"}, defaults={"page":1})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Agency $agency)
    {
        // Find filtered results
        $paginatorHelper = $this->get('eb_paginator_helper');
        $paginator = $this->get('doctrine.orm.default_entity_manager')->getRepository(Legal::class)->getPaginator($paginatorHelper, ['agency' => $agency]);

        return [
            'agency' => $agency,
            'paginator' => $paginator,
        ];
    }

    /**
     * File
     *
     * @param Agency $agency Agency
     * @param Legal  $legal
     *
     * @return BinaryFileResponse
     * @Route("/{legal}.{_format}", requirements={"legal":"\d+"})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @ParamConverter("legal", class="EenovDefaultBundle:Legal")
     * @Method("GET")
     */
    public function fileAction(Agency $agency, Legal $legal)
    {
        if ($legal->getAgency()->getId() !== $agency->getId()) {
            throw $this->createNotFoundException();
        }

        return new BinaryFileResponse($legal->getPath(), 200, [
            'Content-Type' => 'application/force-download',
            'Cache-Control' => 'no-cache, private',
            'Content-Disposition' => sprintf('attachment; filename="%s"', $legal->getFilename()),
            'Content-Transfer-Encoding' => 'binary',
            'Content-Length' => $legal->getSize(),
        ]);
    }
}
