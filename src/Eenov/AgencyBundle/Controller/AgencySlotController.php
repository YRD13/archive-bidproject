<?php

namespace Eenov\AgencyBundle\Controller;

use Eenov\DefaultBundle\Entity\Agency;
use Eenov\DefaultBundle\Entity\MoneyIn;
use Eenov\DefaultBundle\Entity\Slot;
use Eenov\DefaultBundle\Entity\SubscriptionPack;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class AgencySlotController
 *
*
 * @Route("/{agency}/credits", requirements={"agency":"\d+"})
 */
class AgencySlotController extends AbstractAgencyController
{
    /**
     * Index
     *
     * @param Agency $agency Agency
     *
     * @return array
     * @Route("/{page}", requirements={"page":"\d+"}, defaults={"page":1})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Agency $agency)
    {

        return $this->redirectToRoute('eenov_agency_agencybill_index', [
            'agency' => $agency->getId(),
        ]);
        
        $em = $this->get('doctrine.orm.default_entity_manager');

        // Find filtered results
        $paginatorHelper = $this->get('eb_paginator_helper');
        $paginator = $em->getRepository(Slot::class)->getPaginator($paginatorHelper, [
            'agencyOrMother' => $agency,
            'bid' => null,
            'isEnded' => false,
        ]);

        // Current subscription
        $slot = $this->get('eenov.default_bundle.advert.slot_dealer')->findAvailableSlotFor($agency);

        return [
            'agency' => $agency,
            'paginator' => $paginator,
            'slot' => $slot,
            'packs' => SubscriptionPack::getPacks(),
        ];
    }

    /**
     * Go
     *
     * @param Request $request Request
     * @param Agency  $agency  Agency
     * @param string  $id      Pack ID
     *
     * @return RedirectResponse
     * @Route("/{id}/subscribe", requirements={"id":"[a-z]+"})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @Method("GET|POST")
     * @Template()
     */
    public function goAction(Request $request, Agency $agency, $id)
    {
        $packs = SubscriptionPack::getPacks();
        if (!isset($packs[$id])) {
            throw $this->createNotFoundException();
        }
        $pack = $packs[$id];
        $user = $this->getUser();

        $form = $this->createForm('eenov_agency_pack');
        if ($form->handleRequest($request)->isValid()) {
            $code = $form->get('code')->getData();

            $client = $this->get('eenov.default_bundle.lemon_way.client');
            
            if (!$client->getOrCreateWallet($user)) {
                return $this->render('EenovAgencyBundle:AgencySlot:noWallet.html.twig', [
                    'agency' => $agency,
                ]);
            }

            $moneyIn = new MoneyIn();
            $moneyIn
                ->setUser($user)
                ->setPack($id)
                ->setCode($code)
                ->setSlotCount($pack['slots'])
                ->setPricePerSlot($pack['pricePerSlot'])
                ->setSlotDurationInDays($pack['duration']);

            $em = $this->getDoctrine()->getManager();
            $em->persist($moneyIn);
            $em->flush();

            $urlParameters = [
                'agency' => $agency->getId(),
                'id' => $id,
            ];

            $returnUrl = $this->generateUrl('eenov_agency_agencyslot_return', $urlParameters, UrlGeneratorInterface::ABSOLUTE_URL);
            $cancelUrl = $this->generateUrl('eenov_agency_agencyslot_cancel', $urlParameters, UrlGeneratorInterface::ABSOLUTE_URL);
            $errorUrl = $this->generateUrl('eenov_agency_agencyslot_error', $urlParameters, UrlGeneratorInterface::ABSOLUTE_URL);
            $cssUrl = null;

            if (null === $url = $client->prepareMoneyIn($moneyIn, $returnUrl, $cancelUrl, $errorUrl, $cssUrl)) {
                return $this->render('EenovAgencyBundle:AgencySlot:noMoneyIn.html.twig', [
                    'agency' => $agency,
                ]);
            }

            return new RedirectResponse($url);
        }

        return [
            'agency' => $agency,
            'pack' => $pack,
            'form' => $form->createView(),
        ];
    }

    /**
     * Return (from lemon way)
     *
     * @param Request $request Request
     * @param int     $agency  Agency ID
     * @param string  $id      Pack ID
     *
     * @return RedirectResponse
     * @Route("/{id}/subscribe-return-lemon-way", requirements={"id":"[a-z]+"})
     * @Method("GET|POST")
     */
    public function returnAction(Request $request, $agency, $id)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var Agency $agency */
        if (null === $agency = $em->getRepository(Agency::class)->find($agency)) {
            throw $this->createNotFoundException('L\'agence demandée n\'existe pas');
        }

        if (null === $moneyIn = $this->findAndEnsureMoneyInArgument($request, $id, $request->isMethod('GET'))) {
            throw $this->createNotFoundException('Aucun MoneyIn valide trouvé');
        }

        $packs = SubscriptionPack::getPacks();
        if (!isset($packs[$id])) {
            throw $this->createNotFoundException('La pack demandé n\'existe pas');
        }

        $pack = $packs[$id];

        if ($request->isMethod('POST')) {
            $this->get('eenov.default_bundle.lemon_way.client')->validateAndFill(
                $moneyIn,
                $agency,
                $pack['duration'],
                $request->request->getInt('response_transactionId'),
                (float)$request->request->get('response_transactionAmount'),
                $request->request->get('response_transactionMessage')
            );

            // Create a bill
            $this->get('eenov.default_bundle.pdf.bill')->generateFromMoneyIn($agency, $moneyIn);

            return new Response();
        }

        $this->addFlash('success', 'Votre souscription a été validée.');

        return $this->redirectToRoute('eenov_agency_agency_read', [
            'agency' => $agency->getId(),
        ]);
    }

    /**
     * Cancel (from lemon way)
     *
     * @param Request $request Request
     * @param int     $agency  Agency ID
     * @param string  $id      Pack ID
     *
     * @return RedirectResponse
     * @Route("/{id}/subscribe-cancel-lemon-way", requirements={"id":"[a-z]+"})
     * @Method("GET|POST")
     */
    public function cancelAction(Request $request, $agency, $id)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var Agency $agency */
        if (null === $agency = $em->getRepository(Agency::class)->find($agency)) {
            throw $this->createNotFoundException();
        }
        if (null === $moneyIn = $this->findAndEnsureMoneyInArgument($request, $id, $request->isMethod('GET'))) {
            throw $this->createNotFoundException();
        }

        if ($request->isMethod('POST')) {
            $moneyIn->setCanceled(new \DateTime());
            $em->flush();

            return new Response();
        }

        $this->addFlash('warning', 'Vous avez annulé votre demande de souscription.');

        return $this->redirectToRoute('eenov_agency_agency_read', [
            'agency' => $agency->getId(),
        ]);
    }

    /**
     * Error (from lemon way)
     *
     * @param Request $request Request
     * @param int     $agency  Agency ID
     * @param string  $id      Pack ID
     *
     * @return RedirectResponse
     * @Route("/{id}/subscribe-error-lemon-way", requirements={"pack":"[a-z]+"})
     * @Method("GET|POST")
     */
    public function errorAction(Request $request, $agency, $id)
    {
        $em = $this->getDoctrine()->getManager();

        if (null === $agency = $em->getRepository(Agency::class)->find($agency)) {
            throw $this->createNotFoundException();
        }
        if (null === $moneyIn = $this->findAndEnsureMoneyInArgument($request, $id, $request->isMethod('GET'))) {
            throw $this->createNotFoundException();
        }

        if ($request->isMethod('POST')) {
            $moneyIn->setErrored(new \DateTime());
            $em->flush();

            return new Response();
        }

        $this->addFlash('danger', 'Une erreur s\'est produite. N\'hésitez pas à contacter le support si le problème persiste.');

        return $this->redirectToRoute('eenov_agency_agency_read', [
            'agency' => $agency->getId(),
        ]);
    }

    /**
     * Find And Ensure Money In Argument
     *
     * @param Request $request   Request
     * @param string  $packId    Pack ID
     * @param bool    $testToken Test Token
     *
     * @return null|MoneyIn
     */
    private function findAndEnsureMoneyInArgument(Request $request, $packId = null, $testToken = false)
    {
        if (0 === $moneyInId = $request->query->getInt('response_wkToken')) {
            if (0 === $moneyInId = $request->request->getInt('response_wkToken')) {
                return null;
            }
        }

        $em = $this->getDoctrine()->getManager();
        if (null === $moneyIn = $em->getRepository(MoneyIn::class)->find($moneyInId)) {
            return null;
        }

        if (null !== $packId) {
            if ($packId !== $moneyIn->getPack()) {
                return null;
            }
        }

        if ($testToken) {
            if (null !== $user = $this->getUser()) {
                if ($moneyIn->getUser()->getId() !== $user->getId()) {
                    return null;
                }
            }
        }

        return $moneyIn;
    }
}
