<?php

namespace Eenov\AgencyBundle\Controller;

use Eenov\DefaultBundle\Entity\Agency;
use Eenov\DefaultBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AgencyOwnerController
 *
*
 * @Route("/proprietaire")
 */
class AgencyOwnerController extends AbstractAgencyController
{
    /**
     * Index
     *
     * @return array
     * @Route("/{page}", requirements={"page":"\d+"}, defaults={"page":1})
     * @Method("GET")
     * @Security("is_granted('ROLE_COMMERCIAL')")
     * @Template()
     */
    public function indexAction()
    {
        $ph = $this->get('eb_paginator_helper');
        $paginator = $this->get('doctrine.orm.default_entity_manager')->getRepository(User::class)->getPaginator($ph, [
            'role' => User::ROLE_AGENCY_OWNER,
        ]);

        return [
            'paginator' => $paginator,
        ];
    }

    /**
     * Create
     *
     * @param Request $request
     *
     * @return array
     * @Route("/create")
     * @Method("GET|POST")
     * @Security("is_granted('ROLE_COMMERCIAL')")
     * @Template()
     */
    public function createAction(Request $request)
    {
        $user = new User(User::ROLE_AGENCY_OWNER);
        $form = $this->createForm('eenov_agency_user', $user);
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('eenov_agency_agencyowner_createagency', [
                'user' => $user->getId(),
            ]);
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * Create
     *
     * @param Request $request Request
     * @param User    $user    Owner
     *
     * @return array
     * @Route("/create/{user}", requirements={"user":"\d+"})
     * @ParamConverter("user", class="EenovDefaultBundle:User")
     * @Method("GET|POST")
     * @Security("is_granted('ROLE_COMMERCIAL')")
     * @Template()
     */
    public function createAgencyAction(Request $request, User $user)
    {
        if (User::ROLE_AGENCY_OWNER !== $user->getRole()) {
            throw $this->createNotFoundException();
        }

        $agency = new Agency();
        $agency
            ->setOwner($user)
            ->setCommercial($this->getUser());
        $form = $this->createForm('eenov_agency_agency', $agency);
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->persist($agency);
            $em->flush();

            return $this->redirectToRoute('eenov_agency_agencyowner_index');
        }

        return [
            'user' => $user,
            'form' => $form->createView(),
        ];
    }
}
