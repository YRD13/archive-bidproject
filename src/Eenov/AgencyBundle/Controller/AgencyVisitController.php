<?php

namespace Eenov\AgencyBundle\Controller;

use Eenov\DefaultBundle\Entity\Agency;
use Eenov\DefaultBundle\Entity\Bid;
use Eenov\DefaultBundle\Entity\Visit;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AgencyVisitController
 *
*
 * @Route("/{agency}/annonces/{bid}/gerer-les-visites", requirements={"agency":"\d+","bid":"\d+"})
 */
class AgencyVisitController extends AbstractAgencyController
{
    /**
     * API
     *
     * @param Request $request Request
     * @param Agency  $agency  Agency
     * @param Bid     $bid     Bid
     *
     * @return JsonResponse
     * @Route("/api.{_format}", requirements={"_format":"json"})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @Method("GET")
     */
    public function apiAction(Request $request, Agency $agency, Bid $bid)
    {
        if ($agency->getId() !== $bid->getAgency()->getId()) {
            throw $this->createNotFoundException();
        }

        $start = \DateTime::createFromFormat('Y-m-d', $request->query->get('start'));
        $end = \DateTime::createFromFormat('Y-m-d', $request->query->get('end'));

        $visits = $this->get('doctrine.orm.default_entity_manager')->getRepository(Visit::class)->findVisitsBetween($start, $end, $bid);

        $events = array_map(function (Visit $visit) {
            return $this->get('eenov.default_bundle.calendar.bid_calendar')->visitToEvent($visit);
        }, $visits);

        $events[] = $this->get('eenov.default_bundle.calendar.bid_calendar')->bidToEvent($bid);

        return new JsonResponse($events);
    }

    /**
     * Delete
     *
     * @param Agency $agency Agency
     * @param Bid    $bid    Bid
     * @param Visit  $visit  Visit
     *
     * @return array|RedirectResponse
     * @Route("/{visit}/supprimer", requirements={"visit":"\d+"})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @ParamConverter("visit", class="EenovDefaultBundle:Visit")
     * @Method("GET")
     */
    public function deleteAction(Agency $agency, Bid $bid, Visit $visit)
    {
        $em = $this->get('doctrine.orm.default_entity_manager');
        $em->remove($visit);
        $em->flush();

        return $this->redirectToRoute('eenov_agency_agencyvisit_index', [
            'agency' => $agency->getId(),
            'bid' => $bid->getId(),
        ]);
    }

    /**
     * Index
     *
     * @param Request $request Request
     * @param Agency  $agency  Agency
     * @param Bid     $bid     Bid
     *
     * @return array
     * @Route("/{page}", requirements={"page":"\d+"}, defaults={"page":1})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @Method("GET|POST")
     * @Template()
     */
    public function indexAction(Request $request, Agency $agency, Bid $bid)
    {
        $form = $this->createForm('eenov_agency_visits', $bid);
        if ($form->handleRequest($request)->isSubmitted() && $form->isValid()) {
            $this->get('doctrine.orm.default_entity_manager')->flush();

            $this->get('eenov.default_bundle.session.session')->success('Visites programmées');

            return $this->redirectToRoute('eenov_agency_agencyvisit_index', [
                'agency' => $agency->getId(),
                'bid' => $bid->getId(),
            ]);
        }

        return [
            'form' => $form->createView(),
            'agency' => $agency,
            'bid' => $bid,
        ];
    }
}
