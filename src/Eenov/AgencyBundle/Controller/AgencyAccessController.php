<?php

namespace Eenov\AgencyBundle\Controller;

use Eenov\DefaultBundle\Entity\Access;
use Eenov\DefaultBundle\Entity\Agency;
use Eenov\DefaultBundle\Entity\Bid;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AgencyAccessController
 *
*
 * @Route("/{agency}/annonces/{bid}/gerer-les-acces", requirements={"agency":"\d+","bid":"\d+"})
 */
class AgencyAccessController extends AbstractAgencyController
{
    /**
     * Delete
     *
     * @param Agency $agency Agency
     * @param Bid    $bid    Bid
     * @param Access $access Access
     *
     * @return array|RedirectResponse
     * @Route("/{access}/supprimer", requirements={"access":"\d+"})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @ParamConverter("access", class="EenovDefaultBundle:Access")
     * @Method("GET|POST")
     */
    public function deleteAction(Agency $agency, Bid $bid, Access $access)
    {
        if (false === $access->getReadonly()) {
            $this->get('eenov.default_bundle.session.session')->danger('Vous ne pouvez pas supprimer les accès des clients');
        } else {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->remove($access);
            $em->flush();

            $this->get('eenov.default_bundle.session.session')->success('Accès supprimé');
        }

        return $this->redirectToRoute('eenov_agency_agencyaccess_index', [
            'agency' => $agency->getId(),
            'bid' => $bid->getId(),
        ]);
    }

    /**
     * Index
     *
     * @param Request $request Request
     * @param Agency  $agency  Agency
     * @param Bid     $bid     Bid
     *
     * @return array
     * @Route("/{page}", requirements={"page":"\d+"}, defaults={"page":1})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @Method("GET|POST")
     * @Template()
     */
    public function indexAction(Request $request, Agency $agency, Bid $bid)
    {
        // Find filtered results
        $paginatorHelper = $this->get('eb_paginator_helper');
        $paginator = $this->get('doctrine.orm.default_entity_manager')->getRepository(Access::class)->getPaginator($paginatorHelper, ['bid' => $bid]);

        $access = new Access();
        $access
            ->setBid($bid)
            ->setReadonly(true);
        $form = $this->createForm('eenov_agency_access', $access);
        if ($form->handleRequest($request)->isSubmitted() && $form->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->persist($access);
            $em->flush();

            $this->get('eenov.default_bundle.session.session')->success('Accès ajouté');

            return $this->redirectToRoute('eenov_agency_agencyaccess_index', [
                'agency' => $agency->getId(),
                'bid' => $bid->getId(),
            ]);
        }

        return [
            'form' => $form->createView(),
            'bid' => $bid,
            'agency' => $agency,
            'paginator' => $paginator,
        ];
    }

    /**
     * Update
     *
     * @param Request $request Request
     * @param Agency  $agency  Agency
     * @param Bid     $bid     Bid
     * @param Access  $access  Access
     *
     * @return array|RedirectResponse
     * @Route("/{access}/modifier", requirements={"access":"\d+"})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @ParamConverter("access", class="EenovDefaultBundle:Access")
     * @Method("GET|POST")
     * @Template()
     */
    public function updateAction(Request $request, Agency $agency, Bid $bid, Access $access)
    {
        if ($access->getReadonly()) {
            $this->get('eenov.default_bundle.session.session')->danger('Vous ne pouvez pas revoir les accès des utilisateurs en lecture seule');

            return $this->redirectToRoute('eenov_agency_agencyaccess_index', [
                'agency' => $agency->getId(),
                'bid' => $bid->getId(),
            ]);
        }

        $form = $this->createForm('eenov_agency_access_validate', $access);
        if ($form->handleRequest($request)->isValid()) {
            $this->get('doctrine.orm.default_entity_manager')->flush();

            return $this->redirectToRoute('eenov_agency_agencyaccess_index', [
                'agency' => $agency->getId(),
                'bid' => $bid->getId(),
            ]);
        }

        return [
            'agency' => $agency,
            'bid' => $bid,
            'access' => $access,
            'form' => $form->createView(),
        ];
    }
}
