<?php

namespace Eenov\AgencyBundle\Controller;

use Eenov\DefaultBundle\Entity\Agency;
use Eenov\DefaultBundle\Entity\Bid;
use Eenov\DefaultBundle\Entity\Image;
use Eenov\DefaultBundle\Entity\ImageType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AgencyImageController
 *
*
 * @Route("/{agency}/annonces/{bid}/modifier-photos", requirements={"agency":"\d+","bid":"\d+"})
 */
class AgencyImageController extends AbstractAgencyController
{
    /**
     * Default
     *
     * @param Agency $agency Agency
     * @param Bid    $bid    Bid
     * @param Image  $image  Image
     *
     * @return JsonResponse
     * @Route("/{image}/modifier-previsualisation")
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @ParamConverter("image", class="EenovDefaultBundle:Image")
     */
    public function defaultAction(Agency $agency, Bid $bid, Image $image)
    {
        $bid->getDraft()->setFile($this->get('eenov.default_bundle.advert.advert_cloner')->duplicateFile($image));
        $this->get('doctrine.orm.default_entity_manager')->flush();

        return $this->redirectToRoute('eenov_agency_agencyimage_index', [
            'agency' => $agency->getId(),
            'bid' => $bid->getId(),
        ]);
    }

    /**
     * Delete
     *
     * @param Agency $agency Agency
     * @param Bid    $bid    Bid
     * @param Image  $image  Image
     *
     * @return RedirectResponse
     * @Route("/{image}/supprimer")
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @ParamConverter("image", class="EenovDefaultBundle:Image")
     */
    public function deleteAction(Agency $agency, Bid $bid, Image $image)
    {
        $em = $this->get('doctrine.orm.default_entity_manager');
        $em->remove($image);
        $em->flush();

        return $this->redirectToRoute('eenov_agency_agencyimage_index', [
            'agency' => $agency->getId(),
            'bid' => $bid->getId(),
        ]);
    }

    /**
     * Index
     *
     * @param Agency $agency Agency
     * @param Bid    $bid
     *
     * @return array
     * @Route()
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Agency $agency, Bid $bid)
    {
        $images = array_map(function (Image $image) use ($agency, $bid) {
            return $this->imageToArray($agency, $bid, $image);
        }, $bid->getDraft()->getImages()->toArray());

        return [
            'agency' => $agency,
            'bid' => $bid,
            'images' => $images,
            'imagesTypes' => $this->get('doctrine.orm.default_entity_manager')->getRepository(ImageType::class)->findAll(),
        ];
    }

    /**
     * Upload
     *
     * @param Request $request Request
     * @param Agency  $agency  Agency
     * @param Bid     $bid     Bid
     *
     * @return JsonResponse
     * @Route("/televerser")
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     */
    public function uploadAction(Request $request, Agency $agency, Bid $bid)
    {
        $data = [];
        $em = $this->get('doctrine.orm.default_entity_manager');
        if (null !== $type = $em->getRepository(ImageType::class)->find($request->request->getInt('type'))) {
            foreach ($request->files as $file) {
                $image = new Image();
                $image
                    ->setAdvert($bid->getDraft())
                    ->setFile($file)
                    ->setType($type);

                if (0 === $this->get('validator')->validate($image)->count()) {
                    $em->persist($image);
                    $em->flush();

                    $data[] = $this->imageToArray($agency, $bid, $image);
                }
            }
        }

        return new JsonResponse(['files' => $data, 'errors' => []]);
    }

    /**
     * imageToArray
     *
     * @param Agency $agency Agency
     * @param Bid    $bid    Bid
     * @param Image  $image  Image
     *
     * @return array
     */
    private function imageToArray(Agency $agency, Bid $bid, Image $image)
    {
        return [
            'id' => $image->getId(),
            'type' => (string)$image->getType(),
            'path' => $image->getUri(),
            'delete_uri' => $this->generateUrl('eenov_agency_agencyimage_delete', [
                'agency' => $agency->getId(),
                'bid' => $bid->getId(),
                'image' => $image->getId(),
            ]),
            'default_uri' => $this->generateUrl('eenov_agency_agencyimage_default', [
                'agency' => $agency->getId(),
                'bid' => $bid->getId(),
                'image' => $image->getId(),
            ]),
            'crop_uri' => (string)$this->get('liip_imagine.cache.manager')->generateUrl($image->getUri(), 'bid_default_view'),
            'isThumbnail' => false,
        ];
    }
}
