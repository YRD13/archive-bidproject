<?php

namespace Eenov\AgencyBundle\Controller;

use Eenov\DefaultBundle\Entity\Agency;
use Eenov\DefaultBundle\Entity\Subscription;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class AgencySubscriptionController
 *
*
 * @Route("/{agency}/abonnements", requirements={"agency":"\d+"})
 */
class AgencySubscriptionController extends AbstractAgencyController
{
    /**
     * Index
     *
     * @param Agency $agency Agency
     *
     * @return array
     * @Route("/{page}", requirements={"page":"\d+"}, defaults={"page":1})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Agency $agency)
    {
        $em = $this->get('doctrine.orm.default_entity_manager');

        // Find filtered results
        $paginatorHelper = $this->get('eb_paginator_helper');
        $paginator = $em->getRepository(Subscription::class)->getPaginator($paginatorHelper, ['agency' => $agency]);

        // Current subscription
        $slot = $this->get('eenov.default_bundle.advert.slot_dealer')->findAvailableSlotFor($agency);

        return [
            'agency' => $agency,
            'paginator' => $paginator,
            'slot' => $slot,
        ];
    }
}
