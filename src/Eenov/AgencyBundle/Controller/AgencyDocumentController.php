<?php

namespace Eenov\AgencyBundle\Controller;

use Eenov\DefaultBundle\Entity\Agency;
use Eenov\DefaultBundle\Entity\Bid;
use Eenov\DefaultBundle\Entity\Document;
use Eenov\DefaultBundle\Entity\DocumentType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AgencyDocumentController
 *
*
 * @Route("/{agency}/annonces/{bid}/modifier-documents", requirements={"agency":"\d+","bid":"\d+"})
 */
class AgencyDocumentController extends AbstractAgencyController
{
    /**
     * Index
     *
     * @param Agency $agency Agency
     * @param Bid    $bid
     *
     * @return array
     * @Route()
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Agency $agency, Bid $bid)
    {
        $documents = array_map(function (Document $document) use ($agency, $bid) {
            return $this->documentToArray($agency, $bid, $document);
        }, $bid->getDraft()->getDocuments()->toArray());

        return [
            'agency' => $agency,
            'bid' => $bid,
            'documents' => $documents,
            'types' => $this->get('doctrine.orm.default_entity_manager')->getRepository(DocumentType::class)->getAllTypesOrdered(),
        ];
    }

    /**
     * Zip
     *
     * @param Agency $agency Agency
     * @param Bid    $bid    Bid
     *
     * @return array
     * @Route(".{_format}", requirements={"_format":"zip"})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @Method("GET")
     */
    public function zipAction(Agency $agency, Bid $bid)
    {
        $path = sys_get_temp_dir() . '/' . uniqid() . '.zip';

        $zip = new \ZipArchive();
        $zip->open($path, \ZipArchive::CREATE);
        foreach ($bid->getDraft()->getDocuments() as $document) {
            $name = sprintf(
                '%s (%u).%s',
                $this->get('eb_string')->removeAccents($document->getType()->getName()),
                $document->getId(),
                $document->getExtension()
            );

            $zip->addFile($document->getPath(), $name);
        }
        $zip->close();

        $content = file_get_contents($path);
        $this->get('filesystem')->remove($path);

        return new Response($content, 200, [
            'Content-Type' => 'application/zip',
            'Cache-Control' => 'no-cache, private',
            'Content-Disposition' => sprintf('attachment; filename="documents-annonce-%u-%s.zip"', $bid->getId(), date('Ymd-His')),
        ]);
    }

    /**
     * Delete
     *
     * @param Agency   $agency   Agency
     * @param Bid      $bid      Bid
     * @param Document $document Document
     *
     * @return JsonResponse
     * @Route("/{document}/supprimer")
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @ParamConverter("document", class="EenovDefaultBundle:Document")
     */
    public function deleteAction(Agency $agency, Bid $bid, Document $document)
    {
        $em = $this->get('doctrine.orm.default_entity_manager');
        $em->remove($document);
        $em->flush();

        return $this->redirectToRoute('eenov_agency_agencydocument_index', [
            'agency' => $agency->getId(),
            'bid' => $bid->getId(),
        ]);
    }

    /**
     * Upload
     *
     * @param Request $request Request
     * @param Agency  $agency  Agency
     * @param Bid     $bid     Bid
     *
     * @return JsonResponse
     * @Route("/televerser")
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     */
    public function uploadAction(Request $request, Agency $agency, Bid $bid)
    {
        $data = [];
        $em = $this->get('doctrine.orm.default_entity_manager');
        if (null !== $type = $em->getRepository(DocumentType::class)->find($request->request->getInt('type'))) {
            foreach ($request->files as $file) {
                $document = new Document();
                $document
                    ->setAdvert($bid->getDraft())
                    ->setFile($file)
                    ->setType($type);

                if (0 === $this->get('validator')->validate($document)->count()) {
                    $em->persist($document);
                    $em->flush();

                    $data[] = $this->documentToArray($agency, $bid, $document);
                }
            }
        }

        return new JsonResponse(['files' => $data, 'errors' => []]);
    }

    /**
     * documentToArray
     *
     * @param Agency   $agency   Agency
     * @param Bid      $bid      Bid
     * @param Document $document Document
     *
     * @return array
     */
    private function documentToArray(Agency $agency, Bid $bid, Document $document)
    {
        return [
            'id' => $document->getId(),
            'type' => (string)$document->getType(),
            'path' => $this->generateUrl('eenov_default_document_download', [
                'document' => $document->getId(),
                '_format' => $document->getExtension(),
            ]),
            'delete_uri' => $this->generateUrl('eenov_agency_agencydocument_delete', [
                'agency' => $agency->getId(),
                'bid' => $bid->getId(),
                'document' => $document->getId(),
            ]),
            'isThumbnail' => false,
        ];
    }
}
