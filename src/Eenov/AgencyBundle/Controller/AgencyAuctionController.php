<?php

namespace Eenov\AgencyBundle\Controller;

use Eenov\DefaultBundle\Entity\Agency;
use Eenov\DefaultBundle\Entity\Auction;
use Eenov\DefaultBundle\Entity\Bid;
use Eenov\DefaultBundle\Entity\Mandate;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AgencyAuctionController
 *
*
 * @Route("/{agency}/annonces/{bid}/gerer-les-encheres", requirements={"agency":"\d+","bid":"\d+"})
 */
class AgencyAuctionController extends AbstractAgencyController
{
    /**
     * Index
     *
     * @param Agency $agency Agency
     * @param Bid    $bid    Bid
     *
     * @return array
     * @Route("/{page}", requirements={"page":"\d+"}, defaults={"page":1})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Agency $agency, Bid $bid)
    {
        // Find filtered results
        $paginatorHelper = $this->get('eb_paginator_helper');
        $paginator = $this->get('doctrine.orm.default_entity_manager')->getRepository(Auction::class)->getPaginator($paginatorHelper, ['bid' => $bid]);

        return [
            'bid' => $bid,
            'agency' => $agency,
            'paginator' => $paginator,
        ];
    }

    /**
     * Pdf
     *
     * @param Agency $agency Agency
     * @param Bid    $bid    Bid
     *
     * @return array
     * @Route("/resultats.{_format}", requirements={"_format":"pdf"})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid")
     * @Method("GET")
     */
    public function pdfAction(Agency $agency, Bid $bid)
    {
        $em = $this->get('doctrine.orm.default_entity_manager');
        $auctions = $em->getRepository(Auction::class)->findFinalAuctions($bid);
        $mandates = $em->getRepository(Mandate::class)->findBy(['bid' => $bid], []);

        $mandatesByUser = [];
        foreach ($mandates as $mandate) {
            if (null !== $access = $mandate->getAccess()) {
                $mandatesByUser[$access->getUser()->getId()] = $mandate;
            }
        }

        $validated = clone $bid->getSellEnded();
        $validated->add(new \DateInterval('P15D'));

        $content = $this->renderView('EenovAgencyBundle::_offer_content.pdf.twig', [
            'css' => file_get_contents(__DIR__ . '/../../DefaultBundle/Resources/public/css/bootstrap.min.css'),
            'dot' => base64_encode(file_get_contents(__DIR__ . '/../../DefaultBundle/Resources/public/img/dot_20.png')),
            'auctions' => $auctions,
            'agency' => $agency,
            'bid' => $bid,
            'mandates' => $mandatesByUser,
            'validated' => $validated,
        ]);

        return new Response($this->get('knp_snappy.pdf')->getOutputFromHtml($content), 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'filename="facture.pdf"',
        ]);
    }
}
