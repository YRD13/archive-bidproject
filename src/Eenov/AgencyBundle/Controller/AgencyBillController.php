<?php

namespace Eenov\AgencyBundle\Controller;

use Eenov\DefaultBundle\Entity\Agency;
use Eenov\DefaultBundle\Entity\Bill;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

/**
 * Class AgencyBillController
 *
*
 * @Route("/{agency}/factures", requirements={"agency":"\d+"})
 */
class AgencyBillController extends AbstractAgencyController
{
    /**
     * Index
     *
     * @param Agency $agency Agency
     *
     * @return array
     * @Route("/{page}", requirements={"page":"\d+"}, defaults={"page":1})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Agency $agency)
    {
        // Find filtered results
        $paginatorHelper = $this->get('eb_paginator_helper');
        $paginator = $this->get('doctrine.orm.default_entity_manager')->getRepository(Bill::class)->getPaginator($paginatorHelper, ['agency' => $agency]);

        return [
            'agency' => $agency,
            'paginator' => $paginator,
        ];
    }

    /**
     * File
     *
     * @param Agency $agency Agency
     * @param Bill   $bill
     *
     * @return BinaryFileResponse
     * @Route("/{bill}.{_format}", requirements={"bill":"\d+"})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @ParamConverter("bill", class="EenovDefaultBundle:Bill")
     * @Method("GET")
     */
    public function fileAction(Agency $agency, Bill $bill)
    {
        if ($bill->getAgency()->getId() !== $agency->getId()) {
            throw $this->createNotFoundException();
        }

        return new BinaryFileResponse($bill->getPath(), 200, [
            'Content-Type' => 'application/force-download',
            'Cache-Control' => 'no-cache, private',
            'Content-Disposition' => sprintf('attachment; filename="%s"', $bill->getFilename()),
            'Content-Transfer-Encoding' => 'binary',
            'Content-Length' => $bill->getSize(),
        ]);
    }
}
