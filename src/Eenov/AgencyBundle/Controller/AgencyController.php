<?php

namespace Eenov\AgencyBundle\Controller;

use Eenov\DefaultBundle\Entity\Agency;
use Eenov\DefaultBundle\Entity\Bid;
use Eenov\DefaultBundle\Entity\User;
use Eenov\DefaultBundle\Entity\Visit;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\SubmitButton;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AgencyController
 *
*
 */
class AgencyController extends AbstractAgencyController
{
    /**
     * Index
     *
     * @return RedirectResponse
     * @Route()
     * @Method("GET")
     */
    public function indexAction()
    {
        if ($this->isGranted(User::ROLE_COMMERCIAL)) {
            return $this->redirectToRoute('eenov_agency_agencyowner_index');
        }

        if (null === $agency = $this->getUser()->getAgency()) {
            $this->get('eenov.default_bundle.session.session')->danger('Impossible de vous connecter, aucune agence ne vous a été rendue accessible actuellement.');

            return $this->redirect($this->generateUrl('eenov_default_default_index'));
        }

        return $this->redirectToRoute('eenov_agency_agency_read', [
            'agency' => $agency->getId(),
        ]);
    }

    /**
     * API
     *
     * @param Request $request Request
     * @param Agency  $agency  Agency
     *
     * @return JsonResponse
     * @Route("/{agency}/api.{_format}", requirements={"agency":"\d+","_format":"json"})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @Method("GET")
     */
    public function apiAction(Request $request, Agency $agency)
    {
        $start = \DateTime::createFromFormat('Y-m-d', $request->query->get('start'));
        $end = \DateTime::createFromFormat('Y-m-d', $request->query->get('end'));

        $events = [];

        $bids = $this->get('doctrine.orm.default_entity_manager')->getRepository(Bid::class)->findBidsBetween($start, $end, null, $agency);
        $events = array_merge($events, array_map(function (Bid $bid) {
            return $this->get('eenov.default_bundle.calendar.bid_calendar')->bidToEvent($bid);
        }, $bids));

        $visits = $this->get('doctrine.orm.default_entity_manager')->getRepository(Visit::class)->findVisitsBetween($start, $end, null, $agency);
        $events = array_merge($events, array_map(function (Visit $visit) {
            return $this->get('eenov.default_bundle.calendar.bid_calendar')->visitToEvent($visit);
        }, $visits));

        return new JsonResponse($events);
    }

    /**
     * Responsable
     *
     * @param Request $request Request
     * @param Agency  $agency  Agency
     *
     * @return array
     * @Route("/{agency}/responsable", requirements={"agency":"\d+"})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @Method("GET|POST")
     * @Template()
     */
    public function responsibleAction(Request $request, Agency $agency)
    {
        if (null === $user = $agency->getResponsible()) {
            $user = new User(User::ROLE_AGENCY_RESPONSIBLE);
            $user
                ->setAgencyManaged($agency);
            $form = $this->createForm('eenov_agency_user', $user);
            if ($form->handleRequest($request)->isValid()) {
                $em = $this->get('doctrine.orm.default_entity_manager');
                $em->persist($user);
                $em->flush();

                $this->get('eenov.default_bundle.session.session')->success('Le compte a été créé');

                return $this->redirectToRoute('eenov_agency_agency_responsible', [
                    'agency' => $agency->getId(),
                ]);
            }
        } else {
            $form = $this->createForm('eenov_agency_user', $user);
            $form->add('delete', 'submit', ['label' => 'Supprimer ce responsable']);
            if ($form->handleRequest($request)->isValid()) {
                $em = $this->get('doctrine.orm.default_entity_manager');
                if (null !== $deleteButton = $form->get('delete')) {
                    if ($deleteButton instanceof SubmitButton) {
                        if ($deleteButton->isClicked()) {
                            $em->remove($user);
                            $agency->setContact(null);
                            $em->flush();

                            $this->get('eenov.default_bundle.session.session')->success('Le compte a été supprimé');

                            return $this->redirectToRoute('eenov_agency_agency_responsible', [
                                'agency' => $agency->getId(),
                            ]);
                        }
                    }
                }

                $em->flush();

                $this->get('eenov.default_bundle.session.session')->success('Le compte a été mis à jour');

                return $this->redirectToRoute('eenov_agency_agency_responsible', [
                    'agency' => $agency->getId(),
                ]);
            }
        }

        return [
            'agency' => $agency,
            'form' => $form->createView(),
        ];
    }

    /**
     * Contact
     *
     * @param Request $request Request
     * @param Agency  $agency  Agency
     *
     * @return array
     * @Route("/{agency}/contact", requirements={"agency":"\d+"})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @Method("GET|POST")
     * @Template()
     */
    public function contactAction(Request $request, Agency $agency)
    {
        if (null === $user = $agency->getContact()) {
            $user = new User(User::ROLE_AGENCY_CONTACT);
            $user
                ->setAgencyRepresented($agency);
            $form = $this->createForm('eenov_agency_user', $user);
            if ($form->handleRequest($request)->isValid()) {
                $em = $this->get('doctrine.orm.default_entity_manager');
                $em->persist($user);
                $em->flush();

                $this->get('eenov.default_bundle.session.session')->success('Le compte a été créé');

                return $this->redirectToRoute('eenov_agency_agency_contact', [
                    'agency' => $agency->getId(),
                ]);
            }
        } else {
            $form = $this->createForm('eenov_agency_user', $user);
            $form->add('delete', 'submit', ['label' => 'Supprimer ce contact']);
            if ($form->handleRequest($request)->isValid()) {
                $em = $this->get('doctrine.orm.default_entity_manager');
                if (null !== $deleteButton = $form->get('delete')) {
                    if ($deleteButton instanceof SubmitButton) {
                        if ($deleteButton->isClicked()) {
                            $em->remove($user);
                            $agency->setContact(null);
                            $em->flush();

                            $this->get('eenov.default_bundle.session.session')->success('Le compte a été supprimé');

                            return $this->redirectToRoute('eenov_agency_agency_contact', [
                                'agency' => $agency->getId(),
                            ]);
                        }
                    }
                }

                $em->flush();

                $this->get('eenov.default_bundle.session.session')->success('Le compte a été mis à jour');

                return $this->redirectToRoute('eenov_agency_agency_contact', [
                    'agency' => $agency->getId(),
                ]);
            }
        }

        return [
            'agency' => $agency,
            'form' => $form->createView(),
        ];
    }

    /**
     * Documents
     *
     * @param Agency $agency Agency
     *
     * @return array
     * @Route("/{agency}/documents-utiles", requirements={"agency":"\d+"})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @Method("GET")
     * @Template()
     */
    public function documentsAction(Agency $agency)
    {
        return [
            'version' => time(),
            'agency' => $agency,
        ];
    }

    /**
     * Owner
     *
     * @param Request $request Request
     * @param Agency  $agency  Agency
     *
     * @return array
     * @Route("/{agency}/proprietaire", requirements={"agency":"\d+"})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @Method("GET|POST")
     * @Template()
     */
    public function ownerAction(Request $request, Agency $agency)
    {
        $user = $agency->getOwner();
        $form = $this->createForm('eenov_agency_user', $user);
        if ($form->handleRequest($request)->isValid()) {
            $this->get('doctrine.orm.default_entity_manager')->flush();

            $this->get('eenov.default_bundle.session.session')->success('Le compte a été mis à jour');

            return $this->redirectToRoute('eenov_agency_agency_owner', [
                'agency' => $agency->getId(),
            ]);
        }

        return [
            'agency' => $agency,
            'form' => $form->createView(),
        ];
    }

    /**
     * Read
     *
     * @param Agency $agency Agency
     *
     * @return array
     * @Route("/{agency}/tableau-de-bord", requirements={"agency":"\d+"})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @Method("GET")
     * @Template()
     */
    public function readAction(Agency $agency)
    {
        return [
            'agency' => $agency,
        ];
    }

    /**
     * Update
     *
     * @param Request $request Request
     * @param Agency  $agency  Agency
     *
     * @return array
     * @Route("/{agency}/agence", requirements={"agency":"\d+"})
     * @ParamConverter("agency", class="EenovDefaultBundle:Agency")
     * @Method("GET|POST")
     * @Template()
     */
    public function updateAction(Request $request, Agency $agency)
    {
        $form = $this->createForm('eenov_agency_agency', $agency);
        if ($form->handleRequest($request)->isValid()) {
            $this->get('doctrine.orm.default_entity_manager')->flush();

            $this->get('eenov.default_bundle.session.session')->success(sprintf('Agence %s modifiée avec succès.', $agency));

            return $this->redirectToRoute('eenov_agency_agency_update', [
                'agency' => $agency->getId(),
            ]);
        }

        return [
            'agency' => $agency,
            'form' => $form->createView(),
        ];
    }

    /**
     * Agencies Selector
     *
     * @param Request $request
     *
     * @return array
     * @Template()
     */
    public function agenciesSelectorAction(Request $request)
    {
        $user = $this->getUser();

        /** @var null|Agency $agency */
        $agency = $request->attributes->get('agency');

        // Commercial
        $agencies = [];
        if ($this->isGranted(User::ROLE_COMMERCIAL)) {
            $agencies = $this->get('doctrine.orm.default_entity_manager')->getRepository(Agency::class)->getCommercialAgencies($user);
        } elseif ($this->isGranted(User::ROLE_AGENCY_OWNER)) {
            $agencies = $user->getOwnedAgencies()->toArray();
        } elseif ($this->isGranted(User::ROLE_AGENCY_RESPONSIBLE)) {
            $agencies = $this->getUser()->getResponsibleAgencies();
        }

        return [
            'agency' => $agency,
            'agencies' => $agencies,
        ];
    }
}
