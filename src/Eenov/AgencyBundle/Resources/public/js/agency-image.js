$(document).ready(function () {

    var $picture = $('#pictures[data-images]'),
        $files = $('#files'),
        $upload = $('#images-upload[data-uri]'),
        $progress = $('#progress-images').find('.progress-bar'),
        template = '<div class="col-md-3 col-xs-6 bounceInDown"><h3>__TYPE__</h3><img src="__IMAGE__" style="width:100%"/><a href="__DEFAULT__" class="btn col-xs-6 btn-success"><i class="fa fa-pin"></i> Principale</a><a href="__DELETE__" class="btn col-xs-6 btn-danger"><i class="fa fa-trash-o"></i> Supprimer</a></div>';

    // Load images
    $.each($picture.data('images'), function (k, image) {
        addImageLine(image);
    });

    // Upload link
    $picture.on('click', '#images-upload', function (e) {
        if ('' === $(this).closest('form').find('#type').val()) {
            e.preventDefault();
        }
    });

    // Create an uploader
    $upload.each(function () {
        $(this).fileupload({
            url: $(this).data('uri'),
            dataType: 'json',
            done: function (e, data) {
                if (data && data.result && data.result.files) {
                    $.each(data.result.files, function (k, file) {
                        addImageLine(file, true);
                    });
                }
                $progress.width(0)
            },
            progressall: function (e, data) {
                $progress.css('width', parseInt(data.loaded / data.total * 100, 10) + '%');
            }
        });
    });

    function addImageLine(image, animated) {
        $(template.replace('__TYPE__', image.type).replace('__IMAGE__', image.crop_uri).replace('__DELETE__', image.delete_uri).replace('__DEFAULT__', image.default_uri))
            .addClass(animated ? 'animated' : '')
            .attr('id', 'image' + image.id)
            .attr('data-image', image)
            .hide()
            .prependTo($files)
            .show();
    }
});
