$(document).ready(function () {

    var $bidForm = $('form[name=eenov_agency_bid]'),
        bidFormNeedsToBeSaved = false;

    // Select2
    $('select').each(function () {
        $(this).select2({
            allowClear: !$(this).prop('required')
        });
    });

    $('select', '#agencyselector').on('change', function (e) {
        var href = document.location.href,
            id = $(e.currentTarget).select2('val'),
            newHref;

        if (href.match('/\/agence\/[0-9]+\//')) {
            newHref = href.replace(/agence\/\d+/g, 'agence/' + id);
        } else {
            newHref = '/agence/' + id + '/tableau-de-bord';
        }

        document.location.replace(newHref);
    });

    // Visits
    $('form[name=eenov_agency_visits]').each(function () {
        var $form = $(this),
            prototype = $(this).data('prototype');

        $form.on('click', '.create', function () {
            var $tbody = $(this).closest('table').children('tbody'),
                $tr = $(prototype.replace(/__name__/g, (new Date()).getMilliseconds()));


            $tr.find('.datepicker').datepicker({
                language: 'fr'
            });

            $tr.find('.timepicker').timepicker({
                autoclose: true,
                minuteStep: 5,
                showSeconds: false,
                showMeridian: false
            });

            $tbody.append($tr);
        });

        $form.on('click', '.delete', function () {
            $(this).closest('tr').remove();
        });
    });

    // Bid update
    if (1 === $bidForm.length) {
        $bidForm.on('keyup', 'input', function () {
            bidFormNeedsToBeSaved = true;
        });
        $bidForm.on('change', 'input,select', function () {
            bidFormNeedsToBeSaved = true;
        });
        $bidForm.on('submit', function () {
            bidFormNeedsToBeSaved = false;
        });
        $bidForm.on('keydown', '[type=submit]', function () {
            bidFormNeedsToBeSaved = false;
        });

        window.onbeforeunload = function (e) {
            if (bidFormNeedsToBeSaved) {

                var message = 'Attention, certaines modifications n\'ont pas été sauvegardées. Pensez à cliquer sur "Valider" en bas à droite de cette page pour sauvegarder vos changements.';
                e = e || window.event;
                if (e) {
                    e.returnValue = message;
                }
                console.log(e);

                return message;

            }
        };
    }

    // Toggle with checkbox
    $('[data-check-will-toggle]').each(function () {
        var targets = $(this).data('check-will-toggle'),
            $targets = $(targets),
            $firstInput = $(this).find('input[type=checkbox]').first();

        $(this).on('change', 'input[type=checkbox]', function () {
            $targets.toggle($(this).prop('checked'));
        });
        if ($firstInput.length) {
            $targets.toggle($firstInput.prop('checked'));
        }
    });

});
