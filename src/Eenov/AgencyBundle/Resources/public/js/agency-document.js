$(document).ready(function () {
    var $type = $('#type'),
        $upload = $('#documents-upload[data-uri]'),
        $progress = $('#progress-documents').find('.progress-bar');

    // Disable click if no type is selected
    $upload.click(function (e) {
        if ('' === $type.val()) {
            e.preventDefault();
        }
    });

    // Select a type when
    $(document.body).on('click', '.add[data-type]', function (e) {
        e.preventDefault();
        $type.select2('val', $(this).data('type'));
        $upload.click();
    });

    // Create an uploader
    $upload.each(function () {
        $(this).fileupload({
            url: $(this).data('uri'),
            dataType: 'json',
            done: function () {
                window.location.reload();
            },
            progressall: function (e, data) {
                $progress.css('width', parseInt(data.loaded / data.total * 100, 10) + '%');
            }
        });
    });
});
