<?php

namespace Eenov\UserBundle\DependencyInjection;

use Eenov\DefaultBundle\DependencyInjection\AbstractExtension;
use Eenov\UserBundle\Form as UserType;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Parameter;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class EenovUserExtension
 *
*
 */
class EenovUserExtension extends AbstractExtension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $forms = [
            UserType\AccessType::class => [],
            UserType\AuctionType::class => [new Parameter('unity_money')],
            UserType\AuctionFirmType::class => [],
            UserType\ProfileType::class => [new Parameter('unity_money')],
            UserType\SearchType::class => [],
            UserType\UserNewsletterType::class => [],
            UserType\UserPasswordType::class => [new Reference('security.encoder_factory')],
            UserType\UserType::class => [],
        ];

        $this->addForms($container, $forms);
    }
}
