<?php

namespace Eenov\UserBundle\Tests\Controller;

use Eenov\UserBundle\Test\WebTestCase;

/**
 * Class UserControllerTest
 *
*
 */
class UserControllerTest extends WebTestCase
{
    public function testIndexAction()
    {
        $this->client->request('GET', '/mon-compte/');
        $this->assertResponse($this->client);
    }
}
