<?php

namespace Eenov\UserBundle\Test;

use Eenov\DefaultBundle\Entity\User;
use Eenov\DefaultBundle\Test\WebTestCase as BaseWebTestCase;
use Symfony\Bundle\FrameworkBundle\Client;

/**
 * Class WebTestCase
 *
*
 */
class WebTestCase extends BaseWebTestCase
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        parent::setUp();
        $this->client = static::createClient();
        $this->login($this->client, 'user1@oxioneo.com', [User::ROLE_USER]);
    }
}
