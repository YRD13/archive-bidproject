<?php

namespace Eenov\UserBundle\Controller;

use Eenov\DefaultBundle\Entity\Access;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class UserHistoryController
 *
*
 * @Route("/mon-historique")
 */
class UserHistoryController extends Controller
{
    /**
     * Index
     *
     * @return array
     * @Route()
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        return [
            'accesses' => $this->get('doctrine.orm.default_entity_manager')->getRepository(Access::class)->findAccesses($this->getUser()),
        ];
    }

    /**
     * Coming
     *
     * @return array
     * @Route("/ventes-passees")
     * @Method("GET")
     * @Template()
     */
    public function comingAction()
    {
        return [
            'accesses' => $this->get('doctrine.orm.default_entity_manager')->getRepository(Access::class)->findComingAccesses($this->getUser()),
        ];
    }

    /**
     * Past
     *
     * @return array
     * @Route("/ventes-terminees")
     * @Method("GET")
     * @Template()
     */
    public function finishedAction()
    {
        return [
            'accesses' => $this->get('doctrine.orm.default_entity_manager')->getRepository(Access::class)->findFinishedAccesses($this->getUser()),
        ];
    }

    /**
     * Waiting
     *
     * @return array
     * @Route("/ventes-non-accessibles")
     * @Method("GET")
     * @Template()
     */
    public function waitingAction()
    {
        return [
            'accesses' => $this->get('doctrine.orm.default_entity_manager')->getRepository(Access::class)->findWaitingAccesses($this->getUser()),
        ];
    }
}
