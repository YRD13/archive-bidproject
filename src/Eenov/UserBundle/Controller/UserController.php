<?php

namespace Eenov\UserBundle\Controller;

use Eenov\DefaultBundle\Entity\Search;
use Eenov\DefaultBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class UserController
 *
*
 */
class UserController extends Controller
{
    /**
     * Index
     *
     * @return array
     * @Route()
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        return [
            'searches' => $this->get('doctrine.orm.default_entity_manager')->getRepository(Search::class)->findBy(['user' => $this->getUser()], ['created' => 'DESC']),
        ];
    }

    /**
     * Email me again
     *
     * @param Request $request
     *
     * @return array|RedirectResponse
     * @Route("/me-renvoyer-un-email-de-confirmation")
     * @Method("GET|POST")
     * @Template()
     */
    public function emailMeAgainAction(Request $request)
    {
        if (null === $this->getUser()->getKey()) {
            throw $this->createNotFoundException();
        }

        $form = $this->createForm('form');
        if ($form->handleRequest($request)->isValid()) {
            $this->get('eb_email.mailer.mailer')->send('email_validation', $this->getUser(), [
                'user' => $this->getUser(),
            ]);

            $this->get('eenov.default_bundle.session.session')->success('Un email contenant le code de confirmation vient de vous être envoyé.');

            return $this->redirectToRoute('eenov_user_user_index');
        }

        return [
            'form' => $form->createView(),
        ];
    }
}
