<?php

namespace Eenov\UserBundle\Controller;

use Eenov\DefaultBundle\Entity\Search;
use Eenov\DefaultBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class UserProfileController
 *
*
 * @Route("/mon-profil")
 */
class UserProfileController extends Controller
{
    /**
     * Index
     *
     * @param Request $request
     *
     * @return array
     * @Route()
     * @Method("GET|POST")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $this->isOxioneoAccount($this->getUser());
        $em = $this->get('doctrine.orm.default_entity_manager');
        $user = $em->getRepository(User::class)->find($this->getUser()->getId());
        $form = $this->createForm('eenov_user_user', $user);
        if ($form->handleRequest($request)->isValid()) {
            $this->get('doctrine.orm.default_entity_manager')->flush();

            $this->get('eenov.default_bundle.session.session')->success('Informations mises à jour.');

            return $this->redirectToRoute('eenov_user_userprofile_index');
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * Save a new search
     *
     * @todo Faille CSRF
     *
     * @param Request $request
     *
     * @return RedirectResponse
     * @Route("/mes-recherches-sauvegardees")
     * @Method("POST")
     */
    public function createSearchAction(Request $request)
    {
        $this->isOxioneoAccount($this->getUser());
        $form = $this->get('form.factory')->createNamed(null, 'eenov_default_advanced_search', [], ['csrf_protection' => false]);
        if ($form->handleRequest($request)->isValid()) {
            // We don't actually store the form
            // data beacause it add checkbox keys
            // even when they are not checked.
            // This means that the form will consider
            // then checked when going back to
            // the form ...
            $search = new Search();
            $search
                ->setUser($this->getUser())
                ->setFilters($request->request->all())
                ->setIsSentByEmail(false);

            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->persist($search);
            $em->flush();

            $this->get('eenov.default_bundle.session.session')->success('Votre recherche a été sauvegardée dans votre espace Mon profil » Mes recherches sauvegardées');
        } else {
            $this->get('eenov.default_bundle.session.session')->danger('Votre recherche n\'a pas été sauvegardée');
        }

        return $this->redirectToRoute('eenov_default_bid_index', $request->request->all());
    }

    /**
     * Delete search
     *
     * @param Request $request Request
     * @param Search  $search  Search
     *
     * @return RedirectResponse
     * @Route("/mes-recherches-sauvegardees/{search}", requirements={"search":"\d+"})
     * @ParamConverter("search", class="EenovDefaultBundle:Search")
     * @Method("DELETE")
     */
    public function deleteSearch(Request $request, Search $search)
    {
        $this->isOxioneoAccount($this->getUser());
        if ($search->getUser()->getId() !== $this->getUser()->getId()) {
            throw $this->createNotFoundException();
        }

        $form = $this->createForm('form', [], ['method' => 'DELETE']);
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->remove($search);
            $em->flush();

            $this->get('eenov.default_bundle.session.session')->success('Recherche sauvegardée supprimée avec succès !');
        } else {
            $this->get('eenov.default_bundle.session.session')->danger('Votre recherche sauvegardée n\'a pas été supprimée.');
        }

        return $this->redirectToRoute('eenov_user_userprofile_searches');
    }

    /**
     * Parameters
     *
     * @param Request $request
     *
     * @return array
     * @Route("/parametres-du-compte")
     * @Method("GET|POST")
     * @Template()
     */
    public function parametersAction(Request $request)
    {
        $this->isOxioneoAccount($this->getUser());
        $em = $this->get('doctrine.orm.default_entity_manager');
        $user = $em->getRepository(User::class)->find($this->getUser()->getId());

        // Password
        $passwordForm = $this->createForm('eenov_user_user_password', $user);
        if ($passwordForm->handleRequest($request)->isSubmitted() && $passwordForm->isValid()) {
            $this->get('doctrine.orm.default_entity_manager')->flush();

            $this->get('eenov.default_bundle.session.session')->success('Mot de passe mis à jour !');

            return $this->redirectToRoute('eenov_user_userprofile_parameters');
        }

        // Newsletter
        $newsletterForm = $this->createForm('eenov_user_user_newsletter', $user);
        if ($newsletterForm->handleRequest($request)->isSubmitted() && $newsletterForm->isValid()) {
            $this->get('doctrine.orm.default_entity_manager')->flush();

            $this->get('eenov.default_bundle.session.session')->success('Abonnement à la newsletter mis à jour !');

            return $this->redirectToRoute('eenov_user_userprofile_parameters');
        }

        return [
            'passwordForm' => $passwordForm->createView(),
            'newsletterForm' => $newsletterForm->createView(),
        ];
    }

    /**
     * Delete
     *
     * @param Request $request
     *
     * @return array
     * @Route("/supprimer-mon-compte")
     * @Method("GET|POST")
     * @Template()
     */
    public function parametersDeleteAction(Request $request)
    {
        $this->isOxioneoAccount($this->getUser());
        $form = $this->createForm('form');
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');

            $user = $em->getRepository(User::class)->find($this->getUser()->getId());
            $em->remove($user);
            $em->flush();

            $this->get('security.token_storage')->setToken(null);

            $this->get('eenov.default_bundle.session.session')->success('Adieu ...');

            return $this->redirectToRoute('eenov_default_default_index');
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * Profile
     *
     * @param Request $request
     *
     * @return array
     * @Route("/details")
     * @Method("GET|POST")
     * @Template()
     */
    public function profileAction(Request $request)
    {
        $this->isOxioneoAccount($this->getUser());
        $profile = $this->getUser()->getProfile();
        $form = $this->createForm('eenov_user_profile', $profile);
        if ($form->handleRequest($request)->isValid()) {
            $this->get('doctrine.orm.default_entity_manager')->flush();

            if (null !== $return = $request->query->get('return')) {
                $this->get('eenov.default_bundle.session.session')->success('Informations mises à jour, vous pouvez maintenant vous inscrire à cette enchère.');

                return $this->redirect(sprintf('%s/%s', rtrim($this->generateUrl('eenov_default_default_index', [], UrlGeneratorInterface::ABSOLUTE_URL), '/'), ltrim($return, '/')));
            }

            $this->get('eenov.default_bundle.session.session')->success('Informations mises à jour.');

            return $this->redirectToRoute('eenov_user_userprofile_profile');
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * Searches
     *
     * @return array
     * @Route("/mes-recherches-sauvegardees")
     * @Method("GET")
     * @Template()
     */
    public function searchesAction()
    {
        return [
            'searches' => $this->get('doctrine.orm.default_entity_manager')->getRepository(Search::class)->findBy(['user' => $this->getUser()], ['created' => 'DESC']),
        ];
    }

    /**
     * Update search
     *
     * @param Request $request Request
     * @param Search  $search  Search
     *
     * @return RedirectResponse
     * @Route("/mes-recherches-sauvegardees/{search}", requirements={"search":"\d+"})
     * @ParamConverter("search", class="EenovDefaultBundle:Search")
     * @Method("PUT")
     */
    public function updateSearch(Request $request, Search $search)
    {
        $this->isOxioneoAccount($this->getUser());
        if ($search->getUser()->getId() !== $this->getUser()->getId()) {
            throw $this->createNotFoundException();
        }

        $form = $this->createForm('eenov_user_search', $search, ['method' => 'PUT']);
        if ($form->handleRequest($request)->isValid()) {
            $this->get('doctrine.orm.default_entity_manager')->flush();

            $this->get('eenov.default_bundle.session.session')->success('Préférences mises à jour !');
        }

        return $this->redirectToRoute('eenov_user_userprofile_searches');
    }

    private function isOxioneoAccount(User $user){
        if($user->getUsername() == "spectateur@oxioneo.com"){
            throw $this->createAccessDeniedException();
        }
    }
}
