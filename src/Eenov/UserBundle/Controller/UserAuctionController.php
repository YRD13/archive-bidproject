<?php

namespace Eenov\UserBundle\Controller;

use Eenov\DefaultBundle\Entity\Access;
use Eenov\DefaultBundle\Entity\Auction;
use Eenov\DefaultBundle\Entity\Bid;
use Eenov\DefaultBundle\Entity\User;
use Eenov\DefaultBundle\Entity\Wish;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class UserAuctionController
 *
*
 * @Route("/salles-des-offres-achats")
 */
class UserAuctionController extends Controller
{
    /**
     * Index
     *
     * @param Request $request Request
     * @param Bid     $bid     Bid
     *
     * @return array
     * @Route("/{bid}-{slug}", requirements={"bid":"\d+"})
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid", options={"read_only":true,"allow_see":true})
     * @Method("GET|POST")
     * @Template()
     */
    public function indexAction(Request $request, Bid $bid)
    {
        // Vars
        $advert = $request->attributes->get('advert', $bid->getPublish());
        $em = $this->get('doctrine.orm.default_entity_manager');
        $auctions = $em->getRepository(Auction::class)->findBy(['bid' => $bid, 'firm' => false], ['created' => 'DESC']);
        $access = $em->getRepository(Access::class)->findAccess($bid, $this->getUser());

        // Sale is ended ?
        if ($bid->isEnded()) {
            return $this->render('EenovUserBundle:UserAuction:ended.html.twig', [
                'bid' => $bid,
                'advert' => $advert,
                'access' => $access,
            ]);
        }

        // Lock ?
        if (true === $bid->getLock()) {
            throw $this->createAccessDeniedException('Cette enchère est suspendue temporairement');
        }

        // Readonly ?
        if (null !== $access && $access->getReadonly()) {
            return $this->render('EenovUserBundle:UserAuction:readonly.html.twig', [
                'bid' => $bid,
                'advert' => $advert,
                'auctions' => $auctions,
            ]);
        }

        // Agencies are granted readonly access
        if ($this->get('security.authorization_checker')->isGranted($bid->getAgency()->getRole())) {
            return $this->render('EenovUserBundle:UserAuction:readonly.html.twig', [
                'bid' => $bid,
                'advert' => $advert,
                'auctions' => $auctions,
            ]);
        }

        // Ok for all admins and moderators
        if ($this->get('security.authorization_checker')->isGranted(User::ROLE_ADMIN) || $this->get('security.authorization_checker')->isGranted(User::ROLE_MODERATOR)) {
            return $this->render('EenovUserBundle:UserAuction:readonly.html.twig', [
                'bid' => $bid,
                'advert' => $advert,
                'auctions' => $auctions,
            ]);
        }

        // This user must have a valid email
        if (null !== $this->getUser()->getKey()) {
            $this->get('eenov.default_bundle.session.session')->danger('Vous devez valider votre email avant d\'accéder à une enchère');

            return $this->redirectToRoute('eenov_user_user_index');
        }

        // This user must have a valid profile
        if (false === $this->getUser()->getProfile()->isValid()) {
            $this->get('eenov.default_bundle.session.session')->danger('Vous devez compléter votre profil avant d\'accéder à une enchère');

            if (null === $wish = $em->getRepository(Wish::class)->findOneBy(['user' => $this->getUser(), 'bid' => $bid])) {
                $wish = new Wish($this->getUser());
                $wish->setBid($bid);
                $em->persist($wish);
                $em->flush();

                $this->get('eenov.default_bundle.session.session')->success('L\'annonce a été sauvegardée dans votre liste d\'envies pour vous aider à la retrouver');
            } else {
                $this->get('eenov.default_bundle.session.session')->success('Vous pouvez retrouver l\'annonce dans votre liste d\'envies');
            }

            return $this->redirectToRoute('eenov_user_userprofile_profile', [
                'return' => $this->generateUrl('eenov_user_userauction_index', [
                    'bid' => $bid->getId(),
                    'advert' => $advert,
                    'slug' => $bid->getSlug(),
                ])
            ]);
        }

        // Access hasn't been asked
        if (null === $access) {
            $access = new Access();
            $access
                ->setBid($bid)
                ->setUser($this->getUser());

            $form = $this->createForm('eenov_user_access', $access);
            if ($form->handleRequest($request)->isValid()) {
                $em->persist($access);
                $em->flush();

                return $this->redirectToRoute('eenov_user_userauction_index', [
                    'bid' => $bid->getId(),
                    'advert' => $advert,
                    'slug' => $bid->getSlug(),
                ]);
            }

            return $this->render('EenovUserBundle:UserAuction:access.html.twig', [
                'bid' => $bid,
                'advert' => $advert,
                'form' => $form->createView(),
            ]);
        }

        // Access has been reviewed ?
        if (false === $access->hasBeenValidated()) {
            return $this->render('EenovUserBundle:UserAuction:waiting.html.twig', [
                'bid' => $bid,
                'advert' => $advert,
                'access' => $access,
            ]);
        }

        // Access has been denied ?
        if (false === $access->isValid()) {
            return $this->render('EenovUserBundle:UserAuction:denied.html.twig', [
                'bid' => $bid,
                'advert' => $advert,
                'access' => $access,
            ]);
        }

        // Sale is started ?
        if ($bid->isNotStarted()) {
            return $this->render('EenovUserBundle:UserAuction:okWait.html.twig', [
                'bid' => $bid,
                'advert' => $advert,
                'access' => $access,
            ]);
        }

        // Create a dynamic form
        $form = $this->getForm($request, $bid);
        if ($form instanceof Response) {
            return $form;
        }

        // Create a dynamic firm
        $firm = $this->getFirm($request, $bid);
        if ($firm instanceof Response) {
            return $firm;
        }

        return [
            'form' => $form ? $form->createView() : null,
            'firm' => $firm ? $firm->createView() : null,
            'auctions' => $auctions,
            'bid' => $bid,
            'advert' => $advert,
            'access' => $access,
            'firmAuction' => $em->getRepository(Auction::class)->findFirmAuctionFor($this->getUser(), $bid),
        ];
    }

    /**
     * Api
     *
     * @param Request $request Request
     * @param Bid     $bid     Bid
     *
     * @return array
     * @Route("/api/{bid}", requirements={"bid":"\d+"})
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid", options={"read_only":true})
     * @Method("GET")
     * @Template()
     */
    public function apiAction(Request $request, Bid $bid)
    {
        $em = $this->get('doctrine.orm.default_entity_manager');
        $auctions = $em->getRepository(Auction::class)->findBy(['bid' => $bid, 'firm' => false], ['created' => 'DESC']);
        $access = $em->getRepository(Access::class)->findAccess($bid, $this->getUser());

        if (
            false === $this->get('security.authorization_checker')->isGranted(User::ROLE_USER) ||
            null !== $this->getUser()->getKey() ||
            false === $this->getUser()->getProfile()->isValid() ||
            null === $access ||
            $access->getReadonly() ||
            false === $access->hasBeenValidated() ||
            false === $access->isValid() ||
            $bid->isNotStarted() ||
            $bid->isEnded()
        ) {
            throw $this->createAccessDeniedException();
        }

        // Has a firm offer ?
        $firmAuction = $em->getRepository(Auction::class)->findFirmAuctionFor($this->getUser(), $bid);

        // Create a dynamic form
        $form = $this->getForm($request, $bid);
        if ($form instanceof Response) {
            return $form;
        }

        // Create a dynamic firm
        $firm = $this->getFirm($request, $bid);
        if ($firm instanceof Response) {
            return $firm;
        }

        return [
            'form' => $form ? $form->createView() : null,
            'firm' => $firm ? $firm->createView() : null,
            'auctions' => $auctions,
            'bid' => $bid,
            'access' => $access,
            'firmAuction' => $firmAuction,
        ];
    }

    /**
     * Get form
     *
     * @param Request $request
     * @param Bid     $bid
     *
     * @return null|Form|RedirectResponse
     */
    private function getForm(Request $request, Bid $bid)
    {
        // Manager
        $em = $this->get('doctrine.orm.default_entity_manager');

        // Has a firm offer ?
        if (null !== $firmAuction = $em->getRepository(Auction::class)->findFirmAuctionFor($this->getUser(), $bid)) {
            return null;
        }

        // Advert
        $advert = $request->attributes->get('advert', $bid->getPublish());

        // Find current auction
        $price = $bid->getFirstPrice();
        $stepPrice = $bid->getStepPrice();
        if (null !== $auction = $em->getRepository(Auction::class)->findLatestAuctionFor($bid)) {
            if (null !== $stepPrice) {
                $price = $auction->getAmount() + $bid->getStepPrice();
            }
        }

        $form = null;
        if (null !== $price && null !== $stepPrice && (null === $auction || $auction->getUser()->getId() !== $this->getUser()->getId())) {
            $auction = new Auction();
            $auction
                ->setBid($bid)
                ->setUser($this->getUser());

            $form = $this->createForm('eenov_user_auction', $auction, ['price' => $price, 'step' => $bid->getStepPrice()]);
            try {
                if ($form->handleRequest($request)->isSubmitted()) {
                    if (!$form->isValid()) {
                        throw new \Exception();
                    }

                    $em->persist($auction);
                    $em->flush();

                    // Send SMS
                    try {
                        $user = $this->getUser();
                        $users = array_map(function (Access $access) use ($user) {
                            if ($access->getUser()->getId() === $user->getId()) {
                                return null;
                            }

                            return $access->getUser();
                        }, $em->getRepository(Access::class)->findUsers($bid));

                        $message = mb_strcut(sprintf('www.oxioneo-immo.com : nouvelle offre sur %s', $bid), 0, 160);
                        $this->container->get('eenov.default_bundle.sms.ovh')->send($message, $users);
                    } catch (\Exception $e) {
                    }

                    $this->get('eenov.default_bundle.session.session')->success('Votre offre d\'achat a été prise en compte');

                    return $this->redirectToRoute('eenov_user_userauction_index', [
                        'bid' => $bid->getId(),
                        'advert' => $advert,
                        'slug' => $bid->getSlug(),
                    ]);
                }
            } catch (\Exception $e) {
                $this->get('eenov.default_bundle.session.session')->danger('Votre offre n\'a pas été enregistrée, il est possible que quelqu\'un ait surenchéri avant vous.');

                return $this->redirectToRoute('eenov_user_userauction_index', [
                    'bid' => $bid->getId(),
                    'advert' => $advert,
                    'slug' => $bid->getSlug(),
                ]);
            }
        }

        return $form;
    }

    /**
     * Get form
     *
     * @param Request $request
     * @param Bid     $bid
     *
     * @return null|Form|RedirectResponse
     */
    private function getFirm(Request $request, Bid $bid)
    {
        // This must be allowed
        if (!$bid->getAllowFirmAuction()) {
            return null;
        }

        // Manager
        $em = $this->get('doctrine.orm.default_entity_manager');

        // Has a firm offer ?
        $firmAuction = $em->getRepository(Auction::class)->findFirmAuctionFor($this->getUser(), $bid);

        $firm = null;
        if (null === $firmAuction) {
            $firmAuction = new Auction();
            $firmAuction
                ->setBid($bid)
                ->setUser($this->getUser())
                ->setFirm(true);

            $firm = $this->createForm('eenov_user_auction_firm', $firmAuction);
            try {
                if ($firm->handleRequest($request)->isValid()) {
                    $em->persist($firmAuction);
                    $em->flush();

                    $this->get('eenov.default_bundle.session.session')->success('Votre offre d\'achat a été prise en compte');

                    return $this->redirectToRoute('eenov_user_userauction_index', [
                        'bid' => $bid->getId(),
                        'slug' => $bid->getSlug(),
                    ]);
                }
            } catch (\Exception $e) {
                $firm->addError(new FormError('Votre offre n\'a pas été enregistrée, il est possible que quelqu\'un ait surenchéri avant vous.'));
            }
        }

        return $firm;
    }
}
