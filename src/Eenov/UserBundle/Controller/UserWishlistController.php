<?php

namespace Eenov\UserBundle\Controller;

use Eenov\DefaultBundle\Entity\Wish;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class UserWishlistController
 *
*
 * @Route("/mes-envies")
 */
class UserWishlistController extends Controller
{
    /**
     * Index
     *
     * @return array
     * @Route()
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $paginatorHelper = $this->get('eb_paginator_helper');
        $paginator = $this->get('doctrine.orm.default_entity_manager')->getRepository(Wish::class)->getPaginator($paginatorHelper, ['user' => $this->getUser()]);

        return [
            'paginator' => $paginator,
        ];
    }
}
