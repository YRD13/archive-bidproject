<?php

namespace Eenov\UserBundle\Controller;

use Eenov\DefaultBundle\Entity\Bid;
use Eenov\DefaultBundle\Entity\Wish;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Csrf\CsrfToken;

/**
 * Class UserWishlistApiController
 *
*
 * @Route("/mes-envies")
 */
class UserWishlistApiController extends Controller
{
    /**
     * Index
     *
     * @param Request $request Request
     * @param Bid     $bid     Bid
     *
     * @return JsonResponse
     * @Route("/{bid}.{_format}", requirements={"bid":"\d+","_format":"json"})
     * @ParamConverter("bid", class="EenovDefaultBundle:Bid", options={"read_only":true})
     * @Method("POST")
     */
    public function toggleAction(Request $request, Bid $bid)
    {
        if (false === $this->get('security.csrf.token_manager')->isTokenValid(new CsrfToken(Wish::class, $request->request->get('_token')))) {
            throw $this->createAccessDeniedException();
        }

        $em = $this->get('doctrine.orm.default_entity_manager');
        if (null === $wich = $em->getRepository(Wish::class)->findOneBy(['user' => $this->getUser(), 'bid' => $bid])) {
            $wich = new Wish();
            $wich
                ->setBid($bid)
                ->setUser($this->getUser());

            $em->persist($wich);
        } else {
            $em->remove($wich);
        }
        $em->flush();

        return new JsonResponse();
    }
}
