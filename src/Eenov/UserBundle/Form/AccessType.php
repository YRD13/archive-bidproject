<?php

namespace Eenov\UserBundle\Form;

use Eenov\DefaultBundle\Entity\Access;
use Eenov\DefaultBundle\Form\FormNameTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AccessType
 *
*
 */
class AccessType extends AbstractType
{
    use FormNameTrait;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
                $access = $event->getData();
                if ($access instanceof Access) {
                    if (null === $access->getId()) {
                        $event->getForm()->add('userComment', 'textarea', [
                            'label' => 'Commentaire pour l\'agence',
                            'placeholder' => 'Sans obligation',
                            'required' => false,
                        ]);
                    }
                }
            });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Access::class,
        ]);
    }
}
