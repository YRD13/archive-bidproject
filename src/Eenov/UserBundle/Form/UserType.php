<?php

namespace Eenov\UserBundle\Form;

use Eenov\DefaultBundle\Entity\User;
use Eenov\DefaultBundle\Form\FormNameTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class UserType
 *
*
 */
class UserType extends AbstractType
{
    use FormNameTrait;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', 'email', [
                'label' => 'Email',
                'placeholder' => 'Email',
            ])
            ->add('firstname', 'text', [
                'label' => 'Prénom',
                'placeholder' => 'Prénom',
            ])
            ->add('lastname', 'text', [
                'label' => 'Nom',
                'placeholder' => 'Nom',
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
