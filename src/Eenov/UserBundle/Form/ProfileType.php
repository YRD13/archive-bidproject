<?php

namespace Eenov\UserBundle\Form;

use Doctrine\ORM\EntityRepository;
use Eenov\DefaultBundle\Entity\AdvertType;
use Eenov\DefaultBundle\Entity\Profile;
use Eenov\DefaultBundle\Entity\Seller;
use Eenov\DefaultBundle\Form\FormNameTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ProfileType
 *
*
 */
class ProfileType extends AbstractType
{
    use FormNameTrait;

    /**
     * @var string
     */
    private $unityMoney;

    /**
     * @param string $unityMoney
     */
    public function __construct($unityMoney)
    {
        $this->unityMoney = $unityMoney;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('phone', 'text', [
                'label' => 'Téléphone',
                'required' => false,
            ])
            ->add('cellphone', 'text', [
                'label' => 'Téléphone portable',
                'required' => false,
            ])
            ->add('title', 'choice', [
                'label' => 'Civilité',
                'required' => false,
                'choices' => Profile::getTitleNameList(),
            ])
            ->add('birthday', 'birthday', [
                'label' => 'Date de naissance (jj/mm/aaaa)',
                'required' => true,
            ])
            ->add('placeOfBirth', 'text', [
                'label' => 'Lieu de naissance',
                'required' => false,
            ])
            ->add('address', 'textarea', [
                'label' => 'Adresse',
                'required' => false,
            ])
            ->add('zip', 'text', [
                'label' => 'Code postal',
                'required' => false,
            ])
            ->add('city', 'text', [
                'label' => 'Ville',
                'required' => false,
            ])
            ->add('country', 'text', [
                'label' => 'Pays',
                'required' => true,
            ])
            ->add('childCount', 'integer', [
                'label' => 'Nombre d\'enfants',
                'required' => false,
                'attr' => [
                    'min' => 0,
                ],
            ])
            ->add('maritalStatus', 'choice', [
                'label' => 'Situation maritale',
                'required' => false,
                'choices' => Profile::getMaritalStatusNameList(),
            ])
            ->add('buyerStatus', 'choice', [
                'label' => 'Vous êtes',
                'required' => false,
                'choices' => Profile::getBuyerStatusNameList(),
            ])
            ->add('buyerStatusDate', 'birthday', [
                'label' => 'Depuis quelle date ?',
                'required' => false,
            ])
            ->add('advertType', 'entity', [
                'label' => 'Type de bien occupé',
                'class' => AdvertType::class,
                'required' => false,
                'query_builder' => function (EntityRepository $er) {
                    return $er
                        ->createQueryBuilder('a')
                        ->orderBy('a.name', 'ASC');
                },
            ])
            ->add('advertTypes', 'entity', [
                'label' => 'Type(s) de bien(s) recherché(s)',
                'class' => AdvertType::class,
                'required' => false,
                'expanded' => true,
                'multiple' => true,
                'query_builder' => function (EntityRepository $er) {
                    return $er
                        ->createQueryBuilder('a')
                        ->orderBy('a.name', 'ASC');
                },
            ])
            ->add('budget', 'choice', [
                'label' => sprintf('Budget global (%s)', $this->unityMoney),
                'choices' => Profile::getBudgetNameList(),
                'required' => false,
            ])
            ->add('job', 'text', [
                'label' => 'Emploi',
                'required' => false,
            ])
            ->add('situation', 'choice', [
                'label' => 'Situation professionnelle',
                'required' => false,
                'choices' => Profile::getSituationNameList()
            ])
            ->add('situationDate', 'birthday', [
                'label' => 'Depuis quand ?',
                'required' => false,
            ])
            ->add('projectNature', 'choice', [
                'label' => 'Nature du projet',
                'required' => false,
                'choices' => Profile::getProjectNatureNameList(),
            ])
            ->add('acquisition', 'choice', [
                'label' => 'Type d\'acquisition',
                'required' => false,
                'choices' => Profile::getAcquisitionNameList(),
            ])
            ->add('sellBeforeBuying', 'choice', [
                'required' => false,
                'empty_value' => 'Choisissez...',
                'empty_data' => null,
                'label' => 'Faut-il vendre pour acheter ?',
                'expanded' => false,
                'multiple' => false,
                'choices' => [
                    0 => 'Non',
                    1 => 'Oui',
                ],
                'attr' => [
                    'data-hide' => json_encode([
                        0 => [
                            sprintf('%s_sellSinceDate', $this->getName()),
                            sprintf('%s_sellers', $this->getName()),
                        ],
                    ]),
                    'data-hide-reset' => json_encode([
                        sprintf('%s_sellSinceDate', $this->getName()),
                        sprintf('%s_sellers', $this->getName())
                    ]),
                ],
            ])
            ->add('sellSinceDate', 'birthday', [
                'label' => 'Depuis quand ?',
                'required' => false,
            ])
            ->add('sellers', 'entity', [
                'required' => false,
                'label' => 'Avec qui ?',
                'class' => Seller::class,
                'property' => 'name',
                'expanded' => true,
                'multiple' => true,
                'query_builder' => function (EntityRepository $er) {
                    return $er
                        ->createQueryBuilder('s')
                        ->orderBy('s.name', 'ASC');
                },
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Profile::class,
        ]);
    }
}
