<?php

namespace Eenov\UserBundle\Form;

use Eenov\DefaultBundle\Entity\User;
use Eenov\DefaultBundle\Form\FormNameTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

/**
 * Class UserPasswordType
 *
*
 */
class UserPasswordType extends AbstractType
{
    use FormNameTrait;

    /**
     * @var EncoderFactoryInterface
     */
    private $encoderFactory;

    /**
     * @param EncoderFactoryInterface $encoderFactory
     */
    public function __construct(EncoderFactoryInterface $encoderFactory)
    {
        $this->encoderFactory = $encoderFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('previousPassword', 'password', [
                'label' => 'Ancien mot de passe',
                'placeholder' => 'Ancien mot de passe',
                'mapped' => false,
            ])
            ->add('newPassword', 'repeated', [
                'type' => 'password',
                'first_options' => [
                    'label' => 'Nouveau mot de passe',
                    'placeholder' => 'Nouveau mot de passe',
                ],
                'second_options' => [
                    'label' => 'Confirmer le mot de passe',
                    'placeholder' => 'Confirmer le mot de passe',
                ],
                'mapped' => false,
            ])
            ->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
                $user = $event->getData();
                if ($user instanceof User) {
                    $rawPassword = $event->getForm()->get('previousPassword')->getData();
                    if (false === $this->encoderFactory->getEncoder($user)->isPasswordValid($user->getPassword(), $rawPassword, $user->getSalt())) {
                        $event
                            ->getForm()
                            ->get('previousPassword')
                            ->addError(new FormError('L\'ancien mot de passe n\'est pas valide !'));
                    } else {
                        $user->setRawPassword($event->getForm()->get('newPassword')->getData());
                    }
                }
            });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
