<?php

namespace Eenov\UserBundle\Form;

use Eenov\DefaultBundle\Entity\Auction;
use Eenov\DefaultBundle\Form\FormNameTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AuctionType
 *
*
 */
class AuctionType extends AbstractType
{
    use FormNameTrait;

    /**
     * @var string
     */
    private $unityMoney;

    /**
     * @param string $unityMoney
     */
    public function __construct($unityMoney)
    {
        $this->unityMoney = $unityMoney;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $price = (int)$options['price'];
        $step = (int)$options['step'];

        $builder
            ->add('amount', 'choice', [
                'choices' => [
                    $price => sprintf('%s %s', number_format($price, 0, ',', ' '), $this->unityMoney),
                    $price + $step => sprintf('%s %s', number_format($price + $step, 0, ',', ' '), $this->unityMoney),
                ],
                'expanded' => true,
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults(['data_class' => Auction::class])
            ->setRequired(['price', 'step']);
    }
}
